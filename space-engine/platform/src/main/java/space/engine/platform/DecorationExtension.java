package space.engine.platform;

import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.ConfigurationContainer;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.file.DuplicatesStrategy;
import org.gradle.api.file.FileCollection;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.Sync;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.compile.AbstractCompile;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Order of compile operations:
 * <p>
 * Compiling:
 * SymbolGen -> CompileJava -> Decorate -> Merge -> Output
 * <p>
 * Running:
 * Output -> Package -> Runtime
 */
public abstract class DecorationExtension {
	
	public static final String CONFIGURATION_SYMBOL_GEN_CLASSPATH = "SymbolGenClasspath";
	public static final String CONFIGURATION_SYMBOL_GEN_OUTPUT = "SymbolGenOutput";
	public static final String CONFIGURATION_DECORATE_CLASSPATH = "DecorateClasspath";
	public static final String CONFIGURATION_DECORATE_OUTPUT = "DecorateOutput";
	public static final String CONFIGURATION_PACKAGE_CLASSPATH = "PackageClasspath";
	public static final String CONFIGURATION_PACKAGE_OUTPUT = "PackageOutput";
	
	public static String getConfigurationName(SourceSet sourceSet, String configurationBaseName) {
		if (sourceSet.getName().equals("main"))
			return Character.toLowerCase(configurationBaseName.charAt(0)) + configurationBaseName.substring(1);
		return sourceSet.getName() + configurationBaseName;
	}
	
	public final Project project;
	
	public final Configuration symbolGenClasspathConfiguration;
	public final Configuration symbolGenOutputConfiguration;
	public final Configuration decorateClasspathConfiguration;
	public final Configuration decorateOutputConfiguration;
	public final Configuration packageClasspathConfiguration;
	public final Configuration packageOutputConfiguration;
	
	public final DirectoryProperty javaCompileOutputDir;
	public final FileCollection classesOutput;
	
	public final ArrayList<FileCollection> decorateChain = new ArrayList<>();
	public final TaskProvider<Sync> mergeTask;
	
	public DecorationExtension(Project project, SourceSet sourceSet) {
		this.project = project;
		this.javaCompileOutputDir = project.getObjects().directoryProperty().convention(project.getLayout().getBuildDirectory().dir("javaCompile/" + sourceSet.getName()));
		ConfigurationContainer configurations = project.getConfigurations();
		
		//symbolGen Classpath
		symbolGenClasspathConfiguration = configurations.maybeCreate(getConfigurationName(sourceSet, CONFIGURATION_SYMBOL_GEN_CLASSPATH));
		symbolGenClasspathConfiguration.setVisible(false);
		symbolGenClasspathConfiguration.extendsFrom(configurations.getByName(sourceSet.getCompileOnlyConfigurationName()), configurations.getByName(sourceSet.getImplementationConfigurationName()));
		symbolGenClasspathConfiguration.setDescription("Symbol Gen classpath for " + sourceSet.getName() + ".");
		symbolGenClasspathConfiguration.setCanBeConsumed(false);
		
		//symbolGen Output
		symbolGenOutputConfiguration = configurations.maybeCreate(getConfigurationName(sourceSet, CONFIGURATION_SYMBOL_GEN_OUTPUT));
		symbolGenOutputConfiguration.setVisible(false);
		symbolGenOutputConfiguration.setDescription("Symbol Gen output for " + sourceSet.getName() + ".");
		symbolGenOutputConfiguration.setCanBeConsumed(false);
		configurations.getByName(sourceSet.getCompileClasspathConfigurationName()).extendsFrom(symbolGenOutputConfiguration);
		
		//decorate Classpath
		decorateClasspathConfiguration = configurations.maybeCreate(getConfigurationName(sourceSet, CONFIGURATION_DECORATE_CLASSPATH));
		decorateClasspathConfiguration.setVisible(false);
		decorateClasspathConfiguration.extendsFrom(configurations.getByName(sourceSet.getCompileClasspathConfigurationName()));
		decorateClasspathConfiguration.setDescription("Decorate classpath for " + sourceSet.getName() + ".");
		decorateClasspathConfiguration.setCanBeConsumed(false);
		
		//decorate Output
		decorateOutputConfiguration = configurations.maybeCreate(getConfigurationName(sourceSet, CONFIGURATION_DECORATE_OUTPUT));
		decorateOutputConfiguration.setVisible(false);
		decorateOutputConfiguration.setDescription("Decorate output for " + sourceSet.getName() + ".");
		decorateOutputConfiguration.setCanBeConsumed(false);
		
		//package Classpath
		packageClasspathConfiguration = configurations.maybeCreate(getConfigurationName(sourceSet, CONFIGURATION_PACKAGE_CLASSPATH));
		packageClasspathConfiguration.setVisible(false);
		packageClasspathConfiguration.extendsFrom(configurations.getByName(sourceSet.getRuntimeOnlyConfigurationName()), configurations.getByName(sourceSet.getImplementationConfigurationName()));
		packageClasspathConfiguration.setDescription("Package classpath for " + sourceSet.getName() + ".");
		packageClasspathConfiguration.setCanBeConsumed(false);
		project.getDependencies().add(packageClasspathConfiguration.getName(), sourceSet.getOutput().getClassesDirs());
		
		//package Output
		packageOutputConfiguration = configurations.maybeCreate(getConfigurationName(sourceSet, CONFIGURATION_PACKAGE_OUTPUT));
		packageOutputConfiguration.setVisible(false);
		packageOutputConfiguration.setDescription("Package output for " + sourceSet.getName() + ".");
		packageOutputConfiguration.setCanBeConsumed(false);
		configurations.getByName(sourceSet.getRuntimeClasspathConfigurationName()).extendsFrom(packageOutputConfiguration);
		
		
		
		//symbolGen
		project.getTasks().named(PrepareTask.TASK_PREPARE, t -> t.dependsOn(symbolGenOutputConfiguration));
		decorateChain.add(symbolGenOutputConfiguration);
		
		//compileJava
		TaskProvider<AbstractCompile> javaCompile = project.getTasks().named(sourceSet.getCompileJavaTaskName(), AbstractCompile.class);
		javaCompile.configure(task -> task.getDestinationDirectory().set(javaCompileOutputDir));
		decorateChain.add(project.files(javaCompileOutputDir).builtBy(javaCompile));
		
		//decorate
		DirectoryProperty classesOutputDir = project.getObjects().directoryProperty();
		this.mergeTask = project.getTasks().register(sourceSet.getTaskName("merge", "Classes"), Sync.class, t -> {
			t.from(project.getProviders().provider(() -> {
				ArrayList<FileCollection> copy = new ArrayList<>(decorateChain);
				Collections.reverse(copy);
				return copy;
			}));
			t.into(classesOutputDir);
			t.setDuplicatesStrategy(DuplicatesStrategy.EXCLUDE);
		});
		sourceSet.getJava().compiledBy(mergeTask, t -> classesOutputDir);
		classesOutput = project.files(classesOutputDir).builtBy(mergeTask);
		project.getTasks().named(sourceSet.getClassesTaskName()).configure(t -> t.dependsOn(mergeTask));
		
		//package
		sourceSet.setRuntimeClasspath(packageOutputConfiguration.plus(sourceSet.getRuntimeClasspath()));
	}
	
	@FunctionalInterface
	public interface SymbolGen<T extends Task> {
		
		Object configureSymbolGen(T t, FileCollection compileClasspath);
	}
	
	public <T extends Task> void addSymbolGen(TaskProvider<T> task, SymbolGen<? super T> symbolGen) {
		Property<Object> property = project.getObjects().property(Object.class);
		task.configure(t -> property.set(symbolGen.configureSymbolGen(t, symbolGenClasspathConfiguration)));
		project.getDependencies().add(symbolGenOutputConfiguration.getName(), project.files(task.map(t -> property)).builtBy(task));
	}
	
	@FunctionalInterface
	public interface Decorate<T extends Task> {
		
		Object configureDecorate(T t, FileCollection compileClasspath, FileCollection source);
	}
	
	public <T extends Task> void addDecorate(TaskProvider<T> task, Decorate<? super T> symbolGen) {
		Property<Object> property = project.getObjects().property(Object.class);
		Object[] currentSource = decorateChain.toArray();
		task.configure(t -> property.set(symbolGen.configureDecorate(t, symbolGenClasspathConfiguration, project.files(currentSource))));
		decorateChain.add(project.files(task.map(t -> property)).builtBy(task));
	}
	
	@FunctionalInterface
	public interface Package<T extends Task> {
		
		Object configurePackage(T t, FileCollection runtimeClasspath);
	}
	
	public <T extends Task> void addPackage(TaskProvider<T> task, Package<? super T> aPackage) {
		Property<Object> property = project.getObjects().property(Object.class);
		task.configure(t -> property.set(aPackage.configurePackage(t, packageClasspathConfiguration)));
		project.getDependencies().add(packageOutputConfiguration.getName(), project.files(task.map(t -> property)).builtBy(task));
	}
}
