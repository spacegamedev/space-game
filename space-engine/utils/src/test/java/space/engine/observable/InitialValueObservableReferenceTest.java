package space.engine.observable;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import space.engine.barrier.BarrierImpl;
import space.engine.barrier.Delay;
import space.engine.barrier.future.FutureNotFinishedException;
import space.engine.observable.ObservableReference.Generator;

import java.util.List;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class InitialValueObservableReferenceTest {
	
	public static List<Arguments> implementationsValue() {
		return List.of(arguments((Function<Integer, ObservableReference<Integer>>) StaticObservableReference::new), arguments((Function<Integer, ObservableReference<Integer>>) MutableObservableReference::new));
	}
	
	public static List<Arguments> implementationsFunction() {
		return List.of(arguments((Function<Generator<Integer>, ObservableReference<Integer>>) StaticObservableReference::new), arguments((Function<Generator<Integer>, ObservableReference<Integer>>) MutableObservableReference::new));
	}
	
	@ParameterizedTest
	@MethodSource("implementationsValue")
	public void testInitialValue(Function<Integer, ? extends ObservableReference<Integer>> creator) {
		ObservableReference<Integer> reference = creator.apply(42);
		assertTrue(reference.future().isDone());
		assertEquals((Integer) 42, reference.future().assertGet());
		assertEquals((Integer) 42, reference.assertGet());
	}
	
	@ParameterizedTest
	@MethodSource("implementationsFunction")
	public void testInitialGenerator(Function<Generator<Integer>, ? extends ObservableReference<Integer>> creator) throws InterruptedException {
		BarrierImpl start = new BarrierImpl();
		ObservableReference<Integer> reference = creator.apply(((Generator<Integer>) (previous) -> {
			throw new Delay(start.toFuture(() -> 42));
		}));
		
		assertFalse(reference.future().isDone());
		try {
			reference.assertGet();
			throw new RuntimeException("No FutureNotFinishedException");
		} catch (FutureNotFinishedException ignored) {
		
		}
		
		start.triggerNow();
		assertEquals((Integer) 42, reference.future().awaitGet());
		assertEquals((Integer) 42, reference.future().assertGet());
		assertEquals((Integer) 42, reference.assertGet());
	}
}
