package space.engine.key.attribute;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.engine.indexmap.IndexMap;

public abstract class AbstractAttributeList<TYPE> {
	
	protected IndexMap<@Nullable Object> indexMap;
	
	protected AbstractAttributeList(IndexMap<@Nullable Object> indexMap) {
		this.indexMap = indexMap;
	}
	
	//abstract
	public abstract @NotNull AttributeListCreator<TYPE> creator();
	
	//direct access
	public <V> @Nullable Object getDirect(@NotNull AttributeKey<V> key) {
		verifyKey(key);
		return indexMap.get(key.id);
	}
	
	public <V> @Nullable Object putDirect(@NotNull AttributeKey<V> key, @Nullable Object value) {
		verifyKey(key);
		return indexMap.put(key.id, value);
	}
	
	//get
	public <V> V get(@NotNull AttributeKey<V> key) {
		verifyKey(key);
		return key.attributeListGet(this);
	}
	
	//other
	public void verifyKey(AttributeKey<?> key) {
		creator().assertKeyOf(key);
	}
	
	public int size() {
		return indexMap.size();
	}
	
	//toString
	
}
