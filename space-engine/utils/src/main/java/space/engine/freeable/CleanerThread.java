package space.engine.freeable;

import org.jetbrains.annotations.Nullable;
import space.engine.Shutdown;
import space.engine.barrier.Barrier;
import space.engine.barrier.BarrierImpl;
import space.engine.barrier.Pool;
import space.engine.barrier.event.EventEntry;
import space.engine.barrier.functions.Starter;
import space.engine.logger.Logger;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.Objects;

import static space.engine.logger.LogLevel.INFO;

public final class CleanerThread {
	
	//the QUEUE
	public static final ReferenceQueue<Object> QUEUE = new ReferenceQueue<>();
	
	//exit event entry
	public static final EventEntry<Starter<Barrier>> SHUTDOWN_ENTRY_FREEABLE_STOP;
	
	//instance management
	@Nullable
	private static ThreadInfo cleanupThreadInfo;
	
	private static final class ThreadInfo {
		
		Thread thread;
		volatile boolean doRun = true;
	}
	
	//logger
	public static final Logger cleanupLogger = Logger.rootLogger("Cleaner");
	public static volatile boolean cleanupLoggerDebug = false;
	
	public static boolean isCleanupLoggerDebug() {
		return cleanupLoggerDebug;
	}
	
	public static void setCleanupLoggerDebug(boolean cleanupLoggerDebug) {
		CleanerThread.cleanupLoggerDebug = cleanupLoggerDebug;
	}
	
	//static init
	static {
		startCleanupThread();
		
		Shutdown.SHOWDOWN_EVENT.addHook(SHUTDOWN_ENTRY_FREEABLE_STOP = new EventEntry<>(() -> {
			BarrierImpl finishBarrier = new BarrierImpl();
			new Thread(() -> {
				Logger logger = cleanupLogger.subLogger("final");
				logger.log(INFO, "Final cleanup...");
				logger.log(INFO, "1. Stopping cleanup thread");
				try {
					stopAndJoinCleanupThread();
				} catch (InterruptedException ignore) {
				
				}
				
				logger.log(INFO, "2. ROOT_LIST free");
				Freeable.ROOT_LIST.free().awaitUninterrupted();
				
				logger.log(INFO, "3. gc free");
				System.gc();
				System.runFinalization();
				while (true) {
					try {
						handle(100);
						break;
					} catch (InterruptedException ignored) {
					}
				}
				
				logger.log(INFO, "Final cleanup complete!");
				finishBarrier.triggerNow();
			}, "FreeableStorageCleaner-Final").start();
			return finishBarrier;
		}, new EventEntry<?>[] {Pool.SHUTDOWN_ENTRY_POOL_STOP}, new EventEntry<?>[] {Shutdown.SHUTDOWN_ENTRY_GLOBAL_SHUTDOWN}));
	}
	
	//start
	public static synchronized void startCleanupThread() throws IllegalStateException {
		if (cleanupThreadInfo != null)
			stopCleanupThread();
		
		ThreadInfo info = new ThreadInfo();
		Thread thread = info.thread = new Thread(() -> {
			while (info.doRun) {
				try {
					handle(0);
				} catch (InterruptedException ignore) {
				
				} catch (Throwable e) {
					Thread th = Thread.currentThread();
					th.getUncaughtExceptionHandler().uncaughtException(th, e);
				}
			}
		});
		thread.setName("FreeableStorageCleaner");
		thread.setPriority(8);
		thread.setDaemon(true);
		thread.start();
		
		cleanupThreadInfo = info;
	}
	
	//handle
	private static void handle(long timeout) throws InterruptedException {
		Reference<?> ref1 = QUEUE.remove(timeout);
		if (ref1 == null)
			return;
		handle(ref1);
		
		//handle all remaining objects in the queue
		int count = 1;
		Reference<?> ref;
		for (; (ref = QUEUE.poll()) != null; count++) {
			handle(ref);
		}
		
		//log object count
		cleanupLogger.log(INFO, "Cleaning up " + count + " Objects via GC");
	}
	
	private static void handle(Reference<?> ref) {
		cleanupLogger.finer("Cleaning up " + ref);
		if (ref instanceof Freeable)
			((Freeable) ref).free();
		else
			throw new IllegalArgumentException("Inappropriate Reference of type " + ref.getClass().getName() + ": " + ref);
	}
	
	//stop
	public static synchronized boolean stopCleanupThread() {
		if (cleanupThreadInfo == null)
			return false;
		
		cleanupThreadInfo.doRun = false;
		cleanupThreadInfo.thread.interrupt();
		cleanupThreadInfo = null;
		return true;
	}
	
	public static boolean stopAndJoinCleanupThread() throws InterruptedException {
		Thread thread;
		synchronized (CleanerThread.class) {
			ThreadInfo cleanupThreadInfo = CleanerThread.cleanupThreadInfo;
			if (!stopCleanupThread())
				return false;
			thread = Objects.requireNonNull(cleanupThreadInfo).thread;
		}
		thread.join();
		return true;
	}
	
	//isRunning
	public static synchronized boolean hasCleanupThread() {
		return cleanupThreadInfo != null;
	}
}
