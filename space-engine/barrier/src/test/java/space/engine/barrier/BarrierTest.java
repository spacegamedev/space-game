package space.engine.barrier;

import org.junit.jupiter.api.Test;
import space.engine.barrier.functions.RunnableWithDelay;
import space.engine.barrier.future.CompletableFuture;
import space.engine.barrier.future.CompletableFutureWith2Exception;
import space.engine.barrier.future.CompletableFutureWith3Exception;
import space.engine.barrier.future.CompletableFutureWith4Exception;
import space.engine.barrier.future.CompletableFutureWith5Exception;
import space.engine.barrier.future.CompletableFutureWithException;
import space.engine.barrier.future.Future;

import java.util.IllegalFormatException;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class BarrierTest {
	
	/**
	 * These types must never cause {@link Barrier#shouldThenRunDelegateToNowRun()} to return true if they are not triggered.
	 * Otherwise {@link Barrier#thenRun(RunnableWithDelay)} and the like will completely break.
	 */
	@Test
	public void testBarrierShouldThenRunDelegateToNowRun() {
		assertFalse(new BarrierImpl().shouldThenRunDelegateToNowRun());
		assertFalse(new CompletableFuture<>().shouldThenRunDelegateToNowRun());
		assertFalse(new CompletableFutureWithException<>(IllegalAccessException.class).shouldThenRunDelegateToNowRun());
		assertFalse(new CompletableFutureWith2Exception<>(IllegalAccessException.class, IllegalMonitorStateException.class).shouldThenRunDelegateToNowRun());
		assertFalse(new CompletableFutureWith3Exception<>(IllegalAccessException.class, IllegalMonitorStateException.class, IllegalStateException.class).shouldThenRunDelegateToNowRun());
		assertFalse(new CompletableFutureWith4Exception<>(IllegalAccessException.class, IllegalMonitorStateException.class, IllegalStateException.class, IllegalFormatException.class).shouldThenRunDelegateToNowRun());
		assertFalse(new CompletableFutureWith5Exception<>(IllegalAccessException.class, IllegalMonitorStateException.class, IllegalStateException.class, IllegalFormatException.class, IllegalArgumentException.class).shouldThenRunDelegateToNowRun());
		assertFalse(new BarrierImpl().toFuture(() -> 1).shouldThenRunDelegateToNowRun());
		assertFalse(new BarrierImpl().dereference().shouldThenRunDelegateToNowRun());
	}
	
	@Test
	public void testBarrierImpl() {
		BarrierImpl barrier = new BarrierImpl();
		assertFalse(barrier.isDone());
		barrier.triggerNow();
		assertTrue(barrier.isDone());
	}
	
	@Test
	public void testBarrierImplHooks() {
		BarrierImpl barrier = new BarrierImpl();
		
		boolean[] called = new boolean[1];
		barrier.addHook(() -> called[0] = true);
		
		assertFalse(barrier.isDone());
		assertFalse(called[0]);
		barrier.triggerNow();
		assertTrue(barrier.isDone());
		assertTrue(called[0]);
	}
	
	@Test
	public void testBarrierWhen() {
		BarrierImpl[] barriers = IntStream
				.range(0, 5)
				.mapToObj(i -> new BarrierImpl())
				.toArray(BarrierImpl[]::new);
		Barrier all = Barrier.when(barriers);
		
		for (BarrierImpl barrier : barriers) {
			assertFalse(all.isDone());
			barrier.triggerNow();
		}
		assertTrue(all.isDone());
	}
	
	@Test
	public void testBarrierInner() {
		BarrierImpl outer = new BarrierImpl();
		BarrierImpl inner = new BarrierImpl();
		Future<Barrier> future = outer.toFuture(() -> inner);
		Barrier all = Barrier.inner(future);
		
		assertFalse(outer.isDone());
		assertFalse(inner.isDone());
		assertFalse(all.isDone());
		
		outer.triggerNow();
		assertTrue(outer.isDone());
		assertFalse(inner.isDone());
		assertFalse(all.isDone());
		
		inner.triggerNow();
		assertTrue(outer.isDone());
		assertTrue(inner.isDone());
		assertTrue(all.isDone());
	}
	
	@Test
	public void testBarrierInnerReversedOrder() {
		BarrierImpl outer = new BarrierImpl();
		BarrierImpl inner = new BarrierImpl();
		Future<Barrier> future = outer.toFuture(() -> inner);
		Barrier all = Barrier.inner(future);
		
		assertFalse(outer.isDone());
		assertFalse(inner.isDone());
		assertFalse(all.isDone());
		
		inner.triggerNow();
		assertFalse(outer.isDone());
		assertTrue(inner.isDone());
		assertFalse(all.isDone());
		
		outer.triggerNow();
		assertTrue(outer.isDone());
		assertTrue(inner.isDone());
		assertTrue(all.isDone());
	}
}
