package space.engine.barrier.pool;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class SimpleThreadPoolTest {
	
	@Test
	@Timeout(1)
	public void testLifecycle() {
		SimpleThreadPool pool = new SimpleThreadPool(4);
		AtomicInteger counter = new AtomicInteger();
		pool.executeAll(IntStream.range(0, 32).mapToObj(i -> counter::incrementAndGet));
		pool.stop().awaitUninterrupted();
		assertEquals(32, counter.get());
	}
	
	@Test
	public void testRejectedExecution() {
		SimpleThreadPool pool = new SimpleThreadPool(1);
		pool.stop();
		//noinspection CodeBlock2Expr
		assertThrows(RejectedExecutionException.class, () -> {
			pool.execute(() -> {
				throw new RuntimeException("Should not be executed!");
			});
		});
	}
}
