package space.engine.barrier.property;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.params.provider.Arguments.arguments;
import static space.engine.barrier.property.PropertyTest.*;

public class PropertyComputeExtensiveTest {
	
	@SuppressWarnings("unused")
	private static Stream<Arguments> testComputeFunction1Variants() {
		//not the prettiest but works
		return Stream.of(PropertySetMethod.values())
					 .map(List::of)
					 .flatMap(a -> Stream.of(false, true).map(b -> Stream.concat(a.stream(), Stream.of(b)).collect(Collectors.toList())))
					 .map(a -> arguments(a.toArray()));
	}
	
	@ParameterizedTest
	@MethodSource
	public void testComputeFunction1Variants(@NotNull PropertySetMethod setA, boolean inita) {
		final int VALUE_A = 5;
		final int VALUE_C = VALUE_A + 1;
		final int VALUE_A_2 = 25;
		final int VALUE_C_2 = VALUE_A_2 + 1;
		
		Property<Integer> a = new Property<>();
		
		if (inita) {
			setA.set(a, VALUE_A);
			assertResult(VALUE_A, a);
		} else {
			assertNotSet(a);
		}
		
		Property<Integer> c = new Property<>();
		assertNotSet(c);
		
		c.set(a, (a1, prev) -> a1 + 1);
		if (!inita)
			assertNotSetUpdating(c);
		
		if (!inita)
			setA.set(a, VALUE_A);
		assertResult(VALUE_A, a);
		assertResult(VALUE_C, c);
		
		a.set(VALUE_A_2);
		assertResult(VALUE_A_2, a);
		assertResult(VALUE_C_2, c);
	}
	
	@SuppressWarnings("unused")
	private static Stream<Arguments> testComputeFunction2Variants() {
		//not the prettiest but works
		return Stream.of(PropertySetMethod.values())
					 .map(List::of)
					 .flatMap(a -> Stream.of(PropertySetMethod.values()).map(b -> Stream.concat(a.stream(), Stream.of(b)).collect(Collectors.toList())))
					 .flatMap(a -> Stream.of(false, true).map(b -> Stream.concat(a.stream(), Stream.of(b)).collect(Collectors.toList())))
					 .flatMap(a -> Stream.of(false, true).map(b -> Stream.concat(a.stream(), Stream.of(b)).collect(Collectors.toList())))
					 .map(a -> arguments(a.toArray()));
	}
	
	@ParameterizedTest
	@MethodSource
	public void testComputeFunction2Variants(@NotNull PropertySetMethod setA, @NotNull PropertySetMethod setB, boolean inita, boolean initb) {
		final int VALUE_A = 5;
		final int VALUE_B = 10;
		final int VALUE_C = VALUE_A + VALUE_B;
		final int VALUE_B_2 = 25;
		final int VALUE_C_2 = VALUE_A + VALUE_B_2;
		
		Property<Integer> a = new Property<>();
		Property<Integer> b = new Property<>();
		
		if (inita) {
			setA.set(a, VALUE_A);
			assertResult(VALUE_A, a);
		} else {
			assertNotSet(a);
		}
		if (initb) {
			setB.set(b, VALUE_B);
			assertResult(VALUE_B, b);
		} else {
			assertNotSet(b);
		}
		
		Property<Integer> c = new Property<>();
		assertNotSet(c);
		
		c.set(a, b, (a1, b1, prev) -> a1 + b1);
		if (!(inita && initb))
			assertNotSetUpdating(c);
		
		if (!inita)
			setA.set(a, VALUE_A);
		if (!initb)
			setB.set(b, VALUE_B);
		assertResult(VALUE_A, a);
		assertResult(VALUE_B, b);
		assertResult(VALUE_C, c);
		
		b.set(VALUE_B_2);
		assertResult(VALUE_B_2, b);
		assertResult(VALUE_C_2, c);
	}
}
