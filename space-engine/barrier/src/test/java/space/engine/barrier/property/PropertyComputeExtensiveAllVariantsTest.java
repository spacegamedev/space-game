package space.engine.barrier.property;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import static org.junit.jupiter.params.provider.Arguments.arguments;
import static space.engine.barrier.property.PropertyTest.*;

@Disabled("questionable usefulness and being extremely heavy due to exploding input set size")
public class PropertyComputeExtensiveAllVariantsTest {
	
	private static final int WIDTH = 9 - 1;
	
	/**
	 * do NOT use all PropertySetMethod values, dataset explodes otherwise. Or reduce width to 4.
	 */
	private static final PropertySetMethod[] INPUT_PROPERTY_SET_METHODS = {PropertySetMethod.VALUE};
	private static final Boolean[] INPUT_INITS = {true, false};
	
	@SuppressWarnings({"unused", "CodeBlock2Expr"})
	private static Stream<Arguments> testComputeExtensiveAll() {
		
		Builder<Arguments> ret = Stream.builder();
		
		class Entry {
			
			final List<PropertySetMethod> set;
			final List<Boolean> init;
			
			public Entry(Stream<PropertySetMethod> set, Stream<Boolean> init) {
				this.set = List.of(set.toArray(PropertySetMethod[]::new));
				this.init = List.of(init.toArray(Boolean[]::new));
			}
		}
		Stream<Entry> stream = Stream
				.of(INPUT_PROPERTY_SET_METHODS)
				.flatMap(s -> {
					return Stream.of(INPUT_INITS)
								 .map(init -> new Entry(Stream.of(s), Stream.of(init)));
				});
		
		for (int i = 0; i < WIDTH; i++) {
			stream = stream
					.peek(e -> ret.add(arguments(e.set.toArray(PropertySetMethod[]::new), e.init.toArray(Boolean[]::new))))
					.flatMap(e -> {
						return Stream.of(INPUT_PROPERTY_SET_METHODS)
									 .flatMap(s -> {
										 return Stream.of(INPUT_INITS)
													  .map(init -> new Entry(
															  Stream.concat(e.set.stream(), Stream.of(s)),
															  Stream.concat(e.init.stream(), Stream.of(init))
													  ));
									 });
					});
		}
		stream.forEach(e -> ret.add(arguments(e.set.toArray(PropertySetMethod[]::new), e.init.toArray(Boolean[]::new))));
		return ret.build();
	}
	
	@ParameterizedTest
	@MethodSource
	public void testComputeExtensiveAll(@NotNull PropertySetMethod[] set, Boolean[] init) {
		final int width = set.length;
		final int result = IntStream.range(0, width).reduce(0, Integer::sum);
		
		//noinspection unchecked,SuspiciousToArrayCall,SuspiciousToArrayCall
		Property<Integer>[] p = (Property<Integer>[])
				IntStream.range(0, width)
						 .mapToObj(i -> new Property<Integer>())
						 .toArray(Property[]::new);
		
		IntStream.range(0, width).forEach(i -> {
			if (init[i]) {
				set[i].set(p[i], i);
				assertResult(i, p[i]);
			} else {
				assertNotSet(p[i]);
			}
		});
		
		Property<Integer> c = new Property<>();
		assertNotSet(c);
		
		switch (width) {
			case 1:
				c.set(p[0], (i1, previous) -> i1);
				break;
			case 2:
				c.set(p[0], p[1], (i1, i2, previous) -> i1 + i2);
				break;
			case 3:
				c.set(p[0], p[1], p[2], (i1, i2, i3, previous) -> i1 + i2 + i3);
				break;
			case 4:
				c.set(p[0], p[1], p[2], p[3], (i1, i2, i3, i4, previous) -> i1 + i2 + i3 + i4);
				break;
			case 5:
				c.set(p[0], p[1], p[2], p[3], p[4], (i1, i2, i3, i4, i5, previous) -> i1 + i2 + i3 + i4 + i5);
				break;
			case 6:
				c.set(p[0], p[1], p[2], p[3], p[4], p[5], (i1, i2, i3, i4, i5, i6, previous) -> i1 + i2 + i3 + i4 + i5 + i6);
				break;
			case 7:
				c.set(p[0], p[1], p[2], p[3], p[4], p[5], p[6], (i1, i2, i3, i4, i5, i6, i7, previous) -> i1 + i2 + i3 + i4 + i5 + i6 + i7);
				break;
			case 8:
				c.set(p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], (i1, i2, i3, i4, i5, i6, i7, i8, previous) -> i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8);
				break;
			default:
				c.set(p, (values, previous) -> Arrays.stream(values).mapToInt(o -> (Integer) o).reduce(0, Integer::sum));
				break;
		}
		
		if (Arrays.stream(init).reduce(true, (l, r) -> l && r))
			assertResult(result, c);
		else
			assertNotSetUpdating(c);
		
		IntStream.range(0, width).forEach(i -> {
			if (!init[i]) {
				set[i].set(p[i], i);
			}
			assertResult(i, p[i]);
		});
		assertResult(result, c);
		
		IntStream.range(0, width).forEach(i -> {
			set[i].set(p[i], i + 1);
			assertResult(i + 1, p[i]);
			assertResult(result + i + 1, c);
		});
	}
}
