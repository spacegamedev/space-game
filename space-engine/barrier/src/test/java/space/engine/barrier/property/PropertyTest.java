package space.engine.barrier.property;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import space.engine.barrier.Barrier;
import space.engine.barrier.BarrierImpl;
import space.engine.barrier.Delay;
import space.engine.barrier.future.CompletableFuture;
import space.engine.barrier.future.FutureNotFinishedException;

import static org.junit.jupiter.api.Assertions.*;

public class PropertyTest {
	
	public static void assertNotSet(Property<?> property) {
		assertFalse(property.future().isDone());
		assertThrows(FutureNotFinishedException.class, () -> property.future().assertGet());
		assertThrows(FutureNotFinishedException.class, property::assertGet);
		
		assertNull(ReadOnlyProperty.testingGetT(property));
		assertTrue(ReadOnlyProperty.testingGetUpdatingCounter(property) > 0, "updatingCounter > 0");
		assertEquals(0, ReadOnlyProperty.testingGetSourceModId(property));
		assertFalse(ReadOnlyProperty.testingGetBalueDelayUpdatingCounterIncremented(property));
	}
	
	public static void assertNotSetUpdating(Property<?> property) {
		assertFalse(property.future().isDone());
		assertThrows(FutureNotFinishedException.class, () -> property.future().assertGet());
		assertThrows(FutureNotFinishedException.class, property::assertGet);
		
		assertNull(ReadOnlyProperty.testingGetT(property));
		assertTrue(ReadOnlyProperty.testingGetUpdatingCounter(property) > 0, "updatingCounter > 0");
		assertNotEquals(0, ReadOnlyProperty.testingGetSourceModId(property));
	}
	
	public static void assertSet(Property<?> property) {
		assertTrue(property.future().isDone());
		assertDoesNotThrow(() -> property.future().assertGet());
		assertDoesNotThrow(property::assertGet);
		
		assertEquals(0, ReadOnlyProperty.testingGetUpdatingCounter(property), "updatingCounter == 0");
		assertNotEquals(0, ReadOnlyProperty.testingGetSourceModId(property));
		assertFalse(ReadOnlyProperty.testingGetBalueDelayUpdatingCounterIncremented(property));
	}
	
	public static void assertSetUpdating(Property<?> property) {
		assertTrue(property.future().isDone());
		assertDoesNotThrow(() -> property.future().assertGet());
		assertDoesNotThrow(property::assertGet);
		
		assertTrue(ReadOnlyProperty.testingGetUpdatingCounter(property) > 0, "updatingCounter > 0");
		assertNotEquals(0, ReadOnlyProperty.testingGetSourceModId(property));
	}
	
	public static <T> void assertResult(T expected, Property<T> property) {
		assertSet(property);
		assertEquals(expected, property.future().assertGet());
		assertEquals(expected, property.assertGet());
		assertEquals(expected, ReadOnlyProperty.testingGetT(property));
	}
	
	public static <T> void assertResultUpdating(T expected, Property<T> property) {
		assertSetUpdating(property);
		assertEquals(expected, property.future().assertGet());
		assertEquals(expected, property.assertGet());
		assertEquals(expected, ReadOnlyProperty.testingGetT(property));
	}
	
	//tests
	@Test
	public void testPropertyNotSet() {
		assertNotSet(new Property<Integer>());
	}
	
	@ParameterizedTest
	@EnumSource(PropertySetMethod.class)
	public void testPropertySetValue(PropertySetMethod method) {
		Property<Integer> property = new Property<>();
		assertNotSet(property);
		
		method.set(property, 42);
		assertResult(42, property);
	}
	
	@Test
	public void testPropertySetFuture() {
		Property<Integer> property = new Property<>();
		assertNotSet(property);
		
		CompletableFuture<Integer> future = new CompletableFuture<>();
		property.set(future);
		assertNotSetUpdating(property);
		
		future.complete(42);
		assertResult(42, property);
	}
	
	@ParameterizedTest
	@EnumSource(PropertySetMethod.class)
	public void testPropertySetFutureOverride(PropertySetMethod method) {
		Property<Integer> property = new Property<>();
		assertNotSet(property);
		
		CompletableFuture<Integer> future = new CompletableFuture<>();
		property.set(future);
		assertNotSetUpdating(property);
		
		method.set(property, 42);
		assertResult(42, property);
		
		future.complete(128);
		assertResult(42, property);
	}
	
	@ParameterizedTest
	@EnumSource(PropertySetMethod.class)
	public void testPropertySetProperty(PropertySetMethod method) {
		Property<Integer> property = new Property<>();
		Property<Integer> next = new Property<>();
		assertNotSet(property);
		assertNotSet(next);
		
		//setup property -> next
		next.set(property);
		assertNotSet(property);
		assertNotSetUpdating(next);
		
		//initial value
		method.set(property, 42);
		assertResult(42, property);
		assertResult(42, next);
		
		//new value
		method.set(property, 50);
		assertResult(50, property);
		assertResult(50, next);
		
		//next gets new source, independent from property
		method.set(next, -10);
		assertResult(50, property);
		assertResult(-10, next);
		
		//check independence
		method.set(property, 100);
		assertResult(100, property);
		assertResult(-10, next);
	}
	
	@Test
	public void testPropertySetProperty2() {
		Property<Integer> property = new Property<>();
		assertNotSet(property);
		
		Property<Integer> next = new Property<>();
		assertNotSet(next);
		
		property.set(42);
		assertResult(42, property);
		assertNotSet(next);
		
		//property -> next source with property having a value already
		next.set(property);
		assertResult(42, property);
		assertResult(42, next);
	}
	
	@Test
	public void testPropertySetComputeFunction2() {
		Property<Integer> a = new Property<>(42);
		Property<Integer> b = new Property<>();
		Property<Integer> c = new Property<>();
		c.set(a, b, (a1, b1, previous) -> a1 + b1);
		
		assertResult(42, a);
		assertNotSet(b);
		assertNotSetUpdating(c);
		
		b.set(8);
		assertResult(42, a);
		assertResult(8, b);
		assertResult(50, c);
		
		b.set(10);
		assertResult(42, a);
		assertResult(10, b);
		assertResult(52, c);
		
		CompletableFuture<Integer> future = new CompletableFuture<>();
		b.set(future);
		future.complete(12);
		assertResult(42, a);
		assertResult(12, b);
		assertResult(54, c);
	}
	
	@ParameterizedTest
	@EnumSource(PropertySetMethod.class)
	public void testComputeDelayed(PropertySetMethod setMethod) {
		Property<Integer> a = new Property<>(42);
		assertResult(42, a);
		
		Property<Integer> b = new Property<>();
		Barrier[] wait = new Barrier[1];
		BarrierImpl barrierInitial = new BarrierImpl();
		wait[0] = barrierInitial;
		b.set(a, (a1, prev) -> {
			throw new Delay(wait[0].toFuture(() -> a1));
		});
		assertNotSetUpdating(b);
		
		//b finishes compute
		barrierInitial.triggerNow();
		assertResult(42, b);
		
		//new value for a
		BarrierImpl barrierNewValue = new BarrierImpl();
		wait[0] = barrierNewValue;
		setMethod.set(a, 52);
		assertResultUpdating(42, b);
		
		//b new value finishes compute
		barrierNewValue.triggerNow();
		assertResult(52, b);
	}
	
	@ParameterizedTest
	@EnumSource(PropertySetMethod.class)
	public void testComputeDelayedOverridden(PropertySetMethod setMethod) {
		Property<Integer> a = new Property<>(42);
		assertResult(42, a);
		
		Property<Integer> b = new Property<>();
		Barrier[] wait = new Barrier[1];
		BarrierImpl barrierInitial = new BarrierImpl();
		wait[0] = barrierInitial;
		b.set(a, (a1, prev) -> {
			throw new Delay(wait[0].toFuture(() -> a1));
		});
		assertNotSetUpdating(b);
		
		//new value for a
		BarrierImpl barrierNewValue = new BarrierImpl();
		wait[0] = barrierNewValue;
		setMethod.set(a, 52);
		assertNotSetUpdating(b);
		
		//before b finishes compute
		barrierInitial.triggerNow();
		assertNotSetUpdating(b);
		
		//b new value finishes compute
		barrierNewValue.triggerNow();
		assertResult(52, b);
	}
}
