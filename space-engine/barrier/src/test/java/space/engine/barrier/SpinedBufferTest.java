package space.engine.barrier;

import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

public class SpinedBufferTest {
	
	@Test
	public void test0Length() {
		SpinedBuffer<Integer> buffer = new SpinedBuffer<>();
		assertEquals(0, buffer.length());
		assertFalse(buffer.iterator().hasNext());
		assertThrows(NoSuchElementException.class, buffer.iterator()::next);
	}
	
	@Test
	public void test1Length() {
		SpinedBuffer<Integer> buffer = new SpinedBuffer<>();
		assertEquals(0, buffer.length());
		assertFalse(buffer.iterator().hasNext());
		
		buffer.add(42);
		assertEquals(1, buffer.length());
		Iterator<Integer> iter = buffer.iterator();
		assertTrue(iter.hasNext());
		assertEquals(42, (int) iter.next());
		assertFalse(iter.hasNext());
	}
	
	@Test
	public void testFullRange() {
		int chunkSize = 13;
		int maxLength = chunkSize * SpinedBuffer.SPINE_INIT_SIZE * 4;
		SpinedBuffer<Integer> buffer = new SpinedBuffer<>(chunkSize);
		
		for (int i = 0; i < maxLength; i++) {
			try {
				buffer.add(i);
				assertEquals(i + 1, buffer.length());
				
				Iterator<Integer> iter = buffer.iterator();
				for (int k = 0; k <= i; k++) {
					assertTrue(iter.hasNext());
					assertEquals(k, (int) iter.next());
				}
				
				assertFalse(iter.hasNext());
				assertThrows(NoSuchElementException.class, iter::next);
			} catch (RuntimeException e) {
				throw new RuntimeException("index " + i, e);
			} catch (Error e) {
				throw new Error("index " + i, e);
			}
		}
	}
}
