package space.engine.barrier.property;

import space.engine.barrier.BarrierImpl;
import space.engine.barrier.Delay;
import space.engine.barrier.future.CompletableFuture;
import space.engine.barrier.future.Future;

public enum PropertySetMethod {
	
	VALUE {
		@Override
		public <T> void set(Property<T> property, T value) {
			property.set(value);
		}
	},
	FUTURE {
		@Override
		public <T> void set(Property<T> property, T value) {
			CompletableFuture<T> future = new CompletableFuture<>();
			property.set(future);
			future.complete(value);
		}
	},
	FUTURE_DONE {
		@Override
		public <T> void set(Property<T> property, T value) {
			property.set(Future.finished(value));
		}
	},
	SUPPLIER {
		@Override
		public <T> void set(Property<T> property, T value) {
			property.set(() -> value);
		}
	},
	MODIFY {
		@Override
		public <T> void set(Property<T> property, T value) {
			property.setReplace(old -> value);
		}
	},
	PROPERTY {
		@Override
		public <T> void set(Property<T> property, T value) {
			Property<T> p = new Property<>();
			property.set(p);
			p.set(value);
		}
	},
	PROPERTY_DONE {
		@Override
		public <T> void set(Property<T> property, T value) {
			property.set(new Property<>(value));
		}
	},
	COMPUTE {
		@Override
		public <T> void set(Property<T> property, T value) {
			Property<T> a = new Property<>();
			property.set(a, (t, previous) -> t);
			a.set(value);
		}
	},
	COMPUTE_DONE {
		@Override
		public <T> void set(Property<T> property, T value) {
			Property<T> a = new Property<>(value);
			property.set(a, (t, previous) -> t);
		}
	},
	COMPUTE_DELAYED {
		@Override
		public <T> void set(Property<T> property, T value) {
			BarrierImpl barrier = new BarrierImpl();
			Property<T> a = new Property<>(value);
			property.set(a, (a1, prev) -> {
				throw new Delay(barrier.toFuture(() -> a1));
			});
			barrier.triggerNow();
		}
	};
	
	public abstract <T> void set(Property<T> property, T value);
}
