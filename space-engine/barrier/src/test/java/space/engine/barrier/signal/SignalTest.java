package space.engine.barrier.signal;

import org.junit.jupiter.api.Test;
import space.engine.barrier.signal.Signal.SignalBind;
import space.engine.barrier.typehandler.TypeHandlerConsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

public class SignalTest {
	
	@Test
	public void testBasic() {
		Signal<Consumer<Integer>> signal = new Signal<>();
		signal.trigger(new TypeHandlerConsumer<>(-1));
		
		int[] expected = new int[1];
		Thread th = Thread.currentThread();
		signal.addHook(i -> {
			assertEquals(th, Thread.currentThread());
			assertNotNull(i);
			assertEquals(expected[0], (int) i);
		});
		
		for (int i = 42; i < 50; i++) {
			expected[0] = i;
			signal.trigger(new TypeHandlerConsumer<>(i));
		}
	}
	
	@Test
	public void testAddAndRemove() {
		Signal<Consumer<Integer>> signal = new Signal<>();
		Integer[] wasCalled = new Integer[] {null};
		SignalBind signalBind = signal.addHook(i -> wasCalled[0] = i);
		assertNull(wasCalled[0]);
		
		signal.trigger(new TypeHandlerConsumer<>(42));
		assertEquals((Integer) 42, wasCalled[0]);
		wasCalled[0] = null;
		
		signal.trigger(new TypeHandlerConsumer<>(43));
		assertEquals((Integer) 43, wasCalled[0]);
		wasCalled[0] = null;
		
		signalBind.unbind();
		signal.trigger(new TypeHandlerConsumer<>(44));
		assertNull(wasCalled[0]);
	}
	
	@Test
	public void testIdAllocation() {
		final int LENGTH = 20;
		Signal<Consumer<Integer>> signal = new Signal<>();
		
		List<SignalBind> signalBinds = new ArrayList<>();
		for (int i = 0; i < LENGTH; i++) {
			SignalBind signalBind = signal.addHook(i2 -> {});
			assertEquals(i, signalBind.internalId());
			signalBinds.add(signalBind);
		}
		
		for (int i = 0; i < LENGTH; i++) {
			SignalBind old = signalBinds.get(i);
			assertEquals(i, old.internalId());
			old.unbind();
			assertEquals(Signal.SIGNALBIND_ID_RELEASED, old.internalId());
			
			SignalBind signalBind = signal.addHook(i2 -> {});
			assertEquals(i, signalBind.internalId());
			signalBind.unbind();
			assertEquals(Signal.SIGNALBIND_ID_RELEASED, signalBind.internalId());
		}
		
		for (int i = 0; i < LENGTH; i++) {
			SignalBind signalBind = signal.addHook(i2 -> {});
			assertEquals(LENGTH - i - 1, signalBind.internalId());
			signalBinds.set(i, signalBind);
		}
		
		SignalBind signalBind = signal.addHook(i2 -> {});
		assertEquals(LENGTH, signalBind.internalId());
		signalBind.unbind();
		assertEquals(Signal.SIGNALBIND_ID_RELEASED, signalBind.internalId());
	}
	
	@Test
	public void testUnbindBeforeBind() {
		Signal<Consumer<Integer>> signal = new Signal<>();
		
		for (int i = 0; i < 2; i++) {
			boolean unbindBefore = i == 1;
			SignalBind signalBind = signal.newSignalBind();
			if (unbindBefore)
				signalBind.unbind();
			int[] out = new int[] {0};
			signal.addHook(signalBind, k -> out[0] = k);
			signal.trigger(new TypeHandlerConsumer<>(42 + i));
			assertEquals(unbindBefore ? 0 : 42 + i, out[0]);
		}
	}
}
