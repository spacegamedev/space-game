package space.engine.barrier;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static space.engine.barrier.Barrier.*;

public class TasksTest {
	
	@Test
	public void testRun() {
		BarrierImpl start = new BarrierImpl();
		AtomicBoolean b = new AtomicBoolean(false);
		Barrier task = start.thenRun(() -> b.set(true));
		
		assertFalse(b.get());
		start.triggerNow();
		task.awaitUninterrupted();
		assertTrue(b.get());
	}
	
	@Test
	public void testRunDelayedReturn() {
		BarrierImpl start = new BarrierImpl();
		AtomicBoolean b = new AtomicBoolean(false);
		Barrier task = start.thenStart(() -> nowRun(() -> b.set(true)));
		
		assertFalse(b.get());
		start.triggerNow();
		task.awaitUninterrupted();
		assertTrue(b.get());
	}
	
	@Test
	public void testRunDelayedException() {
		BarrierImpl start = new BarrierImpl();
		AtomicBoolean b = new AtomicBoolean(false);
		Barrier task = start.thenRun(() -> {
			throw new Delay(nowRun(() -> b.set(true)));
		});
		
		assertFalse(b.get());
		start.triggerNow();
		task.awaitUninterrupted();
		assertTrue(b.get());
	}
	
	@Test
	public void testFuture() {
		assertEquals(nowFuture(() -> "string").awaitGetUninterrupted(), "string");
	}
	
	@Test
	public void testFutureWithException() {
		//noinspection CodeBlock2Expr
		assertThrows(IOException.class, () -> {
			nowFutureWithException(IOException.class, () -> {
				throw new IOException("inside task");
			}).awaitGetUninterrupted();
		});
	}
	
	@Test
	public void testFutureWithXException() {
		//noinspection CodeBlock2Expr
		assertThrows(IOException.class, () -> {
			Barrier.<Object, NoSuchMethodException, IOException, ClassNotFoundException>nowFutureWith3Exception(NoSuchMethodException.class, IOException.class, ClassNotFoundException.class, () -> {
				throw new IOException("inside task");
			}).awaitGetUninterrupted();
		});
	}
	
	@Test
	public void testFutureWithExceptionDelayed() {
		//noinspection CodeBlock2Expr
		assertThrows(IOException.class, () -> {
			nowFutureWithException(IOException.class, () -> {
				throw new Delay(nowFutureWithException(IOException.class, () -> {
					throw new IOException("inside task");
				}));
			}).awaitGetUninterrupted();
		});
	}
	
	@Test
	public void testFutureWithXExceptionDelayed() {
		//noinspection CodeBlock2Expr
		assertThrows(IOException.class, () -> {
			nowFutureWith3Exception(RuntimeException.class, IOException.class, ClassNotFoundException.class, () -> {
				throw new Delay(nowFutureWithException(IOException.class, () -> {
					throw new IOException("inside task");
				}));
			}).awaitGetUninterrupted();
		});
	}
}
