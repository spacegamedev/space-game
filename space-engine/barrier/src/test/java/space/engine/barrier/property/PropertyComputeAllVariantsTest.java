package space.engine.barrier.property;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static space.engine.barrier.property.PropertyTest.*;

public class PropertyComputeAllVariantsTest {
	
	private static final int WIDTH = 9;
	
	@SuppressWarnings("unused")
	private static Stream<Arguments> testComputeAll() {
		return IntStream.rangeClosed(0, WIDTH).boxed().flatMap(
				width -> IntStream.range(0, width).boxed().flatMap(
						indexToTest -> Arrays.stream(PropertySetMethod.values()).map(
								set -> arguments(width, indexToTest, set)
						)
				)
		);
	}
	
	@ParameterizedTest
	@MethodSource
	public void testComputeAll(int width, int indexToTest, @NotNull PropertySetMethod set) {
		assertThat(indexToTest, lessThan(width));
		final int result = IntStream.range(0, width).reduce(0, Integer::sum);
		
		//noinspection unchecked,SuspiciousToArrayCall,SuspiciousToArrayCall
		Property<Integer>[] p = (Property<Integer>[])
				IntStream.range(0, width)
						 .mapToObj(i -> new Property<Integer>())
						 .toArray(Property[]::new);
		
		IntStream.range(0, width).forEach(i -> {
			if (i != indexToTest) {
				p[i].set(i);
				assertResult(i, p[i]);
			} else {
				assertNotSet(p[i]);
			}
		});
		
		Property<Integer> c = new Property<>();
		assertNotSet(c);
		
		switch (width) {
			case 1:
				c.set(p[0], (i1, previous) -> i1);
				break;
			case 2:
				c.set(p[0], p[1], (i1, i2, previous) -> i1 + i2);
				break;
			case 3:
				c.set(p[0], p[1], p[2], (i1, i2, i3, previous) -> i1 + i2 + i3);
				break;
			case 4:
				c.set(p[0], p[1], p[2], p[3], (i1, i2, i3, i4, previous) -> i1 + i2 + i3 + i4);
				break;
			case 5:
				c.set(p[0], p[1], p[2], p[3], p[4], (i1, i2, i3, i4, i5, previous) -> i1 + i2 + i3 + i4 + i5);
				break;
			case 6:
				c.set(p[0], p[1], p[2], p[3], p[4], p[5], (i1, i2, i3, i4, i5, i6, previous) -> i1 + i2 + i3 + i4 + i5 + i6);
				break;
			case 7:
				c.set(p[0], p[1], p[2], p[3], p[4], p[5], p[6], (i1, i2, i3, i4, i5, i6, i7, previous) -> i1 + i2 + i3 + i4 + i5 + i6 + i7);
				break;
			case 8:
				c.set(p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], (i1, i2, i3, i4, i5, i6, i7, i8, previous) -> i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8);
				break;
			default:
				c.set(p, (values, previous) -> Arrays.stream(values).mapToInt(o -> (Integer) o).reduce(0, Integer::sum));
				break;
		}
		assertNotSetUpdating(c);
		
		set.set(p[indexToTest], indexToTest);
		assertResult(result, c);
		
		set.set(p[indexToTest], indexToTest + 1);
		assertResult(result + 1, c);
	}
}
