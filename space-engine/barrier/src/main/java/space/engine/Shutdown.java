package space.engine;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Barrier;
import space.engine.barrier.event.Event;
import space.engine.barrier.event.EventEntry;
import space.engine.barrier.event.SequentialEventBuilder;
import space.engine.barrier.functions.Starter;

public class Shutdown {
	
	public static final Event<Starter<Barrier>> SHOWDOWN_EVENT = new SequentialEventBuilder<>();
	public static final @NotNull EventEntry<Starter<Barrier>> SHUTDOWN_ENTRY_GLOBAL_SHUTDOWN;
	public static final @NotNull EventEntry<Starter<Barrier>> SHUTDOWN_ENTRY_FINAL_EXIT;
	
	static {
		SHOWDOWN_EVENT.addHook(SHUTDOWN_ENTRY_GLOBAL_SHUTDOWN = new EventEntry<>(Starter.noop()));
		SHOWDOWN_EVENT.addHook(SHUTDOWN_ENTRY_FINAL_EXIT = new EventEntry<>(Starter.noop(), SHUTDOWN_ENTRY_GLOBAL_SHUTDOWN));
	}
	
	public static @NotNull Barrier exit() {
		return SHOWDOWN_EVENT.submit(Starter::start);
	}
}
