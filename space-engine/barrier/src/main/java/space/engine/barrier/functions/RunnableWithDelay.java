package space.engine.barrier.functions;

import space.engine.barrier.Delay;

@FunctionalInterface
public interface RunnableWithDelay {
	
	void run() throws Delay;
}
