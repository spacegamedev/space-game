package space.engine.barrier.functions;

import space.engine.barrier.Delay;

//thenFuture
@FunctionalInterface
public
interface SupplierWithDelay<T> {
	
	T get() throws Delay;
}
