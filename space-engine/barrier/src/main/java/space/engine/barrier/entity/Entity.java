package space.engine.barrier.entity;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.lock.SyncLock;

public interface Entity {
	
	@NotNull SyncLock syncLock();
}
