package space.engine.barrier;

public class CancelableBarrierImpl extends BarrierImpl implements CancelableBarrier, CanceledCheck {
	
	private volatile boolean canceled;
	
	public CancelableBarrierImpl() {
	}
	
	@Override
	public void cancel() {
		canceled = true;
	}
	
	@Override
	public boolean isCanceled() {
		return canceled;
	}
}
