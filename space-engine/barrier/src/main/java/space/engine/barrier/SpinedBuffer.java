package space.engine.barrier;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class SpinedBuffer<E> implements Iterable<E> {
	
	public static final int CHUNK_SIZE_DEFAULT = 16;
	public static final int SPINE_INIT_SIZE = 4;
	
	private @NotNull Object[] chunk;
	private int chunkIndex;
	private @Nullable Object[][] spine;
	private int spineIndex;
	
	public SpinedBuffer() {
		this(CHUNK_SIZE_DEFAULT);
	}
	
	public SpinedBuffer(int chunkSize) {
		chunk = new Object[chunkSize];
	}
	
	public void add(E e) {
		if (chunkIndex >= chunk.length) {
			if (spine == null) {
				spine = new Object[SPINE_INIT_SIZE][];
				spine[0] = chunk;
				spineIndex = 1;
			} else if (++spineIndex >= spine.length) {
				spine = Arrays.copyOf(spine, spine.length * 2);
			}
			chunk = new Object[chunk.length];
			spine[spineIndex] = chunk;
			chunkIndex = 0;
		}
		chunk[chunkIndex++] = e;
	}
	
	public int length() {
		return chunkIndex + chunk.length * spineIndex;
	}
	
	@NotNull
	@Override
	public Iterator<E> iterator() {
		return new Iterator<>() {
			
			final int length = length();
			int index = 0;
			
			@Override
			public boolean hasNext() {
				return index < length;
			}
			
			@Override
			@SuppressWarnings({"unchecked", "ConstantConditions"})
			public E next() {
				if (index >= length)
					throw new NoSuchElementException();
				if (spine == null)
					return (E) chunk[index++];
				return (E) spine[index / chunk.length][index++ % chunk.length];
			}
		};
	}
	
	@Override
	public @NotNull Spliterator<E> spliterator() {
		return Spliterators.spliterator(iterator(), length(), 0);
	}
	
	public @NotNull Stream<E> stream() {
		return StreamSupport.stream(spliterator(), false);
	}
}
