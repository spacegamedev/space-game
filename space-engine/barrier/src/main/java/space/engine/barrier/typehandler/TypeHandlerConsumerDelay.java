package space.engine.barrier.typehandler;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Delay;
import space.engine.barrier.functions.ConsumerWithDelay;

/**
 * Sequential only.
 */
public class TypeHandlerConsumerDelay<T> implements TypeHandler<ConsumerWithDelay<T>> {
	
	public T obj;
	
	public TypeHandlerConsumerDelay(T obj) {
		this.obj = obj;
	}
	
	@Override
	public void accept(@NotNull ConsumerWithDelay<T> c) throws Delay {
		c.accept(obj);
	}
}
