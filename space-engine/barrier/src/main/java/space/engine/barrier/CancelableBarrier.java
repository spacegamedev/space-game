package space.engine.barrier;

import org.jetbrains.annotations.NotNull;

public interface CancelableBarrier extends Barrier, Cancelable {
	
	class CancelableDoneBarrier extends Barrier.DoneBarrier implements CancelableBarrier {
		
		@Override
		public void cancel() {
		
		}
	}
	
	@NotNull CancelableBarrier CANCELABLE_DONE_BARRIER = new CancelableDoneBarrier();
	
	static @NotNull CancelableBarrier cancelableDone() {
		return CANCELABLE_DONE_BARRIER;
	}
}
