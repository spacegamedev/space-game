package space.engine.barrier.future;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Barrier;

import java.util.concurrent.TimeUnit;

public interface GenericFuture<R> extends Barrier {
	
	R awaitGetAnyException() throws Throwable;
	
	R awaitGetAnyException(long time, TimeUnit unit) throws Throwable;
	
	R assertGetAnyException() throws Throwable;
	
	static @NotNull RuntimeException newUnexpectedException(@NotNull Throwable e) {
		return new RuntimeException("Exception '" + e.getClass().getName() + "' caught by Future that was not expected to be thrown", e);
	}
}
