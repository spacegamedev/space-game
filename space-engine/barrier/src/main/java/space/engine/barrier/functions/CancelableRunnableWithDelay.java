package space.engine.barrier.functions;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.CanceledCheck;
import space.engine.barrier.Delay;

@FunctionalInterface
public interface CancelableRunnableWithDelay {
	
	void run(@NotNull CanceledCheck check) throws Delay;
}
