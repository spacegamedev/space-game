package space.engine.barrier.typehandler;

import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

/**
 * Allows parallel calls.
 */
public class TypeHandlerConsumer<T> implements TypeHandlerParallel<Consumer<T>>, TypeHandlerNoDelay<Consumer<T>> {
	
	public T obj;
	
	public TypeHandlerConsumer(T obj) {
		this.obj = obj;
	}
	
	@Override
	public void accept(@NotNull Consumer<T> c) {
		c.accept(obj);
	}
}
