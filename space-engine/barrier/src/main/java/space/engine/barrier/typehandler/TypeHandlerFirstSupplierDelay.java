package space.engine.barrier.typehandler;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Delay;
import space.engine.barrier.functions.SupplierWithDelay;

/**
 * The {@link #result()} will be the first {@link SupplierWithDelay} returning something != null.
 * Sequential only.
 */
public class TypeHandlerFirstSupplierDelay<T> implements TypeHandler<SupplierWithDelay<T>> {
	
	private T result;
	
	@Override
	public void accept(@NotNull SupplierWithDelay<T> task) throws Delay {
		if (result == null)
			result = task.get();
	}
	
	public T result() {
		return result;
	}
}
