package space.engine.barrier.future;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Delay;
import space.engine.barrier.functions.Callable;

public interface GenericCompletable<R> extends GenericFuture<R> {
	
	default void completeCallableNoDelay(@NotNull Callable<R> callable) {
		try {
			completeCallable(callable);
		} catch (Delay e) {
			throw GenericFuture.newUnexpectedException(e);
		}
	}
	
	void completeCallable(@NotNull Callable<R> callable) throws Delay;
	
	void complete(R result);
}
