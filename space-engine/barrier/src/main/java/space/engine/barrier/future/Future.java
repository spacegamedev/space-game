package space.engine.barrier.future;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Barrier;
import space.engine.barrier.BarrierImpl;
import space.engine.barrier.Delay;
import space.engine.barrier.Delegate;
import space.engine.barrier.Executor;
import space.engine.barrier.functions.ConsumerWithDelay;
import space.engine.barrier.functions.FunctionWithDelay;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static space.engine.barrier.Barrier.done;
import static space.engine.barrier.Pool.pool;

public interface Future<T> extends GenericFuture<T>, Barrier {
	
	class DelegateFuture<T> implements Delegate<Future<T>, CompletableFuture<T>> {
		
		@Override
		public @NotNull CompletableFuture<T> createCompletable() {
			return new CompletableFuture<>();
		}
		
		@Override
		public void complete(@NotNull CompletableFuture<T> ret, @NotNull Future<T> delegate) {
			ret.complete(delegate.assertGet());
		}
	}
	
	@NotNull DelegateFuture<Object> DELEGATE = new DelegateFuture<>();
	
	@SuppressWarnings("unchecked")
	static <T> @NotNull Delegate<Future<T>, CompletableFuture<T>> delegate() {
		return (Delegate<Future<T>, CompletableFuture<T>>) (Object) DELEGATE;
	}
	
	//hooks
	
	/**
	 * Adds a CompletableFuture hook to be completed with the same value when this Future triggers
	 *
	 * @param run the hook as a {@link CompletableFuture}
	 */
	default void addHook(@NotNull CompletableFuture<T> run) {
		addHook(() -> run.complete(assertGet()));
	}
	
	//abstract get
	T awaitGet() throws InterruptedException;
	
	T awaitGet(long time, TimeUnit unit) throws InterruptedException, TimeoutException;
	
	T assertGet() throws FutureNotFinishedException;
	
	//awaitGetUninterrupted
	
	/**
	 * Waits until event is triggered and doesn't return when interrupted.
	 * The interrupt status of this {@link Thread} will be restored.
	 */
	default T awaitGetUninterrupted() {
		boolean interrupted = false;
		try {
			while (true) {
				try {
					return awaitGet();
				} catch (InterruptedException e) {
					interrupted = true;
				}
			}
		} finally {
			if (interrupted)
				Thread.currentThread().interrupt();
		}
	}
	
	/**
	 * Waits until event is triggered with a timeout and doesn't return when interrupted.
	 * The interrupt status of this {@link Thread} will be restored.
	 *
	 * @throws TimeoutException thrown if waiting takes longer than the specified timeout
	 */
	default T awaitGetUninterrupted(long time, TimeUnit unit) throws TimeoutException {
		boolean interrupted = false;
		try {
			while (true) {
				try {
					return awaitGet(time, unit);
				} catch (InterruptedException e) {
					interrupted = true;
				}
			}
		} finally {
			if (interrupted)
				Thread.currentThread().interrupt();
		}
	}
	
	//anyException
	@Override
	default T awaitGetAnyException() throws Throwable {
		return awaitGet();
	}
	
	@Override
	default T awaitGetAnyException(long time, TimeUnit unit) throws Throwable {
		return awaitGet(time, unit);
	}
	
	@Override
	default T assertGetAnyException() {
		return assertGet();
	}
	
	//default
	@Override
	default @NotNull Future<T> dereference() {
		if (isDone())
			return finished(assertGet());
		
		CompletableFuture<T> future = new CompletableFuture<>();
		this.addHook(future);
		return future;
	}
	
	default <R> R throwDelay() throws Delay {
		throw new Delay(this);
	}
	
	//map (function)
	default <R> @NotNull Future<R> thenStartMap(@NotNull FunctionWithDelay<T, R> runnable) {
		return shouldThenRunDelegateToNowRun() ? nowMap(assertGet(), runnable) : thenMap(Runnable::run, runnable);
	}
	
	default <R> @NotNull Future<R> thenMap(@NotNull FunctionWithDelay<T, R> runnable) {
		return shouldThenRunDelegateToNowRun() ? nowMap(assertGet(), runnable) : thenMap(pool(), runnable);
	}
	
	default <R> @NotNull Future<R> thenMap(@NotNull Executor executor, @NotNull FunctionWithDelay<T, R> runnable) {
		CompletableFuture<R> ret = new CompletableFuture<>();
		addHook(() -> executor.execute(() -> {
			try {
				ret.complete(runnable.apply(assertGet()));
			} catch (Delay delay) {
				if (!(delay.barrier instanceof Future))
					throw new IllegalArgumentException("DelayTask.barrier is not a Future<?>!", delay);
				//noinspection unchecked
				Future<R> future = (Future<R>) delay.barrier;
				future.addHook(ret);
			}
		}));
		return ret;
	}
	
	static <T, R> @NotNull Future<R> nowMap(T t, @NotNull FunctionWithDelay<T, R> runnable) {
		try {
			return Future.finished(runnable.apply(t));
		} catch (Delay delay) {
			if (!(delay.barrier instanceof Future))
				throw new IllegalArgumentException("DelayTask.barrier is not a Future<?>!", delay);
			//noinspection unchecked
			return (Future<R>) delay.barrier;
		}
	}
	
	static <T, R> @NotNull Future<R> nowMap(@NotNull Executor executor, @NotNull T t, @NotNull FunctionWithDelay<T, R> runnable) {
		return finished(t).thenMap(executor, runnable);
	}
	
	//consume
	default @NotNull Barrier thenStartConsume(@NotNull ConsumerWithDelay<T> runnable) {
		return shouldThenRunDelegateToNowRun() ? nowConsume(assertGet(), runnable) : thenConsume(Runnable::run, runnable);
	}
	
	default @NotNull Barrier thenConsume(@NotNull ConsumerWithDelay<T> runnable) {
		return shouldThenRunDelegateToNowRun() ? nowConsume(assertGet(), runnable) : thenConsume(pool(), runnable);
	}
	
	default @NotNull Barrier thenConsume(@NotNull Executor executor, @NotNull ConsumerWithDelay<T> runnable) {
		BarrierImpl ret = new BarrierImpl();
		addHook(() -> executor.execute(() -> {
			try {
				runnable.accept(assertGet());
				ret.triggerNow();
			} catch (Delay delay) {
				delay.barrier.addHook(ret);
			}
		}));
		return ret;
	}
	
	static <T> @NotNull Barrier nowConsume(T t, @NotNull ConsumerWithDelay<T> runnable) {
		try {
			runnable.accept(t);
			return done();
		} catch (Delay delay) {
			return delay.barrier;
		}
	}
	
	static <T> @NotNull Barrier nowConsume(@NotNull Executor executor, @NotNull T t, @NotNull ConsumerWithDelay<T> runnable) {
		return finished(t).thenConsume(executor, runnable);
	}
	
	//static
	static <R> @NotNull Future<R> finished(R get) {
		class Finished extends Barrier.DoneBarrier implements Future<R> {
			
			@Override
			public R awaitGet() {
				return get;
			}
			
			@Override
			public R awaitGet(long time, TimeUnit unit) {
				return get;
			}
			
			@Override
			public R assertGet() throws FutureNotFinishedException {
				return get;
			}
		}
		return new Finished();
	}
	
	/**
	 * Returns a new {@link Future} which triggers when the 'inner' {@link Future} of the supplied {@link Future} is triggered.
	 * The value of the returned {@link Future} is the same as the 'inner' {@link Future}.
	 *
	 * @param future the Future containing the Barrier to await for
	 * @return see description
	 */
	static <R> @NotNull Future<R> innerFuture(@NotNull Future<? extends Future<R>> future) {
		return Barrier.inner(future).toFuture(future.assertGet());
	}
}
