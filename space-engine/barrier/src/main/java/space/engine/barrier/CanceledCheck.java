package space.engine.barrier;

@FunctionalInterface
public interface CanceledCheck {
	
	boolean isCanceled();
}
