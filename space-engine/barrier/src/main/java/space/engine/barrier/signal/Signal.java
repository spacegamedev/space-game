package space.engine.barrier.signal;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import space.engine.Empties;
import space.engine.barrier.typehandler.TypeHandlerNoDelay;

import java.util.Arrays;

/**
 * @implNote Memory efficiency > add and remove performance
 */
public class Signal<T> {
	
	public static int SIGNALBIND_ID_UNBOUND = -1;
	public static int SIGNALBIND_ID_RELEASED = -2;
	
	public static int ENTRY_ARRAY_INITIAL_SIZE = 8;
	public static int RELEASED_ARRAY_INITIAL_SIZE = 8;
	
	private int entryIndex;
	private Object[] entryArray = Empties.EMPTY_OBJECT_ARRAY;
	
	private int releasedIndex;
	private int[] releasedArray = Empties.EMPTY_INT_ARRAY;
	
	@Contract("_ -> new")
	public @NotNull SignalBind addHook(@NotNull T t) {
		return addHook(newSignalBind(), t);
	}
	
	@Contract("_, _ -> param1")
	public synchronized @NotNull SignalBind addHook(@NotNull SignalBind signalBind, @NotNull T t) {
		if (signalBind.id == SIGNALBIND_ID_RELEASED)
			return signalBind;
		
		if (signalBind.id == SIGNALBIND_ID_UNBOUND) {
			int id;
			if (releasedIndex != 0) {
				id = releasedArray[--releasedIndex];
			} else {
				id = entryIndex++;
				if (id >= entryArray.length)
					entryArray = Arrays.copyOf(entryArray, Integer.max(entryArray.length << 1, ENTRY_ARRAY_INITIAL_SIZE));
			}
			entryArray[id] = t;
			signalBind.id = id;
			return signalBind;
		}
		
		throw new IllegalStateException("SignalBind " + signalBind + " already in use!");
	}
	
	protected synchronized void unbind(int id) {
		entryArray[id] = null;
		int releasedId = releasedIndex++;
		if (releasedId >= releasedArray.length)
			releasedArray = Arrays.copyOf(releasedArray, Integer.max(releasedArray.length << 1, RELEASED_ARRAY_INITIAL_SIZE));
		releasedArray[releasedId] = id;
	}
	
	public synchronized void trigger(@NotNull TypeHandlerNoDelay<T> typeHandler) {
		for (int i = 0; i < entryIndex; i++) {
			if (entryArray[i] != null) {
				//noinspection unchecked
				typeHandler.accept((T) entryArray[i]);
			}
		}
	}
	
	@Contract("-> new")
	public @NotNull SignalBind newSignalBind() {
		return new SignalBind(this);
	}
	
	public static class SignalBind {
		
		private final @NotNull Signal<?> signal;
		private int id = SIGNALBIND_ID_UNBOUND;
		
		public SignalBind(@NotNull Signal<?> signal) {
			this.signal = signal;
		}
		
		@Deprecated
		public int internalId() {
			return id;
		}
		
		public void unbind() {
			synchronized (signal) {
				if (id == SIGNALBIND_ID_RELEASED)
					return;
				if (id != SIGNALBIND_ID_UNBOUND)
					signal.unbind(id);
				id = SIGNALBIND_ID_RELEASED;
			}
		}
		
		@Override
		public @NotNull String toString() {
			return "SignalBind{" +
					"signal=" + signal +
					", id=" + id +
					'}';
		}
	}
}
