package space.engine.barrier.functions;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Barrier;
import space.engine.barrier.CanceledCheck;
import space.engine.barrier.Delay;

@FunctionalInterface
public interface CancelableStarter {
	
	@NotNull Barrier start(@NotNull CanceledCheck check) throws Delay;
	
	default @NotNull Barrier startNoException(@NotNull CanceledCheck check) {
		try {
			return start(check);
		} catch (Delay delay) {
			return delay.barrier;
		}
	}
	
	static @NotNull CancelableStarter noop() {
		return check -> Barrier.done();
	}
}
