package space.engine.barrier.entity;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.lock.SyncLock;
import space.engine.barrier.lock.SyncLockImpl;

public abstract class AbstractEntity implements Entity {
	
	protected @NotNull SyncLockImpl syncLock = new SyncLockImpl();
	
	@Override
	public @NotNull SyncLock syncLock() {
		return syncLock;
	}
}
