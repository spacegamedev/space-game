package space.engine.barrier.functions;

import space.engine.barrier.Delay;

@FunctionalInterface
public interface FunctionWithDelay<T, R> {
	
	R apply(T t) throws Delay;
}
