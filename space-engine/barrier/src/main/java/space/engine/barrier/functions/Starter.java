package space.engine.barrier.functions;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Barrier;
import space.engine.barrier.Delay;

@FunctionalInterface
public interface Starter<@NotNull B extends Barrier> {
	
	@NotNull B start() throws Delay;
	
	default @NotNull B startInlineException() {
		try {
			return start();
		} catch (Delay delay) {
			//noinspection unchecked
			return (B) delay.barrier;
		}
	}
	
	static Starter<Barrier> noop() {
		return Barrier::done;
	}
}
