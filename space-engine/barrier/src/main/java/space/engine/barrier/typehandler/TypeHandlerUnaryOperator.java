package space.engine.barrier.typehandler;

import org.jetbrains.annotations.NotNull;

import java.util.function.UnaryOperator;

/**
 * Sequential only.
 */
public class TypeHandlerUnaryOperator<T> implements TypeHandlerNoDelay<UnaryOperator<T>> {
	
	public T obj;
	
	public TypeHandlerUnaryOperator(T obj) {
		this.obj = obj;
	}
	
	@Override
	public void accept(@NotNull UnaryOperator<T> unary) {
		obj = unary.apply(obj);
	}
}
