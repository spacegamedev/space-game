package space.engine.barrier;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.future.Future;

/**
 * Interface for delegating specialized Barriers
 *
 * @param <B> BASE: the base class of the specialized {@link Barrier}
 * @param <C> COMPLETABLE: the class of the Completable made for the BASE class
 */
public interface Delegate<@NotNull B extends Barrier, @NotNull C extends B> {
	
	/**
	 * Creates a new COMPLETABLE, an Object extends BASE which can be completed by calling {@link #complete(Barrier, Barrier)}
	 *
	 * @return a new COMPLETABLE
	 */
	@NotNull C createCompletable();
	
	/**
	 * Completes a created Completable. The Completable should be finished with any additional parameters gotten from the #delegate param.
	 * The #delegate will be finished when this function is called so functions like {@link Future#assertGet()} can be called.
	 *
	 * @param completable the completable to complete
	 * @param delegate    the delegate to get any additional required parameters from
	 */
	void complete(@NotNull C completable, @NotNull B delegate);
	
	/**
	 * Adds a hook to #delegate to call {@link #complete(Barrier, Barrier)} when it finishes.
	 */
	default void addHookAndComplete(@NotNull C completable, @NotNull B delegate) {
		delegate.addHook(() -> complete(completable, delegate));
	}
}
