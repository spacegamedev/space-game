package space.engine.barrier;

import org.jetbrains.annotations.NotNull;
import space.engine.Shutdown;
import space.engine.barrier.event.EventEntry;
import space.engine.barrier.functions.Starter;
import space.engine.barrier.pool.SimpleMessagePool;
import space.engine.barrier.pool.SimpleThreadPool;
import space.engine.simpleQueue.ConcurrentLinkedSimpleQueue;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import static space.engine.barrier.Barrier.done;

@SuppressWarnings("unused")
public class Pool {
	
	private static final @NotNull SimpleThreadPool POOL;
	public static final @NotNull EventEntry<Starter<Barrier>> SHUTDOWN_ENTRY_POOL_STOP;
	
	public static @NotNull Executor pool() {
		return POOL;
	}
	
	static {
		POOL = new SimpleThreadPool(Runtime.getRuntime().availableProcessors(), new ThreadFactory() {
			private final AtomicInteger COUNT = new AtomicInteger();
			
			@Override
			public @NotNull Thread newThread(@NotNull Runnable r) {
				return new Thread(r, "space-pool-" + COUNT.incrementAndGet());
			}
		}, new ConcurrentLinkedSimpleQueue<>(), SimpleMessagePool.DEFAULT_PAUSE_COUNTDOWN);
		
		//don't wait on pool exit -> will cause deadlock
		Shutdown.SHOWDOWN_EVENT.addHook(SHUTDOWN_ENTRY_POOL_STOP = new EventEntry<>(() -> {
			POOL.stop();
			return done();
		}, new EventEntry[] {Shutdown.SHUTDOWN_ENTRY_FINAL_EXIT}, new EventEntry[] {Shutdown.SHUTDOWN_ENTRY_GLOBAL_SHUTDOWN}));
	}
}
