package space.engine.barrier.entity;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Barrier;
import space.engine.barrier.Delegate;
import space.engine.barrier.functions.StarterWithParameter;
import space.engine.barrier.lock.SyncLock;

import java.util.function.BooleanSupplier;

public interface EntityRef<E extends Entity> extends SyncLock {
	
	@NotNull EntityAccessKey<E> getAccessKey();
	
	//static
	static <@NotNull E extends Entity> @NotNull EntityRef<E> createRef(@NotNull E entity) {
		return new BasicEntityRef<>(entity);
	}
	
	class BasicEntityRef<@NotNull T extends Entity> implements EntityRef<T> {
		
		public final @NotNull T entity;
		
		public BasicEntityRef(@NotNull T entity) {
			this.entity = entity;
		}
		
		//synclock
		@Override
		public boolean tryLockNow() {
			return entity.syncLock().tryLockNow();
		}
		
		@Override
		public void tryLockLater(BooleanSupplier callback) {
			entity.syncLock().tryLockLater(callback);
		}
		
		@Override
		public @NotNull Runnable unlock() {
			return entity.syncLock().unlock();
		}
		
		//accesskey
		@Override
		public @NotNull EntityAccessKey<T> getAccessKey() {
			return new EntityAccessKey<>() {
				
				@Override
				public <B extends Barrier, C extends B> B startOn0(@NotNull StarterWithParameter<? extends B, T> runnable, @NotNull Delegate<B, C> delegate) {
					return runnable.startInlineException(entity);
				}
			};
		}
	}
}
