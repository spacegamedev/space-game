package space.engine.barrier.typehandler;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Delay;

/**
 * Version if {@link TypeHandler} without {@link Delay} exception.
 */
@FunctionalInterface
public interface TypeHandlerNoDelay<FUNCTION> extends TypeHandler<FUNCTION> {
	
	@Override
	void accept(@NotNull FUNCTION function);
}
