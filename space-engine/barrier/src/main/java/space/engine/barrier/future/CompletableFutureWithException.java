package space.engine.barrier.future;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.BarrierImpl;
import space.engine.barrier.Delay;
import space.engine.barrier.functions.Callable;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CompletableFutureWithException<R, @NotNull EX extends Throwable> extends BarrierImpl implements FutureWithException<R, EX>, GenericCompletable<R> {
	
	public final @NotNull Class<EX> exceptionClass;
	
	protected R result;
	protected EX exception;
	
	public CompletableFutureWithException(@NotNull Class<EX> exceptionClass) {
		this.exceptionClass = exceptionClass;
	}
	
	//complete
	@Override
	public void completeCallable(@NotNull Callable<R> callable) throws Delay {
		try {
			complete(callable.call());
		} catch (Delay delay) {
			throw delay;
		} catch (Throwable e) {
			if (exceptionClass.isInstance(e))
				//noinspection unchecked
				completeExceptional((EX) e);
			else
				throw GenericFuture.newUnexpectedException(e);
		}
	}
	
	//no synchronized -> deadlocks in triggerNow() callback handling
	@Override
	public void complete(R result) {
		this.result = result;
		super.triggerNow();
	}
	
	public void completeExceptional(@NotNull EX exception) {
		this.exception = exception;
		super.triggerNow();
	}
	
	/**
	 * Use {@link #complete(Object)}
	 */
	@Override
	@Deprecated
	public boolean triggerNow() {
		throw new UnsupportedOperationException("Use #compete(Object)");
	}
	
	//get
	@Override
	public R awaitGet() throws InterruptedException, EX {
		await();
		return getInternal();
	}
	
	@Override
	public R awaitGet(long time, @NotNull TimeUnit unit) throws InterruptedException, TimeoutException, EX {
		await(time, unit);
		return getInternal();
	}
	
	@Override
	public R assertGet() throws FutureNotFinishedException, EX {
		if (!isDone())
			throw new FutureNotFinishedException(this);
		return getInternal();
	}
	
	protected R getInternal() throws EX {
		if (exception != null)
			throw exception;
		return result;
	}
}
