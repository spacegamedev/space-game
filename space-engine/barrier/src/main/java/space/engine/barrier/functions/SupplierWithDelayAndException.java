package space.engine.barrier.functions;

import space.engine.barrier.Delay;

@FunctionalInterface
public interface SupplierWithDelayAndException<T, EX extends Throwable> {
	
	T get() throws Delay, EX;
}
