package space.engine.barrier;

@FunctionalInterface
public interface Cancelable {
	
	void cancel();
}
