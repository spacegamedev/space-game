package space.engine.barrier.typehandler;

import org.jetbrains.annotations.NotNull;

/**
 * Use {@link #INSTANCE} instead of creating a new {@link TypeHandlerRunnable}.
 * Allows parallel calls.
 */
public class TypeHandlerRunnable implements TypeHandlerParallel<Runnable>, TypeHandlerNoDelay<Runnable> {
	
	public static final TypeHandlerRunnable INSTANCE = new TypeHandlerRunnable();
	
	private TypeHandlerRunnable() {
	}
	
	@Override
	public void accept(@NotNull Runnable runnable) {
		runnable.run();
	}
}
