package space.engine.barrier.property;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.typehandler.TypeHandlerNoDelay;

import java.util.function.Consumer;

@FunctionalInterface
public interface PropertyCallback<T> extends Consumer<T> {
	
	static <T> @NotNull TypeHandlerNoDelay<Consumer<? super T>> typeHandlerUpdating() {
		return f -> {
			if (f instanceof PropertyCallback<?>)
				((PropertyCallback<?>) f).updating();
		};
	}
	
	static <T> @NotNull TypeHandlerNoDelay<Consumer<? super T>> typeHandlerAccept(T t, boolean hasSentUpdating) {
		return f -> {
			if (f instanceof PropertyCallback<?>)
				//noinspection unchecked
				((PropertyCallback<? super T>) f).accept(t, hasSentUpdating);
			else
				f.accept(t);
		};
	}
	
	//object
	default void updating() {
	}
	
	@Override
	@Deprecated
	default void accept(T t) {
		throw new UnsupportedOperationException("should call accept(T, boolean) instead");
	}
	
	void accept(T t, boolean hasSentUpdating);
}
