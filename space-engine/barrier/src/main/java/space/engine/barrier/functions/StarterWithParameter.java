package space.engine.barrier.functions;

import space.engine.barrier.Barrier;
import space.engine.barrier.Delay;

@FunctionalInterface
public interface StarterWithParameter<B extends Barrier, P> {
	
	B start(P p) throws Delay;
	
	default B startInlineException(P p) {
		try {
			return start(p);
		} catch (Delay delay) {
			//noinspection unchecked
			return (B) delay.barrier;
		}
	}
}
