package space.engine.barrier.future;

import org.jetbrains.annotations.Nullable;

public class FutureNotFinishedException extends RuntimeException {
	
	public final @Nullable Object future;
	
	public FutureNotFinishedException(@Nullable Object future) {
		super("Future: " + future);
		this.future = future;
	}
}
