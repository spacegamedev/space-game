package space.engine.barrier.future;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.BarrierImpl;
import space.engine.barrier.Delay;
import space.engine.barrier.functions.Callable;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CompletableFutureWith2Exception<R, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable> extends BarrierImpl implements FutureWith2Exception<R, EX1, EX2>, GenericCompletable<R> {
	
	public final @NotNull Class<EX1> exceptionClass1;
	public final @NotNull Class<EX2> exceptionClass2;
	
	protected R result;
	protected EX1 exception1;
	protected EX2 exception2;
	
	public CompletableFutureWith2Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2) {
		this.exceptionClass1 = exceptionClass1;
		this.exceptionClass2 = exceptionClass2;
	}
	
	//complete
	@Override
	public void completeCallable(@NotNull Callable<R> callable) throws Delay {
		try {
			complete(callable.call());
		} catch (Delay delay) {
			throw delay;
		} catch (Throwable e) {
			if (exceptionClass1.isInstance(e))
				//noinspection unchecked
				completeExceptional1((EX1) e);
			else if (exceptionClass2.isInstance(e))
				//noinspection unchecked
				completeExceptional2((EX2) e);
			else
				throw GenericFuture.newUnexpectedException(e);
		}
	}
	
	//no synchronized -> deadlocks in triggerNow() callback handling
	@Override
	public void complete(R result) {
		this.result = result;
		super.triggerNow();
	}
	
	public void completeExceptional1(@NotNull EX1 exception) {
		this.exception1 = exception;
		super.triggerNow();
	}
	
	public void completeExceptional2(@NotNull EX2 exception) {
		this.exception2 = exception;
		super.triggerNow();
	}
	
	/**
	 * Use {@link #complete(Object)}
	 */
	@Override
	@Deprecated
	public boolean triggerNow() {
		throw new UnsupportedOperationException("Use #compete(Object)");
	}
	
	//get
	@Override
	public R awaitGet() throws InterruptedException, EX1, EX2 {
		await();
		return getInternal();
	}
	
	@Override
	public R awaitGet(long time, @NotNull TimeUnit unit) throws InterruptedException, TimeoutException, EX1, EX2 {
		await(time, unit);
		return getInternal();
	}
	
	@Override
	public R assertGet() throws FutureNotFinishedException, EX1, EX2 {
		if (!isDone())
			throw new FutureNotFinishedException(this);
		return getInternal();
	}
	
	protected R getInternal() throws EX1, EX2 {
		if (exception1 != null)
			throw exception1;
		if (exception2 != null)
			throw exception2;
		return result;
	}
}
