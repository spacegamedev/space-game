package space.engine.barrier.property;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Delay;
import space.engine.barrier.functions.SupplierWithDelay;
import space.engine.barrier.future.CompletableFuture;
import space.engine.barrier.future.Future;
import space.engine.barrier.future.FutureNotFinishedException;
import space.engine.barrier.signal.Signal;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.VarHandle;
import java.util.function.Consumer;

/**
 * The base Property class, which publicly only exposes read methods. For write access see {@link Property}.
 * <p>
 * Internally it does however have all fields and protected internal methods available for writing the property.
 * How to implement set methods:
 * First call {@link #internalSource(Object)} or {@link #internalSourceLater()} depending if you want to set the value immediately or later. If you want to later update the value store the returned short modid.
 * If the value should be updated immediately, call {@link #internalValue(short, Object)} with the modid from above and the new value.
 * If an update counting system is preferred, use {@link #internalUpdatingIncrement(short)} and {@link #internalUpdatingDecrement(short, boolean, SupplierWithDelay)} again with the modid from above.
 * This is the only one which also offers support for delayed updates by the {@link SupplierWithDelay} throwing a {@link Delay} exception.
 * All three methods return a boolean for checking whether the current modid is the most recent one. When false is returned, the value was not updated and the source is encouraged to clean up it's resources.
 * <b>DO NOT</b> mix one version with the other.
 */
public abstract class ReadOnlyProperty<T> extends Signal<Consumer<? super T>> {
	
	protected static final VarHandle INITIALIZED;
	protected static final VarHandle T;
	
	static {
		try {
			Lookup lookup = MethodHandles.lookup();
			INITIALIZED = lookup.findVarHandle(Property.class, "initialized", boolean.class);
			T = lookup.findVarHandle(Property.class, "t", Object.class);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new ExceptionInInitializerError(e);
		}
	}
	
	/**
	 * boolean whether #t has been initialized (aka. set at least once) and can be read.
	 * May be read volatile.
	 */
	@SuppressWarnings("unused")
	private volatile boolean initialized;
	/**
	 * the reference to the object T stored in the property. Valid if {@link #initialized} is true.
	 * May be read volatile.
	 */
	@SuppressWarnings("unused")
	private volatile T t;
	/**
	 * A modid for the source of values. Incremented on every call to {@link #internalSource(Object)} or {@link #internalSourceLater()} method, which in turn are called by all public set() methods.
	 * Internal and synchronized only.
	 */
	@SuppressWarnings("unused")
	private short sourceModId;
	/**
	 * A counter to count the amount of updating calls on {@link #internalUpdatingIncrement(short)} and {@link #internalUpdatingDecrement(short, boolean, SupplierWithDelay)}. Used for set() with compute methods.
	 * Internal and synchronized only.
	 */
	@SuppressWarnings("unused")
	private short updatingCounter = 1;
	/**
	 * A modid for delayed value updates. Always use in combination with {@link #sourceModId}. Used with {@link #internalUpdatingDecrement(short, boolean, SupplierWithDelay)} when Supplier throws a {@link Delay} to order multiple Delays canceling each other.
	 * Internal and synchronized only.
	 */
	private short valueModId;
	/**
	 * A boolean for knowing that in increment of {@link #updatingCounter} has happened due to {@link #internalUpdatingDecrement(short, boolean, SupplierWithDelay)} throwing {@link Delay}, which has to be reverted (decremented) of there is a new value or source.
	 * Internal and synchronized only.
	 */
	private boolean valueDelayUpdatingCounterIncremented;
	
	//internal get
	@SuppressWarnings("unchecked")
	protected T internalGet() {
		return (T) T.getAcquire(this);
	}
	
	//internal set
	protected short internalSource(T t) {
		synchronized (this) {
			short modId = ++this.sourceModId;
			
			this.t = t;
			INITIALIZED.setRelease(this, true);
			
			boolean hasSentUpdating = this.updatingCounter != 0;
			this.updatingCounter = 0;
			this.valueDelayUpdatingCounterIncremented = false;
			trigger(PropertyCallback.typeHandlerAccept(t, hasSentUpdating));
			return modId;
		}
	}
	
	protected short internalSourceLater() {
		synchronized (this) {
			short modId = ++this.sourceModId;
			
			boolean callUpdating = this.updatingCounter == 0;
			this.updatingCounter = (short) 1;
			this.valueDelayUpdatingCounterIncremented = false;
			if (callUpdating)
				trigger(PropertyCallback.typeHandlerUpdating());
			return modId;
		}
	}
	
	protected boolean internalValue(final short modId, T t) {
		synchronized (this) {
			if (this.sourceModId != modId)
				return false;
			
			this.t = t;
			INITIALIZED.setRelease(this, true);
			++this.valueModId;
			
			boolean hasSentUpdating = this.updatingCounter != 0;
			this.updatingCounter = 0;
			//not needed, already set in internalSourceLater() (except when mixing with internalUpdatingDecrement(), which must not be done)
//			this.valueDelayUpdatingCounterIncremented = false;
			trigger(PropertyCallback.typeHandlerAccept(t, hasSentUpdating));
			return true;
		}
	}
	
	protected boolean internalUpdatingIncrement(final short modId) {
		synchronized (this) {
			if (this.sourceModId != modId)
				return false;
			
			++this.valueModId;
			
			boolean callUpdating = this.updatingCounter == 0;
			if (this.valueDelayUpdatingCounterIncremented)
				//pattern changed: decrement and increment -> just don't do anything
				this.valueDelayUpdatingCounterIncremented = false;
			else
				this.updatingCounter++;
			if (callUpdating)
				trigger(PropertyCallback.typeHandlerUpdating());
			return true;
		}
	}
	
	protected boolean internalUpdatingDecrement(final short modId, boolean hasSentUpdating, @NotNull SupplierWithDelay<T> supplier) {
		synchronized (this) {
			if (this.sourceModId != modId)
				return false;
			
			final short valueModId = ++this.valueModId;
			boolean updatingCounterWas0 = this.updatingCounter == 0;
			if (hasSentUpdating)
				this.updatingCounter--;
			if (this.valueDelayUpdatingCounterIncremented)
				this.updatingCounter--;
			this.valueDelayUpdatingCounterIncremented = false;
			if (this.updatingCounter > 0)
				return true;
			
			try {
				T t1 = supplier.get();
				this.t = t1;
				INITIALIZED.setRelease(this, true);
				
				trigger(PropertyCallback.typeHandlerAccept(t1, hasSentUpdating));
			} catch (Delay delay) {
				this.valueDelayUpdatingCounterIncremented = true;
				this.updatingCounter++;
				if (updatingCounterWas0)
					trigger(PropertyCallback.typeHandlerUpdating());
				
				delay.barrier.addHook(() -> {
					synchronized (this) {
						if (this.sourceModId != modId || this.valueModId != valueModId)
							return;
						
						//noinspection unchecked
						T t2 = ((Future<T>) delay.barrier).assertGet();
						this.t = t2;
						INITIALIZED.setRelease(this, true);
						
						//if one of the following fields are changed, sourceModId or valueModId must have also changed and this code will have returned early
						//valueDelayUpdatingCounterIncremented must be true
						this.valueDelayUpdatingCounterIncremented = false;
						//updatingCounter must be 1
						this.updatingCounter = 0;
						trigger(PropertyCallback.typeHandlerAccept(t2, true));
					}
				});
			}
			return true;
		}
	}
	
	//hook
	@Override
	public @NotNull SignalBind addHook(@NotNull Consumer<? super T> t) {
		return addHook(newSignalBind(), t);
	}
	
	@Override
	public @NotNull SignalBind addHook(@NotNull SignalBind signalBind, @NotNull Consumer<? super T> propertyCallback) {
		synchronized (this) {
			super.addHook(signalBind, propertyCallback);
			if (hasValue())
				PropertyCallback.typeHandlerAccept(t, false).accept(propertyCallback);
			if (this.updatingCounter != 0)
				PropertyCallback.<T>typeHandlerUpdating().accept(propertyCallback);
		}
		return signalBind;
	}
	
	public @NotNull SignalBind addHookNoInitialValue(@NotNull Consumer<? super T> t) {
		return addHookNoInitialValue(newSignalBind(), t);
	}
	
	public @NotNull SignalBind addHookNoInitialValue(@NotNull SignalBind signalBind, @NotNull Consumer<? super T> propertyCallback) {
		synchronized (this) {
			super.addHook(signalBind, propertyCallback);
			if (this.updatingCounter != 0)
				PropertyCallback.<T>typeHandlerUpdating().accept(propertyCallback);
		}
		return signalBind;
	}
	
	//get
	public boolean hasValue() {
		return (boolean) INITIALIZED.getAcquire(this);
	}
	
	public @NotNull T assertGet() throws FutureNotFinishedException {
		if (!hasValue())
			throw new FutureNotFinishedException(this);
		return internalGet();
	}
	
	public @NotNull Future<T> future() {
		if (hasValue())
			return Future.finished(internalGet());
		
		//required: signal could happen between initialized check and addHook() call
		synchronized (this) {
			if (hasValue())
				return Future.finished(internalGet());
			
			CompletableFuture<T> ret = new CompletableFuture<>();
			SignalBind signalBind = newSignalBind();
			addHook(signalBind, t -> {
				ret.complete(t);
				signalBind.unbind();
			});
			return ret;
		}
	}
	
	//other
	@SuppressWarnings("unused")
	protected abstract void dontExtendThisJustUsePropertyBecausePerformance();
	
	@SuppressWarnings("unchecked")
	public static <T> T testingGetT(ReadOnlyProperty<T> p) {
		return (T) ReadOnlyProperty.T.getAcquire(p);
	}
	
	public static short testingGetSourceModId(@NotNull ReadOnlyProperty<?> p) {
		//noinspection SynchronizationOnLocalVariableOrMethodParameter
		synchronized (p) {
			return p.sourceModId;
		}
	}
	
	public static short testingGetUpdatingCounter(@NotNull ReadOnlyProperty<?> p) {
		//noinspection SynchronizationOnLocalVariableOrMethodParameter
		synchronized (p) {
			return p.updatingCounter;
		}
	}
	
	public static short testingGetValueModId(@NotNull ReadOnlyProperty<?> p) {
		//noinspection SynchronizationOnLocalVariableOrMethodParameter
		synchronized (p) {
			return p.valueModId;
		}
	}
	
	public static boolean testingGetBalueDelayUpdatingCounterIncremented(@NotNull ReadOnlyProperty<?> p) {
		//noinspection SynchronizationOnLocalVariableOrMethodParameter
		synchronized (p) {
			return p.valueDelayUpdatingCounterIncremented;
		}
	}
}
