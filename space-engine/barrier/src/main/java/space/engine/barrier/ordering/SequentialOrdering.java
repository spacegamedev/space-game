package space.engine.barrier.ordering;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Barrier;
import space.engine.barrier.BarrierImpl;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.function.Function;

import static space.engine.barrier.Barrier.DONE_BARRIER;

public class SequentialOrdering {
	
	private static final VarHandle LASTBARRIER;
	
	static {
		try {
			MethodHandles.Lookup l = MethodHandles.lookup();
			LASTBARRIER = l.findVarHandle(SequentialOrdering.class, "lastBarrier", Barrier.class);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new ExceptionInInitializerError(e);
		}
	}
	
	@SuppressWarnings("FieldMayBeFinal")
	private Barrier lastBarrier = DONE_BARRIER;
	
	/**
	 * Enqueues a new function to be executed in the sequence.
	 *
	 * @param function a function generating a new task which will start execution when supplied {@link Barrier} is triggered and returns a {@link Barrier} triggered when execution has finished
	 * @return the Barrier returned by the function parameter
	 */
	public <@NotNull B extends Barrier> @NotNull B next(@NotNull Function<@NotNull ? super Barrier, @NotNull B> function) {
		BarrierImpl next = new BarrierImpl();
		Barrier prev = (Barrier) LASTBARRIER.getAndSet(this, next);
		
		B apply = function.apply(prev);
		apply.addHook(next);
		return apply;
	}
	
	/**
	 * For debugging and highly controlled testing purposes only!
	 */
	@Deprecated
	public @NotNull Barrier getLatestBarrier() {
		return (Barrier) LASTBARRIER.getVolatile(this);
	}
}
