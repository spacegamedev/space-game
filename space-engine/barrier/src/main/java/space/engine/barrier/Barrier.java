package space.engine.barrier;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.entity.Entity;
import space.engine.barrier.entity.EntityAccessKey;
import space.engine.barrier.entity.EntityRef;
import space.engine.barrier.functions.CancelableRunnableWithDelay;
import space.engine.barrier.functions.CancelableStarter;
import space.engine.barrier.functions.ConsumerWithDelay;
import space.engine.barrier.functions.FunctionWithDelay;
import space.engine.barrier.functions.RunnableWithDelay;
import space.engine.barrier.functions.Starter;
import space.engine.barrier.functions.StarterWith2Parameter;
import space.engine.barrier.functions.StarterWith3Parameter;
import space.engine.barrier.functions.StarterWith4Parameter;
import space.engine.barrier.functions.StarterWith5Parameter;
import space.engine.barrier.functions.StarterWithParameter;
import space.engine.barrier.functions.SupplierWithDelay;
import space.engine.barrier.functions.SupplierWithDelayAnd2Exception;
import space.engine.barrier.functions.SupplierWithDelayAnd3Exception;
import space.engine.barrier.functions.SupplierWithDelayAnd4Exception;
import space.engine.barrier.functions.SupplierWithDelayAnd5Exception;
import space.engine.barrier.functions.SupplierWithDelayAndException;
import space.engine.barrier.future.CompletableFuture;
import space.engine.barrier.future.CompletableFutureWith2Exception;
import space.engine.barrier.future.CompletableFutureWith3Exception;
import space.engine.barrier.future.CompletableFutureWith4Exception;
import space.engine.barrier.future.CompletableFutureWith5Exception;
import space.engine.barrier.future.CompletableFutureWithException;
import space.engine.barrier.future.Future;
import space.engine.barrier.future.FutureNotFinishedException;
import space.engine.barrier.future.FutureWith2Exception;
import space.engine.barrier.future.FutureWith3Exception;
import space.engine.barrier.future.FutureWith4Exception;
import space.engine.barrier.future.FutureWith5Exception;
import space.engine.barrier.future.FutureWithException;
import space.engine.barrier.future.GenericFuture;
import space.engine.barrier.lock.SyncLock;
import space.engine.interfaces.Freeable;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static space.engine.barrier.CancelableBarrier.cancelableDone;
import static space.engine.barrier.Pool.pool;

/**
 * An Object which can be {@link #await() awaited} upon. You can also {@link #addHook(Runnable) add a Hook} to be called when the Barrier {@link #isDone() is finished}. <br>
 * <b>It cannot be triggered more than once or reset</b>.
 */
public interface Barrier {
	
	class DoneBarrier implements Barrier {
		
		protected DoneBarrier() {
		}
		
		@Override
		public boolean isDone() {
			return true;
		}
		
		@Override
		public void addHook(@NotNull Runnable run) {
			run.run();
		}
		
		@Override
		public void await() {
		
		}
		
		@Override
		public void await(long time, TimeUnit unit) {
		
		}
		
		@Override
		public boolean shouldThenRunDelegateToNowRun() {
			return true;
		}
	}
	
	DoneBarrier DONE_BARRIER = new DoneBarrier();
	
	class DelegateBarrier implements Delegate<Barrier, BarrierImpl> {
		
		@Override
		public @NotNull BarrierImpl createCompletable() {
			return new BarrierImpl();
		}
		
		@Override
		public void complete(@NotNull BarrierImpl ret, @NotNull Barrier delegate) {
			ret.triggerNow();
		}
	}
	
	DelegateBarrier DELEGATE = new DelegateBarrier();
	
	static @NotNull Delegate<Barrier, BarrierImpl> delegate() {
		return DELEGATE;
	}
	
	//getter
	
	/**
	 * Gets the state if this {@link Barrier}, whether it is finished or not.<br>
	 * NOTE: The triggered state should be considered immediately stale, as it can change any Moment without notice.
	 *
	 * @return whether the {@link Barrier} is triggered or not
	 */
	boolean isDone();
	
	//hooks
	
	/**
	 * Adds a hook to be called when this Barrier triggers
	 *
	 * @param run the hook as a {@link Runnable}
	 */
	void addHook(@NotNull Runnable run);
	
	/**
	 * Adds a Barrier hook to triggered when this Barrier triggers
	 *
	 * @param run the hook as a {@link BarrierImpl}
	 */
	default void addHook(@NotNull BarrierImpl run) {
		addHook(run::triggerNow);
	}
	
	//await
	
	/**
	 * Waits until event is triggered
	 *
	 * @throws InterruptedException if an interrupt occurs
	 */
	void await() throws InterruptedException;
	
	/**
	 * Waits until event is triggered with a timeout
	 *
	 * @throws InterruptedException if an interrupt occurs
	 * @throws TimeoutException     thrown if waiting takes longer than the specified timeout
	 */
	void await(long time, TimeUnit unit) throws InterruptedException, TimeoutException;
	
	/**
	 * Waits until event is triggered and doesn't return when interrupted.
	 * The interrupt status of this {@link Thread} will be restored.
	 */
	default void awaitUninterrupted() {
		boolean interrupted = false;
		try {
			while (true) {
				try {
					await();
					return;
				} catch (InterruptedException e) {
					interrupted = true;
				}
			}
		} finally {
			if (interrupted)
				Thread.currentThread().interrupt();
		}
	}
	
	/**
	 * Waits until event is triggered with a timeout and doesn't return when interrupted.
	 * The interrupt status of this {@link Thread} will be restored.
	 *
	 * @throws TimeoutException thrown if waiting takes longer than the specified timeout
	 */
	default void awaitUninterrupted(long time, TimeUnit unit) throws TimeoutException {
		boolean interrupted = false;
		try {
			while (true) {
				try {
					await(time, unit);
					return;
				} catch (InterruptedException e) {
					interrupted = true;
				}
			}
		} finally {
			if (interrupted)
				Thread.currentThread().interrupt();
		}
	}
	
	//toFuture
	default <R> @NotNull Future<R> toFuture(@NotNull Future<R> supplier) {
		if (shouldThenRunDelegateToNowRun())
			return Future.finished(supplier.assertGet());
		return toFuture(supplier::assertGet);
	}
	
	default <R> @NotNull Future<R> toFuture(@NotNull Supplier<R> supplier) {
		return new Future<>() {
			@Override
			public R awaitGet() throws InterruptedException {
				Barrier.this.await();
				return supplier.get();
			}
			
			@Override
			public R awaitGet(long time, TimeUnit unit) throws InterruptedException, TimeoutException {
				Barrier.this.await(time, unit);
				return supplier.get();
			}
			
			@Override
			public R assertGet() throws FutureNotFinishedException {
				if (!Barrier.this.isDone())
					throw new FutureNotFinishedException(this);
				return supplier.get();
			}
			
			@Override
			public boolean isDone() {
				return Barrier.this.isDone();
			}
			
			@Override
			public void addHook(@NotNull Runnable run) {
				Barrier.this.addHook(run);
			}
			
			@Override
			public void await() throws InterruptedException {
				Barrier.this.await();
			}
			
			@Override
			public void await(long time, TimeUnit unit) throws InterruptedException, TimeoutException {
				Barrier.this.await(time, unit);
			}
		};
	}
	
	/**
	 * Creates a new Barrier which can be used just like this but does not hold a reference to this.
	 * Should be used when holding a Barrier for a very long time (eg. lifetime of an object) to prevent eg. a RunnableTask to stay referenced
	 */
	default @NotNull Barrier dereference() {
		if (isDone())
			return done();
		
		BarrierImpl barrier = new BarrierImpl();
		this.addHook(barrier);
		return barrier;
	}
	
	//shouldThenRunDelegateToNowRun
	
	/**
	 * Should statically return true or false.
	 * <p>
	 * returning true implies that {@link #isDone()} always returns true as well
	 */
	default boolean shouldThenRunDelegateToNowRun() {
		return false;
	}
	
	//run
	default @NotNull Barrier thenRun(@NotNull RunnableWithDelay runnable) {
		return shouldThenRunDelegateToNowRun() ? nowRun(runnable) : thenRun(pool(), runnable);
	}
	
	default @NotNull Barrier thenRun(@NotNull Executor executor, @NotNull RunnableWithDelay runnable) {
		BarrierImpl ret = new BarrierImpl();
		addHook(() -> executor.execute(() -> {
			try {
				runnable.run();
				ret.triggerNow();
			} catch (Delay delay) {
				delay.barrier.addHook(ret);
			}
		}));
		return ret;
	}
	
	static @NotNull Barrier nowRun(@NotNull RunnableWithDelay runnable) {
		try {
			runnable.run();
			return done();
		} catch (Delay delay) {
			return delay.barrier;
		}
	}
	
	static @NotNull Barrier nowRun(@NotNull Executor executor, @NotNull RunnableWithDelay runnable) {
		return when().thenRun(executor, runnable);
	}
	
	//start
	default @NotNull Barrier thenStart(@NotNull Starter<?> runnable) {
		if (shouldThenRunDelegateToNowRun())
			return runnable.startInlineException();
		BarrierImpl ret = new BarrierImpl();
		addHook(() -> runnable.startInlineException().addHook(ret));
		return ret;
	}
	
	default @NotNull Barrier thenStart(@NotNull Executor executor, @NotNull Starter<?> runnable) {
		BarrierImpl ret = new BarrierImpl();
		addHook(() -> executor.execute(() -> runnable.startInlineException().addHook(ret)));
		return ret;
	}
	
	/**
	 * This method just delegates to {@link Starter#startInlineException()}
	 */
	static <@NotNull B extends Barrier> @NotNull B nowStart(@NotNull Starter<? extends B> runnable) {
		return runnable.startInlineException();
	}
	
	static @NotNull Barrier nowStart(@NotNull Executor executor, @NotNull Starter<?> runnable) {
		return when().thenStart(executor, runnable);
	}
	
	//start with delegate
	default <@NotNull B extends Barrier, @NotNull C extends B> @NotNull B thenStart(@NotNull Starter<? extends B> runnable, @NotNull Delegate<B, C> delegate) {
		if (shouldThenRunDelegateToNowRun())
			return runnable.startInlineException();
		C ret = delegate.createCompletable();
		addHook(() -> delegate.addHookAndComplete(ret, runnable.startInlineException()));
		return ret;
	}
	
	default <@NotNull B extends Barrier, @NotNull C extends B> @NotNull B thenStart(@NotNull Executor executor, @NotNull Starter<? extends B> runnable, @NotNull Delegate<B, C> delegate) {
		C ret = delegate.createCompletable();
		addHook(() -> executor.execute(() -> delegate.addHookAndComplete(ret, runnable.startInlineException())));
		return ret;
	}
	
	/**
	 * Call {@link #nowStart(Starter)} instead, without the #delegate param.
	 */
	@Deprecated
	static <@NotNull B extends Barrier, @NotNull C extends B> @NotNull B nowStart(@NotNull Starter<? extends B> runnable, @NotNull @SuppressWarnings("unused") Delegate<B, C> delegate) {
		return runnable.startInlineException();
	}
	
	default <@NotNull B extends Barrier, @NotNull C extends B> @NotNull B nowStart(@NotNull Executor executor, @NotNull Starter<? extends B> runnable, @NotNull Delegate<B, C> delegate) {
		return when().thenStart(executor, runnable, delegate);
	}
	
	//runCancelable
	default @NotNull CancelableBarrier thenRunCancelable(@NotNull CancelableRunnableWithDelay runnable) {
		return shouldThenRunDelegateToNowRun() ? nowRunCancelable(runnable) : thenRunCancelable(pool(), runnable);
	}
	
	default @NotNull CancelableBarrier thenRunCancelable(@NotNull Executor executor, @NotNull CancelableRunnableWithDelay runnable) {
		CancelableBarrierImpl ret = new CancelableBarrierImpl();
		addHook(() -> executor.execute(() -> {
			try {
				runnable.run(ret);
				ret.triggerNow();
			} catch (Delay delay) {
				delay.barrier.addHook(ret);
			}
		}));
		return ret;
	}
	
	static @NotNull CancelableBarrier nowRunCancelable(@NotNull CancelableRunnableWithDelay runnable) {
		CancelableBarrierImpl ret = new CancelableBarrierImpl();
		try {
			runnable.run(ret);
			return cancelableDone();
		} catch (Delay delay) {
			delay.barrier.addHook(ret);
			return ret;
		}
	}
	
	static @NotNull CancelableBarrier nowRunCancelable(@NotNull Executor executor, @NotNull CancelableRunnableWithDelay runnable) {
		return when().thenRunCancelable(executor, runnable);
	}
	
	//startCancelable
	default @NotNull CancelableBarrier thenStartCancelable(@NotNull CancelableStarter runnable) {
		if (shouldThenRunDelegateToNowRun())
			return nowStartCancelable(runnable);
		CancelableBarrierImpl ret = new CancelableBarrierImpl();
		addHook(() -> runnable.startNoException(ret).addHook(ret));
		return ret;
	}
	
	default @NotNull CancelableBarrier thenStartCancelable(@NotNull Executor executor, @NotNull CancelableStarter runnable) {
		CancelableBarrierImpl ret = new CancelableBarrierImpl();
		addHook(() -> executor.execute(() -> runnable.startNoException(ret).addHook(ret)));
		return ret;
	}
	
	static @NotNull CancelableBarrier nowStartCancelable(@NotNull CancelableStarter runnable) {
		try {
			runnable.start(() -> false);
			return cancelableDone();
		} catch (Delay delay) {
			CancelableBarrierImpl ret = new CancelableBarrierImpl();
			delay.barrier.addHook(ret);
			return ret;
		}
	}
	
	static @NotNull CancelableBarrier nowStartCancelable(@NotNull Executor executor, @NotNull CancelableStarter runnable) {
		return when().thenStartCancelable(executor, runnable);
	}
	
	//Future
	default <T> @NotNull Future<T> thenStartFuture(@NotNull SupplierWithDelay<T> runnable) {
		return shouldThenRunDelegateToNowRun() ? nowFuture(runnable) : thenFuture(Runnable::run, runnable);
	}
	
	default <T> @NotNull Future<T> thenFuture(@NotNull SupplierWithDelay<T> runnable) {
		return shouldThenRunDelegateToNowRun() ? nowFuture(runnable) : thenFuture(pool(), runnable);
	}
	
	default <T> @NotNull Future<T> thenFuture(@NotNull Executor executor, @NotNull SupplierWithDelay<T> runnable) {
		CompletableFuture<T> ret = new CompletableFuture<>();
		addHook(() -> executor.execute(() -> {
			try {
				ret.complete(runnable.get());
			} catch (Delay delay) {
				if (!(delay.barrier instanceof Future))
					throw new IllegalArgumentException("DelayTask.barrier is not a Future<?>!", delay);
				
				//noinspection unchecked
				Future<T> future = (Future<T>) delay.barrier;
				future.addHook(ret);
			}
		}));
		return ret;
	}
	
	static <T> @NotNull Future<T> nowFuture(@NotNull SupplierWithDelay<T> runnable) {
		try {
			return Future.finished(runnable.get());
		} catch (Delay delay) {
			if (!(delay.barrier instanceof Future))
				throw new IllegalArgumentException("DelayTask.barrier is not a Future<?>!", delay);
			//noinspection unchecked
			return (Future<T>) delay.barrier;
		}
	}
	
	static <T> @NotNull Future<T> nowFuture(@NotNull Executor executor, @NotNull SupplierWithDelay<T> runnable) {
		return when().thenFuture(executor, runnable);
	}
	
	//FutureWithException
	default <T, EX extends Throwable> @NotNull FutureWithException<T, EX> thenStartFutureWithException(@NotNull Class<EX> exceptionClass, @NotNull SupplierWithDelayAndException<T, EX> runnable) {
		return thenFutureWithException(exceptionClass, Runnable::run, runnable);
	}
	
	default <T> @NotNull Future<T> thenFutureWithException(@NotNull SupplierWithDelay<T> runnable) {
		return thenFuture(pool(), runnable);
	}
	
	default <T, @NotNull EX extends Throwable> @NotNull FutureWithException<T, EX> thenFutureWithException(@NotNull Class<EX> exceptionClass, @NotNull SupplierWithDelayAndException<T, EX> runnable) {
		return thenFutureWithException(exceptionClass, pool(), runnable);
	}
	
	default <T, @NotNull EX extends Throwable> @NotNull FutureWithException<T, EX> thenFutureWithException(@NotNull Class<EX> exceptionClass, @NotNull Executor executor, @NotNull SupplierWithDelayAndException<T, EX> runnable) {
		CompletableFutureWithException<T, EX> ret = new CompletableFutureWithException<>(exceptionClass);
		addHook(() -> executor.execute(() -> {
			try {
				ret.completeCallable(runnable::get);
			} catch (Delay delay) {
				if (!(delay.barrier instanceof GenericFuture))
					throw new IllegalArgumentException("DelayTask.barrier is not a Future<?>!", delay);
				
				//noinspection unchecked
				GenericFuture<T> future = (GenericFuture<T>) delay.barrier;
				future.addHook(() -> {
					try {
						ret.completeCallable(future::assertGetAnyException);
					} catch (Delay e) {
						throw GenericFuture.newUnexpectedException(e);
					}
				});
			}
		}));
		return ret;
	}
	
	static <T, @NotNull EX extends Throwable> @NotNull FutureWithException<T, EX> nowFutureWithException(@NotNull Class<EX> exceptionClass, @NotNull SupplierWithDelayAndException<T, EX> runnable) {
		try {
			return FutureWithException.finished(runnable.get());
		} catch (Delay delay) {
			if (!(delay.barrier instanceof GenericFuture))
				throw new IllegalArgumentException("DelayTask.barrier is not a Future<?>!", delay);
			
			//noinspection unchecked
			GenericFuture<T> future = (GenericFuture<T>) delay.barrier;
			CompletableFutureWithException<T, EX> ret = new CompletableFutureWithException<>(exceptionClass);
			future.addHook(() -> {
				try {
					ret.completeCallable(future::assertGetAnyException);
				} catch (Delay e) {
					throw GenericFuture.newUnexpectedException(e);
				}
			});
			return ret;
		} catch (Throwable e) {
			if (exceptionClass.isInstance(e))
				//noinspection unchecked
				return FutureWithException.finishedException((EX) e);
			else
				throw GenericFuture.newUnexpectedException(e);
		}
	}
	
	static <T, @NotNull EX extends Throwable> @NotNull FutureWithException<T, EX> nowFutureWithException(@NotNull Class<EX> exceptionClass, @NotNull Executor executor, @NotNull SupplierWithDelayAndException<T, EX> runnable) {
		return when().thenFutureWithException(exceptionClass, executor, runnable);
	}
	
	//FutureWith2Exception
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable> @NotNull FutureWith2Exception<T, EX1, EX2> thenStartFutureWith2Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull SupplierWithDelayAnd2Exception<T, EX1, EX2> runnable) {
		return thenFutureWith2Exception(exceptionClass1, exceptionClass2, Runnable::run, runnable);
	}
	
	default <T> @NotNull Future<T> thenFutureWith2Exception(@NotNull SupplierWithDelay<T> runnable) {
		return thenFuture(pool(), runnable);
	}
	
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable> @NotNull FutureWith2Exception<T, EX1, EX2> thenFutureWith2Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull SupplierWithDelayAnd2Exception<T, EX1, EX2> runnable) {
		return thenFutureWith2Exception(exceptionClass1, exceptionClass2, pool(), runnable);
	}
	
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable> @NotNull FutureWith2Exception<T, EX1, EX2> thenFutureWith2Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Executor executor, @NotNull SupplierWithDelayAnd2Exception<T, EX1, EX2> runnable) {
		CompletableFutureWith2Exception<T, EX1, EX2> ret = new CompletableFutureWith2Exception<>(exceptionClass1, exceptionClass2);
		addHook(() -> executor.execute(() -> {
			try {
				ret.completeCallable(runnable::get);
			} catch (Delay delay) {
				if (!(delay.barrier instanceof GenericFuture))
					throw new IllegalArgumentException("DelayTask.barrier is not a Future<?>!", delay);
				
				//noinspection unchecked
				GenericFuture<T> future = (GenericFuture<T>) delay.barrier;
				future.addHook(() -> {
					try {
						ret.completeCallable(future::assertGetAnyException);
					} catch (Delay e) {
						throw GenericFuture.newUnexpectedException(e);
					}
				});
			}
		}));
		return ret;
	}
	
	static <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable> @NotNull FutureWith2Exception<T, EX1, EX2> nowFutureWith2Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull SupplierWithDelayAnd2Exception<T, EX1, EX2> runnable) {
		return when().thenFutureWith2Exception(exceptionClass1, exceptionClass2, runnable);
	}
	
	static <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable> @NotNull FutureWith2Exception<T, EX1, EX2> nowFutureWith2Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Executor executor, @NotNull SupplierWithDelayAnd2Exception<T, EX1, EX2> runnable) {
		return when().thenFutureWith2Exception(exceptionClass1, exceptionClass2, executor, runnable);
	}
	
	//FutureWith3Exception
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable> @NotNull FutureWith3Exception<T, EX1, EX2, EX3> thenStartFutureWith3Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull SupplierWithDelayAnd3Exception<T, EX1, EX2, EX3> runnable) {
		return thenFutureWith3Exception(exceptionClass1, exceptionClass2, exceptionClass3, Runnable::run, runnable);
	}
	
	default <T> @NotNull Future<T> thenFutureWith3Exception(@NotNull SupplierWithDelay<T> runnable) {
		return thenFuture(pool(), runnable);
	}
	
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable> @NotNull FutureWith3Exception<T, EX1, EX2, EX3> thenFutureWith3Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull SupplierWithDelayAnd3Exception<T, EX1, EX2, EX3> runnable) {
		return thenFutureWith3Exception(exceptionClass1, exceptionClass2, exceptionClass3, pool(), runnable);
	}
	
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable> @NotNull FutureWith3Exception<T, EX1, EX2, EX3> thenFutureWith3Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Executor executor, @NotNull SupplierWithDelayAnd3Exception<T, EX1, EX2, EX3> runnable) {
		CompletableFutureWith3Exception<T, EX1, EX2, EX3> ret = new CompletableFutureWith3Exception<>(exceptionClass1, exceptionClass2, exceptionClass3);
		addHook(() -> executor.execute(() -> {
			try {
				ret.completeCallable(runnable::get);
			} catch (Delay delay) {
				if (!(delay.barrier instanceof GenericFuture))
					throw new IllegalArgumentException("DelayTask.barrier is not a Future<?>!", delay);
				
				//noinspection unchecked
				GenericFuture<T> future = (GenericFuture<T>) delay.barrier;
				future.addHook(() -> {
					try {
						ret.completeCallable(future::assertGetAnyException);
					} catch (Delay e) {
						throw GenericFuture.newUnexpectedException(e);
					}
				});
			}
		}));
		return ret;
	}
	
	static <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable> @NotNull FutureWith3Exception<T, EX1, EX2, EX3> nowFutureWith3Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull SupplierWithDelayAnd3Exception<T, EX1, EX2, EX3> runnable) {
		return when().thenFutureWith3Exception(exceptionClass1, exceptionClass2, exceptionClass3, runnable);
	}
	
	static <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable> @NotNull FutureWith3Exception<T, EX1, EX2, EX3> nowFutureWith3Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Executor executor, @NotNull SupplierWithDelayAnd3Exception<T, EX1, EX2, EX3> runnable) {
		return when().thenFutureWith3Exception(exceptionClass1, exceptionClass2, exceptionClass3, executor, runnable);
	}
	
	//FutureWith4Exception
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable, @NotNull EX4 extends Throwable> @NotNull FutureWith4Exception<T, EX1, EX2, EX3, EX4> thenStartFutureWith4Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Class<EX4> exceptionClass4, @NotNull SupplierWithDelayAnd4Exception<T, EX1, EX2, EX3, EX4> runnable) {
		return thenFutureWith4Exception(exceptionClass1, exceptionClass2, exceptionClass3, exceptionClass4, Runnable::run, runnable);
	}
	
	default <T> @NotNull Future<T> thenFutureWith4Exception(@NotNull SupplierWithDelay<T> runnable) {
		return thenFuture(pool(), runnable);
	}
	
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable, @NotNull EX4 extends Throwable> @NotNull FutureWith4Exception<T, EX1, EX2, EX3, EX4> thenFutureWith4Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Class<EX4> exceptionClass4, @NotNull SupplierWithDelayAnd4Exception<T, EX1, EX2, EX3, EX4> runnable) {
		return thenFutureWith4Exception(exceptionClass1, exceptionClass2, exceptionClass3, exceptionClass4, pool(), runnable);
	}
	
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable, @NotNull EX4 extends Throwable> @NotNull FutureWith4Exception<T, EX1, EX2, EX3, EX4> thenFutureWith4Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Class<EX4> exceptionClass4, @NotNull Executor executor, @NotNull SupplierWithDelayAnd4Exception<T, EX1, EX2, EX3, EX4> runnable) {
		CompletableFutureWith4Exception<T, EX1, EX2, EX3, EX4> ret = new CompletableFutureWith4Exception<>(exceptionClass1, exceptionClass2, exceptionClass3, exceptionClass4);
		addHook(() -> executor.execute(() -> {
			try {
				ret.completeCallable(runnable::get);
			} catch (Delay delay) {
				if (!(delay.barrier instanceof GenericFuture))
					throw new IllegalArgumentException("DelayTask.barrier is not a Future<?>!", delay);
				
				//noinspection unchecked
				GenericFuture<T> future = (GenericFuture<T>) delay.barrier;
				future.addHook(() -> {
					try {
						ret.completeCallable(future::assertGetAnyException);
					} catch (Delay e) {
						throw GenericFuture.newUnexpectedException(e);
					}
				});
			}
		}));
		return ret;
	}
	
	static <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable, @NotNull EX4 extends Throwable> @NotNull FutureWith4Exception<T, EX1, EX2, EX3, EX4> nowFutureWith4Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Class<EX4> exceptionClass4, @NotNull SupplierWithDelayAnd4Exception<T, EX1, EX2, EX3, EX4> runnable) {
		return when().thenFutureWith4Exception(exceptionClass1, exceptionClass2, exceptionClass3, exceptionClass4, runnable);
	}
	
	static <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable, @NotNull EX4 extends Throwable> @NotNull FutureWith4Exception<T, EX1, EX2, EX3, EX4> nowFutureWith4Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Class<EX4> exceptionClass4, @NotNull Executor executor, @NotNull SupplierWithDelayAnd4Exception<T, EX1, EX2, EX3, EX4> runnable) {
		return when().thenFutureWith4Exception(exceptionClass1, exceptionClass2, exceptionClass3, exceptionClass4, executor, runnable);
	}
	
	//FutureWith5Exception
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable, @NotNull EX4 extends Throwable, @NotNull EX5 extends Throwable> @NotNull FutureWith5Exception<T, EX1, EX2, EX3, EX4, EX5> thenStartFutureWith5Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Class<EX4> exceptionClass4, @NotNull Class<EX5> exceptionClass5, @NotNull SupplierWithDelayAnd5Exception<T, EX1, EX2, EX3, EX4, EX5> runnable) {
		return thenFutureWith5Exception(exceptionClass1, exceptionClass2, exceptionClass3, exceptionClass4, exceptionClass5, Runnable::run, runnable);
	}
	
	default <T> @NotNull Future<T> thenFutureWith5Exception(@NotNull SupplierWithDelay<T> runnable) {
		return thenFuture(pool(), runnable);
	}
	
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable, @NotNull EX4 extends Throwable, @NotNull EX5 extends Throwable> @NotNull FutureWith5Exception<T, EX1, EX2, EX3, EX4, EX5> thenFutureWith5Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Class<EX4> exceptionClass4, @NotNull Class<EX5> exceptionClass5, @NotNull SupplierWithDelayAnd5Exception<T, EX1, EX2, EX3, EX4, EX5> runnable) {
		return thenFutureWith5Exception(exceptionClass1, exceptionClass2, exceptionClass3, exceptionClass4, exceptionClass5, pool(), runnable);
	}
	
	default <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable, @NotNull EX4 extends Throwable, @NotNull EX5 extends Throwable> @NotNull FutureWith5Exception<T, EX1, EX2, EX3, EX4, EX5> thenFutureWith5Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Class<EX4> exceptionClass4, @NotNull Class<EX5> exceptionClass5, @NotNull Executor executor, @NotNull SupplierWithDelayAnd5Exception<T, EX1, EX2, EX3, EX4, EX5> runnable) {
		CompletableFutureWith5Exception<T, EX1, EX2, EX3, EX4, EX5> ret = new CompletableFutureWith5Exception<>(exceptionClass1, exceptionClass2, exceptionClass3, exceptionClass4, exceptionClass5);
		addHook(() -> executor.execute(() -> {
			try {
				ret.completeCallable(runnable::get);
			} catch (Delay delay) {
				if (!(delay.barrier instanceof GenericFuture))
					throw new IllegalArgumentException("DelayTask.barrier is not a Future<?>!", delay);
				
				//noinspection unchecked
				GenericFuture<T> future = (GenericFuture<T>) delay.barrier;
				future.addHook(() -> {
					try {
						ret.completeCallable(future::assertGetAnyException);
					} catch (Delay e) {
						throw GenericFuture.newUnexpectedException(e);
					}
				});
			}
		}));
		return ret;
	}
	
	static <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable, @NotNull EX4 extends Throwable, @NotNull EX5 extends Throwable> @NotNull FutureWith5Exception<T, EX1, EX2, EX3, EX4, EX5> nowFutureWith5Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Class<EX4> exceptionClass4, @NotNull Class<EX5> exceptionClass5, @NotNull SupplierWithDelayAnd5Exception<T, EX1, EX2, EX3, EX4, EX5> runnable) {
		return when().thenFutureWith5Exception(exceptionClass1, exceptionClass2, exceptionClass3, exceptionClass4, exceptionClass5, runnable);
	}
	
	static <T, @NotNull EX1 extends Throwable, @NotNull EX2 extends Throwable, @NotNull EX3 extends Throwable, @NotNull EX4 extends Throwable, @NotNull EX5 extends Throwable> @NotNull FutureWith5Exception<T, EX1, EX2, EX3, EX4, EX5> nowFutureWith5Exception(@NotNull Class<EX1> exceptionClass1, @NotNull Class<EX2> exceptionClass2, @NotNull Class<EX3> exceptionClass3, @NotNull Class<EX4> exceptionClass4, @NotNull Class<EX5> exceptionClass5, @NotNull Executor executor, @NotNull SupplierWithDelayAnd5Exception<T, EX1, EX2, EX3, EX4, EX5> runnable) {
		return when().thenFutureWith5Exception(exceptionClass1, exceptionClass2, exceptionClass3, exceptionClass4, exceptionClass5, executor, runnable);
	}
	
	//lock
	default @NotNull Barrier thenLock(@NotNull SyncLock @NotNull [] locks, @NotNull Starter<?> runnable) {
		return thenLock(locks, runnable, Barrier.delegate());
	}
	
	default <@NotNull B extends Barrier, @NotNull C extends B> @NotNull B thenLock(@NotNull SyncLock @NotNull [] locks, @NotNull Starter<? extends B> runnable, @NotNull Delegate<B, C> delegate) {
		C ret = delegate.createCompletable();
		addHook(() -> SyncLock.acquireLocks(locks, () -> {
			B barrier = runnable.startInlineException();
			
			//prevents StackOverflow from too many Barrier.addHook() calling the Runnable immediately in combination with SyncLock.unlockLocks()
			//if it is run immediately -> ThenLockUnlockRunnable.immediatelyRun will be false
			ThenLockUnlockRunnable<B, C> run = new ThenLockUnlockRunnable<>(locks, delegate, ret, barrier);
			barrier.addHook(run);
			//if it wasn't run immediately allow it to do so
			run.immediateCAS();
		}));
		return ret;
	}
	
	class ThenLockUnlockRunnable<@NotNull B extends Barrier, @NotNull C extends B> implements Runnable {
		
		private static final @NotNull VarHandle IMMEDIATE;
		
		static {
			try {
				IMMEDIATE = MethodHandles.lookup().findVarHandle(ThenLockUnlockRunnable.class, "immediatelyRun", boolean.class);
			} catch (@NotNull NoSuchFieldException | IllegalAccessException e) {
				throw new ExceptionInInitializerError(e);
			}
		}
		
		private final @NotNull SyncLock[] locks;
		private final @NotNull Delegate<B, C> delegate;
		private final @NotNull C ret;
		private final @NotNull B barrier;
		
		@SuppressWarnings({"unused", "FieldMayBeFinal"})
		private volatile boolean immediatelyRun = true;
		
		public ThenLockUnlockRunnable(@NotNull SyncLock[] locks, @NotNull Delegate<B, C> delegate, @NotNull C ret, @NotNull B barrier) {
			this.locks = locks;
			this.delegate = delegate;
			this.ret = ret;
			this.barrier = barrier;
		}
		
		private boolean immediateCAS() {
			return IMMEDIATE.compareAndSet(this, true, false);
		}
		
		@Override
		public void run() {
			if (immediateCAS()) {
				pool().execute(this);
				return;
			}
			SyncLock.unlockLocks(locks);
			delegate.complete(ret, barrier);
		}
	}
	
	static @NotNull Barrier nowLock(@NotNull SyncLock @NotNull [] locks, @NotNull Starter<?> runnable) {
		return when().thenLock(locks, runnable);
	}
	
	static <@NotNull B extends Barrier, @NotNull C extends B> @NotNull B nowLock(@NotNull SyncLock @NotNull [] locks, @NotNull Starter<? extends B> runnable, @NotNull Delegate<B, C> delegate) {
		return when().thenLock(locks, runnable, delegate);
	}
	
	//lock entity 1
	default <@NotNull E extends Entity> @NotNull Barrier thenLockEntity(@NotNull EntityRef<E> entityRef, @NotNull StarterWithParameter<?, EntityAccessKey<E>> runnable) {
		return thenLockEntity(entityRef, runnable, Barrier.delegate());
	}
	
	default <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E extends Entity> @NotNull B thenLockEntity(@NotNull EntityRef<E> entityRef, @NotNull StarterWithParameter<? extends B, EntityAccessKey<E>> runnable, @NotNull Delegate<B, C> delegate) {
		return thenLock(
				new SyncLock[] {entityRef},
				() -> runnable.startInlineException(entityRef.getAccessKey()),
				delegate
		);
	}
	
	static <@NotNull E extends Entity> @NotNull Barrier nowLockEntity(@NotNull EntityRef<E> entityRef, @NotNull StarterWithParameter<?, EntityAccessKey<E>> runnable) {
		return when().thenLockEntity(entityRef, runnable);
	}
	
	static <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E extends Entity> @NotNull B nowLockEntity(@NotNull EntityRef<E> entityRef, @NotNull StarterWithParameter<? extends B, EntityAccessKey<E>> runnable, @NotNull Delegate<B, C> delegate) {
		return when().thenLockEntity(entityRef, runnable, delegate);
	}
	
	//lock entity 1 and startOn
	default <@NotNull E extends Entity> @NotNull Barrier thenLockEntityAndStartOn(@NotNull EntityRef<E> entityRef, @NotNull StarterWithParameter<?, E> runnable) {
		return thenLockEntity(entityRef, ea -> ea.startOn(runnable));
	}
	
	static <@NotNull E extends Entity> @NotNull Barrier nowLockEntityAndStartOn(@NotNull EntityRef<E> entityRef, @NotNull StarterWithParameter<?, E> runnable) {
		return nowLockEntity(entityRef, ea -> ea.startOn(runnable));
	}
	
	default <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E extends Entity> @NotNull B thenLockEntityAndStartOn(@NotNull EntityRef<E> entityRef, @NotNull StarterWithParameter<? extends B, E> runnable, @NotNull Delegate<B, C> delegate) {
		return thenLockEntity(entityRef, ea -> ea.startOn(runnable, delegate), delegate);
	}
	
	static <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E extends Entity> @NotNull B nowLockEntityAndStartOn(@NotNull EntityRef<E> entityRef, @NotNull StarterWithParameter<? extends B, E> runnable, @NotNull Delegate<B, C> delegate) {
		return nowLockEntity(entityRef, ea -> ea.startOn(runnable, delegate), delegate);
	}
	
	//lock entity 1 and runOn
	default <@NotNull E extends Entity> @NotNull Barrier thenLockEntityAndRunOn(@NotNull EntityRef<E> entityRef, @NotNull ConsumerWithDelay<E> runnable) {
		return thenLockEntity(entityRef, ea -> ea.runOn(runnable));
	}
	
	static <@NotNull E extends Entity> @NotNull Barrier nowLockEntityAndRunOn(@NotNull EntityRef<E> entityRef, @NotNull ConsumerWithDelay<E> runnable) {
		return nowLockEntity(entityRef, ea -> ea.runOn(runnable));
	}
	
	//lock entity 1 and futureOn
	default <@NotNull E extends Entity, R> @NotNull Future<R> thenLockEntityAndFutureOn(@NotNull EntityRef<E> entityRef, @NotNull FunctionWithDelay<E, R> runnable) {
		return thenLockEntity(entityRef, ea -> ea.futureOn(runnable), Future.delegate());
	}
	
	static <@NotNull E extends Entity, R> @NotNull Future<R> nowLockEntityAndFutureOn(@NotNull EntityRef<E> entityRef, @NotNull FunctionWithDelay<E, R> runnable) {
		return nowLockEntity(entityRef, ea -> ea.futureOn(runnable), Future.delegate());
	}
	
	//lock entity 2
	default <@NotNull E1 extends Entity, @NotNull E2 extends Entity> @NotNull Barrier thenLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull StarterWith2Parameter<?, EntityAccessKey<E1>, EntityAccessKey<E2>> runnable) {
		return thenLockEntity(entityRef1, entityRef2, runnable, Barrier.delegate());
	}
	
	default <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E1 extends Entity, @NotNull E2 extends Entity> @NotNull B thenLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull StarterWith2Parameter<? extends B, EntityAccessKey<E1>, EntityAccessKey<E2>> runnable, @NotNull Delegate<B, C> delegate) {
		return thenLock(
				new SyncLock[] {entityRef1, entityRef2},
				() -> runnable.startInlineException(entityRef1.getAccessKey(), entityRef2.getAccessKey()),
				delegate
		);
	}
	
	static <@NotNull E1 extends Entity, @NotNull E2 extends Entity> @NotNull Barrier nowLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull StarterWith2Parameter<?, EntityAccessKey<E1>, EntityAccessKey<E2>> runnable) {
		return when().thenLockEntity(entityRef1, entityRef2, runnable);
	}
	
	static <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E1 extends Entity, @NotNull E2 extends Entity> @NotNull B nowLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull StarterWith2Parameter<? extends B, EntityAccessKey<E1>, EntityAccessKey<E2>> runnable, @NotNull Delegate<B, C> delegate) {
		return when().thenLockEntity(entityRef1, entityRef2, runnable, delegate);
	}
	
	//lock entity 3
	default <@NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity> @NotNull Barrier thenLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull StarterWith3Parameter<?, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>> runnable) {
		return thenLockEntity(entityRef1, entityRef2, entityRef3, runnable, Barrier.delegate());
	}
	
	default <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity> @NotNull B thenLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull StarterWith3Parameter<? extends B, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>> runnable, @NotNull Delegate<B, C> delegate) {
		return thenLock(
				new SyncLock[] {entityRef1, entityRef3},
				() -> runnable.startInlineException(entityRef1.getAccessKey(), entityRef2.getAccessKey(), entityRef3.getAccessKey()),
				delegate
		);
	}
	
	static <@NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity> @NotNull Barrier nowLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull StarterWith3Parameter<?, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>> runnable) {
		return when().thenLockEntity(entityRef1, entityRef2, entityRef3, runnable);
	}
	
	static <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity> @NotNull B nowLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull StarterWith3Parameter<? extends B, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>> runnable, @NotNull Delegate<B, C> delegate) {
		return when().thenLockEntity(entityRef1, entityRef2, entityRef3, runnable, delegate);
	}
	
	//lock entity 4
	default <@NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity, @NotNull E4 extends Entity> @NotNull Barrier thenLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull EntityRef<E4> entityRef4, @NotNull StarterWith4Parameter<?, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>, EntityAccessKey<E4>> runnable) {
		return thenLockEntity(entityRef1, entityRef2, entityRef3, entityRef4, runnable, Barrier.delegate());
	}
	
	default <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity, @NotNull E4 extends Entity> @NotNull B thenLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull EntityRef<E4> entityRef4, @NotNull StarterWith4Parameter<? extends B, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>, EntityAccessKey<E4>> runnable, @NotNull Delegate<B, C> delegate) {
		return thenLock(
				new SyncLock[] {entityRef1, entityRef4},
				() -> runnable.startInlineException(entityRef1.getAccessKey(), entityRef2.getAccessKey(), entityRef3.getAccessKey(), entityRef4.getAccessKey()),
				delegate
		);
	}
	
	static <@NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity, @NotNull E4 extends Entity> @NotNull Barrier nowLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull EntityRef<E4> entityRef4, @NotNull StarterWith4Parameter<?, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>, EntityAccessKey<E4>> runnable) {
		return when().thenLockEntity(entityRef1, entityRef2, entityRef3, entityRef4, runnable);
	}
	
	static <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity, @NotNull E4 extends Entity> @NotNull B nowLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull EntityRef<E4> entityRef4, @NotNull StarterWith4Parameter<? extends B, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>, EntityAccessKey<E4>> runnable, @NotNull Delegate<B, C> delegate) {
		return when().thenLockEntity(entityRef1, entityRef2, entityRef3, entityRef4, runnable, delegate);
	}
	
	//lock entity 5
	default <@NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity, @NotNull E4 extends Entity, @NotNull E5 extends Entity> @NotNull Barrier thenLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull EntityRef<E4> entityRef4, @NotNull EntityRef<E5> entityRef5, @NotNull StarterWith5Parameter<?, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>, EntityAccessKey<E4>, EntityAccessKey<E5>> runnable) {
		return thenLockEntity(entityRef1, entityRef2, entityRef3, entityRef4, entityRef5, runnable, Barrier.delegate());
	}
	
	default <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity, @NotNull E4 extends Entity, @NotNull E5 extends Entity> @NotNull B thenLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull EntityRef<E4> entityRef4, @NotNull EntityRef<E5> entityRef5, @NotNull StarterWith5Parameter<? extends B, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>, EntityAccessKey<E4>, EntityAccessKey<E5>> runnable, @NotNull Delegate<B, C> delegate) {
		return thenLock(
				new SyncLock[] {entityRef1, entityRef5},
				() -> runnable.startInlineException(entityRef1.getAccessKey(), entityRef2.getAccessKey(), entityRef3.getAccessKey(), entityRef4.getAccessKey(), entityRef5.getAccessKey()),
				delegate
		);
	}
	
	static <@NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity, @NotNull E4 extends Entity, @NotNull E5 extends Entity> @NotNull Barrier nowLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull EntityRef<E4> entityRef4, @NotNull EntityRef<E5> entityRef5, @NotNull StarterWith5Parameter<?, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>, EntityAccessKey<E4>, EntityAccessKey<E5>> runnable) {
		return when().thenLockEntity(entityRef1, entityRef2, entityRef3, entityRef4, entityRef5, runnable);
	}
	
	static <@NotNull B extends Barrier, @NotNull C extends B, @NotNull E1 extends Entity, @NotNull E2 extends Entity, @NotNull E3 extends Entity, @NotNull E4 extends Entity, @NotNull E5 extends Entity> @NotNull B nowLockEntity(@NotNull EntityRef<E1> entityRef1, @NotNull EntityRef<E2> entityRef2, @NotNull EntityRef<E3> entityRef3, @NotNull EntityRef<E4> entityRef4, @NotNull EntityRef<E5> entityRef5, @NotNull StarterWith5Parameter<? extends B, EntityAccessKey<E1>, EntityAccessKey<E2>, EntityAccessKey<E3>, EntityAccessKey<E4>, EntityAccessKey<E5>> runnable, @NotNull Delegate<B, C> delegate) {
		return when().thenLockEntity(entityRef1, entityRef2, entityRef3, entityRef4, entityRef5, runnable, delegate);
	}
	
	//free
	default void thenFree(@NotNull Freeable freeable) {
		addHook(freeable::close);
	}
	
	default void thenFree(@NotNull Freeable freeable1, @NotNull Freeable freeable2) {
		addHook(() -> {
			freeable1.close();
			freeable2.close();
		});
	}
	
	default void thenFree(Freeable @NotNull ... freeables) {
		addHook(() -> {
			for (Freeable freeable : freeables)
				freeable.close();
		});
	}
	
	//static
	
	/**
	 * returns a Barrier which is always done
	 */
	static @NotNull Barrier done() {
		return DONE_BARRIER;
	}
	
	/**
	 * Awaits for all {@link Barrier Barriers} to be triggered, then triggers the returned {@link Barrier}. This Operation is non-blocking.
	 * If no Barriers are given, a triggered Barrier is returned.
	 *
	 * @return A {@link Barrier} which is triggered when all supplied {@link Barrier Barriers} have.
	 * @implNote this specific method just returns {@link #done()}
	 */
	static @NotNull Barrier when() {
		return DONE_BARRIER;
	}
	
	/**
	 * Awaits for all {@link Barrier Barriers} to be triggered, then triggers the returned {@link Barrier}. This Operation is non-blocking.
	 * If no Barriers are given, a triggered Barrier is returned.
	 *
	 * @param barrier the {@link Barrier Barriers} to await upon
	 * @return A {@link Barrier} which is triggered when all supplied {@link Barrier Barriers} have.
	 * @implNote this specific method just returns the supplied barrier
	 */
	static <@NotNull B extends Barrier> @NotNull B when(@NotNull B barrier) {
		return barrier;
	}
	
	/**
	 * Awaits for all {@link Barrier Barriers} to be triggered, then triggers the returned {@link Barrier}. This Operation is non-blocking.
	 * If no Barriers are given, a triggered Barrier is returned.
	 *
	 * @param barriers the {@link Barrier Barriers} to await upon
	 * @return A {@link Barrier} which is triggered when all supplied {@link Barrier Barriers} have.
	 */
	static @NotNull Barrier when(@NotNull Collection<@NotNull ? extends Barrier> barriers) {
		return when(barriers.toArray(new Barrier[0]));
	}
	
	/**
	 * Awaits for all {@link Barrier Barriers} to be triggered, then triggers the returned {@link Barrier}. This Operation is non-blocking.
	 * If no Barriers are given, a triggered Barrier is returned.
	 *
	 * @param barriers the {@link Barrier Barriers} to await upon
	 * @return A {@link Barrier} which is triggered when all supplied {@link Barrier Barriers} have.
	 */
	static @NotNull Barrier when(@NotNull Stream<@NotNull ? extends Barrier> barriers) {
		return when(barriers.toArray(Barrier[]::new));
	}
	
	/**
	 * Awaits for all {@link Barrier Barriers} to be triggered, then triggers the returned {@link Barrier}. This Operation is non-blocking.
	 * If no Barriers are given, a triggered Barrier is returned.
	 *
	 * @param barriers the {@link Barrier Barriers} to await upon
	 * @return A {@link Barrier} which is triggered when all supplied {@link Barrier Barriers} have.
	 */
	static @NotNull Barrier when(@NotNull Barrier @NotNull ... barriers) {
		if (barriers.length == 0)
			return when();
		if (barriers.length == 1)
			return when(barriers[0]);
		
		BarrierImpl ret = new BarrierImpl();
		AtomicInteger cnt = new AtomicInteger(barriers.length);
		Runnable hook = () -> {
			if (cnt.decrementAndGet() == 0)
				ret.triggerNow();
		};
		for (Barrier barrier : barriers)
			barrier.addHook(hook);
		return ret;
	}
	
	/**
	 * Returns a new {@link Barrier} which triggers when the 'inner' {@link Barrier} of the supplied {@link Future} is triggered.
	 *
	 * @param future the Future containing the Barrier to await for
	 * @return see description
	 */
	static @NotNull Barrier inner(@NotNull Future<@NotNull ? extends Barrier> future) {
		if (future.isDone())
			return future.assertGet();
		
		BarrierImpl ret = new BarrierImpl();
		future.addHook(() -> future.assertGet().addHook(ret));
		return ret;
	}
}
