package space.engine.barrier.property;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.engine.barrier.Delay;
import space.engine.barrier.future.Future;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * reads are volatile
 * writes are synchronized
 */
public class Property<T> extends ReadOnlyProperty<T> {
	
	public Property() {
	}
	
	@Override
	protected void dontExtendThisJustUsePropertyBecausePerformance() {
	}
	
	//set
	public Property(T t) {
		set(t);
	}
	
	public void set(T t) {
		internalSource(t);
	}
	
	public Property(@NotNull Supplier<T> t) {
		set(t);
	}
	
	public void set(@NotNull Supplier<T> consumer) {
		short modId = internalSourceLater();
		internalValue(modId, consumer.get());
	}
	
	public Property(@NotNull Future<T> t) {
		set(t);
	}
	
	public void set(@NotNull Future<T> future) {
		if (future.shouldThenRunDelegateToNowRun()) {
			set(future.assertGet());
			return;
		}
		
		short modId = internalSourceLater();
		future.addHook(() -> internalValue(modId, future.assertGet()));
	}
	
	public Property(@NotNull ReadOnlyProperty<T> t) {
		set(t);
	}
	
	public void set(@NotNull ReadOnlyProperty<T> property) {
		short modId = internalSourceLater();
		SignalBind signalBind = newSignalBind();
		property.addHook(signalBind, new PropertyCallback<>() {
			@Override
			public void updating() {
				if (!internalUpdatingIncrement(modId))
					signalBind.unbind();
			}
			
			@Override
			public void accept(T t, boolean hasSentUpdating) {
				if (!internalValue(modId, t))
					signalBind.unbind();
			}
		});
	}
	
	//modify
	public void setModify(@NotNull Consumer<T> modify) {
		synchronized (this) {
			T t = internalGet();
			modify.accept(t);
			internalSource(t);
		}
	}
	
	public void setReplace(@NotNull Function<T, T> modify) {
		synchronized (this) {
			internalSource(modify.apply(internalGet()));
		}
	}
	
	//set compute 1
	@FunctionalInterface
	public interface ComputeFunction1<T, I1> {
		
		T compute(I1 i1, @Nullable T previous) throws Delay;
	}
	
	public <I1> Property(@NotNull ReadOnlyProperty<I1> p1, @NotNull ComputeFunction1<T, I1> function) {
		set(p1, function);
	}
	
	public <I1> void set(@NotNull ReadOnlyProperty<I1> p1, @NotNull ComputeFunction1<T, I1> function) {
		final short modId = internalSourceLater();
		PropertyCallback<@Nullable Object> callback = new PropertyCallback<>() {
			@Override
			public void updating() {
				internalUpdatingIncrement(modId);
			}
			
			@Override
			public void accept(@Nullable Object o, boolean hasSentUpdating) {
				internalUpdatingDecrement(modId, hasSentUpdating, () -> function.compute(p1.internalGet(), internalGet()));
			}
		};
		p1.addHook(callback);
		callback.accept(null, true);
	}
	
	//set compute 2
	@FunctionalInterface
	public interface ComputeFunction2<T, I1, I2> {
		
		T compute(I1 i1, I2 i2, @Nullable T previous) throws Delay;
	}
	
	public <I1, I2> Property(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ComputeFunction2<T, I1, I2> function) {
		set(p1, p2, function);
	}
	
	public <I1, I2> void set(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ComputeFunction2<T, I1, I2> function) {
		final short modId = internalSourceLater();
		PropertyCallback<@Nullable Object> callback = new PropertyCallback<>() {
			@Override
			public void updating() {
				internalUpdatingIncrement(modId);
			}
			
			@Override
			public void accept(@Nullable Object o, boolean hasSentUpdating) {
				internalUpdatingDecrement(modId, hasSentUpdating, () -> function.compute(p1.internalGet(), p2.internalGet(), internalGet()));
			}
		};
		p1.addHook(callback);
		p2.addHook(callback);
		callback.accept(null, true);
	}
	
	//set compute 3
	@FunctionalInterface
	public interface ComputeFunction3<T, I1, I2, I3> {
		
		T compute(I1 i1, I2 i2, I3 i3, @Nullable T previous) throws Delay;
	}
	
	public <I1, I2, I3> Property(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ComputeFunction3<T, I1, I2, I3> function) {
		set(p1, p2, p3, function);
	}
	
	public <I1, I2, I3> void set(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ComputeFunction3<T, I1, I2, I3> function) {
		final short modId = internalSourceLater();
		PropertyCallback<@Nullable Object> callback = new PropertyCallback<>() {
			@Override
			public void updating() {
				internalUpdatingIncrement(modId);
			}
			
			@Override
			public void accept(@Nullable Object o, boolean hasSentUpdating) {
				internalUpdatingDecrement(modId, hasSentUpdating, () -> function.compute(p1.internalGet(), p2.internalGet(), p3.internalGet(), internalGet()));
			}
		};
		p1.addHook(callback);
		p2.addHook(callback);
		p3.addHook(callback);
		callback.accept(null, true);
	}
	
	//set compute 4
	@FunctionalInterface
	public interface ComputeFunction4<T, I1, I2, I3, I4> {
		
		T compute(I1 i1, I2 i2, I3 i3, I4 i4, @Nullable T previous) throws Delay;
	}
	
	public <I1, I2, I3, I4> Property(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ReadOnlyProperty<I4> p4, @NotNull ComputeFunction4<T, I1, I2, I3, I4> function) {
		set(p1, p2, p3, p4, function);
	}
	
	public <I1, I2, I3, I4> void set(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ReadOnlyProperty<I4> p4, @NotNull ComputeFunction4<T, I1, I2, I3, I4> function) {
		final short modId = internalSourceLater();
		PropertyCallback<@Nullable Object> callback = new PropertyCallback<>() {
			@Override
			public void updating() {
				internalUpdatingIncrement(modId);
			}
			
			@Override
			public void accept(@Nullable Object o, boolean hasSentUpdating) {
				internalUpdatingDecrement(modId, hasSentUpdating, () -> function.compute(p1.internalGet(), p2.internalGet(), p3.internalGet(), p4.internalGet(), internalGet()));
			}
		};
		p1.addHook(callback);
		p2.addHook(callback);
		p3.addHook(callback);
		p4.addHook(callback);
		callback.accept(null, true);
	}
	
	//set compute 5
	@FunctionalInterface
	public interface ComputeFunction5<T, I1, I2, I3, I4, I5> {
		
		T compute(I1 i1, I2 i2, I3 i3, I4 i4, I5 i5, @Nullable T previous) throws Delay;
	}
	
	public <I1, I2, I3, I4, I5> Property(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ReadOnlyProperty<I4> p4, @NotNull ReadOnlyProperty<I5> p5, @NotNull ComputeFunction5<T, I1, I2, I3, I4, I5> function) {
		set(p1, p2, p3, p4, p5, function);
	}
	
	public <I1, I2, I3, I4, I5> void set(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ReadOnlyProperty<I4> p4, @NotNull ReadOnlyProperty<I5> p5, @NotNull ComputeFunction5<T, I1, I2, I3, I4, I5> function) {
		final short modId = internalSourceLater();
		PropertyCallback<@Nullable Object> callback = new PropertyCallback<>() {
			@Override
			public void updating() {
				internalUpdatingIncrement(modId);
			}
			
			@Override
			public void accept(@Nullable Object o, boolean hasSentUpdating) {
				internalUpdatingDecrement(modId, hasSentUpdating, () -> function.compute(p1.internalGet(), p2.internalGet(), p3.internalGet(), p4.internalGet(), p5.internalGet(), internalGet()));
			}
		};
		p1.addHook(callback);
		p2.addHook(callback);
		p3.addHook(callback);
		p4.addHook(callback);
		p5.addHook(callback);
		callback.accept(null, true);
	}
	
	//set compute 6
	@FunctionalInterface
	public interface ComputeFunction6<T, I1, I2, I3, I4, I5, I6> {
		
		@NotNull T compute(I1 i1, I2 i2, I3 i3, I4 i4, I5 i5, I6 i6, @Nullable T previous) throws Delay;
	}
	
	public <I1, I2, I3, I4, I5, I6> Property(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ReadOnlyProperty<I4> p4, @NotNull ReadOnlyProperty<I5> p5, @NotNull ReadOnlyProperty<I6> p6, @NotNull ComputeFunction6<T, I1, I2, I3, I4, I5, I6> function) {
		set(p1, p2, p3, p4, p5, p6, function);
	}
	
	public <I1, I2, I3, I4, I5, I6> void set(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ReadOnlyProperty<I4> p4, @NotNull ReadOnlyProperty<I5> p5, @NotNull ReadOnlyProperty<I6> p6, @NotNull ComputeFunction6<T, I1, I2, I3, I4, I5, I6> function) {
		final short modId = internalSourceLater();
		PropertyCallback<@Nullable Object> callback = new PropertyCallback<>() {
			@Override
			public void updating() {
				internalUpdatingIncrement(modId);
			}
			
			@Override
			public void accept(@Nullable Object o, boolean hasSentUpdating) {
				internalUpdatingDecrement(modId, hasSentUpdating, () -> function.compute(p1.internalGet(), p2.internalGet(), p3.internalGet(), p4.internalGet(), p5.internalGet(), p6.internalGet(), internalGet()));
			}
		};
		p1.addHook(callback);
		p2.addHook(callback);
		p3.addHook(callback);
		p4.addHook(callback);
		p5.addHook(callback);
		p6.addHook(callback);
		callback.accept(null, true);
	}
	
	//set compute 7
	@FunctionalInterface
	public interface ComputeFunction7<T, I1, I2, I3, I4, I5, I6, I7> {
		
		T compute(I1 i1, I2 i2, I3 i3, I4 i4, I5 i5, I6 i6, I7 i7, @Nullable T previous) throws Delay;
	}
	
	public <I1, I2, I3, I4, I5, I6, I7> Property(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ReadOnlyProperty<I4> p4, @NotNull ReadOnlyProperty<I5> p5, @NotNull ReadOnlyProperty<I6> p6, @NotNull ReadOnlyProperty<I7> p7, @NotNull ComputeFunction7<T, I1, I2, I3, I4, I5, I6, I7> function) {
		set(p1, p2, p3, p4, p5, p6, p7, function);
	}
	
	public <I1, I2, I3, I4, I5, I6, I7> void set(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ReadOnlyProperty<I4> p4, @NotNull ReadOnlyProperty<I5> p5, @NotNull ReadOnlyProperty<I6> p6, @NotNull ReadOnlyProperty<I7> p7, @NotNull ComputeFunction7<T, I1, I2, I3, I4, I5, I6, I7> function) {
		final short modId = internalSourceLater();
		PropertyCallback<@Nullable Object> callback = new PropertyCallback<>() {
			@Override
			public void updating() {
				internalUpdatingIncrement(modId);
			}
			
			@Override
			public void accept(@Nullable Object o, boolean hasSentUpdating) {
				internalUpdatingDecrement(modId, hasSentUpdating, () -> function.compute(p1.internalGet(), p2.internalGet(), p3.internalGet(), p4.internalGet(), p5.internalGet(), p6.internalGet(), p7.internalGet(), internalGet()));
			}
		};
		p1.addHook(callback);
		p2.addHook(callback);
		p3.addHook(callback);
		p4.addHook(callback);
		p5.addHook(callback);
		p6.addHook(callback);
		p7.addHook(callback);
		callback.accept(null, true);
	}
	
	//set compute 8
	@FunctionalInterface
	public interface ComputeFunction8<T, I1, I2, I3, I4, I5, I6, I7, I8> {
		
		T compute(I1 i1, I2 i2, I3 i3, I4 i4, I5 i5, I6 i6, I7 i7, I8 i8, @Nullable T previous) throws Delay;
	}
	
	public <I1, I2, I3, I4, I5, I6, I7, I8> Property(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ReadOnlyProperty<I4> p4, @NotNull ReadOnlyProperty<I5> p5, @NotNull ReadOnlyProperty<I6> p6, @NotNull ReadOnlyProperty<I7> p7, @NotNull ReadOnlyProperty<I8> p8, @NotNull ComputeFunction8<T, I1, I2, I3, I4, I5, I6, I7, I8> function) {
		set(p1, p2, p3, p4, p5, p6, p7, p8, function);
	}
	
	public <I1, I2, I3, I4, I5, I6, I7, I8> void set(@NotNull ReadOnlyProperty<I1> p1, @NotNull ReadOnlyProperty<I2> p2, @NotNull ReadOnlyProperty<I3> p3, @NotNull ReadOnlyProperty<I4> p4, @NotNull ReadOnlyProperty<I5> p5, @NotNull ReadOnlyProperty<I6> p6, @NotNull ReadOnlyProperty<I7> p7, @NotNull ReadOnlyProperty<I8> p8, @NotNull ComputeFunction8<T, I1, I2, I3, I4, I5, I6, I7, I8> function) {
		final short modId = internalSourceLater();
		PropertyCallback<@Nullable Object> callback = new PropertyCallback<>() {
			@Override
			public void updating() {
				internalUpdatingIncrement(modId);
			}
			
			@Override
			public void accept(@Nullable Object o, boolean hasSentUpdating) {
				internalUpdatingDecrement(modId, hasSentUpdating, () -> function.compute(p1.internalGet(), p2.internalGet(), p3.internalGet(), p4.internalGet(), p5.internalGet(), p6.internalGet(), p7.internalGet(), p8.internalGet(), internalGet()));
			}
		};
		p1.addHook(callback);
		p2.addHook(callback);
		p3.addHook(callback);
		p4.addHook(callback);
		p5.addHook(callback);
		p6.addHook(callback);
		p7.addHook(callback);
		p8.addHook(callback);
		callback.accept(null, true);
	}
	
	//set compute more
	@FunctionalInterface
	public interface ComputeFunctionMore<T> {
		
		T compute(Object[] values, @Nullable T previous) throws Delay;
	}
	
	public Property(ReadOnlyProperty<?>[] properties, @NotNull ComputeFunctionMore<T> function) {
		set(properties, function);
	}
	
	public void set(ReadOnlyProperty<?>[] properties, @NotNull ComputeFunctionMore<T> function) {
		final short modId = internalSourceLater();
		PropertyCallback<@Nullable Object> callback = new PropertyCallback<>() {
			@Override
			public void updating() {
				internalUpdatingIncrement(modId);
			}
			
			@Override
			public void accept(@Nullable Object o, boolean hasSentUpdating) {
				Object[] values = Arrays.stream(properties).map(ReadOnlyProperty::internalGet).toArray(Object[]::new);
				internalUpdatingDecrement(modId, hasSentUpdating, () -> function.compute(values, internalGet()));
			}
		};
		Arrays.stream(properties).forEach(p -> p.addHook(callback));
		callback.accept(null, true);
	}
}
