package space.engine.barrier.typehandler;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Delay;

/**
 * A {@link TypeHandler} handles the Execution of FUNCTIONs.
 *
 * @param <FUNCTION> the Function type
 * @see TypeHandlerNoDelay for a version without {@link Delay}
 * @see TypeHandlerParallel if this TypeHandler allows parallel execution.
 */
@FunctionalInterface
public interface TypeHandler<FUNCTION> {
	
	void accept(@NotNull FUNCTION function) throws Delay;
}
