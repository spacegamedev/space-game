package space.engine.stack.multistack;

import org.jetbrains.annotations.Contract;

/**
 * Works <b><i>exactly</i></b> like a stack, you can {@link MultiStack#put(Object)} values on it
 * and use the {@link MultiStack#push()}) and {@link MultiStack#pop()} operations to add more layers and remove them (with their values).
 * You have the option of just {@link MultiStack#push()}-ing and {@link MultiStack#pop()}-ing
 * or you can do a {@link MultiStack#pushPointer()} and {@link MultiStack#popPointer(long)}, if you want to ensure the equilibrium of {@link MultiStack#push()} and {@link MultiStack#pop()}s manually.
 */
public interface MultiStack<T> {
	
	@Contract("null -> null; !null -> !null")
	<X extends T> X put(X t);
	
	void push();
	
	long pushPointer();
	
	void pop();
	
	void popPointer(long pointer);
}