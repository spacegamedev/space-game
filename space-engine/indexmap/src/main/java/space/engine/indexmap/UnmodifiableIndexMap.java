package space.engine.indexmap;

import org.jetbrains.annotations.NotNull;
import space.engine.delegate.collection.ConvertingCollection;
import space.engine.delegate.collection.UnmodifiableCollection;
import space.engine.indexmap.entry.UnmodifiableEntry;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * The {@link UnmodifiableIndexMap} makes the {@link IndexMap} unmodifiable.
 */
public class UnmodifiableIndexMap<VALUE> implements IndexMap<VALUE> {
	
	public IndexMap<VALUE> indexMap;
	
	public UnmodifiableIndexMap(IndexMap<VALUE> indexMap) {
		this.indexMap = indexMap;
	}
	
	//getter
	@Override
	public int size() {
		return indexMap.size();
	}
	
	@Override
	public boolean isEmpty() {
		return indexMap.isEmpty();
	}
	
	@Override
	public boolean contains(int index) {
		return indexMap.contains(index);
	}
	
	@Override
	public VALUE get(int index) {
		return indexMap.get(index);
	}
	
	@Override
	public VALUE[] toArray() {
		return indexMap.toArray();
	}
	
	@Override
	public VALUE[] toArray(@NotNull VALUE[] array) {
		return indexMap.toArray(array);
	}
	
	@Override
	public VALUE getOrDefault(int index, VALUE def) {
		return indexMap.getOrDefault(index, def);
	}
	
	//setter
	@NotNull
	@Override
	public Entry<VALUE> getEntry(int index) {
		return new UnmodifiableEntry<>(indexMap.getEntry(index));
	}
	
	@Override
	public VALUE put(int index, VALUE value) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public VALUE remove(int index) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public void putAll(@NotNull IndexMap<? extends VALUE> indexMap) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public void putAllIfAbsent(@NotNull IndexMap<? extends VALUE> indexMap) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public VALUE putIfAbsent(int index, VALUE value) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public VALUE putIfPresent(int index, VALUE value) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public boolean remove(int index, VALUE value) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public VALUE compute(int index, @NotNull ComputeFunction<? super VALUE, ? extends VALUE> function) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public VALUE computeIfAbsent(int index, @NotNull Supplier<? extends VALUE> supplier) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public boolean replace(int index, VALUE oldValue, VALUE newValue) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public boolean replace(int index, VALUE oldValue, @NotNull Supplier<? extends VALUE> newValue) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public VALUE computeIfPresent(int index, @NotNull Supplier<? extends VALUE> supplier) {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@Override
	public void clear() {
		throw new UnsupportedOperationException("Unmodifiable");
	}
	
	@NotNull
	@Override
	public Collection<VALUE> values() {
		return new UnmodifiableCollection<>(indexMap.values());
	}
	
	@NotNull
	@Override
	public Collection<Entry<VALUE>> entrySet() {
		return new ConvertingCollection.BiDirectionalUnmodifiable<>(indexMap.entrySet(), UnmodifiableEntry::new, entry -> indexMap.getEntry(entry.getIndex()));
	}
}
