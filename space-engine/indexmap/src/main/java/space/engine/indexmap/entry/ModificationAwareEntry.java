package space.engine.indexmap.entry;

import space.engine.indexmap.IndexMap.Entry;

public class ModificationAwareEntry<VALUE> extends DelegatingEntry<VALUE> {
	
	public Entry<VALUE> entry;
	public Runnable onModification;
	
	public ModificationAwareEntry(Entry<VALUE> entry, Runnable onModification) {
		super(entry);
		this.onModification = onModification;
	}
	
	@Override
	public void setValue(VALUE v) {
		entry.setValue(v);
		onModification.run();
	}
}
