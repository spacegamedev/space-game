package space.engine.barrier.timer;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Barrier;
import space.engine.barrier.Delay;

@FunctionalInterface
public interface RepeatingCallback {
	
	@NotNull Barrier start(long currNanos) throws Delay, StopRepeating;
	
	default @NotNull Barrier startInlineException(long currNanos) throws StopRepeating {
		try {
			return start(currNanos);
		} catch (Delay delay) {
			return delay.barrier;
		}
	}
}
