package space.engine.barrier.timer;

import org.jetbrains.annotations.NotNull;
import space.engine.Shutdown;
import space.engine.barrier.Barrier;
import space.engine.barrier.Pool;
import space.engine.barrier.event.EventEntry;
import space.engine.barrier.functions.Starter;
import space.engine.cleaner.CleanerThread;

import java.util.concurrent.TimeUnit;

public class Timer {
	
	public static final BarrierTimer GLOBAL_TIMER;
	public static final EventEntry<Starter<Barrier>> GLOBAL_TIMER_SHUTDOWN_EVENT;
	
	static {
		GLOBAL_TIMER = new BarrierTimer();
		GLOBAL_TIMER_SHUTDOWN_EVENT = Shutdown.SHOWDOWN_EVENT.addHook(
				() -> {
					GLOBAL_TIMER.close();
					return GLOBAL_TIMER.stopBarrier();
				},
				new EventEntry[] {Pool.SHUTDOWN_ENTRY_POOL_STOP},
				new EventEntry[] {CleanerThread.SHUTDOWN_ENTRY_CLEANER_STOP, Shutdown.SHUTDOWN_ENTRY_GLOBAL_SHUTDOWN}
		);
	}
	
	//one time
	public static Barrier in(long time, TimeUnit timeUnit) {
		return GLOBAL_TIMER.in(time, timeUnit);
	}
	
	public static Barrier in(long time, int precision, TimeUnit timeUnit) {
		return GLOBAL_TIMER.in(time, precision, timeUnit);
	}
	
	public static Barrier in(long nanos) {
		return GLOBAL_TIMER.in(nanos);
	}
	
	public static Barrier in(long nanos, int precision) {
		return GLOBAL_TIMER.in(nanos, precision);
	}
	
	public static Barrier at(long time, TimeUnit timeUnit) {
		return GLOBAL_TIMER.at(time, timeUnit);
	}
	
	public static Barrier at(long time, int precision, TimeUnit timeUnit) {
		return GLOBAL_TIMER.at(time, precision, timeUnit);
	}
	
	public static Barrier at(long nanos) {
		return GLOBAL_TIMER.at(nanos);
	}
	
	public static Barrier at(long nanos, int precision) {
		return GLOBAL_TIMER.at(nanos, precision);
	}
	
	//repeating
	public static void repeating(long time, @NotNull TimeUnit timeUnit, @NotNull RepeatingCallback starter) {
		GLOBAL_TIMER.repeating(time, timeUnit, starter);
	}
	
	public static void repeating(long time, @NotNull TimeUnit timeUnit, int bufferedTicks, @NotNull RepeatingCallback starter) {
		GLOBAL_TIMER.repeating(time, timeUnit, bufferedTicks, starter);
	}
	
	public static void repeating(long time, int precision, @NotNull TimeUnit timeUnit, int bufferedTicks, @NotNull RepeatingCallback starter) {
		GLOBAL_TIMER.repeating(time, precision, timeUnit, bufferedTicks, starter);
	}
	
	public static void repeating(long nanos, @NotNull RepeatingCallback starter) {
		GLOBAL_TIMER.repeating(nanos, starter);
	}
	
	public static void repeating(long nanos, int bufferedTicks, @NotNull RepeatingCallback starter) {
		GLOBAL_TIMER.repeating(nanos, bufferedTicks, starter);
	}
	
	public static void repeating(long nanos, int precision, int bufferedTicks, @NotNull RepeatingCallback starter) {
		GLOBAL_TIMER.repeating(nanos, precision, bufferedTicks, starter);
	}
}
