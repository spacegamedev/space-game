package space.engine.barrier.timer;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Barrier;
import space.engine.barrier.BarrierImpl;
import space.engine.barrier.functions.RunnableWithDelay;
import space.engine.cleaner.Cleaner;
import space.engine.cleaner.Resource;
import space.engine.simpleQueue.ConcurrentLinkedSimpleQueue;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.VarHandle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

import static java.lang.Long.max;
import static java.lang.Math.abs;
import static space.engine.barrier.Barrier.done;

public class BarrierTimer implements Resource {
	
	public static final int DEFAULT_PRECISION = 100_000;
	public static final int DEFAULT_BUFFERED_TICKS = 5;
	
	static class TimeBarrier extends BarrierImpl {
		
		public static final Comparator<? super TimeBarrier> COMPARATOR = (o1, o2) -> (int) (o1.time - o2.time);
		
		public final long time;
		public final int precision;
		
		public TimeBarrier(long time, int precision) {
			this.time = time;
			this.precision = precision;
		}
	}
	
	static class DownNode {
		
		public static final Object OBJECT_POLLED = new Object();
		
		static final VarHandle OBJECTARRAY = MethodHandles.arrayElementVarHandle(Object[].class);
		
		final long start;
		final long end;
		final Object[] ref;
		
		public DownNode(long start, long end, int size) {
			long timeDiff = end - start;
			if (timeDiff < size)
				size = (int) timeDiff;
			
			this.start = start;
			this.end = end;
			this.ref = new Object[size];
		}
		
		public final int findIndex(long t) {
			int i = (int) ((t - start) * ref.length / (end - start));
			if (0 <= i && i < ref.length)
				return i;
			throw new IndexOutOfBoundsException("Time " + t + " index " + i);
		}
		
		public final int size() {
			return ref.length;
		}
		
		public Barrier insertDown(@NotNull Internal internal, @NotNull BarrierTimer.TimeBarrier e) {
			int index = findIndex(e.time);
			
			while (true) {
				//slot is empty -> plain insert
				Object prev = OBJECTARRAY.compareAndExchange(ref, index, null, e);
				if (prev == null)
					return e;
				
				//slot is already polled -> time is very close: add fastEvent
				if (prev == OBJECT_POLLED)
					return internal.insertFastEvent(e);
					
					//slot has subnode -> call node
				else if (prev instanceof DownNode) {
					((DownNode) prev).insertDown(internal, e);
					return e;
				}
				
				//slot is occupied by another value -> create node
				else if (prev instanceof TimeBarrier) {
					TimeBarrier prevBarrier = (TimeBarrier) prev;
					//precision == 0 is quite common, so fast path it
					if (e.precision != 0 && abs(e.time - prevBarrier.time) <= e.precision)
						//merge nodes (as they are precise enough)
						return prevBarrier;
					
					//create new node
					long newStart = start + index * (end - start) / ref.length;
					long newEnd = newStart + (end - start) / ref.length;
					DownNode node = new DownNode(newStart, newEnd, ref.length);
					if (node.insertDown(internal, prevBarrier) != prevBarrier || node.insertDown(internal, e) != e)
						throw new RuntimeException("should never happen: precision rounding difference?");
					if (OBJECTARRAY.compareAndSet(ref, index, prev, node))
						return e;
				}
				
				//unhandled type -> should never happen
				else {
					throw new RuntimeException("Unhandled type '" + prev.getClass().getName() + "' encountered: " + prev);
				}
			}
		}
		
		public void pollDown(@NotNull Internal internal) {
			long step = (end - start) / ref.length;
			for (int i = 0; i < ref.length; i++) {
				internal.sleep(start + i * step, DEFAULT_PRECISION);
				Object prev = OBJECTARRAY.getAndSetAcquire(ref, i, OBJECT_POLLED);
				
				//slot is empty -> do nothing
				//noinspection StatementWithEmptyBody
				if (prev == null) {
				
				}
				
				//slot has TimeBarrier -> handle
				else if (prev instanceof TimeBarrier) {
					internal.sleep((TimeBarrier) prev);
				}
				
				//slot has subnode -> call node
				else if (prev instanceof DownNode) {
					((DownNode) prev).pollDown(internal);
				}
				
				//unhandled type -> should never happen
				else {
					throw new RuntimeException("Unhandled type '" + prev.getClass().getName() + "' encountered: " + prev);
				}
			}
		}
	}
	
	static class SideNode extends DownNode {
		
		static final VarHandle NEXT;
		
		static {
			try {
				NEXT = MethodHandles.lookup().findVarHandle(SideNode.class, "next", SideNode.class);
			} catch (NoSuchFieldException | IllegalAccessException e) {
				throw new ExceptionInInitializerError(e);
			}
		}
		
		volatile SideNode next;
		
		public SideNode(long start, long end, int size) {
			super(start, end, size);
		}
		
		public @NotNull BarrierTimer.SideNode getNext() {
			while (true) {
				//get existing node
				SideNode node = (SideNode) NEXT.getAcquire(this);
				if (node != null)
					return node;
				
				//create next node
				node = new SideNode(end, end + end - start, ref.length);
				if (NEXT.weakCompareAndSetRelease(this, null, node))
					return node;
			}
		}
	}
	
	static class Internal extends Cleaner {
		
		private static final VarHandle FIRST;
		private static final VarHandle QUEUED_EVENTS_COUNTER;
		
		static {
			try {
				Lookup lookup = MethodHandles.lookup();
				FIRST = lookup.findVarHandle(Internal.class, "first", SideNode.class);
				QUEUED_EVENTS_COUNTER = lookup.findVarHandle(Internal.class, "queuedEventsCounter", long.class);
			} catch (NoSuchFieldException | IllegalAccessException e) {
				throw new ExceptionInInitializerError(e);
			}
		}
		
		//barrierTimer
		final @NotNull Thread th;
		volatile SideNode first;
		volatile long queuedEventsCounter;
		final @NotNull BarrierImpl stopBarrier = new BarrierImpl();
		
		Internal(BarrierTimer timer, long start, long step, int size, boolean startThread) {
			super(timer);
			FIRST.setRelease(this, new SideNode(start, start + step, size));
			th = new Thread(this::poll, "TimerThread");
			if (startThread)
				th.start();
		}
		
		//insert
		public Barrier insert(long time, int precision) {
			TimeBarrier e = new TimeBarrier(time, precision);
			SideNode first = (SideNode) FIRST.getAcquire(this);
			if (e.time < first.start)
				return done();
			
			SideNode node = first;
			while (e.time >= node.end)
				node = node.getNext();
			Barrier barrier = node.insertDown(this, e);
			if (barrier == e)
				QUEUED_EVENTS_COUNTER.getAndAdd(this, 1);
			return barrier;
		}
		
		//poll
		protected void poll() {
			SideNode node = (SideNode) FIRST.getAcquire(this);
			while (!(freed() && (long) QUEUED_EVENTS_COUNTER.getOpaque(this) == 0)) {
				node.pollDown(this);
				node = node.getNext();
				FIRST.setOpaque(this, node);
			}
			stopBarrier.triggerNow();
		}
		
		@Override
		protected void free() {
			//queried elsewhere via #freed()
		}
		
		//sleep and fastEvent
		private final @NotNull ConcurrentLinkedSimpleQueue<TimeBarrier> fastEventsSubmit = new ConcurrentLinkedSimpleQueue<>();
		private final @NotNull ArrayList<TimeBarrier> fastEventsInternalList = new ArrayList<>();
		
		public Barrier insertFastEvent(TimeBarrier barrier) {
			if (barrier.time < System.nanoTime())
				return done();
			
			fastEventsSubmit.add(barrier);
			th.interrupt();
			return barrier;
		}
		
		void sleep(TimeBarrier barrier) {
			sleep(barrier.time, barrier.precision);
			barrier.triggerNow();
			QUEUED_EVENTS_COUNTER.getAndAddRelease(this, -1);
		}
		
		void sleep(long until, int precision) {
			if (until - System.nanoTime() < 0)
				return;
			
			while (true) {
				try {
					
					//check if new fastEvents arrived
					TimeBarrier fastEventsNew;
					if ((fastEventsNew = fastEventsSubmit.remove()) != null) {
						do
							fastEventsInternalList.add(fastEventsNew);
						while ((fastEventsNew = fastEventsSubmit.remove()) != null);
						fastEventsInternalList.sort(TimeBarrier.COMPARATOR);
					}
					
					//go though fastEvents if any need handling
					int lastIndex;
					while ((lastIndex = fastEventsInternalList.size() - 1) != -1) {
						TimeBarrier fastEventNext = fastEventsInternalList.get(lastIndex);
						if (fastEventNext.time >= until)
							break;
						
						//fastEventNext is closer than sleep requested
						Sleep.sleepUntil(fastEventNext.time, fastEventNext.precision);
						fastEventNext.triggerNow();
						QUEUED_EVENTS_COUNTER.getAndAddRelease(this, -1);
						fastEventsInternalList.remove(lastIndex);
					}
					
					//normal sleep
					Sleep.sleepUntil(until, precision);
					return;
				} catch (InterruptedException ignored) {
					//retry on interrupt, in case a new closest fastEvent poped up
				}
			}
		}
		
		public long statsCurrentQueuedEvents() {
			return (long) QUEUED_EVENTS_COUNTER.getOpaque(this);
		}
	}
	
	final @NotNull Internal internal;
	
	public BarrierTimer() {
		this(1_000_000_000L, 100);
	}
	
	public BarrierTimer(long step, int size) {
		this(System.nanoTime(), step, size, true);
	}
	
	/**
	 * For testing only. Use with startThread = false to test nodes directly.
	 */
	BarrierTimer(long start, long step, int size, boolean startThread) {
		this.internal = new Internal(this, start, step, size, startThread);
	}
	
	@Override
	public @NotNull Cleaner cleaner() {
		return internal;
	}
	
	//in
	public Barrier in(long time, TimeUnit timeUnit) {
		return in(timeUnit.toNanos(time), DEFAULT_PRECISION);
	}
	
	public Barrier in(long time, int precision, TimeUnit timeUnit) {
		return in(timeUnit.toNanos(time), (int) timeUnit.toNanos(precision));
	}
	
	public Barrier in(long nanos) {
		return in(nanos, DEFAULT_PRECISION);
	}
	
	public Barrier in(long nanos, int precision) {
		if (nanos < 0)
			return done();
		return at(System.nanoTime() + nanos, precision);
	}
	
	//at
	public Barrier at(long time, TimeUnit timeUnit) {
		return at(timeUnit.toNanos(time), DEFAULT_PRECISION);
	}
	
	public Barrier at(long time, int precision, TimeUnit timeUnit) {
		return at(timeUnit.toNanos(time), (int) timeUnit.toNanos(precision));
	}
	
	public Barrier at(long nanos) {
		return at(nanos, DEFAULT_PRECISION);
	}
	
	public Barrier at(long nanos, int precision) {
		return internal.insert(nanos, precision);
	}
	
	//repeating
	public void repeating(long time, @NotNull TimeUnit timeUnit, @NotNull RepeatingCallback starter) {
		repeating(timeUnit.toNanos(time), DEFAULT_PRECISION, DEFAULT_BUFFERED_TICKS, starter);
	}
	
	public void repeating(long time, @NotNull TimeUnit timeUnit, int bufferedTicks, @NotNull RepeatingCallback starter) {
		repeating(timeUnit.toNanos(time), DEFAULT_PRECISION, bufferedTicks, starter);
	}
	
	public void repeating(long time, int precision, @NotNull TimeUnit timeUnit, int bufferedTicks, @NotNull RepeatingCallback starter) {
		repeating(timeUnit.toNanos(time), (int) timeUnit.toNanos(precision), bufferedTicks, starter);
	}
	
	public void repeating(long nanos, @NotNull RepeatingCallback starter) {
		repeating(nanos, BarrierTimer.DEFAULT_PRECISION, 5, starter);
	}
	
	public void repeating(long nanos, int bufferedTicks, @NotNull RepeatingCallback starter) {
		repeating(nanos, BarrierTimer.DEFAULT_PRECISION, bufferedTicks, starter);
	}
	
	public void repeating(long nanos, int precision, int bufferedTicks, @NotNull RepeatingCallback starter) {
		new RepeatingSignalHelper(starter, nanos, precision, bufferedTicks);
	}
	
	private class RepeatingSignalHelper implements Runnable, RunnableWithDelay {
		
		private final @NotNull RepeatingCallback starter;
		private final long nanos;
		private final int precision;
		private final long bufferedTime;
		
		private long nextTrigger;
		
		public RepeatingSignalHelper(@NotNull RepeatingCallback starter, long nanos, int precision, int bufferedTicks) {
			this.starter = starter;
			this.nanos = nanos;
			this.precision = precision;
			this.bufferedTime = bufferedTicks * nanos;
			
			nextTrigger = System.nanoTime();
			run();
		}
		
		@Override
		public void run() {
			try {
				starter.startInlineException(nextTrigger).addHook(this::innerRun);
			} catch (StopRepeating ignored) {
			
			}
		}
		
		public void innerRun() {
			long now = System.nanoTime();
			nextTrigger = max(nextTrigger + nanos, now - bufferedTime);
			at(nextTrigger, precision).addHook(this);
		}
	}
	
	//other
	public long statsCurrentQueuedEvents() {
		return internal.statsCurrentQueuedEvents();
	}
	
	public Barrier stopBarrier() {
		return internal.stopBarrier;
	}
}
