package space.engine.barrier.timer;

import java.util.concurrent.locks.LockSupport;

public class Sleep {
	
	/**
	 * The execution time of a {@link System#nanoTime()} call on a Linux machine.
	 * see https://shipilev.net/blog/2014/nanotrusting-nanotime/#_timers
	 */
	public static final long NANOTIME_CALL_AVG_EXECUTION_TIME = 30L;
	
	public static void sleepUntil(long until, @SuppressWarnings("unused") int precision) throws InterruptedException {
		while (true) {
			long nanos = until - NANOTIME_CALL_AVG_EXECUTION_TIME - System.nanoTime();
			if (nanos < 0)
				return;
			LockSupport.parkNanos(nanos);
			if (Thread.interrupted())
				throw new InterruptedException();
		}
	}
}
