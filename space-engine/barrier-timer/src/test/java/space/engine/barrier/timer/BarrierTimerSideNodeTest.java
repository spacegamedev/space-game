package space.engine.barrier.timer;

import org.junit.jupiter.api.Test;
import space.engine.barrier.timer.BarrierTimer.TimeBarrier;

import static org.junit.jupiter.api.Assertions.*;

public class BarrierTimerSideNodeTest {
	
	@Test
	void testSideNode() {
		BarrierTimer timer = new BarrierTimer(0, 8, 4, false);
		BarrierTimer.SideNode node = timer.internal.first;
		
		BarrierTimer.TimeBarrier time4 = (TimeBarrier) timer.at(4, 0);
		assertEquals(4, time4.time);
		assertArrayEquals(new Object[] {null, null, time4, null}, node.ref);
		assertNull(node.next);
		
		BarrierTimer.TimeBarrier time8 = (TimeBarrier) timer.at(8, 0);
		assertEquals(8, time8.time);
		assertArrayEquals(new Object[] {null, null, time4, null}, node.ref);
		assertNotNull(node.next);
		assertArrayEquals(new Object[] {time8, null, null, null}, node.next.ref);
		assertNull(node.next.next);
		
		BarrierTimer.TimeBarrier time11 = (TimeBarrier) timer.at(11, 0);
		assertEquals(11, time11.time);
		assertArrayEquals(new Object[] {null, null, time4, null}, node.ref);
		assertNotNull(node.next);
		assertArrayEquals(new Object[] {time8, time11, null, null}, node.next.ref);
		assertNull(node.next.next);
		
		BarrierTimer.TimeBarrier time16 = (TimeBarrier) timer.at(16, 0);
		assertEquals(16, time16.time);
		assertArrayEquals(new Object[] {null, null, time4, null}, node.ref);
		assertNotNull(node.next);
		assertArrayEquals(new Object[] {time8, time11, null, null}, node.next.ref);
		assertNotNull(node.next.next);
		assertArrayEquals(new Object[] {time16, null, null, null}, node.next.next.ref);
		assertNull(node.next.next.next);
		
		BarrierTimer.TimeBarrier time32 = (TimeBarrier) timer.at(32, 0);
		assertEquals(32, time32.time);
		assertArrayEquals(new Object[] {null, null, time4, null}, node.ref);
		assertNotNull(node.next);
		assertArrayEquals(new Object[] {time8, time11, null, null}, node.next.ref);
		assertNotNull(node.next.next);
		assertArrayEquals(new Object[] {time16, null, null, null}, node.next.next.ref);
		assertNotNull(node.next.next.next);
		assertArrayEquals(new Object[] {null, null, null, null}, node.next.next.next.ref);
		assertNotNull(node.next.next.next.next);
		assertArrayEquals(new Object[] {time32, null, null, null}, node.next.next.next.next.ref);
		assertNull(node.next.next.next.next.next);
		
		assertSame(time32, timer.at(33, 1));
		
		BarrierTimer.TimeBarrier time33 = (TimeBarrier) timer.at(33, 0);
		assertEquals(33, time33.time);
		assertArrayEquals(new Object[] {null, null, time4, null}, node.ref);
		assertNotNull(node.next);
		assertArrayEquals(new Object[] {time8, time11, null, null}, node.next.ref);
		assertNotNull(node.next.next);
		assertArrayEquals(new Object[] {time16, null, null, null}, node.next.next.ref);
		assertNotNull(node.next.next.next);
		assertArrayEquals(new Object[] {null, null, null, null}, node.next.next.next.ref);
		assertNotNull(node.next.next.next.next);
		
		assertNotEquals(time33, node.next.next.next.next.ref[0]); //subnode
		assertNull(node.next.next.next.next.ref[1]);
		assertNull(node.next.next.next.next.ref[2]);
		assertNull(node.next.next.next.next.ref[3]);
		assertNull(node.next.next.next.next.next);
	}
}
