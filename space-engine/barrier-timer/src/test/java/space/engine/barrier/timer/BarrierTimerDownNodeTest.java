package space.engine.barrier.timer;

import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

public class BarrierTimerDownNodeTest {
	
	@Test
	void testIndex() {
		BarrierTimer.DownNode node = new BarrierTimer.DownNode(0, 4, 4);
		assertArrayEquals(IntStream.range(0, 4).toArray(), IntStream.range(0, 4).map(node::findIndex).toArray());
		BarrierTimer.DownNode node2 = new BarrierTimer.DownNode(0, 8, 4);
		assertArrayEquals(IntStream.range(0, 8).map(i -> i / 2).toArray(), IntStream.range(0, 8).map(node2::findIndex).toArray());
	}
	
	@Test
	void testIndexOversize() {
		BarrierTimer.DownNode node = new BarrierTimer.DownNode(0, 4, 42);
		assertEquals(4, node.size(), "size is corrected down to 4");
		int[] actual = IntStream.range(0, 4).map(node::findIndex).toArray();
		assertArrayEquals(IntStream.range(0, 4).toArray(), actual);
	}
	
	@Test
	@SuppressWarnings("ConstantConditions")
	void testInsertNormal() {
		BarrierTimer.DownNode node = new BarrierTimer.DownNode(0, 4, 4);
		
		BarrierTimer.TimeBarrier time0 = new BarrierTimer.TimeBarrier(0, 0);
		assertSame(time0, node.insertDown(null, time0));
		assertArrayEquals(new Object[] {time0, null, null, null}, node.ref);
		
		BarrierTimer.TimeBarrier time1 = new BarrierTimer.TimeBarrier(1, 0);
		assertSame(time1, node.insertDown(null, time1));
		assertArrayEquals(new Object[] {time0, time1, null, null}, node.ref);
		
		BarrierTimer.TimeBarrier time2 = new BarrierTimer.TimeBarrier(2, 0);
		assertSame(time2, node.insertDown(null, time2));
		assertArrayEquals(new Object[] {time0, time1, time2, null}, node.ref);
		
		BarrierTimer.TimeBarrier time3 = new BarrierTimer.TimeBarrier(3, 0);
		assertSame(time3, node.insertDown(null, time3));
		assertArrayEquals(new Object[] {time0, time1, time2, time3}, node.ref);
	}
	
	@Test
	@SuppressWarnings("ConstantConditions")
	void testInsertRounded() {
		BarrierTimer.DownNode node = new BarrierTimer.DownNode(0, 40, 4);
		
		BarrierTimer.TimeBarrier time20 = new BarrierTimer.TimeBarrier(20, 0);
		assertSame(node.insertDown(null, time20), time20);
		assertArrayEquals(new Object[] {null, null, time20, null}, node.ref);
		
		assertSame(time20, node.insertDown(null, new BarrierTimer.TimeBarrier(21, 2)));
		assertArrayEquals(new Object[] {null, null, time20, null}, node.ref);
		
		assertSame(time20, node.insertDown(null, new BarrierTimer.TimeBarrier(22, 2)));
		assertArrayEquals(new Object[] {null, null, time20, null}, node.ref);
		
		BarrierTimer.TimeBarrier time20JustOutside = new BarrierTimer.TimeBarrier(30, 2);
		assertSame(time20JustOutside, node.insertDown(null, time20JustOutside));
		assertNotSame(time20, time20JustOutside);
		assertArrayEquals(new Object[] {null, null, time20, time20JustOutside}, node.ref);
		
		BarrierTimer.TimeBarrier time21Precise = new BarrierTimer.TimeBarrier(21, 0);
		assertSame(time21Precise, node.insertDown(null, time21Precise));
		assertNotSame(time20, time21Precise);
		assertNotSame(time20, node.ref[2]); //new subnode was created here
	}
}
