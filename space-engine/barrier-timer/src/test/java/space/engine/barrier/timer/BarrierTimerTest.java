package space.engine.barrier.timer;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import space.engine.barrier.Barrier;

import java.lang.Thread.State;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static space.engine.barrier.Barrier.done;

public class BarrierTimerTest {
	
	@Test
	void testInPast() {
		BarrierTimer barrierTimer = new BarrierTimer();
		assertSame(done(), barrierTimer.at(0));
		assertSame(done(), barrierTimer.at(100));
	}
	
	@Test
	void testCleanup() throws InterruptedException {
		class Helper {
			
			final @NotNull Thread timerThread;
			final @NotNull Barrier barrier100ms;
			
			public Helper() {
				BarrierTimer timer = new BarrierTimer();
				timerThread = timer.internal.th;
				barrier100ms = timer.in(100, TimeUnit.MILLISECONDS);
				//timer falls out of scope
			}
		}
		
		Helper helper = new Helper();
		for (int i = 0; i < 5; i++) {
			System.gc();
			System.runFinalization();
		}
		
		helper.timerThread.join(2000);
		assertEquals(State.TERMINATED, helper.timerThread.getState());
		assertTrue(helper.barrier100ms.isDone(), "BarrierTimer went out of scope and did not trigger remaining Barrier");
	}
}
