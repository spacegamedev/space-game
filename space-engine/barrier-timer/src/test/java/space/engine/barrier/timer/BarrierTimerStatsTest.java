package space.engine.barrier.timer;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.opentest4j.AssertionFailedError;
import space.engine.barrier.future.Future;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class BarrierTimerStatsTest {
	
	@ParameterizedTest
	@ValueSource(longs = {1000, 10_000, 100_000, 1_000_000})
	void testStats(long sleepFor) {
		BarrierTimer barrierTimer = new BarrierTimer();
		int countWarmup = 100;
		int countMeasure = 100;
		
		long[] allResults = new long[countWarmup + countMeasure];
		for (int i = 0; i < countWarmup + countMeasure; i++) {
			try {
				long sleepUntil = System.nanoTime() + sleepFor;
				Future<Long> finished = barrierTimer.at(sleepUntil).thenStartFuture(System::nanoTime);
				long sleepDiff = finished.awaitGet(2L, TimeUnit.SECONDS) - sleepUntil;
				allResults[i] = sleepDiff;
			} catch (Throwable e) {
				throw new AssertionFailedError("timeout: " + (1L << i), e);
			}
		}
		
		long[] results = Arrays.stream(allResults)
							   .skip(countWarmup)
							   .toArray();
		double avg = Arrays.stream(results).average().orElseThrow();
		double max = Arrays.stream(results).max().orElseThrow();
		double min = Arrays.stream(results).min().orElseThrow();
		
		//output extra stats
		System.out.println(sleepFor + ": avg " + avg + " min " + min + " max " + max);
//		System.out.println(sleepFor + ";" + Arrays.stream(results).mapToObj(Long::toString).collect(Collectors.joining(";")));
		
		//this is too flanky and device dependent to assert about
//		assertTrue(avg < 200_000L, () -> "Average " + (avg / 1_000_000L) + " < 0.2ms");
//		assertTrue(max < 20_000_000L, () -> "Max " + (max / 1_000_000L) + " < 20ms");
	}
}
