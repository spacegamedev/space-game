package space.engine;

import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.Properties;

public class Device {
	
	public static final String OS;
	public static final String USER_DIR;
	
	public static final int POINTER_SIZE;
	public static final boolean BIT64;
	public static final boolean BIT32;
	public static final int PAGE_SIZE;
	
	public static final int CORE_CNT;
	public static final long MEMORY_MAX;
	
	static {
		try {
			Properties properties = System.getProperties();
			Runtime runtime = Runtime.getRuntime();
			
			Field unsafeField = Unsafe.class.getDeclaredField("theUnsafe");
			unsafeField.setAccessible(true);
			Unsafe unsafe = (Unsafe) unsafeField.get(null);
			
			OS = properties.getProperty("sun.desktop");
			USER_DIR = properties.getProperty("user.dir");
			
			POINTER_SIZE = unsafe.addressSize();
			BIT64 = POINTER_SIZE == 8;
			BIT32 = POINTER_SIZE == 4;
			PAGE_SIZE = unsafe.pageSize();
			
			CORE_CNT = runtime.availableProcessors();
			MEMORY_MAX = runtime.maxMemory();
		} catch (IllegalAccessException | NoSuchFieldException e) {
			throw new ExceptionInInitializerError(e);
		}
	}
}
