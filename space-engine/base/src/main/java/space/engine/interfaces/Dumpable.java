package space.engine.interfaces;

import org.jetbrains.annotations.NotNull;

public interface Dumpable {
	
	@NotNull String dump();
}
