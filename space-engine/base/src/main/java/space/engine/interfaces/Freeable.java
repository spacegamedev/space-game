package space.engine.interfaces;

@FunctionalInterface
public interface Freeable extends AutoCloseable {
	
	@Override
	void close();
}
