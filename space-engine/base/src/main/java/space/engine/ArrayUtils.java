package space.engine;

import java.util.function.IntFunction;

public class ArrayUtils {
	
	//optimal size
	public static int getOptimalArraySizeExpansion(int currSize, int requestedSize, int expandShift) {
		int newSizeOld = currSize << expandShift;
		return requestedSize >= newSizeOld ? requestedSize << expandShift : newSizeOld;
	}
	
	public static long getOptimalArraySizeExpansion(long currSize, long requestedSize, long expandShift) {
		long newSizeOld = currSize << expandShift;
		return requestedSize >= newSizeOld ? requestedSize << expandShift : newSizeOld;
	}
	
	public static int getOptimalArraySizeStart(int defaultSize, int requestedSize) {
		return requestedSize == 0 ? 0 : Math.max(requestedSize, defaultSize);
	}
	
	public static long getOptimalArraySizeStart(long defaultSize, long requestedSize) {
		return requestedSize == 0 ? 0 : Math.max(requestedSize, defaultSize);
	}
	
	//merge
	@SuppressWarnings("unchecked")
	public static <T> T[] merge(T[] array1, T[] array2) {
		return merge(length -> (T[]) new Object[length], array1, array2);
	}
	
	public static <T> T[] merge(IntFunction<T[]> arrayCreator, T[] array1, T[] array2) {
		T[] ret = arrayCreator.apply(array1.length + array2.length);
		System.arraycopy(array1, 0, ret, 0, array1.length);
		System.arraycopy(array2, 0, ret, array1.length, array2.length);
		return ret;
	}
	
	@SafeVarargs
	@SuppressWarnings("unchecked")
	public static <T> T[] merge(T[]... arrays) {
		return merge(length -> (T[]) new Object[length], arrays);
	}
	
	@SafeVarargs
	public static <T> T[] merge(IntFunction<T[]> arrayCreator, T[]... arrays) {
		int length = 0;
		for (T[] a : arrays)
			length += a.length;
		
		T[] ret = arrayCreator.apply(length);
		int index = 0;
		for (T[] a : arrays) {
			System.arraycopy(a, 0, ret, index, a.length);
			index += a.length;
		}
		
		return ret;
	}
	
	//mergeIfNeeded
	public static <T> T[] mergeIfNeeded(T[] array1, T[] array2) {
		if (array2.length == 0)
			return array1;
		if (array1.length == 0)
			return array2;
		//noinspection unchecked
		return merge(length -> (T[]) new Object[length], array1, array2);
	}
	
	public static <T> T[] mergeIfNeeded(IntFunction<T[]> arrayCreator, T[] array1, T[] array2) {
		if (array2.length == 0)
			return array1;
		if (array1.length == 0)
			return array2;
		return merge(arrayCreator, array1, array2);
	}
	
	@SafeVarargs
	public static <T> T[] mergeIfNeeded(T[]... arrays) {
		switch (arrays.length) {
			case 0:
				//noinspection unchecked
				return (T[]) new Object[0];
			case 1:
				return arrays[0];
			case 2:
				if (arrays[0].length == 0)
					return arrays[1];
				if (arrays[0].length == 1)
					return arrays[0];
				break;
		}
		//noinspection unchecked
		return mergeIfNeeded(length -> (T[]) new Object[length], arrays);
	}
	
	@SafeVarargs
	public static <T> T[] mergeIfNeeded(IntFunction<T[]> arrayCreator, T[]... arrays) {
		switch (arrays.length) {
			case 0:
				return arrayCreator.apply(0);
			case 1:
				return arrays[0];
			case 2:
				if (arrays[0].length == 0)
					return arrays[1];
				if (arrays[0].length == 1)
					return arrays[0];
				break;
		}
		return merge(arrayCreator, arrays);
	}
}
