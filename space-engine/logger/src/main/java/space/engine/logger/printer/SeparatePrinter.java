package space.engine.logger.printer;

import space.engine.logger.LogLevel;
import space.engine.logger.LogMessage;

import java.util.function.BiConsumer;

public class SeparatePrinter implements BiConsumer<LogMessage, String> {
	
	public BiConsumer<LogMessage, String> out;
	public BiConsumer<LogMessage, String> err;
	public LogLevel minErrLevel;
	
	public SeparatePrinter(BiConsumer<LogMessage, String> out, BiConsumer<LogMessage, String> err) {
		this(out, err, LogLevel.WARNING);
	}
	
	public SeparatePrinter(BiConsumer<LogMessage, String> out, BiConsumer<LogMessage, String> err, LogLevel minErrLevel) {
		this.out = out;
		this.err = err;
		this.minErrLevel = minErrLevel;
	}
	
	@Override
	public void accept(LogMessage logMessage, String msg) {
		if (minErrLevel.allowLog(logMessage.level))
			err.accept(logMessage, msg);
		else
			out.accept(logMessage, msg);
	}
}
