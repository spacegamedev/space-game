package space.engine.logger.printer;

import space.engine.logger.LogMessage;
import space.engine.logger.Logger;

import java.util.function.BiConsumer;

public class PipePrinter implements BiConsumer<LogMessage, String> {
	
	public Logger in;
	
	public PipePrinter(Logger in) {
		this.in = in;
	}
	
	@Override
	public void accept(LogMessage logMessage, String str) {
		in.log(logMessage.level, str);
	}
}
