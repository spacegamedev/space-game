package space.engine.logger;

import org.jetbrains.annotations.NotNull;

public class LogMessage {
	
	public @NotNull Thread thread;
	public @NotNull Logger logger;
	public @NotNull LogLevel level;
	public @NotNull String msg;
	
	public LogMessage(@NotNull Thread thread, @NotNull Logger logger, @NotNull LogLevel level, @NotNull String msg) {
		this.thread = thread;
		this.logger = logger;
		this.level = level;
		this.msg = msg;
	}
}
