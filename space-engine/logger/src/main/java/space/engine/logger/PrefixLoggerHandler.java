package space.engine.logger;

import org.jetbrains.annotations.NotNull;
import space.engine.logger.prefix.Prefix;

import java.util.List;
import java.util.function.BiConsumer;

public class PrefixLoggerHandler implements LoggerHandler {
	
	public static @NotNull PrefixLoggerHandlerBuilder builder() {
		return new PrefixLoggerHandlerBuilder();
	}
	
	private final @NotNull String prefixMessageSeparator;
	private final @NotNull List<Prefix> handler;
	private final @NotNull List<BiConsumer<LogMessage, String>> printer;
	public volatile @NotNull LogLevel minLevel = LogLevel.INFO;
	
	PrefixLoggerHandler(@NotNull String prefixMessageSeparator, @NotNull List<Prefix> handler, @NotNull List<BiConsumer<LogMessage, String>> printer) {
		this.prefixMessageSeparator = prefixMessageSeparator;
		this.handler = handler;
		this.printer = printer;
	}
	
	public void setMinLevel(@NotNull LogLevel minLevel) {
		this.minLevel = minLevel;
	}
	
	@Override
	public void handle(@NotNull LogMessage msg) {
		if (!minLevel.allowLog(msg.level))
			return;
		
		StringBuilder b = new StringBuilder();
		handler.forEach(c -> c.accept(msg, b));
		String str = b + prefixMessageSeparator + msg.msg;
		printer.forEach(c -> c.accept(msg, str));
	}
}
