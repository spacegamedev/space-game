package space.engine.logger.prefix;

import space.engine.logger.LogMessage;
import space.engine.logger.Logger;

public class SubLoggerPrefix extends AbstractPrefix {
	
	public SubLoggerPrefix() {
	}
	
	public SubLoggerPrefix(char start, char end) {
		super(start, end);
	}
	
	@Override
	public void accept(LogMessage logMessage, StringBuilder out) {
		StringBuilder b = new StringBuilder();
		for (Logger logger = logMessage.logger; logger != null; logger = logger.parent)
			b.insert(0, endChar).insert(0, logger.name).insert(0, startChar);
		out.append(b);
	}
}
