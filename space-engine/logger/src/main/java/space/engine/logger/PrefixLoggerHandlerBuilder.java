package space.engine.logger;

import org.jetbrains.annotations.NotNull;
import space.engine.logger.prefix.LogLevelPrefix;
import space.engine.logger.prefix.Prefix;
import space.engine.logger.prefix.SubLoggerPrefix;
import space.engine.logger.prefix.ThreadPrefix;
import space.engine.logger.prefix.TimePrefix;
import space.engine.logger.printer.SeparatePrinter;
import space.engine.logger.printer.SimpleStringPrinter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class PrefixLoggerHandlerBuilder {
	
	public PrefixLoggerHandlerBuilder() {
	}
	
	private @NotNull String prefixMessageSeparator = ": ";
	private final @NotNull List<Prefix> prefixes = new ArrayList<>();
	private final @NotNull List<BiConsumer<LogMessage, String>> printer = new ArrayList<>();
	
	public PrefixLoggerHandlerBuilder setPrefixMessageSeparator(String prefixMessageSeparator) {
		this.prefixMessageSeparator = prefixMessageSeparator;
		return this;
	}
	
	public PrefixLoggerHandler build() {
		return new PrefixLoggerHandler(prefixMessageSeparator, prefixes, printer);
	}
	
	//handler
	public PrefixLoggerHandlerBuilder addPrefix(@NotNull Prefix handler) {
		this.prefixes.add(handler);
		return this;
	}
	
	public PrefixLoggerHandlerBuilder addPrefixTime() {
		return addPrefix(new TimePrefix(new SimpleDateFormat("HH:mm:ss")));
	}
	
	public PrefixLoggerHandlerBuilder addPrefixThread() {
		return addPrefix(new ThreadPrefix());
	}
	
	public PrefixLoggerHandlerBuilder addPrefixLogLevel() {
		return addPrefix(new LogLevelPrefix());
	}
	
	public PrefixLoggerHandlerBuilder addPrefixSubLogger() {
		return addPrefix(new SubLoggerPrefix());
	}
	
	//printer
	public PrefixLoggerHandlerBuilder addPrinter(@NotNull BiConsumer<LogMessage, String> printer) {
		this.printer.add(printer);
		return this;
	}
	
	public PrefixLoggerHandlerBuilder addPrinterOut() {
		return addPrinter(new SimpleStringPrinter(System.out));
	}
	
	public PrefixLoggerHandlerBuilder addPrinterOutAndErr() {
		return addPrinter(new SeparatePrinter(new SimpleStringPrinter(System.out), new SimpleStringPrinter(System.err)));
	}
}
