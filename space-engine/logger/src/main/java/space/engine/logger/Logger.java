package space.engine.logger;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Logger {
	
	//static
	private static volatile @NotNull LoggerHandler defaultHandler =
			PrefixLoggerHandler.builder()
							   .addPrefixTime()
							   .addPrefixLogLevel()
							   .addPrefixThread()
							   .addPrefixSubLogger()
							   .addPrinterOutAndErr()
							   .build();
	
	public static void setDefaultHandler(@NotNull LoggerHandler defaultHandler) {
		Logger.defaultHandler = defaultHandler;
	}
	
	public static @NotNull LoggerHandler getDefaultHandler() {
		return defaultHandler;
	}
	
	public static @NotNull Logger rootLogger(String name) {
		return new Logger(null, name);
	}
	
	//object
	public @Nullable Logger parent;
	public @NotNull String name;
	
	public Logger(@Nullable Logger parent, @NotNull String name) {
		this.parent = parent;
		this.name = name;
	}
	
	public @NotNull Logger subLogger(String name) {
		return new Logger(this, name);
	}
	
	public void log(@NotNull LogLevel level, @NotNull String str) {
		defaultHandler.handle(new LogMessage(Thread.currentThread(), this, level, str));
	}
	
	//log delegate
	public void die(@NotNull String str) {
		log(LogLevel.DIE, str);
	}
	
	public void error(@NotNull String str) {
		log(LogLevel.ERROR, str);
	}
	
	public void warn(@NotNull String str) {
		log(LogLevel.WARNING, str);
	}
	
	public void info(@NotNull String str) {
		log(LogLevel.INFO, str);
	}
	
	public void fine(@NotNull String str) {
		log(LogLevel.FINE, str);
	}
	
	public void finer(@NotNull String str) {
		log(LogLevel.FINER, str);
	}
	
	public void finest(@NotNull String str) {
		log(LogLevel.FINEST, str);
	}
}
