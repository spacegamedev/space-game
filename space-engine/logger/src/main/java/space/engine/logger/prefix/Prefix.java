package space.engine.logger.prefix;

import space.engine.logger.LogMessage;

import java.util.function.BiConsumer;

@FunctionalInterface
public interface Prefix extends BiConsumer<LogMessage, StringBuilder> {
	
}
