package space.engine.logger;

import org.jetbrains.annotations.NotNull;

public interface LoggerHandler {
	
	void handle(@NotNull LogMessage logMessage);
	
	static LoggerHandler nullLoggerHandler() {
		return logMessage -> {};
	}
}
