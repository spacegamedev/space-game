package space.engine.lwjgl;

import org.junit.jupiter.api.Test;
import org.lwjgl.vulkan.VkInstanceCreateInfo;
import org.lwjgl.vulkan.VkInstanceCreateInfo.Buffer;
import space.engine.cleaner.Cleaner;
import space.engine.struct.Allocator;
import space.engine.struct.AllocatorStack.Frame;

import static org.junit.jupiter.api.Assertions.*;
import static space.engine.lwjgl.LwjglStructAllocator.*;

public class LwjglStructAllocatorTest {
	
	@Test
	public void testStructAlloc() {
		try (Frame frame = Allocator.frame()) {
			validateStruct(mallocStruct(frame, VkInstanceCreateInfo::create, VkInstanceCreateInfo.SIZEOF), frame);
			VkInstanceCreateInfo calloc = validateStruct(callocStruct(frame, VkInstanceCreateInfo::create, VkInstanceCreateInfo.SIZEOF), frame);
			assertEquals(calloc.enabledExtensionCount(), 0);
			
			VkInstanceCreateInfo wrap = validateStruct(wrapStruct(VkInstanceCreateInfo::create, calloc.address(), null), Allocator.noop());
			calloc.flags(1);
			assertEquals(wrap.flags(), 1);
		}
	}
	
	private VkInstanceCreateInfo validateStruct(VkInstanceCreateInfo vkInstanceCreateInfo, Allocator allocator) {
		assertNotNull(vkInstanceCreateInfo);
		Object attachment = Attachment.getAttachment(vkInstanceCreateInfo);
		
		Cleaner testCleaner = allocator.cleaner(null, 0);
		if (testCleaner == null) {
			assertNull(attachment);
		} else {
			assertNotNull(attachment);
			assertTrue(attachment instanceof Cleaner);
			assertEquals(attachment.getClass(), testCleaner.getClass());
		}
		return vkInstanceCreateInfo;
	}
	
	@Test
	public void testBufferAlloc() {
		try (Frame frame = Allocator.frame()) {
			validateBuffer(mallocBuffer(frame, VkInstanceCreateInfo::create, VkInstanceCreateInfo.SIZEOF, 5), 5, frame);
			Buffer calloc = validateBuffer(callocBuffer(frame, VkInstanceCreateInfo::create, VkInstanceCreateInfo.SIZEOF, 6), 6, frame);
			assertEquals(calloc.enabledExtensionCount(), 0);
			
			Buffer wrap = validateBuffer(wrapBuffer(VkInstanceCreateInfo::create, calloc.address(), 3, null), 3, Allocator.noop());
			calloc.get(2).flags(1);
			assertEquals(wrap.get(2).flags(), 1);
		}
	}
	
	private Buffer validateBuffer(Buffer buffer, int capacity, Allocator allocator) {
		assertNotNull(buffer);
		assertEquals(buffer.capacity(), capacity);
		Object attachment = Attachment.getAttachment(buffer);
		
		Cleaner testCleaner = allocator.cleaner(null, 0);
		if (testCleaner == null) {
			assertNull(attachment);
		} else {
			assertNotNull(attachment);
			assertTrue(attachment instanceof Cleaner);
			assertEquals(attachment.getClass(), testCleaner.getClass());
		}
		return buffer;
	}
}
