package space.engine.lwjgl;

import org.junit.jupiter.api.Test;
import org.lwjgl.PointerBuffer;
import space.engine.buffer.CPointer;
import space.engine.struct.Allocator;
import space.engine.struct.AllocatorStack.Frame;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PointerBufferWrapperTest {
	
	@Test
	public void testWrapPointerBufferPointer() {
		try (Frame frame = Allocator.frame()) {
			CPointer buffer = CPointer.calloc(frame);
			buffer.setPtr(42);
			assertEquals(buffer.getPtr(), 42);
			
			PointerBuffer wrap = PointerBufferWrapper.wrapPointer(buffer);
			assertEquals(wrap.capacity(), 1);
			assertEquals(wrap.get(0), 42);
		}
	}
	
	@Test
	public void testWrapArrayBufferPointer() {
		try (Frame frame = Allocator.frame()) {
			CPointer.Buffer buffer = CPointer.Buffer.calloc(frame, 5);
			buffer.set(2, 42);
			assertEquals(0, buffer.get(0));
			assertEquals(42, buffer.get(2));
			
			PointerBuffer wrap = PointerBufferWrapper.wrapPointer(buffer);
			assertEquals(wrap.capacity(), 5);
			assertEquals(0, wrap.get(0));
			assertEquals(42, wrap.get(2));
		}
	}
}
