package space.engine.lwjgl;

import org.jetbrains.annotations.Nullable;
import org.lwjgl.system.CustomBuffer;
import org.lwjgl.system.Struct;
import org.lwjgl.system.StructBuffer;
import space.engine.struct.Allocator;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Consumer;

public class LwjglStructAllocator {
	
	//struct
	@FunctionalInterface
	public interface StructCreator<T extends Struct> {
		
		T create(long address);
	}
	
	public static <T extends Struct> T mallocStruct(Allocator allocator, StructCreator<T> create, long sizeOf) {
		long address = allocator.malloc(sizeOf);
		T t = create.create(address);
		Attachment.setAttachment(t, allocator.cleaner(t, address));
		return t;
	}
	
	public static <T extends Struct> T callocStruct(Allocator allocator, StructCreator<T> create, long sizeOf) {
		long address = allocator.calloc(sizeOf);
		T t = create.create(address);
		Attachment.setAttachment(t, allocator.cleaner(t, address));
		return t;
	}
	
	public static <T extends Struct> T createStruct(Allocator allocator, StructCreator<T> create, long address) {
		T t = create.create(address);
		Attachment.setAttachment(t, allocator.cleaner(t, address));
		return t;
	}
	
	public static <T extends Struct> T wrapStruct(StructCreator<T> create, long address, @Nullable Object root) {
		T t = create.create(address);
		Attachment.setAttachment(t, root);
		return t;
	}
	
	//buffer
	@FunctionalInterface
	public interface BufferCreator<T extends CustomBuffer<T>> {
		
		T create(long address, int length);
	}
	
	public static <T extends Struct, B extends StructBuffer<T, B>> B allocBuffer(Allocator allocator, BufferCreator<B> create, long sizeOf, T[] array) {
		B b = mallocBuffer(allocator, create, sizeOf, array.length);
		for (int i = 0; i < array.length; i++)
			b.put(i, array[i]);
		return b;
	}
	
	public static <B extends CustomBuffer<B>> B mallocBuffer(Allocator allocator, BufferCreator<B> create, long sizeOf, int length) {
		long address = allocator.malloc(sizeOf * length);
		B b = create.create(address, length);
		Attachment.setAttachment(b, allocator.cleaner(b, address));
		return b;
	}
	
	public static <B extends CustomBuffer<B>> B callocBuffer(Allocator allocator, BufferCreator<B> create, long sizeOf, int length) {
		long address = allocator.calloc(sizeOf * length);
		B b = create.create(address, length);
		Attachment.setAttachment(b, allocator.cleaner(b, address));
		return b;
	}
	
	public static <B extends CustomBuffer<B>> B createBuffer(Allocator allocator, BufferCreator<B> create, long address, int length) {
		B b = create.create(address, length);
		Attachment.setAttachment(b, allocator.cleaner(b, address));
		return b;
	}
	
	public static <B extends CustomBuffer<B>> B wrapBuffer(BufferCreator<B> create, long address, int length, @Nullable Object root) {
		B b = create.create(address, length);
		Attachment.setAttachment(b, root);
		return b;
	}
	
	//buffer generator
	@SafeVarargs
	public static <T extends Struct, B extends StructBuffer<T, B>> B allocBuffer(Allocator allocator, BufferCreator<B> create, long sizeOf, Consumer<T>... generator) {
		B b = mallocBuffer(allocator, create, sizeOf, generator.length);
		for (int i = 0; i < generator.length; i++)
			generator[i].accept(b.get(i));
		return b;
	}
	
	public static <T extends Struct, B extends StructBuffer<T, B>> B allocBuffer(Allocator allocator, BufferCreator<B> create, long sizeOf, Collection<Consumer<T>> generator) {
		B b = mallocBuffer(allocator, create, sizeOf, generator.size());
		Iterator<Consumer<T>> iter = generator.iterator();
		for (int i = 0; iter.hasNext(); i++)
			iter.next().accept(b.get(i));
		return b;
	}
	
	//buffer wrap
	public static <T extends Struct, B extends StructBuffer<T, B>> B wrapBuffer(BufferCreator<B> create, T struct) {
		B buffer = create.create(struct.address(), 1);
		Attachment.setAttachment(buffer, Attachment.getAttachment(struct));
		return buffer;
	}
}
