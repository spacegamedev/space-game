package space.engine.lwjgl;

import org.lwjgl.PointerBuffer;
import space.engine.Device;
import space.engine.buffer.CPointer;
import space.engine.struct.Struct;

import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class PointerBufferWrapper {
	
	public static PointerBuffer wrapPointer(CPointer.Buffer buffer) {
		return wrapPointer(buffer, 0, buffer.length);
	}
	
	public static PointerBuffer wrapPointer(CPointer buffer) {
		return wrapPointer(buffer, 0, 1);
	}
	
	public static PointerBuffer wrapPointer(Struct buffer, long length) {
		return wrapPointer(buffer, 0, length);
	}
	
	public static PointerBuffer wrapPointer(Struct buffer, long offset, long length) {
		Struct.checkFromIndexSize(offset, length * Device.POINTER_SIZE, buffer.sizeOf());
		int lengthInt = (int) length;
		if (lengthInt != length)
			throw new RuntimeException("Buffer sizeOf " + length + " exceeds 32bit sizeOf limit of DirectBuffer!");
		
		PointerBuffer ret = PointerBuffer.create(buffer.address + offset, lengthInt);
		Attachment.setAttachment(ret, buffer);
		return ret;
	}
	
	public static LongStream streamPointerBuffer(PointerBuffer pb) {
		return IntStream.range(0, pb.capacity()).mapToLong(pb::get);
	}
}
