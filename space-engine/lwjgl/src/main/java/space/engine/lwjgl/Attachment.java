package space.engine.lwjgl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.system.CustomBuffer;
import org.lwjgl.system.Struct;
import space.engine.struct.unsafe.UnsafeInstance;
import sun.misc.Unsafe;

class Attachment {
	
	private static final Unsafe UNSAFE = UnsafeInstance.getUnsafe();
	
	private static final long OFFSET_STRUCT_CONTAINER;
	private static final long OFFSET_CUSTOM_BUFFER_CONTAINER;
	
	static {
		try {
			OFFSET_STRUCT_CONTAINER = UNSAFE.objectFieldOffset(Struct.class.getDeclaredField("container"));
			OFFSET_CUSTOM_BUFFER_CONTAINER = UNSAFE.objectFieldOffset(CustomBuffer.class.getDeclaredField("container"));
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void setAttachment(@NotNull Struct struct, @Nullable Object att) {
		UNSAFE.putObject(struct, OFFSET_STRUCT_CONTAINER, att);
	}
	
	public static void setAttachment(@NotNull CustomBuffer<?> struct, @Nullable Object att) {
		UNSAFE.putObject(struct, OFFSET_CUSTOM_BUFFER_CONTAINER, att);
	}
	
	public static Object getAttachment(@NotNull Struct struct) {
		return UNSAFE.getObject(struct, OFFSET_STRUCT_CONTAINER);
	}
	
	public static Object getAttachment(@NotNull CustomBuffer<?> struct) {
		return UNSAFE.getObject(struct, OFFSET_CUSTOM_BUFFER_CONTAINER);
	}
}
