package space.engine.struct.annotations;

import java.util.function.LongFunction;
import java.util.function.LongToDoubleFunction;
import java.util.function.LongToIntFunction;
import java.util.function.LongUnaryOperator;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * Adds a stream() method to turn the Buffer into a {@link Stream} of Structs.
 * <p>
 * Alternative Options are available to interpret the struct as an {@link IntStream}, {@link LongStream} or {@link DoubleStream}, requiring the struct size to be equal to 4 or 8 depending.
 */
public @interface BufferStream {
	
	StreamType value() default StreamType.STRUCT;
	
	enum StreamType {
		
		STRUCT(Stream.class, Object.class, "mapToObj", LongFunction.class, -1),
		INT(IntStream.class, int.class, "mapToInt", LongToIntFunction.class, 4),
		LONG(LongStream.class, long.class, "map", LongUnaryOperator.class, 8),
		DOUBLE(DoubleStream.class, double.class, "mapToDouble", LongToDoubleFunction.class, 8);
		
		public final Class<?> streamClass;
		public final Class<?> elementClass;
		public final String mapMethodName;
		public final Class<?> mapMethodFunctionType;
		
		//native
		public final int elementSizeOf;
		
		StreamType(Class<?> streamClass, Class<?> elementClass, String mapMethodName, Class<?> mapMethodFunctionType, int elementSizeOf) {
			this.streamClass = streamClass;
			this.elementClass = elementClass;
			this.mapMethodName = mapMethodName;
			this.mapMethodFunctionType = mapMethodFunctionType;
			this.elementSizeOf = elementSizeOf;
		}
	}
}
