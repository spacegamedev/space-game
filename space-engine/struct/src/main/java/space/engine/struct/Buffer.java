package space.engine.struct;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class Buffer extends Struct {
	
	public final long length;
	
	public Buffer(@NotNull Allocator allocator, long address, long length, @Nullable Struct root) {
		super(allocator, address, root);
		this.length = length;
	}
	
	public long length() {
		return length;
	}
}
