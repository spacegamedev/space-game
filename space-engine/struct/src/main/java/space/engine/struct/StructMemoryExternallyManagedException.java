package space.engine.struct;

public class StructMemoryExternallyManagedException extends RuntimeException {
	
	public StructMemoryExternallyManagedException() {
		super("Struct memory is managed by some external object may free at any time independent from the associated Cleaner! eg. Have you used stack allocated memory?");
	}
}
