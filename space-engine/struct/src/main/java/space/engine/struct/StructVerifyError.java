package space.engine.struct;

public class StructVerifyError extends Error {
	
	public StructVerifyError() {
	}
	
	public StructVerifyError(String message) {
		super(message);
	}
	
	public StructVerifyError(String message, Throwable cause) {
		super(message, cause);
	}
	
	public StructVerifyError(Throwable cause) {
		super(cause);
	}
}
