package space.engine.struct.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Adds copy methods to Buffer for array and buffer types:
 * <ul>
 *     <li><code>set(Array[] src)</code></li>
 *     <li><code>set(Array[] src, long srcIndex, long destIndex, long length)</code></li>
 *     <li><code>get(Array[] dest)</code></li>
 *     <li><code>get(long srcIndex, Array[] dest, long destIndex, long length)</code></li>
 * </ul>
 * <p>
 * default: false
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface BufferArrayCopy {
	
	ArrayCopyTypes[] value() default {};
	
	enum ArrayCopyTypes {
		
		BYTE(byte[].class, Byte.BYTES, true),
		SHORT(short[].class, Short.BYTES, true),
		INT(int[].class, Integer.BYTES, true),
		LONG(long[].class, Long.BYTES, true),
		FLOAT(float[].class, Float.BYTES, true),
		DOUBLE(double[].class, Double.BYTES, true),
		CHAR(char[].class, Character.BYTES, true),
		;
		
		public final Class<?> arrayClass;
		public final int componentBytes;
		public final boolean indexIsInt;
		
		ArrayCopyTypes(Class<?> arrayClass, int componentBytes, boolean indexIsInt) {
			this.arrayClass = arrayClass;
			this.componentBytes = componentBytes;
			this.indexIsInt = indexIsInt;
		}
	}
}
