package space.engine.struct.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

/**
 * Adds a nioXBuffer() function to the struct wrapping it into an {@link Buffer nio Buffer}.
 * If sizeOf the struct is not aligned to the Buffer the length is just rounded down.
 * <p>
 * default: {@link NioTypes#GENERIC}
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface NioWrapperMethod {
	
	NioTypes[] value() default NioTypes.GENERIC;
	
	enum NioTypes {
		
		GENERIC(ByteBuffer.class, "nioBuffer", "wrapByte", 1),
		BYTE(ByteBuffer.class, 1),
		SHORT(ShortBuffer.class, 2),
		INT(IntBuffer.class, 4),
		LONG(LongBuffer.class, 8),
		FLOAT(FloatBuffer.class, 4),
		DOUBLE(DoubleBuffer.class, 8),
		CHAR(CharBuffer.class, 2),
		;
		
		public final Class<? extends Buffer> bufferClass;
		public final String methodName;
		public final String wrapName;
		public final int elementSizeOf;
		
		NioTypes(Class<? extends Buffer> bufferClass, String methodName, String wrapName, int elementSizeOf) {
			this.bufferClass = bufferClass;
			this.methodName = methodName;
			this.wrapName = wrapName;
			this.elementSizeOf = elementSizeOf;
		}
		
		NioTypes(Class<? extends Buffer> bufferClass, int elementSizeOf) {
			this(bufferClass, getMethodNameFromClass(bufferClass), getWrapNameFromClass(bufferClass), elementSizeOf);
		}
		
		private static String getMethodNameFromClass(Class<? extends Buffer> bufferClass) {
			String name = bufferClass.getSimpleName();
			return "nio" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
		}
		
		private static String getWrapNameFromClass(Class<? extends Buffer> bufferClass) {
			String name = bufferClass.getSimpleName();
			if (!name.endsWith("Buffer"))
				throw new RuntimeException("Buffer class name " + name + " does not end with Buffer!");
			name = name.substring(0, name.length() - "Buffer".length());
			return "wrap" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
		}
	}
}
