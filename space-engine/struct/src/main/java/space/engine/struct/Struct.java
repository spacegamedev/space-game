package space.engine.struct;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.engine.cleaner.Cleaner;
import space.engine.cleaner.Resource;
import space.engine.interfaces.Dumpable;
import space.engine.struct.Allocator.Noop;
import space.engine.struct.unsafe.UnsafeInstance;
import sun.misc.Unsafe;

public abstract class Struct implements Resource, Dumpable {
	
	//static constants
	protected static final Unsafe UNSAFE = UnsafeInstance.getUnsafe();
	public static final int ADDRESS_SIZE = UNSAFE.addressSize();
	public static final Struct NULL = new Struct(Noop.INSTANCE, 0, null) {
		@Override
		public long sizeOf() {
			return 0;
		}
	};
	
	//object
	public final long address;
	
	/**
	 * The cleaner of a Struct may be null if something else is responsible for cleaning up the memory, like the {@link AllocatorStack Stack} poping or the memory being freed by some external library like {@link Allocator.Noop}.
	 * The cleaner being null also means that one must not depend on that memory keep being allocated and with that should not need to refIncrement the cleaner anyway.
	 */
	private final @Nullable Cleaner cleaner;
	
	/**
	 * This field is used to keep a reference to the Struct on which the cleaner is targeted to prevent the Cleaners WeakReference from freeing.
	 * <p>
	 * The proper way would be to have a separate Cleaner object for each Struct with an effectively free clean method (apart from refDecrement()-ing the parent struct).
	 * But it would require a creation of a WeakReference (over Cleaner) every time which is impossible to inline for JIT and so could hurt performance a decent bit.
	 */
	@SuppressWarnings({"unused", "FieldCanBeLocal"})
	private final @Nullable Struct root;
	
	protected Struct(@NotNull Allocator allocator, long address, @Nullable Struct root) {
		this.address = address;
		this.cleaner = allocator.cleaner(this, address);
		this.root = root;
	}
	
	@Override
	public @NotNull Cleaner cleaner() throws StructMemoryExternallyManagedException {
		if (cleaner == null)
			throw new StructMemoryExternallyManagedException();
		return cleaner;
	}
	
	public abstract long sizeOf();
	
	//static helpers
	
	//sizeof verify
	public static void isSizeOfEqualTo(int sizeOf, int check, String errorStr) {
		if (sizeOf != check)
			throw new StructVerifyError(errorStr);
	}
	
	public static void isSizeOfMultipleOf(int sizeOf, int check, String errorStr) {
		if (sizeOf % check != 0)
			throw new StructVerifyError(errorStr);
	}
	
	//index checks (copied from Object.checkIndex())
	public static long checkIndex(long index, long bufferLength) {
		if (index < 0 || index >= bufferLength)
			throw new IndexOutOfBoundsException(String.format("Index %d out-of-bounds for length %d", index, bufferLength));
		return index;
	}
	
	public static long checkFromToIndex(long fromIndex, long toIndex, long bufferLength) {
		if (fromIndex < 0 || fromIndex > toIndex || toIndex > bufferLength)
			throw new IndexOutOfBoundsException(String.format("Range [%d, %d) out-of-bounds for length %d", fromIndex, toIndex, bufferLength));
		return fromIndex;
	}
	
	public static long checkFromIndexSize(long fromIndex, long size, long bufferLength) {
		if ((bufferLength | fromIndex | size) < 0 || size > bufferLength - fromIndex)
			throw new IndexOutOfBoundsException(String.format("Range [%d, %<d + %d) out-of-bounds for length %d", fromIndex, size, bufferLength));
		return fromIndex;
	}
	
	//dump
	/**
	 * from Integer.DIGITS, but that is package-private...
	 */
	public final static char[] DIGITS = {
			//@formatter:off
			'0', '1', '2', '3', '4', '5', '6', '7', '8',
			'9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
			'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
			'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
			//@formatter:on
	};
	
	@Override
	public @NotNull String dump() {
		return dumpBuffer(address, sizeOf());
	}
	
	public static String dumpBuffer(long address, long length) {
		if (length > (1 << 30))
			return "Buffer too big!";
		int lengthInt = (int) length;
		
		StringBuilder[] b = new StringBuilder[] {new StringBuilder(lengthInt * 3), new StringBuilder(lengthInt * 3)};
		for (int i = 0; i < lengthInt; i++) {
			int pos = i * 3;
			byte d = UNSAFE.getByte(address + i);
			
			if (i % 8 == 0)
				fillUntil(b[0], pos).append(Integer.toHexString(i));
			fillUntil(b[1], pos).append(DIGITS[(d >>> 4) & 0xF]).append(DIGITS[d & 0xF]);
		}
		return b[0].append('\n').append(b[1]).toString();
	}
	
	private static StringBuilder fillUntil(StringBuilder b, int until) {
		int toFill = until - b.length();
		if (toFill > 0)
			b.append(" ".repeat(toFill));
		return b;
	}
}
