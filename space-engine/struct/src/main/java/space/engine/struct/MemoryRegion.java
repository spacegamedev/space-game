package space.engine.struct;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.engine.cleaner.FreedException;
import space.engine.struct.Allocator.Noop;

public class MemoryRegion extends Struct {
	
	/**
	 * Allocates a new {@link MemoryRegion} of length. The Contents are undefined. If the {@link MemoryRegion} is freed, it will free the memory.
	 */
	public static MemoryRegion malloc(Allocator allocator, long length) {
		return new MemoryRegion(allocator, allocator.malloc(length), length, null);
	}
	
	/**
	 * Allocates a new {@link MemoryRegion} of length. The Contents are initialized to 0. If the {@link MemoryRegion} is freed, it will free the memory.
	 */
	public static MemoryRegion calloc(Allocator allocator, long length) {
		return new MemoryRegion(allocator, allocator.calloc(length), length, null);
	}
	
	/**
	 * Creates a new {@link MemoryRegion} from the given address and length. If the {@link MemoryRegion} is freed, it <b>WILL</b> free the memory.
	 */
	public static MemoryRegion create(Allocator allocator, long address, long length) {
		return new MemoryRegion(allocator, address, length, null);
	}
	
	/**
	 * Creates a new {@link MemoryRegion} from the given address and length. It will <b>NEVER</b> free the memory but will still throw {@link FreedException} if it is freed.
	 */
	public static MemoryRegion wrap(long address, long length, @Nullable Struct root) {
		return new MemoryRegion(Noop.INSTANCE, address, length, root);
	}
	
	final long sizeOf;
	
	public MemoryRegion(@NotNull Allocator allocator, long address, long sizeOf, @Nullable Struct root) {
		super(allocator, address, root);
		this.sizeOf = sizeOf;
	}
	
	@Override
	public long sizeOf() {
		return sizeOf;
	}
}
