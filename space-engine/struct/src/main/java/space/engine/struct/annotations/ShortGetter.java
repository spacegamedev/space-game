package space.engine.struct.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Adds an additional getter called $name$() next to the existing get$Name$().
 * <p>
 * default: true
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.CLASS)
public @interface ShortGetter {
	
	boolean value() default true;
}
