package space.engine.struct.annotations;

/**
 * Sets the Layouter for this Struct. The Layouter is responsible to layout which offsets the members of a Struct should have.
 * Not compatible with {@link Union}.
 * <p>
 * Default: OPTIMIZING
 */
public @interface Layouter {
	
	Layouters value();
	
	enum Layouters {
		
		NO_PADDING,
		PADDING,
		OPTIMIZING,
		SINGLE_MEMBER,
	}
}
