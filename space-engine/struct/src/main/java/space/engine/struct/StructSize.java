package space.engine.struct;

/**
 * Declares the size and alignment of the struct. Values MUST be the same as their constant fields.
 * Used to later be able to retrieve that information when other structs use this struct.
 */
public @interface StructSize {
	
	/**
	 * MUST be the same as SIZEOF field.
	 */
	int sizeOf();
	
	/**
	 * MUST be the same as ALIGNMENT field.
	 */
	int alignment();
}
