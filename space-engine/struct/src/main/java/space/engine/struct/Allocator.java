package space.engine.struct;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.engine.cleaner.Cleaner;
import space.engine.struct.unsafe.UnsafeInstance;
import sun.misc.Unsafe;

public interface Allocator {
	
	//object
	long malloc(long sizeOf);
	
	long calloc(long sizeOf);
	
	@Nullable Cleaner cleaner(@Nullable Object front, long address);
	
	static Allocator noop() {
		return Noop.INSTANCE;
	}
	
	static Allocator heap() {
		return Heap.INSTANCE;
	}
	
	static AllocatorStack.Frame frame() {
		return Stack.frame();
	}
	
	//implementations
	enum Noop implements Allocator {
		
		INSTANCE;
		
		@Override
		public long malloc(long sizeOf) {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public long calloc(long sizeOf) {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public @Nullable Cleaner cleaner(@Nullable Object front, long address) {
			return null;
		}
	}
	
	enum Heap implements Allocator {
		
		INSTANCE;
		
		private static final Unsafe UNSAFE = UnsafeInstance.getUnsafe();
		
		@Override
		public long malloc(long sizeOf) {
			return UNSAFE.allocateMemory(sizeOf);
		}
		
		@Override
		public long calloc(long sizeOf) {
			//calloc sadly not supported by Unsafe
			long address = UNSAFE.allocateMemory(sizeOf);
			UNSAFE.setMemory(address, sizeOf, (byte) 0);
			return address;
		}
		
		@Override
		public @NotNull Cleaner cleaner(@Nullable Object front, long address) {
			return new Cleaner(front, address);
		}
		
		public static class Cleaner extends space.engine.cleaner.Cleaner {
			
			final long address;
			
			public Cleaner(@Nullable Object referent, long address) {
				super(referent);
				this.address = address;
			}
			
			@Override
			protected void free() {
				UNSAFE.freeMemory(address);
			}
		}
	}
	
	class Stack {
		
		private static final ThreadLocal<AllocatorStack> ALLOCATOR_STACK = ThreadLocal.withInitial(() -> new AllocatorStack(Heap.INSTANCE, 1024L * 1024));
		
		public static @NotNull AllocatorStack.Frame frame() {
			return ALLOCATOR_STACK.get().frame();
		}
	}
}
