package space.engine.struct.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares this to be a Union, not a Struct.
 * Setting a {@link Layouter} is unsupported on Unions as they have a special Layouter.
 * <p>
 * Default: false
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface Union {

}
