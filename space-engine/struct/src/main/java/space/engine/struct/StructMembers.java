package space.engine.struct;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares member names and types of a generated Struct class.
 * Used to later be able to retrieve that information when other structs use this struct.
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
public @interface StructMembers {
	
	String[] names();
	
	Class<?>[] types();
}
