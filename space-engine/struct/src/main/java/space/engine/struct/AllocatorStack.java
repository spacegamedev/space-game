package space.engine.struct;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.engine.cleaner.Cleaner;
import space.engine.interfaces.Freeable;
import space.engine.struct.unsafe.UnsafeInstance;
import sun.misc.Unsafe;

public class AllocatorStack {
	
	private static final Unsafe UNSAFE = UnsafeInstance.getUnsafe();
	
	protected final Struct buffer;
	protected final long capacity;
	protected @Nullable Frame current;
	
	public AllocatorStack(Allocator allocator, long capacity) {
		this.buffer = MemoryRegion.malloc(allocator, capacity);
		this.capacity = capacity;
	}
	
	public final @NotNull Frame frame() {
		return current = new Frame(current);
	}
	
	public class Frame implements Allocator, Freeable {
		
		protected @Nullable Frame prev;
		protected long pointerStack;
		
		public Frame(@Nullable Frame prev) {
			this.prev = prev;
			this.pointerStack = prev != null ? prev.pointerStack : 0;
		}
		
		//stack
		public boolean isTopFrame() {
			return current == this;
		}
		
		public void assertTopFrame() {
			if (!isTopFrame())
				throw new IllegalStateException("this frame is not the current frame");
		}
		
		@Override
		public void close() {
			assertTopFrame();
			current = prev;
			prev = null;
		}
		
		//allocator
		@Override
		public long malloc(long sizeOf) {
			assertTopFrame();
			long oldPointerStack = pointerStack;
			//align to 8
			pointerStack += (sizeOf + 0x7) & ~0x7;
			if (pointerStack > capacity)
				throw new OutOfMemoryError("Stack overflow!");
			return buffer.address + oldPointerStack;
		}
		
		@Override
		public long calloc(long sizeOf) {
			long address = malloc(sizeOf);
			UNSAFE.setMemory(address, sizeOf, (byte) 0);
			return address;
		}
		
		@Override
		public @Nullable Cleaner cleaner(@Nullable Object front, long address) {
			return null;
		}
	}
}
