package space.engine.struct.annotations;

import space.engine.struct.Struct;

/**
 * Adds a set method which sets all members. Has multiple modes for how this method is generated.
 * If signatures of modes match the later mode is skipped. Supply an empty array to disable.
 * <p>
 * default: all modes (<code>SetAllMethodMode.values()</code>)
 */
public @interface SetAllMethod {
	
	SetAllMethodMode[] value();
	
	/**
	 * Names are a combination of {@link RecursiveMode} and {@link PointerMode}.
	 * <p>
	 * {@link RecursiveMode}:
	 * <ul>
	 *     <li>{@link RecursiveMode#FLAT} just this struct</li>
	 *     <li>{@link RecursiveMode#RECURSIVE} this struct and it's substructs</li>
	 *     <li>{@link RecursiveMode#RECURSIVE_POINTER} this struct, it's substructs and known pointers to other structs</li>
	 * </ul>
	 * <p>
	 * {@link PointerMode}:
	 * <ul>
	 *     <li>{@link PointerMode#STRUCT} pointers are {@link space.engine.struct.Struct} </li>
	 *     <li>{@link PointerMode#ADDRESS} pointers are <code>long</code> aka addresses</li>
	 *     <li>{@link PointerMode#NIO} pointers are NIO {@link java.nio.Buffer}</li>
	 * </ul>
	 * <p>
	 * Default should always be FLAT_STRUCT, in case enum is unknown.
	 */
	enum SetAllMethodMode {
		
		FLAT_STRUCT(RecursiveMode.FLAT, PointerMode.STRUCT),
		FLAT_ADDR(RecursiveMode.FLAT, PointerMode.ADDRESS),
		FLAT_NIO(RecursiveMode.FLAT, PointerMode.NIO),
		RECURSIVE_STRUCT(RecursiveMode.RECURSIVE, PointerMode.STRUCT),
		RECURSIVE_ADDRESS(RecursiveMode.RECURSIVE, PointerMode.ADDRESS),
		RECURSIVE_NIO(RecursiveMode.RECURSIVE, PointerMode.NIO),
		//these three are currently unnecessary but will come in use later once Pointer<E> gets implemented, and not just VoidPointer.
		RECURSIVE_POINTER_STRUCT(RecursiveMode.RECURSIVE_POINTER, PointerMode.STRUCT),
		RECURSIVE_POINTER_ADDRESS(RecursiveMode.RECURSIVE_POINTER, PointerMode.ADDRESS),
		RECURSIVE_POINTER_NIO(RecursiveMode.RECURSIVE_POINTER, PointerMode.NIO);
		
		public final RecursiveMode recursiveMode;
		public final PointerMode pointerMode;
		
		SetAllMethodMode(RecursiveMode recursiveMode, PointerMode pointerMode) {
			this.recursiveMode = recursiveMode;
			this.pointerMode = pointerMode;
		}
	}
	
	/**
	 * @see SetAllMethodMode
	 */
	enum RecursiveMode {
		
		FLAT,
		RECURSIVE,
		RECURSIVE_POINTER
	}
	
	/**
	 * @see SetAllMethodMode
	 */
	enum PointerMode {
		
		STRUCT(Struct.class),
		ADDRESS(long.class),
		NIO(java.nio.Buffer.class);
		
		public final Class<?> clazz;
		
		PointerMode(Class<?> clazz) {
			this.clazz = clazz;
		}
	}
}
