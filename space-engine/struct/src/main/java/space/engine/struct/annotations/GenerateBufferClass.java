package space.engine.struct.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Adds a XXX.Buffer class which is an array of the Struct.
 * <p>
 * default: true
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface GenerateBufferClass {
	
	boolean value() default true;
}
