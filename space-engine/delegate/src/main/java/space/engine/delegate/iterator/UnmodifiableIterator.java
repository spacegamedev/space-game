package space.engine.delegate.iterator;

import java.util.Iterator;

/**
 * The {@link UnmodifiableIterator} makes the {@link Iterator} unmodifiable.
 */
public class UnmodifiableIterator<E> extends DelegatingIterator<E> {
	
	public UnmodifiableIterator(Iterator<E> i) {
		super(i);
	}
	
	@Override
	public void remove() {
		throw new UnsupportedOperationException("Unmodifiable");
	}
}
