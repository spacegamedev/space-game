package space.engine.delegate.iterator;

import space.engine.delegate.util.ReferenceUtil;

import java.lang.ref.Reference;
import java.util.Iterator;

/**
 * Remaps all Entries to a {@link Reference} of type E.<br>
 */
public class ReferenceIterator<E> extends ConvertingIterator.OneDirectional<Reference<? extends E>, E> {
	
	public ReferenceIterator(Iterator<Reference<? extends E>> iter) {
		super(iter, ReferenceUtil::getSafe);
	}
}
