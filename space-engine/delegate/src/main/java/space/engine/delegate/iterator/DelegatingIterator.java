package space.engine.delegate.iterator;

import java.util.Iterator;
import java.util.function.Consumer;

/**
 * An {@link Iterator} delegating all calls to it's Field {@link DelegatingIterator#iter}, supplied with the Constructor.
 */
public class DelegatingIterator<E> implements Iteratorable<E> {
	
	public Iterator<E> iter;
	
	public DelegatingIterator(Iterator<E> iter) {
		this.iter = iter;
	}
	
	//methods
	@Override
	public boolean hasNext() {
		return iter.hasNext();
	}
	
	@Override
	public E next() {
		return iter.next();
	}
	
	@Override
	public void remove() {
		iter.remove();
	}
	
	@Override
	public void forEachRemaining(Consumer<? super E> action) {
		iter.forEachRemaining(action);
	}
	
	//toString
}
