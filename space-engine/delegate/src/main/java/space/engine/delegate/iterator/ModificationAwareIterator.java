package space.engine.delegate.iterator;

import java.util.Iterator;

/**
 * The {@link ModificationAwareIterator} will call the {@link ModificationAwareIterator#onModification} {@link Runnable} when the {@link Iterator} is modified.
 */
public class ModificationAwareIterator<E> extends DelegatingIterator<E> {
	
	public Runnable onModification;
	
	public ModificationAwareIterator(Iterator<E> i, Runnable onModification) {
		super(i);
		this.onModification = onModification;
	}
	
	@Override
	public void remove() {
		super.remove();
		onModification.run();
	}
}
