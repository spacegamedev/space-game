package space.engine.struct.plugin;

import net.bytebuddy.description.field.FieldDescription;
import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.type.TypeDefinition;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import space.engine.struct.Allocator;
import space.engine.struct.Allocator.Noop;
import space.engine.struct.Buffer;
import space.engine.struct.NioBufferWrapper;
import space.engine.struct.Struct;
import space.engine.struct.StructMembers;
import sun.misc.Unsafe;

import java.lang.invoke.LambdaMetafactory;
import java.util.stream.LongStream;

import static net.bytebuddy.matcher.ElementMatchers.*;

public class GlobalSymbols {
	
	public static final FieldDescription.InDefinedShape GLOBAL_STRUCT_ADDRESS_SIZE = ForLoadedType.of(Struct.class).getDeclaredFields().filter(named("ADDRESS_SIZE").and(isStatic()).and(isFinal())).getOnly();
	public static final FieldDescription.InDefinedShape GLOBAL_STRUCT_UNSAFE = ForLoadedType.of(Struct.class).getDeclaredFields().filter(named("UNSAFE").and(fieldType(Unsafe.class)).and(isStatic()).and(isFinal())).getOnly();
	public static final FieldDescription.InDefinedShape GLOBAL_STRUCT_ADDRESS = ForLoadedType.of(Struct.class).getDeclaredFields().filter(named("address").and(fieldType(long.class)).and(not(isStatic())).and(isFinal())).getOnly();
	public static final MethodDescription.InDefinedShape GLOBAL_STRUCT_CONSTRUCTOR = ForLoadedType.of(Struct.class).getDeclaredMethods().filter(isConstructor()).getOnly();
	
	public static final MethodDescription.InDefinedShape GLOBAL_STRUCT_CHECK_INDEX = ForLoadedType.of(Struct.class).getDeclaredMethods().filter(named("checkIndex")).getOnly();
	public static final MethodDescription.InDefinedShape GLOBAL_STRUCT_CHECK_FROM_TO_INDEX = ForLoadedType.of(Struct.class).getDeclaredMethods().filter(named("checkFromToIndex")).getOnly();
	public static final MethodDescription.InDefinedShape GLOBAL_STRUCT_CHECK_FROM_INDEX_SIZE = ForLoadedType.of(Struct.class).getDeclaredMethods().filter(named("checkFromIndexSize")).getOnly();
	
	public static final MethodDescription.InDefinedShape GLOBAL_BUFFER_CONSTRUCTOR = ForLoadedType.of(Buffer.class).getDeclaredMethods().filter(isConstructor()).getOnly();
	public static final FieldDescription.InDefinedShape GLOBAL_BUFFER_LENGTH = ForLoadedType.of(Buffer.class).getDeclaredFields().filter(named("length")).getOnly();
	
	public static final MethodDescription.InDefinedShape GLOBAL_ALLOCATOR_MALLOC = ForLoadedType.of(Allocator.class).getDeclaredMethods().filter(named("malloc")).getOnly();
	public static final MethodDescription.InDefinedShape GLOBAL_ALLOCATOR_CALLOC = ForLoadedType.of(Allocator.class).getDeclaredMethods().filter(named("calloc")).getOnly();
	public static final FieldDescription.InDefinedShape GLOBAL_ALLOCATOR_NOOP_INSTANCE = ForLoadedType.of(Noop.class).getDeclaredFields().filter(named("INSTANCE")).getOnly();
	
	public static final MethodDescription.InDefinedShape GLOBAL_STRUCT_MEMBERS_NAMES = ForLoadedType.of(StructMembers.class).getDeclaredMethods().filter(named("names")).getOnly();
	public static final MethodDescription.InDefinedShape GLOBAL_STRUCT_MEMBERS_TYPES = ForLoadedType.of(StructMembers.class).getDeclaredMethods().filter(named("types")).getOnly();
	
	public static final MethodDescription.InDefinedShape GLOBAL_NIO_GET_ADDRESS = ForLoadedType.of(NioBufferWrapper.class).getDeclaredMethods().filter(named("getAddress").and(returns(long.class)).and(takesArguments(java.nio.Buffer.class))).getOnly();
	public static final MethodDescription.InDefinedShape GLOBAL_NIO_GET_LENGTH = ForLoadedType.of(NioBufferWrapper.class).getDeclaredMethods().filter(named("getLength").and(returns(long.class)).and(takesArguments(java.nio.Buffer.class))).getOnly();
	
	public static final MethodDescription.InDefinedShape GLOBAL_MATH_MAX = ForLoadedType.of(Math.class).getDeclaredMethods().filter(named("max").and(returns(int.class)).and(takesArguments(int.class, int.class))).getOnly();
	
	public static final MethodDescription.InDefinedShape GLOBAL_UNSAFE_COPY_MEMORY = ForLoadedType.of(Unsafe.class).getDeclaredMethods().filter(
			named("copyMemory").and(returns(void.class)).and(takesArguments(long.class, long.class, long.class))).getOnly();
	public static final MethodDescription.InDefinedShape GLOBAL_UNSAFE_COPY_MEMORY_OBJECT = ForLoadedType.of(Unsafe.class).getDeclaredMethods().filter(
			named("copyMemory").and(returns(void.class)).and(takesArguments(Object.class, long.class, Object.class, long.class, long.class))).getOnly();
	public static final MethodDescription.InDefinedShape GLOBAL_UNSAFE_ADDRESS_GET = ForLoadedType.of(Unsafe.class).getDeclaredMethods().filter(named("getAddress").and(returns(long.class)).and(takesArguments(long.class))).getOnly();
	public static final MethodDescription.InDefinedShape GLOBAL_UNSAFE_ADDRESS_SET = ForLoadedType.of(Unsafe.class).getDeclaredMethods().filter(named("putAddress").and(returns(void.class)).and(takesArguments(long.class, long.class))).getOnly();
	
	public static final MethodDescription.InDefinedShape GLOBAL_LAMBDAMETAFACTORY_METAFACTORY = ForLoadedType.of(LambdaMetafactory.class).getDeclaredMethods().filter(named("metafactory")).getOnly();
	
	public static final MethodDescription.InDefinedShape GLOBAL_LONGSTREAM_RANGE = ForLoadedType.of(LongStream.class).getDeclaredMethods().filter(named("range").and(isStatic()).and(takesArguments(long.class, long.class))).getOnly();
	
	public static MethodDescription unsafeAccessorGet(TypeDefinition primitive) {
		return ForLoadedType.of(Unsafe.class).getDeclaredMethods().filter(
				named("get" + Character.toUpperCase(primitive.getActualName().charAt(0)) + primitive.getActualName().substring(1))
						.and(takesArguments(long.class))
						.and(returns(primitive.asErasure()))
		).getOnly();
	}
	
	public static MethodDescription unsafeAccessorSet(TypeDefinition primitive) {
		return ForLoadedType.of(Unsafe.class).getDeclaredMethods().filter(
				named("put" + Character.toUpperCase(primitive.getActualName().charAt(0)) + primitive.getActualName().substring(1))
						.and(takesArguments(ForLoadedType.of(long.class), primitive.asErasure()))
						.and(returns(TypeDescription.VOID))
		).getOnly();
	}
	
	public static FieldDescription unsafeArrayBaseOffset(TypeDefinition primitive) {
		return unsafeArrayConstant(primitive, "_BASE_OFFSET");
	}
	
	public static FieldDescription unsafeArrayIndexScale(TypeDefinition primitive) {
		return unsafeArrayConstant(primitive, "_INDEX_SCALE");
	}
	
	private static FieldDescription unsafeArrayConstant(TypeDefinition primitive, String appendix) {
		String name = "ARRAY_" + (primitive.isPrimitive() ? primitive.getActualName().toUpperCase() : "OBJECT") + appendix;
		return ForLoadedType.of(Unsafe.class).getDeclaredFields().filter(named(name)).getOnly();
	}
}
