package space.engine.struct.plugin.layouter;

import space.engine.struct.plugin.layouter.Layout.LayoutMember;
import space.engine.struct.plugin.layouter.LayouterValidator.Options;
import space.engine.struct.plugin.types.Member;
import space.engine.struct.plugin.types.StructType;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * This Layouter does some basic padding, but does not rearrange any entries.
 */
public class PaddingLayouter extends AbstractLayouter {
	
	@Override
	protected EnumSet<Options> validationOptions() {
		return EnumSet.of(Options.ALLOW_SIZE_0);
	}
	
	@Override
	protected Layout computeLayoutNoValidation(StructType struct) {
		List<LayoutMember> members = new ArrayList<>();
		int offset = 0;
		int structAlignment = 0;
		for (Member m : struct.members()) {
			int align = m.alignment();
			//add padding to next member
			offset = (offset + align - 1) & -align;
			members.add(new LayoutMember(m, offset));
			offset += m.sizeOf();
			structAlignment = Math.max(structAlignment, align);
		}
		return new Layout(struct, members, offset, structAlignment);
	}
}
