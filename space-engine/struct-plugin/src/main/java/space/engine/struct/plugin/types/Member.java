package space.engine.struct.plugin.types;

import net.bytebuddy.description.annotation.AnnotationList;
import net.bytebuddy.description.field.FieldDescription;
import net.bytebuddy.description.field.FieldDescription.InDefinedShape;
import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.method.ParameterDescription.Token;
import net.bytebuddy.description.modifier.ModifierContributor.ForMethod;
import net.bytebuddy.description.modifier.ModifierContributor.Resolver;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import net.bytebuddy.description.type.TypeDescription.Generic;
import net.bytebuddy.implementation.Implementation;
import net.bytebuddy.implementation.bytecode.Addition;
import net.bytebuddy.implementation.bytecode.StackManipulation;
import net.bytebuddy.implementation.bytecode.StackManipulation.Compound;
import net.bytebuddy.implementation.bytecode.assign.primitive.PrimitiveWideningDelegate;
import net.bytebuddy.implementation.bytecode.member.MethodReturn;
import net.bytebuddy.implementation.bytecode.member.MethodVariableAccess;
import org.jetbrains.annotations.NotNull;
import space.engine.plugin.SimpleImplementation;
import space.engine.plugin.SimpleImplementation.Code;
import space.engine.struct.Struct;
import space.engine.struct.VoidPointer;
import space.engine.struct.annotations.SetAllMethod.SetAllMethodMode;
import space.engine.struct.plugin.builder.StructBuilder;

import java.nio.Buffer;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.bytebuddy.description.modifier.Visibility.PUBLIC;
import static net.bytebuddy.implementation.bytecode.member.FieldAccess.forField;
import static net.bytebuddy.implementation.bytecode.member.MethodInvocation.invoke;
import static net.bytebuddy.implementation.bytecode.member.MethodVariableAccess.*;
import static space.engine.struct.plugin.GlobalSymbols.*;

public interface Member {
	
	@NotNull String name();
	
	@NotNull TypeDescription.Generic type();
	
	@NotNull FieldDescription.InDefinedShape offsetConstant();
	
	@NotNull AnnotationList annotations();
	
	@NotNull MemberSymbols symbols();
	
	int sizeOf();
	
	int alignment();
	
	class GetterSetterInfo<F> {
		
		public final @NotNull String name;
		public final @NotNull Generic type;
		public final @NotNull F code;
		
		public GetterSetterInfo(@NotNull String name, @NotNull Generic type, @NotNull F code) {
			this.name = name;
			this.type = type;
			this.code = code;
		}
	}
	
	class GetterInfo extends GetterSetterInfo<Function<Code, Implementation>> {
		
		public GetterInfo(@NotNull String name, @NotNull Generic type, @NotNull Function<Code, Implementation> code) {
			super(name, type, code);
		}
	}
	
	class SetterInfo extends GetterSetterInfo<BiFunction<Code, Integer, Implementation>> {
		
		public SetterInfo(@NotNull String name, @NotNull Generic type, @NotNull BiFunction<Code, Integer, Implementation> code) {
			super(name, type, code);
		}
	}
	
	@NotNull List<GetterInfo> codeGetter();
	
	@NotNull List<SetterInfo> codeSetter();
	
	class SetMethodEntry {
		
		public final @NotNull List<Token> params;
		public final @NotNull SetMethodCode code;
		
		public SetMethodEntry(@NotNull List<Token> params, @NotNull SetMethodCode code) {
			this.params = params;
			this.code = code;
		}
	}
	
	@FunctionalInterface
	interface SetMethodCode {
		
		/**
		 * generate code for setter
		 *
		 * @param paramSlots      splots from where to load from, same order as {@link SetMethodEntry#params}
		 * @param offsetConstants additional offsets to be added to struct address and member offsets, used only for recursive Struct member setters
		 * @param method          method generated
		 * @return code
		 */
		@NotNull StackManipulation.Compound generateCode(int[] paramSlots, @NotNull List<InDefinedShape> offsetConstants, @NotNull MethodDescription method);
	}
	
	SetMethodEntry setMethodEntry(SetAllMethodMode mode);
	
	static Member of(StructType parent, Generic type, String name, AnnotationList annotations) {
		if (type.isPrimitive())
			return new ForPrimitive(parent, type, name, annotations);
		if (type.asErasure().isAssignableTo(VoidPointer.class))
			return new ForVoidPointer(parent, type, name, annotations);
		return new ForStruct(parent, type, name, annotations);
	}
	
	abstract class AbstractMember implements Member {
		
		protected final StructType parent;
		protected final Generic type;
		protected final String name;
		protected final AnnotationList annotations;
		
		public AbstractMember(StructType parent, Generic type, String name, AnnotationList annotations) {
			this.parent = parent;
			this.type = type;
			this.name = name;
			this.annotations = annotations;
		}
		
		@Override
		public @NotNull String name() {
			return name;
		}
		
		@Override
		public @NotNull Generic type() {
			return type;
		}
		
		@Override
		public @NotNull InDefinedShape offsetConstant() {
			return parent.constantSymbols().offset(this);
		}
		
		@Override
		public @NotNull AnnotationList annotations() {
			return annotations;
		}
		
		@Override
		public String toString() {
			return type + " " + name;
		}
		
		@Override
		public @NotNull MemberSymbols symbols() {
			return new AbstractMemberSymbols();
		}
		
		class AbstractMemberSymbols implements MemberSymbols {
			
			@Override
			public MethodDescription.InDefinedShape getter(String name, Generic ret) {
				return new MethodDescription.Latent(
						parent.struct(),
						"get" + Character.toUpperCase(name.charAt(0)) + name.substring(1),
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						ret,
						List.of(),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape getterShort(String name, Generic ret) {
				return new MethodDescription.Latent(
						parent.struct(),
						Character.toLowerCase(name.charAt(0)) + name.substring(1),
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						ret,
						List.of(),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape justGet(String name, Generic ret) {
				return new MethodDescription.Latent(
						parent.struct(),
						"get",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						ret,
						List.of(),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape setter(String name, Generic param) {
				return new MethodDescription.Latent(
						parent.struct(),
						"set" + Character.toUpperCase(name.charAt(0)) + name.substring(1),
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						TypeDescription.VOID.asGenericType(),
						List.of(new Token(param, name, null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
		}
	}
	
	class ForPrimitive extends AbstractMember {
		
		final int sizeOf;
		
		public ForPrimitive(StructType parent, Generic type, String name, AnnotationList annotations) {
			super(parent, type, name, annotations);
			
			if (ForLoadedType.of(char.class).equals(type.asErasure()))
				throw new RuntimeException("char not supported!");
			if (ForLoadedType.of(void.class).equals(type.asErasure()))
				throw new RuntimeException("void is not a valid field type!");
			
			if (ForLoadedType.of(byte.class).equals(type.asErasure())) {
				this.sizeOf = 1;
			} else if (ForLoadedType.of(short.class).equals(type.asErasure())) {
				this.sizeOf = 2;
			} else if (ForLoadedType.of(int.class).equals(type.asErasure())) {
				this.sizeOf = 4;
			} else if (ForLoadedType.of(long.class).equals(type.asErasure())) {
				this.sizeOf = 8;
			} else if (ForLoadedType.of(float.class).equals(type.asErasure())) {
				this.sizeOf = 4;
			} else if (ForLoadedType.of(double.class).equals(type.asErasure())) {
				this.sizeOf = 8;
			} else if (ForLoadedType.of(boolean.class).equals(type.asErasure())) {
				this.sizeOf = 1;
			} else {
				throw new RuntimeException("Unknown primitive type " + type.asErasure().getName() + "!");
			}
		}
		
		@Override
		public int sizeOf() {
			return sizeOf;
		}
		
		@Override
		public int alignment() {
			return sizeOf;
		}
		
		@Override
		public @NotNull List<GetterInfo> codeGetter() {
			return List.of(
					new GetterInfo(name, type, getAddressCode -> new SimpleImplementation(method -> new Compound(
							forField(GLOBAL_STRUCT_UNSAFE).read(),
							getAddressCode.apply(method),
							forField(offsetConstant()).read(),
							PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
							Addition.LONG,
							invoke(unsafeAccessorGet(type)),
							MethodReturn.of(type)
					)))
			);
		}
		
		@Override
		public @NotNull List<SetterInfo> codeSetter() {
			return List.of(
					new SetterInfo(name, type, (getAddressCode, paramSlot) -> new SimpleImplementation(method -> new Compound(
							codeSetter(getAddressCode, paramSlot, List.of()).apply(method),
							MethodReturn.VOID
					)))
			);
		}
		
		private Code codeSetter(Code getAddressCode, int paramSlot, List<InDefinedShape> offsetConstants) {
			return method -> new Compound(
					forField(GLOBAL_STRUCT_UNSAFE).read(),
					getAddressCode.apply(method),
					new Compound(Stream.concat(offsetConstants.stream(), Stream.of(offsetConstant()))
									   .map(offset -> new Compound(
											   forField(offset).read(),
											   PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
											   Addition.LONG
									   ))
									   .collect(Collectors.toList())
					),
					MethodVariableAccess.of(type).loadFrom(paramSlot),
					invoke(unsafeAccessorSet(type))
			);
		}
		
		@Override
		public SetMethodEntry setMethodEntry(SetAllMethodMode mode) {
			return new SetMethodEntry(
					List.of(new Token(type, name, null)),
					(paramSlots, offsetConstants, methodDescription) -> {
						if (paramSlots.length != 1)
							throw new RuntimeException("Slot count " + paramSlots.length + " has to be 1!");
						return codeSetter(StructBuilder.codeGetAddressStruct(), paramSlots[0], offsetConstants).apply(methodDescription);
					}
			);
		}
	}
	
	class ForStruct extends AbstractMember {
		
		protected final StructType structType;
		
		public ForStruct(StructType parent, Generic type, String name, AnnotationList annotations) {
			super(parent, type, name, annotations);
			this.structType = StructType.of(type.asErasure());
		}
		
		@Override
		public int sizeOf() {
			return structType.size().sizeOf();
		}
		
		@Override
		public int alignment() {
			return structType.size().alignment();
		}
		
		@Override
		public @NotNull List<GetterInfo> codeGetter() {
			return List.of(
					new GetterInfo(name, type, getAddressCode -> new SimpleImplementation(method -> new Compound(
							getAddressCode.apply(method),
							forField(offsetConstant()).read(),
							PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
							Addition.LONG,
							loadThis(),
							invoke(structType.structSymbols().wrap()),
							MethodReturn.REFERENCE
					)))
			);
		}
		
		@Override
		public @NotNull List<SetterInfo> codeSetter() {
			return List.of(
					new SetterInfo(name, type, (getAddressCode, paramSlot) -> new SimpleImplementation(method -> new Compound(
							codeSetter(paramSlot, List.of()),
							MethodReturn.VOID
					)))
			);
		}
		
		private Compound codeSetter(int paramSlot, List<InDefinedShape> offsetConstants) {
			return new Compound(
					loadThis(),
					forField(GLOBAL_STRUCT_ADDRESS).read(),
					new Compound(Stream.concat(offsetConstants.stream(), Stream.of(offsetConstant()))
									   .map(offset -> new Compound(
											   forField(offset).read(),
											   PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
											   Addition.LONG
									   ))
									   .collect(Collectors.toList())
					),
					loadThis(),
					invoke(structType.structSymbols().wrap()),
					REFERENCE.loadFrom(paramSlot),
					invoke(structType.structSymbols().setterCopy()));
		}
		
		@Override
		public SetMethodEntry setMethodEntry(SetAllMethodMode mode) {
			switch (mode.recursiveMode) {
				case FLAT:
				default:
					return new SetMethodEntry(
							List.of(new Token(type, name, null)),
							(paramSlots, offsetConstants, methodDescription) -> {
								if (paramSlots.length != 1)
									throw new RuntimeException("Slot count " + paramSlots.length + " has to be 1!");
								return codeSetter(paramSlots[0], offsetConstants);
							}
					);
				case RECURSIVE:
				case RECURSIVE_POINTER:
					List<SetMethodEntry> setMethodEntries = structType.members().stream().map(m -> m.setMethodEntry(mode)).collect(Collectors.toList());
					return new SetMethodEntry(
							setMethodEntries.stream().flatMap(e -> e.params.stream()).collect(Collectors.toList()),
							(paramSlots, offsetConstants, methodDescription) -> {
								int offset = 0;
								Compound ret = new Compound();
								for (SetMethodEntry e : setMethodEntries) {
									int paramSize = e.params.size();
									ret = new Compound(ret, e.code.generateCode(
											Arrays.copyOfRange(paramSlots, offset, offset + paramSize),
											Stream.concat(offsetConstants.stream(), Stream.of(offsetConstant())).collect(Collectors.toList()),
											methodDescription
									));
									offset += paramSize;
								}
								return ret;
							}
					);
			}
		}
	}
	
	class ForVoidPointer extends AbstractMember {
		
		public ForVoidPointer(StructType parent, Generic type, String name, AnnotationList annotations) {
			super(parent, type, name, annotations);
		}
		
		//currently 64-bit only
		@Override
		public int sizeOf() {
			return 8;
		}
		
		@Override
		public int alignment() {
			return 8;
		}
		
		@Override
		public @NotNull List<GetterInfo> codeGetter() {
			return List.of(
					new GetterInfo(name, ForLoadedType.of(long.class).asGenericType(), getAddressCode -> new SimpleImplementation(method -> new Compound(
							forField(GLOBAL_STRUCT_UNSAFE).read(),
							getAddressCode.apply(method),
							forField(offsetConstant()).read(),
							PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
							Addition.LONG,
							invoke(GLOBAL_UNSAFE_ADDRESS_GET),
							MethodReturn.LONG
					)))
			);
		}
		
		@Override
		public @NotNull List<SetterInfo> codeSetter() {
			BiFunction<Code, Integer, Implementation> code = (getAddressCode, paramSlot) -> new SimpleImplementation(method -> new Compound(
					codeSetter(method.getParameters().get(0).getType().asErasure(), 1, List.of()),
					MethodReturn.VOID
			));
			return List.of(
					new SetterInfo(name, ForLoadedType.of(long.class).asGenericType(), code),
					new SetterInfo(name, ForLoadedType.of(Struct.class).asGenericType(), code),
					new SetterInfo(name, ForLoadedType.of(java.nio.Buffer.class).asGenericType(), code)
			);
		}
		
		private Compound codeSetter(TypeDescription paramType, int paramSlot, List<InDefinedShape> offsetConstants) {
			StackManipulation loadParam;
			if (paramType.asErasure().equals(ForLoadedType.of(long.class))) {
				loadParam = LONG.loadFrom(paramSlot);
			} else if (paramType.asErasure().equals(ForLoadedType.of(Struct.class))) {
				loadParam = new Compound(
						REFERENCE.loadFrom(paramSlot),
						forField(GLOBAL_STRUCT_ADDRESS).read()
				);
			} else if (paramType.asErasure().equals(ForLoadedType.of(Buffer.class))) {
				loadParam = new Compound(
						REFERENCE.loadFrom(paramSlot),
						invoke(GLOBAL_NIO_GET_ADDRESS)
				);
			} else {
				throw new RuntimeException();
			}
			
			return new Compound(
					forField(GLOBAL_STRUCT_UNSAFE).read(),
					loadThis(),
					forField(GLOBAL_STRUCT_ADDRESS).read(),
					new Compound(Stream.concat(offsetConstants.stream(), Stream.of(offsetConstant()))
									   .map(offset -> new Compound(
											   forField(offset).read(),
											   PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
											   Addition.LONG
									   ))
									   .collect(Collectors.toList())
					),
					loadParam,
					invoke(GLOBAL_UNSAFE_ADDRESS_SET)
			);
		}
		
		@Override
		public SetMethodEntry setMethodEntry(SetAllMethodMode mode) {
			TypeDescription pointerModeClass = ForLoadedType.of(mode.pointerMode.clazz);
			return new SetMethodEntry(
					List.of(new Token(pointerModeClass.asGenericType(), name, null)),
					(paramSlots, offsetConstants, methodDescription) -> {
						if (paramSlots.length != 1)
							throw new RuntimeException("Slot count " + paramSlots.length + " has to be 1!");
						return codeSetter(pointerModeClass, paramSlots[0], offsetConstants);
					}
			);
		}
	}
}
