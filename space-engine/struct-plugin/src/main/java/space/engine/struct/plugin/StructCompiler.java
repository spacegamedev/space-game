package space.engine.struct.plugin;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.annotation.AnnotationDescription.Loadable;
import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import net.bytebuddy.dynamic.scaffold.subclass.ConstructorStrategy.Default;
import org.gradle.api.tasks.TaskAction;
import org.gradle.work.InputChanges;
import space.engine.plugin.ByteBuddyIncrementalTask;
import space.engine.struct.Buffer;
import space.engine.struct.Struct;
import space.engine.struct.annotations.GenerateBufferClass;
import space.engine.struct.plugin.builder.BufferArrayCopyBuilder;
import space.engine.struct.plugin.builder.BufferBuilder;
import space.engine.struct.plugin.builder.BufferStreamBuilder;
import space.engine.struct.plugin.builder.ConstantBuilder;
import space.engine.struct.plugin.builder.JustGetBuilder;
import space.engine.struct.plugin.builder.MemberBuilder;
import space.engine.struct.plugin.builder.NioWrapperBuilder;
import space.engine.struct.plugin.builder.StructAsBufferBuilder;
import space.engine.struct.plugin.builder.StructBuilder;
import space.engine.struct.plugin.types.StructType.ForSource;

import static net.bytebuddy.description.modifier.Ownership.STATIC;
import static net.bytebuddy.description.modifier.Visibility.PUBLIC;

public abstract class StructCompiler extends ByteBuddyIncrementalTask {
	
	@TaskAction
	protected void work(InputChanges inputChanges) {
		ByteBuddy byteBuddy = new ByteBuddy();
		
		incrementalByteBuddy(inputChanges, (pool, classFileLocator, structDesc) -> {
			ForSource struct = new ForSource(structDesc);
			
			Builders builders = new Builders();
			builders.struct = byteBuddy.subclass(ForLoadedType.of(Struct.class), Default.NO_CONSTRUCTORS)
									   .modifiers(PUBLIC)
									   .name(struct.name());
			
			Loadable<GenerateBufferClass> generateBufferClassAnnotation = struct.annotations().ofType(GenerateBufferClass.class);
			if (generateBufferClassAnnotation == null || generateBufferClassAnnotation.load().value()) {
				builders.buffer = byteBuddy.subclass(ForLoadedType.of(Buffer.class), Default.NO_CONSTRUCTORS)
										   .modifiers(PUBLIC, STATIC)
										   .name(struct.buffer().getName())
										   .innerTypeOf(struct.struct()).asMemberType();
				builders.struct = builders.struct.declaredTypes(struct.buffer());
			}
			
			ConstantBuilder.build(struct, builders);
			StructBuilder.build(struct, builders);
			MemberBuilder.build(struct, builders);
			BufferBuilder.build(struct, builders);
			JustGetBuilder.build(struct, builders);
			BufferArrayCopyBuilder.build(struct, builders);
			BufferStreamBuilder.build(struct, builders);
			NioWrapperBuilder.build(struct, builders);
			StructAsBufferBuilder.build(struct, builders);
			
			return builders.make();
		});
	}
}
