package space.engine.struct.plugin.layouter;

import space.engine.struct.plugin.layouter.Layout.LayoutMember;

import java.util.EnumSet;
import java.util.List;

public class LayouterValidator {
	
	enum Options {
		
		ALLOW_MEMBER_OVERLAP,
		ALLOW_MEMBER_MISALIGNMENT,
		ALLOW_SIZE_0
	}
	
	public static void validate(Layout layout, EnumSet<Options> options) {
		validateSizeOf(layout, options);
		validateAlignment(layout, options);
		validateMemberOffsets(layout, options);
		validateMemberOverlap(layout, options);
	}
	
	private static void validateSizeOf(Layout layout, EnumSet<Options> options) {
		if (layout.sizeOf < 0)
			throw new ValidationFailedException(layout, "sizeOf negative");
		if (!options.contains(Options.ALLOW_SIZE_0) && layout.sizeOf == 0)
			throw new ValidationFailedException(layout, "sizeOf is 0");
	}
	
	private static void validateAlignment(Layout layout, EnumSet<Options> options) {
		if (layout.alignment < 0)
			throw new ValidationFailedException(layout, "alignment negative");
		if (!options.contains(Options.ALLOW_SIZE_0) && layout.alignment == 0)
			throw new ValidationFailedException(layout, "alignment is 0");
		if (!List.of(0, 1, 2, 4, 8).contains(layout.alignment))
			throw new ValidationFailedException(layout, "alignment " + layout.alignment + " is not a power of 2");
	}
	
	private static void validateMemberOffsets(Layout layout, EnumSet<Options> options) {
		boolean checkMisalignment = !options.contains(Options.ALLOW_MEMBER_MISALIGNMENT);
		for (LayoutMember member : layout.members) {
			if (member.offset < 0)
				throw new ValidationFailedException(layout, "Member " + member + " has negative offset!");
			if (member.offset + member.member.sizeOf() > layout.sizeOf)
				throw new ValidationFailedException(layout, "Member " + member + " is larger than total sizeof " + layout.sizeOf + "!");
			if (checkMisalignment && member.offset % member.member.alignment() != 0)
				throw new ValidationFailedException(layout, "Member " + member + " at offset " + member.offset + " is not aligned to " + member.member.alignment() + "!");
		}
	}
	
	private static void validateMemberOverlap(Layout layout, EnumSet<Options> options) {
		if (options.contains(Options.ALLOW_MEMBER_OVERLAP))
			return;
		for (int i = 0; i < layout.members.size(); i++) {
			for (int j = i + 1; j < layout.members.size(); j++) {
				LayoutMember m1 = layout.members.get(i);
				LayoutMember m2 = layout.members.get(j);
				int m1_start = m1.offset;
				int m1_end = m1_start + m1.member.sizeOf();
				int m2_start = m2.offset;
				int m2_end = m2_start + m2.member.sizeOf();
				if (m1_start < m2_end && m2_start < m1_end)
					throw new ValidationFailedException(layout, "Member " + m1 + " and Member " + m2 + " overlap!");
			}
		}
	}
}
