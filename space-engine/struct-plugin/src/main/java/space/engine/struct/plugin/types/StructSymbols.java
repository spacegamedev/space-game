package space.engine.struct.plugin.types;

import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.method.ParameterDescription.Token;
import space.engine.struct.annotations.NioWrapperMethod.NioTypes;
import space.engine.struct.annotations.SetAllMethod.SetAllMethodMode;

import java.util.List;

public interface StructSymbols {
	
	//symbols struct methods
	MethodDescription.InDefinedShape sizeOfVirtual();
	
	MethodDescription.InDefinedShape constructor();
	
	MethodDescription.InDefinedShape allocCopy();
	
	MethodDescription.InDefinedShape malloc();
	
	MethodDescription.InDefinedShape calloc();
	
	MethodDescription.InDefinedShape create();
	
	MethodDescription.InDefinedShape wrap();
	
	MethodDescription.InDefinedShape asBuffer();
	
	MethodDescription.InDefinedShape setterCopy();
	
	MethodDescription.InDefinedShape setterAll(SetAllMethodMode mode, List<Token> params);
	
	MethodDescription.InDefinedShape alloc(SetAllMethodMode mode, List<Token> params);
	
	MethodDescription.InDefinedShape nioBuffer(NioTypes nioType);
}
