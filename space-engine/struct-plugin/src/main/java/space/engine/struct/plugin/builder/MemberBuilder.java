package space.engine.struct.plugin.builder;

import net.bytebuddy.description.annotation.AnnotationDescription;
import net.bytebuddy.description.annotation.AnnotationDescription.Loadable;
import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.method.ParameterDescription.Token;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.implementation.Implementation;
import net.bytebuddy.implementation.bytecode.Duplication;
import net.bytebuddy.implementation.bytecode.StackManipulation.Compound;
import net.bytebuddy.implementation.bytecode.member.MethodReturn;
import net.bytebuddy.implementation.bytecode.member.MethodVariableAccess;
import space.engine.plugin.SimpleImplementation;
import space.engine.struct.StructMembers;
import space.engine.struct.annotations.SetAllMethod;
import space.engine.struct.annotations.SetAllMethod.SetAllMethodMode;
import space.engine.struct.annotations.ShortGetter;
import space.engine.struct.plugin.Builders;
import space.engine.struct.plugin.types.Member;
import space.engine.struct.plugin.types.Member.GetterInfo;
import space.engine.struct.plugin.types.Member.SetMethodEntry;
import space.engine.struct.plugin.types.Member.SetterInfo;
import space.engine.struct.plugin.types.StructType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static net.bytebuddy.implementation.bytecode.member.MethodInvocation.invoke;
import static net.bytebuddy.implementation.bytecode.member.MethodVariableAccess.*;
import static space.engine.struct.plugin.builder.StructBuilder.codeGetAddressStruct;

public class MemberBuilder {
	
	public static void build(StructType struct, Builders builders) {
		buildMembersAnnotation(struct, builders);
		buildGetter(struct, builders);
		buildSetter(struct, builders);
		buildSetterAll(struct, builders);
	}
	
	public static void buildMembersAnnotation(StructType struct, Builders builders) {
		builders.struct = builders.struct.annotateType(
				AnnotationDescription.Builder
						.ofType(StructMembers.class)
						.defineArray("names", struct.members().stream().map(Member::name).toArray(String[]::new))
						.defineTypeArray("types", struct.members().stream().map(m -> m.type().asErasure()).toArray(TypeDescription[]::new))
						.build()
		);
	}
	
	protected static void buildGetter(StructType struct, Builders builders) {
		for (Member member : struct.members()) {
			for (GetterInfo i : member.codeGetter()) {
				Implementation getterStruct = i.code.apply(codeGetAddressStruct());
				builders.struct = builders.struct.define(member.symbols().getter(i.name, i.type)).intercept(getterStruct);
				
				Loadable<ShortGetter> shortGetter = member.annotations().ofType(ShortGetter.class);
				if (shortGetter == null || shortGetter.load().value())
					builders.struct = builders.struct.define(member.symbols().getterShort(i.name, i.type)).intercept(getterStruct);
			}
		}
	}
	
	protected static void buildSetter(StructType struct, Builders builders) {
		for (Member member : struct.members()) {
			for (SetterInfo i : member.codeSetter()) {
				Implementation setter = i.code.apply(codeGetAddressStruct(), 1);
				builders.struct = builders.struct.define(member.symbols().setter(i.name, i.type)).intercept(setter);
			}
		}
	}
	
	public static void buildSetterAll(StructType struct, Builders builders) {
		SetAllMethodMode[] modes = SetAllMethodMode.values();
		{
			Loadable<SetAllMethod> annotation = struct.annotations().ofType(SetAllMethod.class);
			if (annotation != null) {
				modes = annotation.load().value();
				if (modes.length == 0)
					return;
			}
		}
		
		//use ArrayList instead of HashMap as working hashcode is not guaranteed
		ArrayList<MethodDescription> usedSignatures = new ArrayList<>();
		ArrayList<MethodDescription> usedSignaturesBuffer = new ArrayList<>();
		for (SetAllMethodMode mode : modes) {
			List<SetMethodEntry> setMethodEntries = struct.members().stream().map(m -> m.setMethodEntry(mode)).collect(Collectors.toList());
			List<Token> params = setMethodEntries.stream().flatMap(e -> e.params.stream()).collect(Collectors.toList());
			
			//struct
			{
				MethodDescription signature = struct.structSymbols().setterAll(mode, params);
				if (usedSignatures.contains(signature)) {
					//duplicate setter -> skip completely
					continue;
				}
				usedSignatures.add(signature);
				
				//struct setter
				AtomicInteger slotOffset = new AtomicInteger(1);
				builders.struct = builders.struct.define(signature).intercept(new SimpleImplementation(method -> new Compound(
						new Compound(setMethodEntries.stream().map(e -> e.code.generateCode(
								e.params.stream().mapToInt(p -> slotOffset.getAndAdd(p.getType().getStackSize().getSize())).toArray(), List.of(), method
						)).collect(Collectors.toList())),
						MethodReturn.VOID
				)));
			}
			
			//struct alloc
			{
				MethodDescription signature = struct.structSymbols().alloc(mode, params);
				AtomicInteger slotOffset = new AtomicInteger(1);
				builders.struct = builders.struct.define(signature).intercept(new SimpleImplementation(method -> new Compound(
						REFERENCE.loadFrom(0),
						invoke(struct.structSymbols().malloc()),
						Duplication.SINGLE,
						
						new Compound(params.stream().map(e -> {
							int slot = slotOffset.getAndAdd(e.getType().getStackSize().getSize());
							return MethodVariableAccess.of(e.getType()).loadFrom(slot);
						}).collect(Collectors.toList())),
						invoke(struct.structSymbols().setterAll(mode, params)),
						
						MethodReturn.REFERENCE
				)));
			}
			
			//buffer
			if (builders.hasBuffer()) {
				MethodDescription signature = struct.bufferSymbols().setterAll(struct, mode, params);
				if (!usedSignaturesBuffer.contains(signature)) {
					usedSignaturesBuffer.add(signature);
					
					AtomicInteger slotOffset = new AtomicInteger(3);
					builders.buffer = builders.buffer.define(signature).intercept(new SimpleImplementation(method -> new Compound(
							loadThis(),
							LONG.loadFrom(1),
							invoke(struct.bufferSymbols().getStruct()),
							
							new Compound(params.stream().map(e -> {
								int slot = slotOffset.getAndAdd(e.getType().getStackSize().getSize());
								return MethodVariableAccess.of(e.getType()).loadFrom(slot);
							}).collect(Collectors.toList())),
							invoke(struct.structSymbols().setterAll(mode, params)),
							
							MethodReturn.VOID
					)));
				}
			}
		}
	}
}
