package space.engine.struct.plugin.layouter;

import space.engine.struct.plugin.layouter.Layout.LayoutMember;
import space.engine.struct.plugin.layouter.LayouterValidator.Options;
import space.engine.struct.plugin.types.Member;
import space.engine.struct.plugin.types.StructType;

import java.util.EnumSet;
import java.util.List;

public class SingleMemberLayouter extends AbstractLayouter {
	
	@Override
	protected EnumSet<Options> validationOptions() {
		return EnumSet.noneOf(Options.class);
	}
	
	@Override
	protected Layout computeLayoutNoValidation(StructType struct) {
		if (struct.members().size() != 1)
			throw new RuntimeException("SingleMemberLayouter did not get exactly 1 member!");
		Member m = struct.members().get(0);
		return new Layout(struct, List.of(new LayoutMember(m, 0)), m.sizeOf(), m.alignment());
	}
}
