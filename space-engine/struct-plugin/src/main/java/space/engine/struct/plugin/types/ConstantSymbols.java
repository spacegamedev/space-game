package space.engine.struct.plugin.types;

import net.bytebuddy.description.field.FieldDescription;

public interface ConstantSymbols {
	
	//symbols struct constants
	FieldDescription.InDefinedShape sizeOf();
	
	FieldDescription.InDefinedShape alignment();
	
	FieldDescription.InDefinedShape offset(Member member);
}
