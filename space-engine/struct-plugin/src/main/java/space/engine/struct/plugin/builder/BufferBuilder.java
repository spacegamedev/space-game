package space.engine.struct.plugin.builder;

import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import net.bytebuddy.implementation.bytecode.Addition;
import net.bytebuddy.implementation.bytecode.Duplication;
import net.bytebuddy.implementation.bytecode.Multiplication;
import net.bytebuddy.implementation.bytecode.Removal;
import net.bytebuddy.implementation.bytecode.StackManipulation.Compound;
import net.bytebuddy.implementation.bytecode.StackManipulation.Trivial;
import net.bytebuddy.implementation.bytecode.TypeCreation;
import net.bytebuddy.implementation.bytecode.assign.primitive.PrimitiveWideningDelegate;
import net.bytebuddy.implementation.bytecode.constant.NullConstant;
import net.bytebuddy.implementation.bytecode.member.MethodReturn;
import net.bytebuddy.implementation.bytecode.member.MethodVariableAccess;
import space.engine.plugin.SimpleImplementation;
import space.engine.struct.plugin.Builders;
import space.engine.struct.plugin.types.StructType;

import static net.bytebuddy.implementation.bytecode.member.FieldAccess.forField;
import static net.bytebuddy.implementation.bytecode.member.MethodInvocation.invoke;
import static net.bytebuddy.implementation.bytecode.member.MethodVariableAccess.*;
import static space.engine.struct.plugin.GlobalSymbols.*;

public class BufferBuilder {
	
	public static void build(StructType struct, Builders builders) {
		if (!builders.hasBuffer())
			return;
		buildConstructor(struct, builders);
		buildSizeOfVirtualMethod(struct, builders);
		buildMalloc(struct, builders);
		buildCalloc(struct, builders);
		buildCreate(struct, builders);
		buildWrap(struct, builders);
		buildGetter(struct, builders);
		buildSetterCopy(struct, builders);
		buildSubBuffer(struct, builders);
	}
	
	public static void buildConstructor(StructType struct, Builders builders) {
		builders.buffer = builders.buffer.define(struct.bufferSymbols().constructor()).intercept(new SimpleImplementation(method -> new Compound(
				loadThis(),
				REFERENCE.loadFrom(1),
				MethodVariableAccess.LONG.loadFrom(2),
				MethodVariableAccess.LONG.loadFrom(4),
				REFERENCE.loadFrom(6),
				invoke(GLOBAL_BUFFER_CONSTRUCTOR),
				MethodReturn.VOID
		)));
	}
	
	public static void buildSizeOfVirtualMethod(StructType struct, Builders builders) {
		builders.buffer = builders.buffer.define(struct.bufferSymbols().sizeOfVirtual()).intercept(new SimpleImplementation(method -> new Compound(
				forField(struct.constantSymbols().sizeOf()).read(),
				PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
				loadThis(),
				forField(GLOBAL_BUFFER_LENGTH).read(),
				Multiplication.LONG,
				MethodReturn.LONG
		)));
	}
	
	public static void buildMalloc(StructType struct, Builders builders) {
		builders.buffer = builders.buffer.define(struct.bufferSymbols().malloc()).intercept(new SimpleImplementation(method -> codeAlloc(struct, GLOBAL_ALLOCATOR_MALLOC)));
	}
	
	public static void buildCalloc(StructType struct, Builders builders) {
		builders.buffer = builders.buffer.define(struct.bufferSymbols().calloc()).intercept(new SimpleImplementation(method -> codeAlloc(struct, GLOBAL_ALLOCATOR_CALLOC)));
	}
	
	public static Compound codeAlloc(StructType struct, MethodDescription alloc) {
		return new Compound(
				TypeCreation.of(struct.buffer()),
				Duplication.SINGLE,
				REFERENCE.loadFrom(0),
				Duplication.SINGLE,
				forField(struct.constantSymbols().sizeOf()).read(),
				PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
				LONG.loadFrom(1),
				Multiplication.LONG,
				invoke(alloc),
				LONG.loadFrom(1),
				NullConstant.INSTANCE,
				invoke(struct.bufferSymbols().constructor()),
				MethodReturn.REFERENCE
		);
	}
	
	public static void buildCreate(StructType struct, Builders builders) {
		builders.buffer = builders.buffer.define(struct.bufferSymbols().create()).intercept(new SimpleImplementation(method -> new Compound(
				TypeCreation.of(struct.buffer()),
				Duplication.SINGLE,
				REFERENCE.loadFrom(0),
				MethodVariableAccess.LONG.loadFrom(1),
				MethodVariableAccess.LONG.loadFrom(3),
				NullConstant.INSTANCE,
				invoke(struct.bufferSymbols().constructor()),
				MethodReturn.REFERENCE
		)));
	}
	
	public static void buildWrap(StructType struct, Builders builders) {
		builders.buffer = builders.buffer.define(struct.bufferSymbols().wrap()).intercept(new SimpleImplementation(method -> new Compound(
				TypeCreation.of(struct.buffer()),
				Duplication.SINGLE,
				forField(GLOBAL_ALLOCATOR_NOOP_INSTANCE).read(),
				MethodVariableAccess.LONG.loadFrom(0),
				MethodVariableAccess.LONG.loadFrom(2),
				REFERENCE.loadFrom(4),
				invoke(struct.bufferSymbols().constructor()),
				MethodReturn.REFERENCE
		)));
	}
	
	public static void buildGetter(StructType struct, Builders builders) {
		builders.buffer = builders.buffer.define(struct.bufferSymbols().getStruct()).intercept(new SimpleImplementation(method -> new Compound(
				codeGetterWrap(struct, 1, true),
				MethodReturn.REFERENCE
		)));
		
		builders.buffer = builders.buffer.define(struct.bufferSymbols().get(struct.struct().asGenericType())).intercept(new SimpleImplementation(method -> new Compound(
				codeGetterWrap(struct, 1, true),
				MethodReturn.REFERENCE
		)));
	}
	
	public static Compound codeGetterWrap(StructType struct, int addressSlot, boolean indexCheck) {
		return new Compound(
				codeGetterAddress(struct, addressSlot, indexCheck),
				loadThis(),
				invoke(struct.structSymbols().wrap())
		);
	}
	
	public static Compound codeGetterAddress(StructType struct, int addressSlot, boolean indexCheck) {
		return new Compound(
				indexCheck ? new Compound(
						LONG.loadFrom(addressSlot),
						loadThis(),
						forField(GLOBAL_BUFFER_LENGTH).read(),
						invoke(GLOBAL_STRUCT_CHECK_INDEX),
						Removal.DOUBLE
				) : Trivial.INSTANCE,
				
				LONG.loadFrom(addressSlot),
				forField(struct.constantSymbols().sizeOf()).read(),
				PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
				Multiplication.LONG,
				loadThis(),
				forField(GLOBAL_STRUCT_ADDRESS).read(),
				Addition.LONG
		);
	}
	
	public static void buildSetterCopy(StructType struct, Builders builders) {
		builders.buffer = builders.buffer.define(struct.bufferSymbols().setterCopy()).intercept(new SimpleImplementation(method -> new Compound(
				forField(GLOBAL_STRUCT_UNSAFE).read(),
				REFERENCE.loadFrom(3),
				forField(GLOBAL_STRUCT_ADDRESS).read(),
				codeGetterAddress(struct, 1, true),
				forField(struct.constantSymbols().sizeOf()).read(),
				PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
				invoke(GLOBAL_UNSAFE_COPY_MEMORY),
				MethodReturn.VOID
		)));
	}
	
	public static void buildSubBuffer(StructType struct, Builders builders) {
		builders.buffer = builders.buffer.define(struct.bufferSymbols().subBuffer()).intercept(new SimpleImplementation(method -> new Compound(
				LONG.loadFrom(1),
				LONG.loadFrom(3),
				loadThis(),
				forField(GLOBAL_BUFFER_LENGTH).read(),
				invoke(GLOBAL_STRUCT_CHECK_FROM_INDEX_SIZE),
				Removal.DOUBLE,
				
				codeGetterAddress(struct, 1, false),
				LONG.loadFrom(3),
				loadThis(),
				invoke(struct.bufferSymbols().wrap()),
				MethodReturn.REFERENCE
		)));
	}
}
