package space.engine.struct.plugin.builder;

import net.bytebuddy.description.annotation.AnnotationDescription.Loadable;
import net.bytebuddy.implementation.Implementation;
import org.jetbrains.annotations.NotNull;
import space.engine.plugin.SimpleImplementation.Code;
import space.engine.struct.annotations.JustGet;
import space.engine.struct.plugin.Builders;
import space.engine.struct.plugin.types.Member;
import space.engine.struct.plugin.types.Member.GetterInfo;
import space.engine.struct.plugin.types.Member.GetterSetterInfo;
import space.engine.struct.plugin.types.StructType;
import space.engine.struct.plugin.types.StructType.ForSource;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static space.engine.struct.plugin.builder.StructBuilder.codeGetAddressStruct;

public class JustGetBuilder {
	
	public static void build(ForSource struct, Builders builders) {
		validateJustGet(struct);
		buildJustGet(struct, builders);
	}
	
	public static void validateJustGet(StructType struct) {
		List<Member> justGetFields = struct.members().stream().filter(m -> m.annotations().isAnnotationPresent(JustGet.class)).collect(Collectors.toList());
		if (justGetFields.size() > 1)
			throw new RuntimeException("Multiple members have the @JustGet Annotation: " + justGetFields.stream().map(Object::toString).collect(Collectors.joining(", ")));
	}
	
	private static void buildJustGet(ForSource struct, Builders builders) {
		for (Member member : struct.members()) {
			Loadable<JustGet> justGetLoad = member.annotations().ofType(JustGet.class);
			if (justGetLoad == null)
				continue;
			JustGet justGet = justGetLoad.load();
			if (!(justGet.value() || justGet.bufferGet()))
				continue;
			
			@NotNull List<GetterInfo> info = member.codeGetter();
			if (info.isEmpty())
				throw new RuntimeException("@JustGet on member " + member + " but member has no getter available");
			GetterSetterInfo<Function<Code, Implementation>> i = info.get(0);
			
			if (justGet.value())
				builders.struct = builders.struct.define(member.symbols().justGet(i.name, i.type))
												 .intercept(i.code.apply(codeGetAddressStruct()));
			
			if (builders.hasBuffer() && justGet.bufferGet()) {
				builders.buffer = builders.buffer.define(struct.bufferSymbols().get(i.type))
												 .intercept(i.code.apply(method -> BufferBuilder.codeGetterAddress(struct, 1, true)));
			}
		}
	}
}
