package space.engine.struct.plugin.builder;

import net.bytebuddy.description.annotation.AnnotationDescription.Loadable;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import net.bytebuddy.implementation.bytecode.Addition;
import net.bytebuddy.implementation.bytecode.Duplication;
import net.bytebuddy.implementation.bytecode.Multiplication;
import net.bytebuddy.implementation.bytecode.Removal;
import net.bytebuddy.implementation.bytecode.StackManipulation;
import net.bytebuddy.implementation.bytecode.StackManipulation.Compound;
import net.bytebuddy.implementation.bytecode.assign.primitive.PrimitiveWideningDelegate;
import net.bytebuddy.implementation.bytecode.collection.ArrayLength;
import net.bytebuddy.implementation.bytecode.constant.LongConstant;
import net.bytebuddy.implementation.bytecode.constant.NullConstant;
import net.bytebuddy.implementation.bytecode.member.MethodReturn;
import org.jetbrains.annotations.NotNull;
import space.engine.plugin.SimpleImplementation;
import space.engine.struct.annotations.BufferArrayCopy;
import space.engine.struct.annotations.BufferArrayCopy.ArrayCopyTypes;
import space.engine.struct.plugin.Builders;
import space.engine.struct.plugin.types.StructType;

import static net.bytebuddy.implementation.bytecode.member.FieldAccess.forField;
import static net.bytebuddy.implementation.bytecode.member.MethodInvocation.invoke;
import static net.bytebuddy.implementation.bytecode.member.MethodVariableAccess.*;
import static space.engine.struct.plugin.GlobalSymbols.*;

public class BufferArrayCopyBuilder {
	
	public static void build(StructType struct, Builders builders) {
		if (!builders.hasBuffer())
			return;
		Loadable<BufferArrayCopy> bufferArrayCopyLoadable = struct.annotations().ofType(BufferArrayCopy.class);
		if (bufferArrayCopyLoadable != null) {
			ArrayCopyTypes[] arrayCopyTypes = bufferArrayCopyLoadable.load().value();
			if (arrayCopyTypes.length == 0)
				throw new RuntimeException("BufferArrayCopy annotation present without any array types defined!");
			for (ArrayCopyTypes copyType : arrayCopyTypes) {
				TypeDescription arrayClass = ForLoadedType.of(copyType.arrayClass);
				
				if (copyType.componentBytes != struct.size().sizeOf())
					throw new RuntimeException("BufferArrayCopy: Struct " + struct.name() + " must be equal size to primitive " + copyType.arrayClass.getComponentType().getName() + " (size " + copyType.componentBytes + ")!");
				
				builders.buffer = builders.buffer.define(struct.bufferSymbols().arrayGetFull(arrayClass)).intercept(new SimpleImplementation(method -> new Compound(
						forField(GLOBAL_STRUCT_UNSAFE).read(),
						codeUnsafeAddressBuffer(copyType, 0, 1, 6),
						codeUnsafeAddressArray(copyType, 3, 4, 6),
						LONG.loadFrom(6),
						LongConstant.forValue(copyType.componentBytes),
						Multiplication.LONG,
						invoke(GLOBAL_UNSAFE_COPY_MEMORY_OBJECT),
						MethodReturn.VOID
				)));
				builders.buffer = builders.buffer.define(struct.bufferSymbols().arrayGetShort(arrayClass)).intercept(new SimpleImplementation(method -> new Compound(
						loadThis(),
						LongConstant.ZERO,
						REFERENCE.loadFrom(1),
						LongConstant.ZERO,
						REFERENCE.loadFrom(1),
						codeArrayOrBufferLength(arrayClass),
						invoke(struct.bufferSymbols().arrayGetFull(arrayClass)),
						MethodReturn.VOID
				)));
				if (copyType.indexIsInt) {
					builders.buffer = builders.buffer.define(struct.bufferSymbols().arrayGetFull(arrayClass, true)).intercept(new SimpleImplementation(method -> new Compound(
							loadThis(),
							LONG.loadFrom(1),
							REFERENCE.loadFrom(3),
							INTEGER.loadFrom(4),
							PrimitiveWideningDelegate.INTEGER.widenTo(ForLoadedType.of(long.class)),
							INTEGER.loadFrom(5),
							PrimitiveWideningDelegate.INTEGER.widenTo(ForLoadedType.of(long.class)),
							invoke(struct.bufferSymbols().arrayGetFull(arrayClass)),
							MethodReturn.VOID
					)));
				}
				
				builders.buffer = builders.buffer.define(struct.bufferSymbols().arraySetFull(arrayClass)).intercept(new SimpleImplementation(method -> new Compound(
						forField(GLOBAL_STRUCT_UNSAFE).read(),
						codeUnsafeAddressArray(copyType, 1, 2, 6),
						codeUnsafeAddressBuffer(copyType, 0, 4, 6),
						LONG.loadFrom(6),
						LongConstant.forValue(copyType.componentBytes),
						Multiplication.LONG,
						invoke(GLOBAL_UNSAFE_COPY_MEMORY_OBJECT),
						MethodReturn.VOID
				)));
				builders.buffer = builders.buffer.define(struct.bufferSymbols().arraySetShort(arrayClass)).intercept(new SimpleImplementation(method -> new Compound(
						loadThis(),
						REFERENCE.loadFrom(1),
						LongConstant.ZERO,
						LongConstant.ZERO,
						REFERENCE.loadFrom(1),
						codeArrayOrBufferLength(arrayClass),
						invoke(struct.bufferSymbols().arraySetFull(arrayClass)),
						MethodReturn.VOID
				)));
				if (copyType.indexIsInt) {
					builders.buffer = builders.buffer.define(struct.bufferSymbols().arraySetFull(arrayClass, true)).intercept(new SimpleImplementation(method -> new Compound(
							loadThis(),
							REFERENCE.loadFrom(1),
							INTEGER.loadFrom(2),
							PrimitiveWideningDelegate.INTEGER.widenTo(ForLoadedType.of(long.class)),
							LONG.loadFrom(3),
							INTEGER.loadFrom(5),
							PrimitiveWideningDelegate.INTEGER.widenTo(ForLoadedType.of(long.class)),
							invoke(struct.bufferSymbols().arraySetFull(arrayClass)),
							MethodReturn.VOID
					)));
				}
				
				builders.buffer = builders.buffer.define(struct.bufferSymbols().arrayAlloc(arrayClass)).intercept(new SimpleImplementation(method -> new Compound(
						REFERENCE.loadFrom(1),
						codeArrayOrBufferLength(arrayClass),
						LONG.storeAt(2),
						
						REFERENCE.loadFrom(0),
						LONG.loadFrom(2),
						invoke(struct.bufferSymbols().malloc()),
						
						Duplication.SINGLE,
						REFERENCE.loadFrom(1),
						LongConstant.ZERO,
						LongConstant.ZERO,
						LONG.loadFrom(2),
						invoke(struct.bufferSymbols().arraySetFull(arrayClass)),
						
						MethodReturn.REFERENCE
				), 2));
			}
		}
	}
	
	private static @NotNull StackManipulation codeArrayOrBufferLength(TypeDescription arrayClass) {
		return arrayClass.isArray()
				? new Compound(ArrayLength.INSTANCE, PrimitiveWideningDelegate.INTEGER.widenTo(ForLoadedType.of(long.class)))
				: forField(GLOBAL_BUFFER_LENGTH).read();
	}
	
	private static @NotNull Compound codeUnsafeAddressArray(ArrayCopyTypes copyType, int slotArray, int slotIndex, int slotLength) {
		return new Compound(
				LONG.loadFrom(slotIndex),
				LONG.loadFrom(slotLength),
				REFERENCE.loadFrom(slotArray),
				ArrayLength.INSTANCE,
				PrimitiveWideningDelegate.INTEGER.widenTo(ForLoadedType.of(long.class)),
				invoke(GLOBAL_STRUCT_CHECK_FROM_INDEX_SIZE),
				Removal.DOUBLE,
				
				REFERENCE.loadFrom(slotArray),
				LONG.loadFrom(slotIndex),
				copyType.componentBytes != 1 ? new Compound(
						LongConstant.forValue(copyType.componentBytes),
						Multiplication.LONG
				) : new Compound(),
				forField(unsafeArrayBaseOffset(ForLoadedType.of(copyType.arrayClass))).read(),
				PrimitiveWideningDelegate.INTEGER.widenTo(ForLoadedType.of(long.class)),
				Addition.LONG
		);
	}
	
	private static @NotNull Compound codeUnsafeAddressBuffer(ArrayCopyTypes copyType, int slotBuffer, int slotIndex, int slotLength) {
		return new Compound(
				LONG.loadFrom(slotIndex),
				LONG.loadFrom(slotLength),
				REFERENCE.loadFrom(slotBuffer),
				forField(GLOBAL_BUFFER_LENGTH).read(),
				invoke(GLOBAL_STRUCT_CHECK_FROM_INDEX_SIZE),
				Removal.DOUBLE,
				
				NullConstant.INSTANCE,
				LONG.loadFrom(slotIndex),
				copyType.componentBytes != 1 ? new Compound(
						LongConstant.forValue(copyType.componentBytes),
						Multiplication.LONG
				) : new Compound(),
				REFERENCE.loadFrom(slotBuffer),
				forField(GLOBAL_STRUCT_ADDRESS).read(),
				Addition.LONG
		);
	}
}
