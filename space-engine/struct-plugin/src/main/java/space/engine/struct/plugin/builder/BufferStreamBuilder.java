package space.engine.struct.plugin.builder;

import net.bytebuddy.description.annotation.AnnotationDescription.Loadable;
import net.bytebuddy.description.method.MethodDescription.InDefinedShape;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import net.bytebuddy.implementation.bytecode.Addition;
import net.bytebuddy.implementation.bytecode.Multiplication;
import net.bytebuddy.implementation.bytecode.StackManipulation.Compound;
import net.bytebuddy.implementation.bytecode.constant.LongConstant;
import net.bytebuddy.implementation.bytecode.member.MethodReturn;
import net.bytebuddy.utility.JavaConstant;
import space.engine.plugin.SimpleImplementation;
import space.engine.struct.annotations.BufferStream;
import space.engine.struct.annotations.BufferStream.StreamType;
import space.engine.struct.plugin.Builders;
import space.engine.struct.plugin.types.StructType;

import java.util.List;
import java.util.stream.LongStream;

import static net.bytebuddy.implementation.bytecode.member.FieldAccess.forField;
import static net.bytebuddy.implementation.bytecode.member.MethodInvocation.invoke;
import static net.bytebuddy.implementation.bytecode.member.MethodVariableAccess.*;
import static net.bytebuddy.matcher.ElementMatchers.*;
import static space.engine.struct.plugin.GlobalSymbols.*;

public class BufferStreamBuilder {
	
	public static void build(StructType struct, Builders builders) {
		if (!builders.hasBuffer())
			return;
		
		Loadable<BufferStream> loadable = struct.annotations().ofType(BufferStream.class);
		StreamType streamType = loadable == null ? StreamType.STRUCT : loadable.load().value();
		TypeDescription returnType = ForLoadedType.of(streamType.streamClass);
		TypeDescription map_functionType = ForLoadedType.of(streamType.mapMethodFunctionType);
		InDefinedShape map_functionType_method = map_functionType.getDeclaredMethods().filter(isAbstract()).getOnly();
		InDefinedShape map = ForLoadedType.of(LongStream.class).getDeclaredMethods().filter(named(streamType.mapMethodName).and(not(isStatic())).and(takesArguments(map_functionType))).getOnly();
		if (!map.getReturnType().asErasure().equals(returnType))
			throw new RuntimeException("BufferStream.StreamType " + streamType.name() + " map method returns " + map.getReturnType().asErasure().getTypeName() + " but must the same type as it's declared streamClass " + returnType.getName() + "!");
		
		InDefinedShape dynamicBindMethod;
		if (streamType == StreamType.STRUCT) {
			dynamicBindMethod = struct.bufferSymbols().getStruct();
		} else {
			TypeDescription elementType = ForLoadedType.of(streamType.elementClass);
			if (streamType.elementSizeOf != struct.size().sizeOf())
				throw new RuntimeException("BufferStream " + streamType.name() + " requires struct to have a sizeOf = " + streamType.elementSizeOf + " but structs size is " + struct.size().sizeOf() + "!");
			
			dynamicBindMethod = struct.bufferSymbols().streamGetter(streamType);
			builders.buffer = builders.buffer.define(dynamicBindMethod).intercept(new SimpleImplementation(method -> new Compound(
					forField(GLOBAL_STRUCT_UNSAFE).read(),
					loadThis(),
					forField(GLOBAL_STRUCT_ADDRESS).read(),
					LONG.loadFrom(1),
					LongConstant.forValue(streamType.elementSizeOf),
					Multiplication.LONG,
					Addition.LONG,
					invoke(unsafeAccessorGet(elementType)),
					MethodReturn.of(elementType)
			)));
		}
		
		builders.buffer = builders.buffer.define(struct.bufferSymbols().stream(streamType)).intercept(new SimpleImplementation(method -> new Compound(
				LongConstant.ZERO,
				loadThis(),
				forField(GLOBAL_BUFFER_LENGTH).read(),
				invoke(GLOBAL_LONGSTREAM_RANGE),
				
				loadThis(),
				invoke(GLOBAL_LAMBDAMETAFACTORY_METAFACTORY).dynamic(
						map_functionType_method.getName(),
						map_functionType,
						List.of(struct.buffer()),
						List.of(
								JavaConstant.MethodType.of(map_functionType_method),
								JavaConstant.MethodHandle.of(dynamicBindMethod),
								JavaConstant.MethodType.of(map_functionType_method)
						)
				),
				
				invoke(map),
				MethodReturn.REFERENCE
		)));
	}
}
