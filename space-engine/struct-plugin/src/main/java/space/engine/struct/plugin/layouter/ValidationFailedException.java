package space.engine.struct.plugin.layouter;

import org.jetbrains.annotations.NotNull;

public class ValidationFailedException extends RuntimeException {
	
	public ValidationFailedException(@NotNull Layout layout, @NotNull String message) {
		super("Layout of " + layout.struct.name() + " is invalid: " + message);
	}
}
