package space.engine.struct.plugin.layouter;

import space.engine.struct.plugin.types.Member;
import space.engine.struct.plugin.types.Size;
import space.engine.struct.plugin.types.StructType;

import java.util.List;

public class Layout implements Size {
	
	public final StructType struct;
	public final List<LayoutMember> members;
	public final int sizeOf;
	public final int alignment;
	
	public Layout(StructType struct, List<LayoutMember> members, int sizeOf, int alignment) {
		this.struct = struct;
		this.members = members;
		this.sizeOf = sizeOf;
		this.alignment = alignment;
	}
	
	@Override
	public int sizeOf() {
		return sizeOf;
	}
	
	@Override
	public int alignment() {
		return alignment;
	}
	
	@Override
	public String toString() {
		return struct.toString();
	}
	
	public static Layout empty(StructType struct) {
		return new Layout(struct, List.of(), 0, 0);
	}
	
	public static class LayoutMember {
		
		public final Member member;
		public final int offset;
		
		public LayoutMember(Member member, int offset) {
			this.member = member;
			this.offset = offset;
		}
		
		@Override
		public String toString() {
			return member.type().getTypeName() + " " + member.name() + "[" + offset + "+" + member.sizeOf() + "]";
		}
	}
}
