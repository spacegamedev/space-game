package space.engine.struct.plugin.builder;

import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import net.bytebuddy.implementation.bytecode.Duplication;
import net.bytebuddy.implementation.bytecode.StackManipulation.Compound;
import net.bytebuddy.implementation.bytecode.TypeCreation;
import net.bytebuddy.implementation.bytecode.assign.primitive.PrimitiveWideningDelegate;
import net.bytebuddy.implementation.bytecode.constant.NullConstant;
import net.bytebuddy.implementation.bytecode.member.MethodReturn;
import net.bytebuddy.implementation.bytecode.member.MethodVariableAccess;
import org.jetbrains.annotations.NotNull;
import space.engine.plugin.SimpleImplementation;
import space.engine.struct.plugin.Builders;
import space.engine.struct.plugin.types.StructType;

import static net.bytebuddy.implementation.bytecode.member.FieldAccess.forField;
import static net.bytebuddy.implementation.bytecode.member.MethodInvocation.invoke;
import static net.bytebuddy.implementation.bytecode.member.MethodVariableAccess.*;
import static space.engine.struct.plugin.GlobalSymbols.*;

public class StructBuilder {
	
	public static void build(StructType struct, Builders builders) {
		buildConstructor(struct, builders);
		buildSizeOfVirtualMethod(struct, builders);
		buildMalloc(struct, builders);
		buildCalloc(struct, builders);
		buildCreate(struct, builders);
		buildWrap(struct, builders);
		buildSetterCopy(struct, builders);
	}
	
	public static @NotNull SimpleImplementation.Code codeGetAddressStruct() {
		return method -> new Compound(
				loadThis(),
				forField(GLOBAL_STRUCT_ADDRESS).read()
		);
	}
	
	public static void buildConstructor(StructType struct, Builders builders) {
		builders.struct = builders.struct.define(struct.structSymbols().constructor()).intercept(new SimpleImplementation(method -> new Compound(
				loadThis(),
				REFERENCE.loadFrom(1),
				MethodVariableAccess.LONG.loadFrom(2),
				REFERENCE.loadFrom(4),
				invoke(GLOBAL_STRUCT_CONSTRUCTOR),
				MethodReturn.VOID
		)));
	}
	
	public static void buildSizeOfVirtualMethod(StructType struct, Builders builders) {
		builders.struct = builders.struct.define(struct.structSymbols().sizeOfVirtual()).intercept(new SimpleImplementation(method -> new Compound(
				forField(struct.constantSymbols().sizeOf()).read(),
				PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
				MethodReturn.LONG
		)));
	}
	
	public static void buildMalloc(StructType struct, Builders builders) {
		builders.struct = builders.struct.define(struct.structSymbols().malloc()).intercept(new SimpleImplementation(method -> new Compound(
				TypeCreation.of(struct.struct()),
				Duplication.SINGLE,
				REFERENCE.loadFrom(0),
				Duplication.SINGLE,
				forField(struct.constantSymbols().sizeOf()).read(),
				PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
				invoke(GLOBAL_ALLOCATOR_MALLOC),
				NullConstant.INSTANCE,
				invoke(struct.structSymbols().constructor()),
				MethodReturn.REFERENCE
		)));
	}
	
	public static void buildCalloc(StructType struct, Builders builders) {
		builders.struct = builders.struct.define(struct.structSymbols().calloc()).intercept(new SimpleImplementation(method -> new Compound(
				TypeCreation.of(struct.struct()),
				Duplication.SINGLE,
				REFERENCE.loadFrom(0),
				Duplication.SINGLE,
				forField(struct.constantSymbols().sizeOf()).read(),
				PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
				invoke(GLOBAL_ALLOCATOR_CALLOC),
				NullConstant.INSTANCE,
				invoke(struct.structSymbols().constructor()),
				MethodReturn.REFERENCE
		)));
	}
	
	public static void buildCreate(StructType struct, Builders builders) {
		builders.struct = builders.struct.define(struct.structSymbols().create()).intercept(new SimpleImplementation(method -> new Compound(
				TypeCreation.of(struct.struct()),
				Duplication.SINGLE,
				REFERENCE.loadFrom(0),
				MethodVariableAccess.LONG.loadFrom(1),
				NullConstant.INSTANCE,
				invoke(struct.structSymbols().constructor()),
				MethodReturn.REFERENCE
		)));
	}
	
	public static void buildWrap(StructType struct, Builders builders) {
		builders.struct = builders.struct.define(struct.structSymbols().wrap()).intercept(new SimpleImplementation(method -> new Compound(
				TypeCreation.of(struct.struct()),
				Duplication.SINGLE,
				forField(GLOBAL_ALLOCATOR_NOOP_INSTANCE).read(),
				MethodVariableAccess.LONG.loadFrom(0),
				REFERENCE.loadFrom(2),
				invoke(struct.structSymbols().constructor()),
				MethodReturn.REFERENCE
		)));
	}
	
	public static void buildSetterCopy(StructType struct, Builders builders) {
		builders.struct = builders.struct.define(struct.structSymbols().setterCopy()).intercept(new SimpleImplementation(method -> new Compound(
				forField(GLOBAL_STRUCT_UNSAFE).read(),
				REFERENCE.loadFrom(1),
				forField(GLOBAL_STRUCT_ADDRESS).read(),
				loadThis(),
				forField(GLOBAL_STRUCT_ADDRESS).read(),
				forField(struct.constantSymbols().sizeOf()).read(),
				PrimitiveWideningDelegate.forPrimitive(ForLoadedType.of(int.class)).widenTo(ForLoadedType.of(long.class)),
				invoke(GLOBAL_UNSAFE_COPY_MEMORY),
				MethodReturn.VOID
		)));
	}
}
