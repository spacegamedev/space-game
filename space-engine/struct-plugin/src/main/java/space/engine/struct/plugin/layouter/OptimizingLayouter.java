package space.engine.struct.plugin.layouter;

import space.engine.struct.plugin.layouter.Layout.LayoutMember;
import space.engine.struct.plugin.layouter.LayouterValidator.Options;
import space.engine.struct.plugin.types.Member;
import space.engine.struct.plugin.types.StructType;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * This Layouter optimizes the entire struct by also rearranging entries.
 */
public class OptimizingLayouter extends AbstractLayouter {
	
	@Override
	protected EnumSet<Options> validationOptions() {
		return EnumSet.of(Options.ALLOW_SIZE_0);
	}
	
	@Override
	protected Layout computeLayoutNoValidation(StructType struct) {
		ArrayList<Member> members = new ArrayList<>(struct.members());
		if (members.isEmpty())
			return Layout.empty(struct);
		
		List<LayoutMember> out = new ArrayList<>();
		boolean[] used = new boolean[members.size()];
		int offset = 0;
		
		outer:
		for (int i = 0; i < members.size(); ) {
			if (used[i]) {
				i++;
				continue;
			}
			
			//find next fitting member
			int leastAlign = Integer.MAX_VALUE;
			for (int j = i; j < members.size(); j++) {
				if (used[j])
					continue;
				
				Member m = members.get(j);
				if (offset % m.alignment() == 0) {
					out.add(new LayoutMember(m, offset));
					offset += m.sizeOf();
					used[j] = true;
					continue outer;
				}
				leastAlign = Math.min(leastAlign, m.alignment());
			}
			
			//add padding to next member
			offset = (offset + leastAlign - 1) & -leastAlign;
		}
		
		int alignment = members.stream().mapToInt(Member::alignment).max().orElseThrow();
		return new Layout(struct, out, offset, alignment);
	}
}
