package space.engine.struct.plugin;

import org.codehaus.groovy.runtime.DefaultGroovyMethods;
import org.gradle.api.GradleException;
import org.gradle.api.NonNullApi;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.api.tasks.compile.JavaCompile;
import org.gradle.plugins.ide.idea.IdeaPlugin;
import org.gradle.plugins.ide.idea.model.IdeaModule;
import org.gradle.util.GUtil;
import space.engine.platform.DecorationExtension;
import space.engine.plugin.IncrementalTask;

import static java.util.Objects.requireNonNull;

@NonNullApi
public class StructPlugin implements Plugin<Project> {
	
	public static final String STRUCT_IDEA_SRC_TYPE = "structIdeaSrcType";
	public static final String STRUCT_IDEA_SRC_TYPE_TEST = "test";
	public static final String STRUCT_IDEA_SRC_TYPE_SRC = "src";
	public static final String STRUCT_IDEA_SRC_TYPE_NONE = "none";
	
	@Override
	public void apply(Project project) {
		project.getPlugins().apply("space-platform-base");
		
		DirectoryProperty buildDirBase = project.getObjects().directoryProperty().convention(project.getLayout().getBuildDirectory().dir("struct"));
		JavaPluginExtension javaExtension = project.getExtensions().getByType(JavaPluginExtension.class);
		javaExtension.getSourceSets().all(sourceSet -> {
			DecorationExtension decoration = sourceSet.getExtensions().getByType(DecorationExtension.class);
			DirectoryProperty buildDir = project.getObjects().directoryProperty().convention(buildDirBase.dir(sourceSet.getName()));
			
			//SourceDirectorySet struct
			SourceDirectorySet structSource = project.getObjects().sourceDirectorySet("struct", GUtil.toWords(sourceSet.getName()) + " Struct source");
			structSource.srcDir("src/" + sourceSet.getName() + "/struct");
			structSource.getFilter().include("**/*.java");
			sourceSet.getExtensions().add("struct", structSource);
			
			//SourceDirectorySet idea integration
			project.getPlugins().withType(IdeaPlugin.class, ideaPlugin -> {
				IdeaModule module = ideaPlugin.getModel().getModule();
				Object structIdeaSrcType = project.getRootProject().findProperty(STRUCT_IDEA_SRC_TYPE);
				if (structIdeaSrcType == null || STRUCT_IDEA_SRC_TYPE_TEST.equals(structIdeaSrcType)) {
					module.setTestSourceDirs(DefaultGroovyMethods.plus(module.getTestSourceDirs(), structSource.getSrcDirs()));
				} else if (STRUCT_IDEA_SRC_TYPE_SRC.equals(structIdeaSrcType)) {
					module.setSourceDirs(DefaultGroovyMethods.plus(module.getSourceDirs(), structSource.getSrcDirs()));
				} else if (!STRUCT_IDEA_SRC_TYPE_NONE.equals(structIdeaSrcType)) {
					throw new GradleException("property '" + STRUCT_IDEA_SRC_TYPE + "' has invalid value '" + structIdeaSrcType + "'!\n\t" +
													  "Valid values: [" + STRUCT_IDEA_SRC_TYPE_TEST + " (default), " + STRUCT_IDEA_SRC_TYPE_SRC + ", " + STRUCT_IDEA_SRC_TYPE_NONE + "]");
				}
				
				module.setSourceDirs(DefaultGroovyMethods.plus(module.getSourceDirs(), project.file(buildDir.dir("process"))));
			});
			
			//compileStruct
			TaskProvider<JavaCompile> taskCompileStruct = project.getTasks().register(sourceSet.getTaskName("compile", "Struct"), JavaCompile.class, t -> {
				t.setDescription("Compiles " + sourceSet + " structs.");
				t.setSource(structSource);
				t.setClasspath(decoration.symbolGenClasspathConfiguration);
				t.getDestinationDirectory().convention(buildDir.dir("compileJava"));
			});
			
			//processStruct
			TaskProvider<StructCompiler> taskProcessStruct = project.getTasks().register(sourceSet.getTaskName("process", "Struct"), StructCompiler.class, t -> {
				t.setDescription("Processes " + sourceSet + " structs.");
				t.getSource().from(taskCompileStruct.map(t2 -> requireNonNull(t2.getDestinationDirectory())));
				t.getCacheDir().convention(buildDir.dir("process-cache"));
				t.getOutputDirectory().convention(buildDir.dir("process"));
			});
			decoration.addSymbolGen(taskProcessStruct, IncrementalTask::configureSymbolGen);
		});
	}
	
	private static final String CONFIGURATION_STRUCT_CLASSPATH = "StructClasspath";
	private static final String CONFIGURATION_STRUCT_OUTPUT = "StructOutput";
	
	public static String getStructClasspathConfigurationName(SourceSet sourceSet) {
		if (sourceSet.getName().equals("main"))
			return Character.toLowerCase(CONFIGURATION_STRUCT_CLASSPATH.charAt(0)) + CONFIGURATION_STRUCT_CLASSPATH.substring(1);
		return sourceSet.getName() + CONFIGURATION_STRUCT_CLASSPATH;
	}
	
	public static String getStructOutputConfigurationName(SourceSet sourceSet) {
		if (sourceSet.getName().equals("main"))
			return Character.toLowerCase(CONFIGURATION_STRUCT_OUTPUT.charAt(0)) + CONFIGURATION_STRUCT_OUTPUT.substring(1);
		return sourceSet.getName() + CONFIGURATION_STRUCT_OUTPUT;
	}
}
