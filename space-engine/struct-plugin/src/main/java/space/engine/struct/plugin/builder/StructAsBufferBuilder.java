package space.engine.struct.plugin.builder;

import net.bytebuddy.implementation.bytecode.StackManipulation.Compound;
import net.bytebuddy.implementation.bytecode.constant.LongConstant;
import net.bytebuddy.implementation.bytecode.member.MethodReturn;
import space.engine.plugin.SimpleImplementation;
import space.engine.struct.plugin.Builders;
import space.engine.struct.plugin.types.StructType;

import static net.bytebuddy.implementation.bytecode.member.FieldAccess.forField;
import static net.bytebuddy.implementation.bytecode.member.MethodInvocation.invoke;
import static net.bytebuddy.implementation.bytecode.member.MethodVariableAccess.loadThis;
import static space.engine.struct.plugin.GlobalSymbols.GLOBAL_STRUCT_ADDRESS;

public class StructAsBufferBuilder {
	
	public static void build(StructType struct, Builders builders) {
		if (!builders.hasBuffer())
			return;
		
		builders.struct = builders.struct.define(struct.structSymbols().asBuffer()).intercept(new SimpleImplementation(method -> new Compound(
				loadThis(),
				forField(GLOBAL_STRUCT_ADDRESS).read(),
				LongConstant.forValue(1),
				loadThis(),
				invoke(struct.bufferSymbols().wrap()),
				MethodReturn.REFERENCE
		)));
	}
}
