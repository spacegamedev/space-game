package space.engine.struct.plugin.types;

import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.type.TypeDescription.Generic;

public interface MemberSymbols {
	
	MethodDescription.InDefinedShape getter(String name, Generic ret);
	
	MethodDescription.InDefinedShape getterShort(String name, Generic ret);
	
	MethodDescription.InDefinedShape justGet(String name, Generic ret);
	
	MethodDescription.InDefinedShape setter(String name, Generic param);
}
