package space.engine.struct.plugin.types;

import net.bytebuddy.description.annotation.AnnotationDescription;
import net.bytebuddy.description.annotation.AnnotationDescription.Loadable;
import net.bytebuddy.description.annotation.AnnotationList;
import net.bytebuddy.description.annotation.AnnotationSource;
import net.bytebuddy.description.field.FieldDescription;
import net.bytebuddy.description.field.FieldDescription.InDefinedShape;
import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.method.MethodDescription.Latent;
import net.bytebuddy.description.method.ParameterDescription.Token;
import net.bytebuddy.description.modifier.ModifierContributor.ForMethod;
import net.bytebuddy.description.modifier.ModifierContributor.Resolver;
import net.bytebuddy.description.type.TypeDefinition;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.description.type.TypeDescription.AbstractBase.OfSimpleType;
import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import net.bytebuddy.description.type.TypeDescription.Generic;
import net.bytebuddy.utility.CompoundList;
import space.engine.struct.Allocator;
import space.engine.struct.Struct;
import space.engine.struct.StructMembers;
import space.engine.struct.StructSize;
import space.engine.struct.annotations.BufferStream.StreamType;
import space.engine.struct.annotations.NioWrapperMethod.NioTypes;
import space.engine.struct.annotations.SetAllMethod.SetAllMethodMode;
import space.engine.struct.plugin.layouter.AbstractLayouter;
import space.engine.struct.plugin.layouter.Layout;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static net.bytebuddy.description.modifier.FieldManifestation.FINAL;
import static net.bytebuddy.description.modifier.Ownership.STATIC;
import static net.bytebuddy.description.modifier.Visibility.*;
import static net.bytebuddy.matcher.ElementMatchers.*;
import static space.engine.struct.plugin.GlobalSymbols.*;

public interface StructType {
	
	String name();
	
	AnnotationList annotations();
	
	List<Member> members();
	
	Size size();
	
	TypeDescription struct();
	
	TypeDescription buffer();
	
	ConstantSymbols constantSymbols();
	
	StructSymbols structSymbols();
	
	BufferSymbols bufferSymbols();
	
	static StructType of(TypeDescription typeDescription) {
		if (!typeDescription.isAssignableTo(Struct.class))
			return new ForSource(typeDescription);
		AnnotationDescription structMembers = typeDescription.getDeclaredAnnotations().ofType(ForLoadedType.of(StructMembers.class));
		if (structMembers != null)
			return new ForDecorated(typeDescription, structMembers);
		return new ForCustom(typeDescription);
	}
	
	abstract class AbstractStructType implements StructType {
		
		final TypeDescription struct;
		
		public AbstractStructType(TypeDescription struct) {
			this.struct = struct;
		}
		
		@Override
		public String name() {
			return struct.getName();
		}
		
		@Override
		public AnnotationList annotations() {
			return struct.getDeclaredAnnotations();
		}
		
		@Override
		public TypeDescription struct() {
			return struct;
		}
		
		@Override
		public TypeDescription buffer() {
			return new OfSimpleType.WithDelegation() {
				@Override
				protected TypeDescription delegate() {
					return struct;
				}
				
				@Override
				public String getName() {
					return struct.getName() + "$Buffer";
				}
				
				@Override
				public TypeDescription getDeclaringType() {
					return struct;
				}
				
				@Override
				public TypeDescription getEnclosingType() {
					return struct;
				}
				
				@Override
				public int getModifiers() {
					return super.getModifiers() | STATIC.getRange();
				}
				
				@Override
				public int getActualModifiers(boolean superFlag) {
					return super.getActualModifiers(superFlag) | STATIC.getRange();
				}
			};
		}
		
		@Override
		public String toString() {
			return this.getClass().getSimpleName() + "[" + struct.toString() + "]";
		}
		
		@Override
		public ConstantSymbols constantSymbols() {
			return new ConstantSym();
		}
		
		public class ConstantSym implements ConstantSymbols {
			
			@Override
			public InDefinedShape sizeOf() {
				return new FieldDescription.Latent(
						struct.asErasure(),
						"SIZEOF",
						Resolver.of(PUBLIC, STATIC, FINAL).resolve(),
						ForLoadedType.of(int.class).asGenericType(),
						null
				);
			}
			
			@Override
			public InDefinedShape alignment() {
				return new FieldDescription.Latent(
						struct.asErasure(),
						"ALIGNMENT",
						Resolver.of(PUBLIC, STATIC, FINAL).resolve(),
						ForLoadedType.of(int.class).asGenericType(),
						null
				);
			}
			
			@Override
			public InDefinedShape offset(Member member) {
				return new FieldDescription.Latent(
						struct.asErasure(),
						"OFFSET_" + member.name().toUpperCase(),
						Resolver.of(PUBLIC, STATIC, FINAL).resolve(),
						ForLoadedType.of(int.class).asGenericType(),
						null
				);
			}
		}
		
		//StructSymbols
		@Override
		public StructSymbols structSymbols() {
			return new StructSym();
		}
		
		public class StructSym implements StructSymbols {
			
			@Override
			public MethodDescription.InDefinedShape sizeOfVirtual() {
				return new MethodDescription.Latent(
						struct.asErasure(),
						"sizeOf",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						ForLoadedType.of(long.class).asGenericType(),
						List.of(),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape constructor() {
				return new MethodDescription.Latent(
						struct.asErasure(),
						MethodDescription.CONSTRUCTOR_INTERNAL_NAME,
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						TypeDescription.VOID.asGenericType(),
						List.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "address", null),
								new Token(ForLoadedType.of(space.engine.struct.Struct.class).asGenericType(), "root", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape allocCopy() {
				return new MethodDescription.Latent(
						struct.asErasure(),
						"alloc",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						struct.asGenericType(),
						List.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null),
								new Token(struct.asGenericType(), "struct", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape malloc() {
				return new MethodDescription.Latent(
						struct.asErasure(),
						"malloc",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						struct.asGenericType(),
						List.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape calloc() {
				return new MethodDescription.Latent(
						struct.asErasure(),
						"calloc",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						struct.asGenericType(),
						List.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape create() {
				return new MethodDescription.Latent(
						struct.asErasure(),
						"create",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						struct.asGenericType(),
						List.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "address", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape wrap() {
				return new MethodDescription.Latent(
						struct.asErasure(),
						"wrap",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						struct.asGenericType(),
						List.of(new Token(ForLoadedType.of(long.class).asGenericType(), "address", null),
								new Token(ForLoadedType.of(space.engine.struct.Struct.class).asGenericType(), "root", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape asBuffer() {
				return new MethodDescription.Latent(
						struct.asErasure(),
						"asBuffer",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						buffer().asGenericType(),
						List.of(),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape setterCopy() {
				return new MethodDescription.Latent(
						struct.asErasure(),
						"set",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						TypeDescription.VOID.asGenericType(),
						List.of(new Token(struct.asGenericType(), "from", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			public MethodDescription.InDefinedShape setterAll(SetAllMethodMode mode, List<Token> params) {
				return new Latent(
						struct.asErasure(),
						"set",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						TypeDescription.VOID.asGenericType(),
						params,
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape alloc(SetAllMethodMode mode, List<Token> params) {
				return new Latent(
						struct.asErasure(),
						"alloc",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						struct.asGenericType(),
						Stream.concat(
								Stream.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null)),
								params.stream()).collect(Collectors.toList()
						),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape nioBuffer(NioTypes nioType) {
				return new MethodDescription.Latent(
						struct,
						nioType.methodName,
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						ForLoadedType.of(nioType.bufferClass).asGenericType(),
						List.of(),
						List.of(),
						List.of(),
						null,
						null
				);
			}
		}
		
		@Override
		public BufferSymbols bufferSymbols() {
			return new BufferSym();
		}
		
		public class BufferSym implements BufferSymbols {
			
			@Override
			public MethodDescription.InDefinedShape.InDefinedShape sizeOfVirtual() {
				return new MethodDescription.Latent(
						buffer(),
						"sizeOf",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						ForLoadedType.of(long.class).asGenericType(),
						List.of(),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape constructor() {
				return new MethodDescription.Latent(
						buffer(),
						MethodDescription.CONSTRUCTOR_INTERNAL_NAME,
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						TypeDescription.VOID.asGenericType(),
						List.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "address", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "length", null),
								new Token(ForLoadedType.of(Struct.class).asGenericType(), "root", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape malloc() {
				return new MethodDescription.Latent(
						buffer(),
						"malloc",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						buffer().asGenericType(),
						List.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "length", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape calloc() {
				return new MethodDescription.Latent(
						buffer(),
						"calloc",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						buffer().asGenericType(),
						List.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "length", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape create() {
				return new MethodDescription.Latent(
						buffer(),
						"create",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						buffer().asGenericType(),
						List.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "address", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "length", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape wrap() {
				return new MethodDescription.Latent(
						buffer(),
						"wrap",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						buffer().asGenericType(),
						List.of(new Token(ForLoadedType.of(long.class).asGenericType(), "address", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "length", null),
								new Token(ForLoadedType.of(Struct.class).asGenericType(), "root", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape getStruct() {
				return new MethodDescription.Latent(
						buffer(),
						"getStruct",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						struct.asGenericType(),
						List.of(new Token(ForLoadedType.of(long.class).asGenericType(), "index", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape get(Generic ret) {
				return new MethodDescription.Latent(
						buffer(),
						"get",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						ret,
						List.of(new Token(ForLoadedType.of(long.class).asGenericType(), "index", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape setterCopy() {
				return new MethodDescription.Latent(
						buffer(),
						"set",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						TypeDescription.VOID.asGenericType(),
						List.of(new Token(ForLoadedType.of(long.class).asGenericType(), "index", null),
								new Token(struct.asGenericType(), "struct", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape setterAll(StructType struct, SetAllMethodMode mode, List<Token> params) {
				return new MethodDescription.Latent(
						buffer(),
						"set",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						TypeDescription.VOID.asGenericType(),
						CompoundList.of(new Token(ForLoadedType.of(long.class).asGenericType(), "index", null), params),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape subBuffer() {
				return new MethodDescription.Latent(
						buffer(),
						"subBuffer",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						buffer().asGenericType(),
						List.of(new Token(ForLoadedType.of(long.class).asGenericType(), "offset", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "length", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape arraySetFull(TypeDefinition srcType) {
				return arraySetFull(srcType, false);
			}
			
			@Override
			public MethodDescription.InDefinedShape arraySetFull(TypeDefinition srcType, boolean srcIndexInt) {
				TypeDescription.Generic srcIndexType = ForLoadedType.of(srcIndexInt ? int.class : long.class).asGenericType();
				return new MethodDescription.Latent(
						buffer(),
						"set",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						Generic.VOID,
						List.of(new Token(srcType.asGenericType(), "buffer", null),
								new Token(srcIndexType, "srcIndex", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "destIndex", null),
								new Token(srcIndexType, "length", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape arraySetShort(TypeDefinition srcType) {
				return new MethodDescription.Latent(
						buffer(),
						"set",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						Generic.VOID,
						List.of(new Token(srcType.asGenericType(), "src", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape arrayGetFull(TypeDefinition destType) {
				return arrayGetFull(destType, false);
			}
			
			@Override
			public MethodDescription.InDefinedShape arrayGetFull(TypeDefinition destType, boolean destIndexInt) {
				TypeDescription.Generic destIndexType = ForLoadedType.of(destIndexInt ? int.class : long.class).asGenericType();
				return new MethodDescription.Latent(
						buffer(),
						"get",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						Generic.VOID,
						List.of(new Token(ForLoadedType.of(long.class).asGenericType(), "srcIndex", null),
								new Token(destType.asGenericType(), "dest", null),
								new Token(destIndexType, "destIndex", null),
								new Token(destIndexType, "length", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape arrayGetShort(TypeDefinition destType) {
				return new MethodDescription.Latent(
						buffer(),
						"get",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						Generic.VOID,
						List.of(new Token(destType.asGenericType(), "dest", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape arrayAlloc(TypeDefinition srcType) {
				return new MethodDescription.Latent(
						buffer(),
						"alloc",
						Resolver.of((ForMethod) PUBLIC, STATIC).resolve(),
						List.of(),
						buffer().asGenericType(),
						List.of(new Token(ForLoadedType.of(Allocator.class).asGenericType(), "allocator", null),
								new Token(srcType.asGenericType(), "array", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape stream(StreamType streamType) {
				return new MethodDescription.Latent(
						buffer(),
						"stream",
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						streamType == StreamType.STRUCT
								? new TypeDescription.Generic.OfParameterizedType.Latent(ForLoadedType.of(streamType.streamClass), null, List.of(struct().asGenericType()), AnnotationSource.Empty.INSTANCE)
								: ForLoadedType.of(streamType.streamClass).asGenericType(),
						List.of(),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape streamGetter(StreamType streamType) {
				return new MethodDescription.Latent(
						buffer(),
						"streamGetter",
						Resolver.of((ForMethod) PROTECTED).resolve(),
						List.of(),
						ForLoadedType.of(streamType.elementClass).asGenericType(),
						List.of(new Token(ForLoadedType.of(long.class).asGenericType(), "index", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape nioBuffer(NioTypes nioType) {
				return new MethodDescription.Latent(
						buffer(),
						nioType.methodName,
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						ForLoadedType.of(nioType.bufferClass).asGenericType(),
						List.of(),
						List.of(),
						List.of(),
						null,
						null
				);
			}
			
			@Override
			public MethodDescription.InDefinedShape nioBufferOffsetLength(NioTypes nioType) {
				return new MethodDescription.Latent(
						buffer(),
						nioType.methodName,
						Resolver.of((ForMethod) PUBLIC).resolve(),
						List.of(),
						ForLoadedType.of(nioType.bufferClass).asGenericType(),
						List.of(new Token(ForLoadedType.of(long.class).asGenericType(), "offset", null),
								new Token(ForLoadedType.of(long.class).asGenericType(), "length", null)),
						List.of(),
						List.of(),
						null,
						null
				);
			}
		}
	}
	
	class ForSource extends AbstractStructType {
		
		private final List<Member> members;
		private final Layout layout;
		
		public ForSource(TypeDescription typeDescription) {
			super(typeDescription);
			this.members = typeDescription.getDeclaredFields()
										  .filter(not(isStatic()))
										  .stream()
										  .map(f -> Member.of(this, f.getType(), f.getName(), f.getDeclaredAnnotations()))
										  .collect(Collectors.toList());
			layout = AbstractLayouter.layout(this);
		}
		
		@Override
		public List<Member> members() {
			return members;
		}
		
		@Override
		public Size size() {
			return layout;
		}
		
		public Layout layout() {
			return layout;
		}
	}
	
	abstract class AbstractSizedStructType extends AbstractStructType implements Size {
		
		private final int sizeOf;
		private final int alignment;
		
		public AbstractSizedStructType(TypeDescription struct) {
			super(struct);
			
			Loadable<StructSize> loadable = struct.getDeclaredAnnotations().ofType(StructSize.class);
			if (loadable == null)
				throw new RuntimeException("Struct " + struct.getName() + " is missing the @StructSize annotation and can therefore not be used as a member!");
			StructSize size = loadable.load();
			this.sizeOf = size.sizeOf();
			this.alignment = size.alignment();
		}
		
		@Override
		public Size size() {
			return this;
		}
		
		@Override
		public int sizeOf() {
			return sizeOf;
		}
		
		@Override
		public int alignment() {
			return alignment;
		}
	}
	
	class ForDecorated extends AbstractSizedStructType {
		
		private final List<Member> members;
		
		public ForDecorated(TypeDescription typeDescription, AnnotationDescription structMembers) {
			super(typeDescription);
			
			String[] names = (String[]) structMembers.getValue(GLOBAL_STRUCT_MEMBERS_NAMES).resolve();
			TypeDescription[] types = (TypeDescription[]) structMembers.getValue(GLOBAL_STRUCT_MEMBERS_TYPES).resolve();
			if (names.length != types.length)
				throw new RuntimeException("Illegal StructMembers Annotation on " + name() + ": names and types have different lengths!");
			
			this.members = IntStream.range(0, names.length)
									.mapToObj(i -> Member.of(this, types[i].asGenericType(), names[i], new AnnotationList.Empty()))
									.collect(Collectors.toList());
		}
		
		@Override
		public List<Member> members() {
			return members;
		}
	}
	
	class ForCustom extends AbstractSizedStructType {
		
		public ForCustom(TypeDescription typeDescription) {
			super(typeDescription);
		}
		
		@Override
		public List<Member> members() {
			return List.of();
		}
	}
}
