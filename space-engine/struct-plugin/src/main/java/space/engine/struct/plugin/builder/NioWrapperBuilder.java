package space.engine.struct.plugin.builder;

import net.bytebuddy.description.annotation.AnnotationDescription.Loadable;
import net.bytebuddy.description.method.MethodDescription.InDefinedShape;
import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import net.bytebuddy.implementation.bytecode.Multiplication;
import net.bytebuddy.implementation.bytecode.Removal;
import net.bytebuddy.implementation.bytecode.StackManipulation.Compound;
import net.bytebuddy.implementation.bytecode.constant.LongConstant;
import net.bytebuddy.implementation.bytecode.member.MethodReturn;
import org.jetbrains.annotations.NotNull;
import space.engine.plugin.SimpleImplementation;
import space.engine.struct.NioBufferWrapper;
import space.engine.struct.Struct;
import space.engine.struct.annotations.NioWrapperMethod;
import space.engine.struct.annotations.NioWrapperMethod.NioTypes;
import space.engine.struct.plugin.Builders;
import space.engine.struct.plugin.types.StructType;

import java.util.Arrays;
import java.util.stream.Collectors;

import static net.bytebuddy.implementation.bytecode.member.FieldAccess.forField;
import static net.bytebuddy.implementation.bytecode.member.MethodInvocation.invoke;
import static net.bytebuddy.implementation.bytecode.member.MethodVariableAccess.*;
import static net.bytebuddy.matcher.ElementMatchers.*;
import static space.engine.struct.plugin.GlobalSymbols.*;

public class NioWrapperBuilder {
	
	public static void build(StructType struct, Builders builders) {
		Loadable<NioWrapperMethod> nioWrapperAnnotation = struct.annotations().ofType(NioWrapperMethod.class);
		NioTypes[] nioTypes = nioWrapperAnnotation != null ? nioWrapperAnnotation.load().value() : new NioTypes[] {NioTypes.GENERIC};
		for (NioTypes nioType : nioTypes) {
			int elementCountPerIndex = elementCountPerIndex(struct, nioType, builders);
			buildForStruct(struct, nioType, elementCountPerIndex, builders);
			buildForBuffer(struct, nioType, elementCountPerIndex, builders);
		}
	}
	
	public static int elementCountPerIndex(StructType struct, NioTypes nioType, Builders builders) {
		int sizeOf = struct.size().sizeOf();
		if (sizeOf % nioType.elementSizeOf != 0)
			throw new RuntimeException("Struct sizeOf " + sizeOf + " is not a multiple of NioTypes " + nioType + " elementSize " + nioType.elementSizeOf + "!");
		return sizeOf / nioType.elementSizeOf;
	}
	
	public static void buildForStruct(StructType struct, NioTypes nioType, int elementCountPerIndex, Builders builders) {
		builders.struct = builders.struct.define(struct.structSymbols().nioBuffer(nioType)).intercept(new SimpleImplementation(method -> new Compound(
				loadThis(),
				LongConstant.forValue(elementCountPerIndex),
				invoke(wrapMethod(nioType, Struct.class, long.class)),
				MethodReturn.REFERENCE
		)));
	}
	
	public static void buildForBuffer(StructType struct, NioTypes nioType, int elementCountPerIndex, Builders builders) {
		if (!builders.hasBuffer())
			return;
		builders.buffer = builders.buffer.define(struct.bufferSymbols().nioBuffer(nioType)).intercept(new SimpleImplementation(method -> new Compound(
				loadThis(),
				loadThis(),
				forField(GLOBAL_BUFFER_LENGTH).read(),
				LongConstant.forValue(elementCountPerIndex),
				Multiplication.LONG,
				invoke(wrapMethod(nioType, Struct.class, long.class)),
				MethodReturn.REFERENCE
		)));
		builders.buffer = builders.buffer.define(struct.bufferSymbols().nioBufferOffsetLength(nioType)).intercept(new SimpleImplementation(method -> new Compound(
				LONG.loadFrom(1),
				LONG.loadFrom(3),
				loadThis(),
				forField(GLOBAL_BUFFER_LENGTH).read(),
				invoke(GLOBAL_STRUCT_CHECK_FROM_INDEX_SIZE),
				Removal.DOUBLE,
				
				loadThis(),
				LONG.loadFrom(1),
				LongConstant.forValue(elementCountPerIndex),
				Multiplication.LONG,
				LONG.loadFrom(3),
				LongConstant.forValue(elementCountPerIndex),
				Multiplication.LONG,
				invoke(wrapMethod(nioType, Struct.class, long.class, long.class)),
				MethodReturn.REFERENCE
		)));
	}
	
	@NotNull
	private static InDefinedShape wrapMethod(NioTypes nioType, Class<?>... arguments) {
		InDefinedShape wrapMethod;
		try {
			wrapMethod = ForLoadedType.of(NioBufferWrapper.class).getDeclaredMethods().filter(named(nioType.wrapName).and(isStatic()).and(takesArguments(arguments))).getOnly();
		} catch (IllegalStateException e) {
			throw new RuntimeException("NioWrapper wrap function '" + nioType.wrapName + "(" + (Arrays.stream(arguments).map(Class::getSimpleName).collect(Collectors.joining(", "))) + ");' was not found!", e);
		}
		if (!wrapMethod.getReturnType().asErasure().isAssignableTo(nioType.bufferClass))
			throw new RuntimeException("NioWrapper wrap function returned type " + wrapMethod.getReturnType() + " is not assignable to it's declared bufferClass '" + nioType.bufferClass + "'!");
		return wrapMethod;
	}
}
