package space.engine.struct.plugin;

import net.bytebuddy.dynamic.DynamicType.Builder;
import net.bytebuddy.dynamic.DynamicType.Unloaded;

import java.util.List;

public class Builders {
	
	public Builder<?> struct;
	public Builder<?> buffer;
	
	public List<Unloaded<?>> make() {
		return hasBuffer() ? List.of(
				struct.make(),
				buffer.make()
		) : List.of(struct.make());
	}
	
	public boolean hasBuffer() {
		return buffer != null;
	}
}
