package space.engine.struct.plugin.builder;

import net.bytebuddy.description.annotation.AnnotationDescription;
import space.engine.struct.StructSize;
import space.engine.struct.plugin.Builders;
import space.engine.struct.plugin.layouter.Layout;
import space.engine.struct.plugin.types.StructType.ForSource;

import static space.engine.plugin.ByteBuddyUtils.reduceMergeUnsupported;

public class ConstantBuilder {
	
	public static void build(ForSource struct, Builders builders) {
		buildConstantFields(struct, builders);
		buildSizeAnnotation(struct, builders);
	}
	
	public static void buildConstantFields(ForSource struct, Builders builders) {
		Layout layout = struct.layout();
		builders.struct = builders.struct.define(struct.constantSymbols().sizeOf()).value(layout.sizeOf)
										 .define(struct.constantSymbols().alignment()).value(layout.alignment);
		builders.struct = layout.members.stream().reduce(builders.struct, (b, f) -> b.define(f.member.offsetConstant()).value(f.offset), reduceMergeUnsupported());
	}
	
	public static void buildSizeAnnotation(ForSource struct, Builders builders) {
		builders.struct = builders.struct.annotateType(AnnotationDescription.
				Builder.ofType(StructSize.class)
					   .define("sizeOf", struct.size().sizeOf())
					   .define("alignment", struct.size().alignment())
					   .build()
		);
	}
}
