package space.engine.struct.plugin.layouter;

import space.engine.struct.plugin.layouter.Layout.LayoutMember;
import space.engine.struct.plugin.layouter.LayouterValidator.Options;
import space.engine.struct.plugin.types.Member;
import space.engine.struct.plugin.types.StructType;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import static space.engine.struct.plugin.layouter.LayouterValidator.Options.ALLOW_SIZE_0;

/**
 * This Layouter does no padding and expects the user to do padding manually.
 */
public class NoPaddingLayouter extends AbstractLayouter {
	
	@Override
	protected EnumSet<Options> validationOptions() {
		return EnumSet.of(ALLOW_SIZE_0);
	}
	
	@Override
	protected Layout computeLayoutNoValidation(StructType struct) {
		List<LayoutMember> members = new ArrayList<>();
		int offset = 0;
		int alignment = 0;
		for (Member m : struct.members()) {
			members.add(new LayoutMember(m, offset));
			offset += m.sizeOf();
			alignment = Math.max(alignment, m.alignment());
		}
		return new Layout(struct, members, offset, alignment);
	}
}
