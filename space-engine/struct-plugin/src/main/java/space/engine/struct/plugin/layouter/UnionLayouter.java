package space.engine.struct.plugin.layouter;

import space.engine.struct.plugin.layouter.Layout.LayoutMember;
import space.engine.struct.plugin.layouter.LayouterValidator.Options;
import space.engine.struct.plugin.types.Member;
import space.engine.struct.plugin.types.StructType;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This Layouter optimizes the entire struct by also rearranging entries.
 */
public class UnionLayouter extends AbstractLayouter {
	
	@Override
	protected EnumSet<Options> validationOptions() {
		return EnumSet.of(Options.ALLOW_SIZE_0, Options.ALLOW_MEMBER_OVERLAP);
	}
	
	@Override
	protected Layout computeLayoutNoValidation(StructType struct) {
		ArrayList<Member> members = new ArrayList<>(struct.members());
		if (members.isEmpty())
			return Layout.empty(struct);
		
		int sizeOf = members.stream().mapToInt(Member::sizeOf).max().orElseThrow();
		int alignment = members.stream().mapToInt(Member::alignment).max().orElseThrow();
		List<LayoutMember> out = members.stream().map(m -> new LayoutMember(m, 0)).collect(Collectors.toList());
		return new Layout(struct, out, sizeOf, alignment);
	}
}
