package space.engine.struct.plugin.layouter;

import net.bytebuddy.description.annotation.AnnotationDescription.Loadable;
import org.jetbrains.annotations.NotNull;
import space.engine.struct.annotations.Layouter;
import space.engine.struct.annotations.Layouter.Layouters;
import space.engine.struct.annotations.Union;
import space.engine.struct.plugin.layouter.LayouterValidator.Options;
import space.engine.struct.plugin.types.StructType;

import java.util.EnumSet;

public abstract class AbstractLayouter {
	
	public final Layout computeLayout(StructType struct) {
		Layout layout = computeLayoutNoValidation(struct);
		LayouterValidator.validate(layout, validationOptions());
		return layout;
	}
	
	protected abstract EnumSet<Options> validationOptions();
	
	protected abstract Layout computeLayoutNoValidation(StructType struct);
	
	public static Layout layout(StructType struct) {
		return getLayouter(struct).computeLayout(struct);
	}
	
	public static @NotNull AbstractLayouter getLayouter(StructType struct) {
		if (struct.annotations().isAnnotationPresent(Union.class)) {
			if (struct.annotations().isAnnotationPresent(Layouter.class))
				throw new RuntimeException("Unions cannot have a set Layouter!");
			return new UnionLayouter();
		}
		
		Loadable<Layouter> annot = struct.annotations().ofType(Layouter.class);
		if (annot != null)
			return getLayouter(annot.load().value());
		else
			return getDefaultLayouter(struct);
	}
	
	public static @NotNull AbstractLayouter getLayouter(Layouters layouter) {
		switch (layouter) {
			case NO_PADDING:
				return new NoPaddingLayouter();
			case PADDING:
				return new PaddingLayouter();
			case OPTIMIZING:
				return new OptimizingLayouter();
			case SINGLE_MEMBER:
				return new SingleMemberLayouter();
		}
		throw new UnsupportedOperationException("Layouter " + layouter.name() + " does not have a AbstractLayouter assigned!");
	}
	
	//TODO default layouter will change later
	public static @NotNull AbstractLayouter getDefaultLayouter(StructType struct) {
		if (struct.members().size() == 1)
			return new SingleMemberLayouter();
		else
			return new OptimizingLayouter();
	}
}
