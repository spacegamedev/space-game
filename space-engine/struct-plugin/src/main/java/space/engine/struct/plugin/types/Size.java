package space.engine.struct.plugin.types;

public interface Size {
	
	int sizeOf();
	
	int alignment();
}
