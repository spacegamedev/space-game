package space.engine.struct.plugin.types;

import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.method.ParameterDescription.Token;
import net.bytebuddy.description.type.TypeDefinition;
import net.bytebuddy.description.type.TypeDescription;
import space.engine.struct.annotations.BufferStream.StreamType;
import space.engine.struct.annotations.NioWrapperMethod.NioTypes;
import space.engine.struct.annotations.SetAllMethod.SetAllMethodMode;

import java.util.List;

public interface BufferSymbols {
	
	MethodDescription.InDefinedShape sizeOfVirtual();
	
	MethodDescription.InDefinedShape constructor();
	
	MethodDescription.InDefinedShape malloc();
	
	MethodDescription.InDefinedShape calloc();
	
	MethodDescription.InDefinedShape create();
	
	MethodDescription.InDefinedShape wrap();
	
	MethodDescription.InDefinedShape getStruct();
	
	MethodDescription.InDefinedShape get(TypeDescription.Generic ret);
	
	MethodDescription.InDefinedShape setterCopy();
	
	MethodDescription.InDefinedShape setterAll(StructType struct, SetAllMethodMode mode, List<Token> params);
	
	MethodDescription.InDefinedShape subBuffer();
	
	MethodDescription.InDefinedShape arraySetFull(TypeDefinition srcType);
	
	MethodDescription.InDefinedShape arraySetFull(TypeDefinition srcType, boolean srcIndexInt);
	
	MethodDescription.InDefinedShape arraySetShort(TypeDefinition srcType);
	
	MethodDescription.InDefinedShape arrayGetFull(TypeDefinition destType);
	
	MethodDescription.InDefinedShape arrayGetFull(TypeDefinition destType, boolean destIndexInt);
	
	MethodDescription.InDefinedShape arrayGetShort(TypeDefinition destType);
	
	MethodDescription.InDefinedShape arrayAlloc(TypeDefinition srcType);
	
	MethodDescription.InDefinedShape stream(StreamType streamType);
	
	MethodDescription.InDefinedShape streamGetter(StreamType streamType);
	
	MethodDescription.InDefinedShape nioBuffer(NioTypes nioType);
	
	MethodDescription.InDefinedShape nioBufferOffsetLength(NioTypes nioType);
}
