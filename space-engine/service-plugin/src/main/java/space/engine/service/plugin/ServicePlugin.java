package space.engine.service.plugin;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.TaskProvider;
import space.engine.platform.DecorationExtension;

@SuppressWarnings("CodeBlock2Expr")
public class ServicePlugin implements Plugin<Project> {
	
	@Override
	public void apply(Project project) {
		project.getPlugins().apply("space-platform-base");
		
		DirectoryProperty buildDir = project.getObjects().directoryProperty().value(project.getLayout().getBuildDirectory().dir("decorate/service"));
		
		JavaPluginExtension javaExtension = project.getExtensions().getByType(JavaPluginExtension.class);
		javaExtension.getSourceSets().all(sourceSet -> {
			DecorationExtension decoration = sourceSet.getExtensions().getByType(DecorationExtension.class);
			
			TaskProvider<LinkServiceTargetsTask> linkServiceTargetsTask = project.getTasks().register(sourceSet.getTaskName("link", "ServiceTargets"), LinkServiceTargetsTask.class, t -> {
				t.getOutputDirectory().set(buildDir.dir(sourceSet.getName()));
			});
			decoration.addPackage(linkServiceTargetsTask, (t, runtimeClasspath) -> {
				t.getSource().from(runtimeClasspath);
				return t.getOutputDirectory();
			});
		});
	}
}
