package space.engine.service.plugin;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.annotation.AnnotationList;
import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.method.MethodDescription.InDefinedShape;
import net.bytebuddy.description.method.ParameterDescription;
import net.bytebuddy.description.modifier.Visibility;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import net.bytebuddy.description.type.TypeDescription.Generic;
import net.bytebuddy.dynamic.ClassFileLocator;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.dynamic.DynamicType.Unloaded;
import net.bytebuddy.dynamic.scaffold.subclass.ConstructorStrategy.Default;
import net.bytebuddy.implementation.Implementation.Composable;
import net.bytebuddy.implementation.MethodCall;
import net.bytebuddy.matcher.ElementMatchers;
import net.bytebuddy.pool.TypePool;
import net.bytebuddy.pool.TypePool.Resolution;
import org.gradle.api.DefaultTask;
import org.gradle.api.GradleException;
import org.gradle.api.file.ConfigurableFileCollection;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.PathSensitive;
import org.gradle.api.tasks.SkipWhenEmpty;
import org.gradle.api.tasks.TaskAction;
import org.jetbrains.annotations.NotNull;
import space.engine.plugin.ByteBuddyUtils;
import space.engine.service.AbstractTarget;
import space.engine.service.Service;
import space.engine.service.Target;
import space.engine.simpleQueue.ConcurrentLinkedSimpleQueue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;
import java.util.stream.StreamSupport;

import static org.gradle.api.tasks.PathSensitivity.RELATIVE;

public abstract class LinkServiceTargetsTask extends DefaultTask {
	
	@InputFiles
	@SkipWhenEmpty
	@PathSensitive(RELATIVE)
	public abstract @NotNull ConfigurableFileCollection getSource();
	
	@OutputDirectory
	public abstract @NotNull DirectoryProperty getOutputDirectory();
	
	static class FileUnit {
		
		final String className;
		final TypeDescription clazz;
		
		public FileUnit(String className, TypeDescription clazz) {
			this.className = className;
			this.clazz = clazz;
		}
		
		@Override
		public @NotNull String toString() {
			return className;
		}
	}
	
	static class ServiceUnit {
		
		final TypeDescription declClazz;
		final MethodDescription.InDefinedShape declMethod;
		final boolean isConstructor;
		final boolean skipServiceCheck;
		final boolean isTarget;
		
		TypeDescription.Generic service;
		String serviceVarName;
		TypeDescription.Generic[] dependenciesClasses;
		ServiceUnit[] dependencies;
		
		public ServiceUnit(TypeDescription declClazz, InDefinedShape declMethod) {
			this(declClazz, declMethod, false, false);
		}
		
		public ServiceUnit(TypeDescription declClazz, InDefinedShape declMethod, boolean skipServiceCheck, boolean isTarget) {
			this.declClazz = declClazz;
			this.declMethod = declMethod;
			this.isConstructor = declMethod.isConstructor();
			this.skipServiceCheck = skipServiceCheck;
			this.isTarget = isTarget;
		}
		
		public String[] dependenciesVarNames() {
			return Arrays.stream(dependencies).map(serviceUnit -> serviceUnit.serviceVarName).toArray(String[]::new);
		}
		
		@Override
		public String toString() {
			return service.getTypeName() + " (by " + declMethod.toString() + ")";
		}
	}
	
	@TaskAction
	protected void work() {
		ConcurrentLinkedSimpleQueue<String> error = new ConcurrentLinkedSimpleQueue<>();
		Throwable rethrow = null;
		try {
			work0(error);
		} catch (RuntimeException e) {
			rethrow = e;
		}
		
		if (rethrow != null)
			error.add("thrown Exception: " + rethrow.getMessage());
		if (error.hasEntry())
			throw new GradleException("Errors were encountered during " + this.getClass().getSimpleName() + ": \n" + error.stream().collect(Collectors.joining("\n")), rethrow);
	}
	
	protected void work0(ConcurrentLinkedSimpleQueue<String> error) throws RuntimeException {
		ClassFileLocator classFileLocator = ByteBuddyUtils.classFileLocatorFromClassPath(getSource());
		TypePool typePool = TypePool.Default.of(classFileLocator);
		
		InDefinedShape objectConstructor = typePool.describe(Object.class.getName()).resolve().getDeclaredMethods().stream().filter(c -> c.isConstructor() && c.getParameters().isEmpty())
												   .findAny().orElseThrow(() -> new RuntimeException("public Object() not found!"));
		
		//services discovery and validation
		Map<String, ServiceUnit> services = StreamSupport
				.stream(getSource().spliterator(), true)
				.flatMap(dir -> {
					Builder<String> ret = Stream.builder();
					(dir.isFile() ? getProject().zipTree(dir) : getProject().fileTree(dir)).visit(d -> {
						String className = ByteBuddyUtils.getClassNameFromFile(d.getRelativePath().getPathString());
						if (!d.isDirectory() && className != null)
							ret.add(className);
					});
					return ret.build();
				})
				.map(className -> {
					//load clazz
					Resolution resolution = typePool.describe(className);
					if (!resolution.isResolved()) {
						error.add(className + ": error: found as file but not in created classpath!");
						return null;
					}
					//resolution.resolve().getDeclaredMethods().stream().filter(o -> o.isConstructor()).forEach(o -> ((LazyMethodDescription) o).returnTypeDescriptor = o.getDeclaringType());
					return new FileUnit(className, resolution.resolve());
				})
				.filter(Objects::nonNull)
				.flatMap(unit -> {
					
					//check class / constructor
					Optional<ServiceUnit> constructorSelectedFromClass = Optional.empty();
					{
						AnnotationList annotations = unit.clazz.getDeclaredAnnotations();
						boolean clazzIsTarget = annotations.isAnnotationPresent(Target.class);
						if (clazzIsTarget || annotations.isAnnotationPresent(Service.class)) {
							InDefinedShape[] constructors = unit.clazz.getDeclaredMethods().stream().filter(mth -> mth.isConstructor() && mth.isPublic()).toArray(InDefinedShape[]::new);
							if (constructors.length == 0) {
								error.add(unit + ": Class is a " + (clazzIsTarget ? "@Target" : "@Service") + " but no public constructors are available");
							} else {
								long constructorsWithService = Arrays.stream(constructors).filter(c -> c.getDeclaredAnnotations().isAnnotationPresent(Service.class)).count();
								
								if (constructorsWithService > 1) {
									error.add(unit + ": Class has multiple public constructors, don't know which one to choose! Mark the correct one with @Service additionally.");
								} else if (constructorsWithService == 0) {
									constructorSelectedFromClass = Optional.of(new ServiceUnit(unit.clazz, constructors[0], true, clazzIsTarget));
								}
								//constructorsWithService == 1 -> will be found by method search
							}
						}
					}
					
					//check methods
					return Stream
							.concat(
									unit.clazz.getDeclaredMethods().stream().map(mth -> new ServiceUnit(unit.clazz, mth)),
									constructorSelectedFromClass.stream()
							)
							.filter(service -> {
								
								//find Methods with @Service
								if (!service.skipServiceCheck) {
									AnnotationList annotations = service.declMethod.getDeclaredAnnotations();
									if (annotations.isAnnotationPresent(Target.class)) {
										error.add(service + ": @Target on method is illegal. @Target is only allowed on classes");
										return false;
									}
									if (!(annotations.isAnnotationPresent(Service.class)))
										return false;
								}
								
								//get service infos
								service.service = service.isConstructor ? service.declClazz.asGenericType() : service.declMethod.getReturnType();
								service.serviceVarName = service.service.getTypeName().replace('/', '_').replace('.', '_');
								service.dependenciesClasses = service.declMethod.getParameters().stream().map(ParameterDescription::getType).toArray(TypeDescription.Generic[]::new);
								
								//validate method
								if (!service.declMethod.isPublic()) {
									error.add(service + ": Service method has to be public");
									return false;
								}
								if (!service.isConstructor) {
									if (!service.declMethod.isStatic()) {
										error.add(service + ": Service method has to be static");
										return false;
									}
									if (service.declMethod.isAbstract()) {
										error.add(service + ": Service method may not be abstract!");
										return false;
									}
								}
								
								//validate service
								if (!service.isConstructor) {
									if (service.service.equals(TypeDescription.Generic.VOID)) {
										error.add(service + ": Service method cannot return void!");
										return false;
									}
									if (service.service.isPrimitive()) {
										error.add(service + ": Service method cannot return a primitive!");
										return false;
									}
									if (service.service.isArray()) {
										error.add(service + ": Service method cannot return an array!");
										return false;
									}
								}
								
								//validate requirements
								for (TypeDescription.Generic requirement : service.dependenciesClasses) {
									if (service.service.equals(TypeDescription.Generic.VOID)) {
										error.add(service + ": dependency method cannot be void!");
										return false;
									}
									if (requirement.isPrimitive()) {
										error.add(service + ": Service dependency cannot be a primitive!");
										return false;
									}
									if (requirement.isArray()) {
										error.add(service + ": Service dependency cannot be an array!");
										return false;
									}
								}
								return true;
							});
				})
				.collect(Collectors.groupingBy(service -> service.service.getTypeName()))
				.values()
				.parallelStream()
				.map(serviceUnits -> {
					//check duplication in same class
					if (serviceUnits.size() == 0)
						throw new RuntimeException("cannot happen");
					if (serviceUnits.size() > 1)
						error.add(serviceUnits.get(0).service.getTypeName() + ": Service is declared " + (serviceUnits.size() == 2 ? "twice" : "multiple times") + "!\n" +
										  serviceUnits.stream().map(s -> "\t- " + s.declMethod.getGenericSignature()).collect(Collectors.joining("\n")));
					return serviceUnits.get(0);
				})
				.collect(Collectors.toMap(service -> service.service.getTypeName(), Function.identity()));
		
		
		
		//link service dependencies
		services.values().parallelStream().forEach(service -> service.dependencies =
				Arrays.stream(service.dependenciesClasses).map(r -> {
					ServiceUnit dep = services.get(r.getTypeName());
					if (dep != null)
						return dep;
					error.add(service + ": error: dependent service " + r.getTypeName() + " is not a known Service!");
					return null;
				}).filter(Objects::nonNull).toArray(ServiceUnit[]::new));
		
		
		
		//clear output
		getProject().delete((Object[]) getOutputDirectory().get().getAsFile().listFiles());
		
		
		
		//generate target class
		ByteBuddy byteBuddy = new ByteBuddy();
		TypeDescription typeAbstractTarget = ForLoadedType.of(AbstractTarget.class);
		List<ServiceUnit> targets = services.values().stream().filter(service -> service.isTarget).collect(Collectors.toList());
		targets.parallelStream().filter(target -> {
			
			//validate
			if (!target.declClazz.equals(target.service.asErasure())) {
				//this should actually never happen, but just in case
				error.add(target + ": Target and declaring Class of Target differ! (works only with Services)");
				return false;
			}
			
			//validate: extends AbstractTarget
			for (Generic supers = target.service; ; supers = supers.getSuperClass()) {
				if (supers == null) {
					error.add(target + ": Target does not extend AbstractTarget!");
					return false;
				}
				if (supers.asErasure().equals(typeAbstractTarget))
					break;
			}
			
			//flatten and deduplicate dependencies
			ArrayList<ServiceUnit> dependencyList = new ArrayList<>();
			{
				HashSet<ServiceUnit> deduplication = new HashSet<>();
				new Consumer<ServiceUnit>() {
					@Override
					public void accept(ServiceUnit service) {
						for (ServiceUnit dependency : service.dependencies)
							accept(dependency);
						if (deduplication.add(service))
							dependencyList.add(service);
					}
				}.accept(target);
			}
			
			//create class
			Unloaded<?> decoratedTarget;
			{
				DynamicType.Builder<?> builder = byteBuddy.subclass(Object.class, Default.NO_CONSTRUCTORS)
														  .name(AbstractTarget.getDecoratedName(target.declClazz.getName()));
				builder = dependencyList.stream().reduce(
						builder,
						(b, service) -> b.defineField(service.serviceVarName, service.service.asErasure(), Visibility.PRIVATE),
						(b1, b2) -> {throw new UnsupportedOperationException();}
				);
				builder = builder.defineConstructor(Visibility.PUBLIC).intercept(
						MethodCall.invoke(objectConstructor).andThen(
								dependencyList.stream()
											  .map(service -> (Composable) (service.isConstructor ? MethodCall.construct(service.declMethod) : MethodCall.invoke(service.declMethod))
													  .withField(service.dependenciesVarNames())
													  .setsField(ElementMatchers.named(service.serviceVarName)))
											  .reduce(Composable::andThen).orElseThrow()
						)
				);
				decoratedTarget = builder.make();
			}
			
			//make and write class
			try {
				decoratedTarget.saveIn(getOutputDirectory().get().getAsFile());
			} catch (IOException e) {
				error.add(target + ": io error: could not write Target class: " + e.getMessage());
				return false;
			}
			
			return true;
		}).forEach(unit -> {});
	}
}
