package space.engine.simpleQueue;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.VarHandle;

/**
 * A concurrent threadsafe linking based FILO queue.
 */
public class HighlyConcurrentSimpleQueue<E> implements SimpleQueue<E> {
	
	private static final int STATE_OPEN = 0;
	private static final int STATE_ADDING = 1;
	private static final int STATE_REMOVING = 2;
	
	private static final VarHandle HEAD;
	private static final VarHandle TAIL;
	private static final VarHandle STATE;
	private static final VarHandle NEXT;
	
	static {
		try {
			Lookup lookup = MethodHandles.lookup();
			HEAD = lookup.findVarHandle(HighlyConcurrentSimpleQueue.class, "head", Node.class);
			TAIL = lookup.findVarHandle(HighlyConcurrentSimpleQueue.class, "tail", Node.class);
			STATE = lookup.findVarHandle(Node.class, "state", int.class);
			NEXT = lookup.findVarHandle(Node.class, "next", Node.class);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new ExceptionInInitializerError(e);
		}
	}
	
	private final int blockSize;
	@SuppressWarnings({"FieldCanBeLocal", "FieldMayBeFinal"})
	private volatile @NotNull Node<E> head;
	@SuppressWarnings({"FieldCanBeLocal", "FieldMayBeFinal"})
	private volatile @NotNull Node<E> tail;
	private final ThreadLocal<Cache<E>> cache = ThreadLocal.withInitial(Cache::new);
	
	public HighlyConcurrentSimpleQueue(int blockSize) {
		if (blockSize <= 0)
			throw new IllegalArgumentException("blocksize");
		
		this.blockSize = blockSize;
		Node<E> starterNode = new Node<>(0);
		head = starterNode;
		tail = starterNode;
	}
	
	@Override
	public boolean add(E e) {
		Cache<E> cache = this.cache.get();
		Node<E> node = cache.add;
		
		//no node
		boolean newNode = node == null;
		boolean unlock = false;
		//lock node -> new node if fails
		if (!newNode)
			newNode = !((node == cache.remove) || (unlock = STATE.compareAndSet(node, STATE_OPEN, STATE_ADDING)));
		//node is full
		if (!newNode)
			newNode = node.itemsIndex == node.items.length;
		
		//newNode -> create new node
		if (newNode) {
			if (unlock)
				STATE.setRelease(node, STATE_OPEN);
			node = new Node<>(blockSize);
			cache.add = node;
		}
		
		//add item
		node.items[node.itemsIndex++] = e;
		
		//newNode -> append to TAIL
		if (newNode) {
			//noinspection unchecked
			Node<E> oldTail = (Node<E>) TAIL.getAndSetRelease(this, node);
			NEXT.setOpaque(oldTail, node);
		}
		
		//existing node -> unlock if necessary
		else if (unlock) {
			STATE.setRelease(node, STATE_OPEN);
		}
		
		return true;
	}
	
	@Nullable
	@Override
	public E remove() {
		Cache<E> cache = this.cache.get();
		Node<E> node = cache.remove;
		
		//no cached node -> get new node
		if (node == null)
			if ((node = getNewNode(cache)) == null)
				return null;
		
		//get item
		node.itemsIndex--;
		E item = node.items[node.itemsIndex];
		node.items[node.itemsIndex] = null;
		
		//node is empty -> clear remove cache
		if (node.itemsIndex == 0)
			cache.remove = null;
		
		return item;
	}
	
	@SuppressWarnings("unchecked")
	protected @Nullable Node<E> getNewNode(Cache<E> cache) {
		Node<E> node;
		
		label:
		while (true) {
			//TODO: only works if we expect things to be handled in order
//				if (cache.add != null && cache.add.state == Node.STATE_OPEN) {
//					//use cache.add node
//					node = cache.add;
//				} else {
			//get next node from LinkedList
			Node<E> head;
			do {
				head = (Node<E>) HEAD.getAcquire(this);
				node = (Node<E>) NEXT.getOpaque(head); //node will be acquired in the next block
				if (node == null)
					return null;
			} while (!HEAD.weakCompareAndSetRelease(this, head, node));
//				}
			
			//lock node
			while (true) {
				int stateCurr = (int) STATE.compareAndExchangeAcquire(node, STATE_OPEN, STATE_REMOVING);
				//locking success
				if (stateCurr == STATE_OPEN)
					break label;
				//node already getting removed by another thread -> retry
				if (stateCurr == STATE_REMOVING)
					continue label;
				//node.state == Node.STATE_ADDING
				//currently adding -> wait a little
				Thread.onSpinWait();
			}
		}
		
		cache.remove = node;
		return node;
	}
	
	/**
	 * Always returns true
	 *
	 * @return always true
	 */
	@Override
	@Deprecated
	public boolean hasEntry() {
		return true;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public int size() {
		int i = 0;
		for (Node<E> node = (Node<E>) HEAD.getAcquire(this); node != null; node = (Node<E>) NEXT.getAcquire(node))
			i++;
		return i;
	}
	
	private static class Node<E> {
		
		//object
		@SuppressWarnings("FieldMayBeFinal")
		private volatile int state = STATE_OPEN;
		private volatile @Nullable Node<E> next;
		
		private int itemsIndex;
		private final E[] items;
		
		public Node(int blockSize) {
			//noinspection unchecked
			this.items = (E[]) new Object[blockSize];
		}
	}
	
	private static class Cache<E> {
		
		private @Nullable Node<E> add;
		private @Nullable Node<E> remove;
	}
}
