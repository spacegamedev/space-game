package space.engine.simpleQueue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

public class SimpleQueueTest {
	
	public static List<Arguments> implementations() {
		return List.of(
				() -> new Object[] {new ArraySimpleQueue<>(16)},
				() -> new Object[] {new LinkedSimpleQueue<>()},
				() -> new Object[] {new ConcurrentLinkedSimpleQueue<>()},
				() -> new Object[] {new HighlyConcurrentSimpleQueue<>(2)}
		);
	}
	
	@ParameterizedTest
	@MethodSource("implementations")
	public void testNormalUse(final SimpleQueue<Integer> queue) {
		queue.add(0);
		queue.add(1);
		queue.add(2);
		queue.add(3);
		
		int[] slots = new int[4];
		for (int i = 0; i < 4; i++)
			slots[Objects.requireNonNull(queue.remove())] += 1;
		assertArrayEquals(new int[] {1, 1, 1, 1}, slots);
	}
	
	@ParameterizedTest
	@MethodSource("implementations")
	public void testQueueDryup(final SimpleQueue<Integer> queue) {
		assertNull(queue.remove());
		
		queue.add(0);
		assertEquals((Integer) 0, queue.remove());
		assertNull(queue.remove());
	}
	
	@ParameterizedTest
	@MethodSource("implementations")
	public void testAddAll(final SimpleQueue<Integer> queue) {
		Runnable remove4Numbers = () -> {
			int[] slots = new int[4];
			for (int i = 0; i < 4; i++)
				slots[Objects.requireNonNull(queue.remove())] = 1;
			assertEquals(4, Arrays.stream(slots).sum());
		};
		
		//collection
		queue.addCollection(List.of(0, 1, 2, 3));
		remove4Numbers.run();
		
		//array
		queue.addArray(new Integer[] {0, 1, 2, 3});
		remove4Numbers.run();
	}
	
	@ParameterizedTest
	@MethodSource("implementations")
	public void testAddAndRemoveMixed(final SimpleQueue<Integer> queue) {
		int[] slots = new int[3];
		
		queue.add(0);
		queue.add(1);
		slots[Objects.requireNonNull(queue.remove())] += 1;
		queue.add(2);
		slots[Objects.requireNonNull(queue.remove())] += 1;
		slots[Objects.requireNonNull(queue.remove())] += 1;
		assertNull(queue.remove());
		
		assertArrayEquals(new int[] {1, 1, 1}, slots);
	}
}
