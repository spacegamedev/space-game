package space.engine.buffer;

import space.engine.struct.Allocator;
import space.engine.struct.Struct;

import static java.nio.charset.StandardCharsets.*;

public class StringConverter {
	
	public static final byte NULL_CHARACTER = 0;
	
	//string[] to pointerbuffer
	//won't work yet as the CBytes will dereference and free, or will not free at all
//	public static PointerStruct.Buffer stringArrayToUTF8(Allocator allocator, String[] strings, boolean nullTerm) {
//		CByte.Buffer[] array = new CByte.Buffer[strings.length];
//		for (int i = 0; i < strings.length; i++)
//			array[i] = stringToUTF8(allocator, strings[i], nullTerm);
//		return PointerStruct.Buffer.allocOwn(allocator, array);
//	}
//
//	public static PointerStruct.Buffer stringArrayToUTF16(Allocator allocator, String[] strings, boolean nullTerm) {
//		CByte.Buffer[] array = new CByte.Buffer[strings.length];
//		for (int i = 0; i < strings.length; i++)
//			array[i] = stringToUTF16(allocator, strings[i], nullTerm);
//		return PointerStruct.Buffer.allocOwn(allocator, array);
//	}
//
//	public static PointerStruct.Buffer stringArrayToASCII(Allocator allocator, String[] strings, boolean nullTerm) {
//		CByte.Buffer[] array = new CByte.Buffer[strings.length];
//		for (int i = 0; i < strings.length; i++)
//			array[i] = stringToASCII(allocator, strings[i], nullTerm);
//		return PointerStruct.Buffer.allocOwn(allocator, array);
//	}
	
	//string to buffer
	public static CByte.Buffer stringToUTF8(Allocator allocator, String str, boolean nullTerm) {
		byte[] bytes = str.getBytes(UTF_8);
		CByte.Buffer buffer = CByte.Buffer.malloc(allocator, bytes.length + (nullTerm ? 1 : 0));
		buffer.set(bytes);
		if (nullTerm)
			buffer.set(bytes.length, (byte) 0);
		return buffer;
	}
	
	public static CByte.Buffer stringToUTF16(Allocator allocator, String str, boolean nullTerm) {
		byte[] bytes = str.getBytes(UTF_16);
		CByte.Buffer buffer = CByte.Buffer.malloc(allocator, bytes.length + (nullTerm ? 2 : 0));
		buffer.set(bytes);
		if (nullTerm) {
			buffer.set(bytes.length, (byte) 0);
			buffer.set(bytes.length + 1, (byte) 0);
		}
		return buffer;
	}
	
	public static CByte.Buffer stringToASCII(Allocator allocator, String str, boolean nullTerm) {
		byte[] bytes = str.getBytes(US_ASCII);
		CByte.Buffer buffer = CByte.Buffer.malloc(allocator, bytes.length + (nullTerm ? 1 : 0));
		buffer.set(bytes);
		if (nullTerm)
			buffer.set(bytes.length, (byte) 0);
		return buffer;
	}
	
	//address to string
	@SuppressWarnings("ConstantConditions")
	public static String UTF8ToString(long address) {
		return UTF8ToString(CByte.Buffer.wrap(address, Long.MAX_VALUE, null));
	}
	
	@SuppressWarnings("ConstantConditions")
	public static String UTF16ToString(long address) {
		return UTF16ToString(CByte.Buffer.wrap(address, Long.MAX_VALUE, null));
	}
	
	@SuppressWarnings("ConstantConditions")
	public static String ASCIIToString(long address) {
		return ASCIIToString(CByte.Buffer.wrap(address, Long.MAX_VALUE, null));
	}
	
	//buffer to string
	public static String UTF8ToString(CByte.Buffer buffer) {
		return UTF8ToString(buffer, findNullCharacter(buffer, 1));
	}
	
	public static String UTF16ToString(CByte.Buffer buffer) {
		return UTF16ToString(buffer, findNullCharacter(buffer, 2));
	}
	
	public static String ASCIIToString(CByte.Buffer buffer) {
		return ASCIIToString(buffer, findNullCharacter(buffer, 1));
	}
	
	//buffer with length to string
	public static String UTF8ToString(CByte.Buffer buffer, int length) {
		byte[] bytes = new byte[length];
		buffer.get(0, bytes, 0, length);
		return new String(bytes, 0, length, UTF_8);
	}
	
	public static String UTF16ToString(CByte.Buffer buffer, int length) {
		byte[] bytes = new byte[length];
		buffer.get(0, bytes, 0, length);
		return new String(bytes, 0, length, UTF_16);
	}
	
	public static String ASCIIToString(CByte.Buffer buffer, int length) {
		byte[] bytes = new byte[length];
		buffer.get(0, bytes, 0, length);
		return new String(bytes, 0, length, US_ASCII);
	}
	
	//other
	private static int findNullCharacter(CByte.Buffer buffer, int step) {
		for (int i = 0; i < buffer.sizeOf(); i += step)
			if (buffer.get(i) == NULL_CHARACTER)
				return i;
		throw new StringBufferNotNullTerminatedException(buffer);
	}
	
	public static class StringBufferNotNullTerminatedException extends RuntimeException {
		
		public StringBufferNotNullTerminatedException(Struct buffer) {
			super("Buffer not terminated with null character: \n" + buffer.dump());
		}
	}
}
