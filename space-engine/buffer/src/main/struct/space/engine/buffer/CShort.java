package space.engine.buffer;

import space.engine.struct.annotations.BufferArrayCopy;
import space.engine.struct.annotations.BufferArrayCopy.ArrayCopyTypes;
import space.engine.struct.annotations.JustGet;
import space.engine.struct.annotations.NioWrapperMethod;
import space.engine.struct.annotations.NioWrapperMethod.NioTypes;
import space.engine.struct.annotations.ShortGetter;

@NioWrapperMethod({NioTypes.GENERIC, NioTypes.SHORT})
@BufferArrayCopy(ArrayCopyTypes.SHORT)
public class CShort {
	
	@ShortGetter(false)
	@JustGet
	short Short;
}
