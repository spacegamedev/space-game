package space.engine.buffer;

import space.engine.struct.annotations.BufferArrayCopy;
import space.engine.struct.annotations.BufferArrayCopy.ArrayCopyTypes;
import space.engine.struct.annotations.JustGet;
import space.engine.struct.annotations.NioWrapperMethod;
import space.engine.struct.annotations.NioWrapperMethod.NioTypes;
import space.engine.struct.annotations.ShortGetter;

@NioWrapperMethod({NioTypes.GENERIC, NioTypes.BYTE})
@BufferArrayCopy(ArrayCopyTypes.BYTE)
public class CByte {
	
	@ShortGetter(false)
	@JustGet
	byte Byte;
}
