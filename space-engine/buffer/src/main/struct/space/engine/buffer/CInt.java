package space.engine.buffer;

import space.engine.struct.annotations.BufferArrayCopy;
import space.engine.struct.annotations.BufferArrayCopy.ArrayCopyTypes;
import space.engine.struct.annotations.BufferStream;
import space.engine.struct.annotations.BufferStream.StreamType;
import space.engine.struct.annotations.JustGet;
import space.engine.struct.annotations.NioWrapperMethod;
import space.engine.struct.annotations.NioWrapperMethod.NioTypes;
import space.engine.struct.annotations.ShortGetter;

@NioWrapperMethod({NioTypes.GENERIC, NioTypes.INT})
@BufferArrayCopy(ArrayCopyTypes.INT)
@BufferStream(StreamType.INT)
public class CInt {
	
	@ShortGetter(false)
	@JustGet
	int Int;
}
