package space.engine.cleaner.generator;

import org.jetbrains.annotations.NotNull;
import space.engine.cleaner.Cleaner;

public abstract class GeneratedCleaner extends Cleaner {
	
	/**
	 * Don't add a constructor, one will be added automatically
	 */
	protected GeneratedCleaner() {
		super(null);
		throw new UnsupportedOperationException();
	}
	
	protected GeneratedCleaner(@NotNull Object referent) {
		super(referent);
	}
}
