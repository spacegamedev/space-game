package space.engine.cleaner;

public class FreedException extends RuntimeException {
	
	public FreedException(Object object) {
		this("Freeable " + object + " was already freed");
	}
	
	public FreedException() {
	}
	
	public FreedException(String message) {
		super(message);
	}
	
	public FreedException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public FreedException(Throwable cause) {
		super(cause);
	}
}
