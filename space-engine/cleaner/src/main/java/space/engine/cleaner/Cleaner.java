package space.engine.cleaner;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import space.engine.interfaces.Freeable;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.VarHandle;
import java.lang.ref.PhantomReference;

public abstract class Cleaner extends PhantomReference<Object> implements Freeable {
	
	private static final VarHandle FREED;
	private static final VarHandle REFCOUNTER;
	
	static {
		try {
			Lookup lookup = MethodHandles.lookup();
			FREED = lookup.findVarHandle(Cleaner.class, "freed", boolean.class);
			REFCOUNTER = lookup.findVarHandle(Cleaner.class, "refCounter", short.class);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new ExceptionInInitializerError(e);
		}
	}
	
	@SuppressWarnings("unused")
	private volatile boolean freed;
	/**
	 * short is used instead of int to reduce object size by 8 bytes (64bit Hotspot VM compressedOOPs)
	 */
	@SuppressWarnings({"unused", "FieldMayBeFinal"})
	private volatile short refCounter = 1;
	
	public Cleaner(@Nullable Object referent) {
		super(referent, CleanerThread.QUEUE);
	}
	
	//refCounter
	@Contract("-> this")
	public <T extends Cleaner> T refIncrement() throws FreedException {
		while (true) {
			short old = (short) REFCOUNTER.getAcquire(this);
			if (old <= 0)
				throw new FreedException(this);
			if (REFCOUNTER.weakCompareAndSetRelease(this, old, (short) (old + 1)))
				break;
		}
		//noinspection unchecked
		return (T) this;
	}
	
	public void refDecrement() {
		short result = (short) REFCOUNTER.getAndAddAcquire(this, (short) -1);
		if (result < 1)
			throw new IllegalStateException("Freed more often than acquired! " + this);
		if (result == 1)
			free();
	}
	
	//free
	protected abstract void free();
	
	public boolean freed() {
		return (short) REFCOUNTER.getAcquire(this) == 0;
	}
	
	public void assertExists() {
		if (freed())
			throw new FreedException(this);
	}
	
	//close
	@Override
	public void close() {
		if (FREED.compareAndSet(this, false, true)) {
			closed();
			refDecrement();
		}
	}
	
	/**
	 * Override this for actions when object is closed.
	 */
	protected void closed() {
	
	}
}
