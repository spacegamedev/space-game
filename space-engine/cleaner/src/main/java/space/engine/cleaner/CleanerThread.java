package space.engine.cleaner;

import org.jetbrains.annotations.Nullable;
import space.engine.Shutdown;
import space.engine.barrier.Barrier;
import space.engine.barrier.BarrierImpl;
import space.engine.barrier.Pool;
import space.engine.barrier.event.EventEntry;
import space.engine.barrier.functions.Starter;
import space.engine.interfaces.Freeable;
import space.engine.logger.Logger;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;

public class CleanerThread {
	
	public static final ReferenceQueue<Object> QUEUE = new ReferenceQueue<>();
	public static final EventEntry<Starter<Barrier>> SHUTDOWN_ENTRY_CLEANER_STOP;
	
	static {
		Logger logger = Logger.rootLogger("Cleaner");
		BarrierImpl shouldStop = new BarrierImpl();
		BarrierImpl hasStopped = new BarrierImpl();
		Thread cleanerThread = new Thread(() -> {
			int cleanedCounter = 0;
			while (true) {
				try {
					@Nullable Reference<?> ref = QUEUE.remove(100L);
					if (ref == null) {
						if (cleanedCounter != 0) {
							logger.info("Closed " + cleanedCounter + " objects with WeakReferences");
							cleanedCounter = 0;
						}
						if (shouldStop.isDone())
							break;
					} else if (ref instanceof Freeable) {
						cleanedCounter++;
						((Freeable) ref).close();
					}
				} catch (InterruptedException ignored) {
				
				}
			}
			hasStopped.triggerNow();
			logger.info("CleanerThread stopped");
		}, "CleanerThread");
		cleanerThread.setDaemon(true);
		cleanerThread.start();
		
		Shutdown.SHOWDOWN_EVENT.addHook(SHUTDOWN_ENTRY_CLEANER_STOP = new EventEntry<>(() -> {
			shouldStop.triggerNow();
			cleanerThread.interrupt();
			return hasStopped;
		}, new EventEntry<?>[] {Pool.SHUTDOWN_ENTRY_POOL_STOP}, new EventEntry<?>[] {Shutdown.SHUTDOWN_ENTRY_GLOBAL_SHUTDOWN}));
	}
}
