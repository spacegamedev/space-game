package space.engine.cleaner;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import space.engine.interfaces.Freeable;

@FunctionalInterface
public interface Resource extends Freeable {
	
	@NotNull Cleaner cleaner();
	
	@Override
	default void close() {
		cleaner().close();
	}
	
	//static
	
	/**
	 * Wrapper function for one-liners: closes oldT and returns newT
	 */
	static <T extends Freeable, R> R replace(@Nullable T oldT, R newT) {
		if (oldT != null)
			oldT.close();
		return newT;
	}
}
