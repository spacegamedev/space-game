package space.engine.cleaner.generator;

import org.jetbrains.annotations.NotNull;
import space.engine.cleaner.Resource;

public interface GeneratedResource<T extends GeneratedCleaner> extends Resource {
	
	default @NotNull T newCleaner() {
		throw new UnsupportedOperationException();
	}
	
	@Override
	default @NotNull T cleaner() {
		throw new UnsupportedOperationException();
	}
}
