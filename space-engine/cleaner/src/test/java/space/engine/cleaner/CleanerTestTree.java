package space.engine.cleaner;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import space.engine.cleaner.CleanerTestTree.Buffer.BufferCleaner;
import space.engine.cleaner.CleanerTestTree.Device.DeviceCleaner;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This tests with objects having dependencies on each other
 */
public class CleanerTestTree {
	
	enum VkObjects {
		DEVICE,
		BUFFER
	}
	
	public static IdValidation<VkObjects> ids = new IdValidation<>();
	
	public static long vkCreateDevice() {
		return ids.create(VkObjects.DEVICE);
	}
	
	public static void vkValidateDevice(long device) {
		ids.validate(device, VkObjects.DEVICE);
	}
	
	public static void vkDestroyDevice(long device) {
		ids.destory(device, VkObjects.DEVICE);
	}
	
	public static long vkCreateBuffer(long device) {
		return ids.create(VkObjects.BUFFER);
	}
	
	public static void vkValidateBuffer(long buffer) {
		ids.validate(buffer, VkObjects.BUFFER);
	}
	
	public static void vkDestroyBuffer(long device, long buffer) {
		ids.validate(device, VkObjects.DEVICE);
		ids.destory(buffer, VkObjects.BUFFER);
	}
	
	public static class Device implements Resource {
		
		public static Device allocDevice() {
			return new Device(vkCreateDevice());
		}
		
		private final DeviceCleaner cleaner;
		
		public Device(long device) {
			this.cleaner = new DeviceCleaner(this, device);
		}
		
		@Override
		public @NotNull DeviceCleaner cleaner() {
			return cleaner;
		}
		
		public long address() {
			return cleaner.address();
		}
		
		public static class DeviceCleaner extends Cleaner {
			
			public DeviceCleaner(Object referent, long device) {
				super(referent);
				this.device = device;
			}
			
			private final long device;
			
			@Override
			public void free() {
				vkDestroyDevice(device);
			}
			
			public long address() {
				assertExists();
				return device;
			}
		}
	}
	
	public static class Buffer implements Resource {
		
		public static Buffer allocBuffer(Device device) {
			return new Buffer(device, vkCreateBuffer(device.address()));
		}
		
		private final BufferCleaner cleaner;
		
		public Buffer(Device device, long buffer) {
			this.cleaner = new BufferCleaner(this, device, buffer);
		}
		
		@Override
		public @NotNull BufferCleaner cleaner() {
			return cleaner;
		}
		
		public long address() {
			return cleaner.address();
		}
		
		public static class BufferCleaner extends Cleaner {
			
			public BufferCleaner(Object referent, Device device, long buffer) {
				super(referent);
				this.buffer = buffer;
				this.device = device.cleaner().refIncrement();
			}
			
			private final DeviceCleaner device;
			private final long buffer;
			
			@Override
			public void free() {
				vkDestroyBuffer(device.address(), buffer);
				device.refDecrement();
			}
			
			public long address() {
				assertExists();
				return buffer;
			}
		}
	}
	
	@Test
	public void testTryWithResource() {
		Device device;
		Buffer buffer;
		
		try (Device device2 = Device.allocDevice()) {
			device = device2;
			assertFalse(device.cleaner().freed());
			
			try (Buffer buffer2 = Buffer.allocBuffer(device)) {
				buffer = buffer2;
				assertFalse(buffer.cleaner().freed());
			}
			assertTrue(buffer.cleaner().freed());
		}
		assertTrue(device.cleaner().freed());
	}
	
	@Test
	public void testFreeDeviceBeforeBuffer() {
		Device device;
		Buffer buffer;
		
		try (Device device2 = Device.allocDevice()) {
			device = device2;
			assertFalse(device.cleaner().freed());
			
			//noinspection resource
			buffer = Buffer.allocBuffer(device);
			assertFalse(buffer.cleaner().freed());
		}
		assertFalse(buffer.cleaner().freed());
		assertFalse(device.cleaner().freed());
		
		buffer.close();
		assertTrue(buffer.cleaner().freed());
		assertTrue(device.cleaner().freed());
	}
	
	@RepeatedTest(20)
	@Timeout(1)
	@SuppressWarnings("resource")
	public void testWeakRef() {
		Device device = Device.allocDevice();
		DeviceCleaner deviceCleaner = device.cleaner();
		assertFalse(deviceCleaner.freed());
		
		Buffer buffer = Buffer.allocBuffer(device);
		BufferCleaner bufferCleaner = buffer.cleaner();
		assertFalse(bufferCleaner.freed());
		
		//noinspection UnusedAssignment
		device = null;
		//noinspection UnusedAssignment
		buffer = null;
		
		int i = 0;
		for (; !(deviceCleaner.freed() || bufferCleaner.freed()); i++) {
			System.gc();
			System.runFinalization();
		}
		
		assertTrue(deviceCleaner.freed() && bufferCleaner.freed(), "both freed at the same time -> no layered freeing");
		System.out.println("Cleaner cleaned after " + i + " GC iterations");
	}
}
