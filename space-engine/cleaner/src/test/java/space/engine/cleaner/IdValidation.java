package space.engine.cleaner;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.*;

public class IdValidation<T> {
	
	private final AtomicLong ids = new AtomicLong();
	private final HashMap<Long, T> map = new HashMap<>();
	
	public long create(T type) {
		long id = ids.getAndAdd(1);
		assertNull(map.put(id, type));
		return id;
	}
	
	public void validate(long id, T type) {
		assertEquals(type, map.get(id));
	}
	
	public void destory(long id, T type) {
		assertEquals(type, map.remove(id));
	}
}
