package space.engine.cleaner;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

public class CleanerExample {
	
	public static class TestResource implements Resource {
		
		private final TestCleaner cleaner;
		
		public TestResource(long ptr) {
			cleaner = new TestCleaner(this, ptr);
		}
		
		@Override
		public @NotNull Cleaner cleaner() {
			return cleaner;
		}
		
		//native calls
		public void nativeCallDelegate(int param) {
			cleaner.nativeCallDelegate(param);
		}
		
		public void nativeCallDirect(boolean param) {
			//doNativeCall(cleaner.ptr, param);
		}
		
		public static class TestCleaner extends Cleaner {
			
			private long ptr;
			
			public TestCleaner(TestResource referent, long ptr) {
				super(referent);
				this.ptr = ptr;
			}
			
			@Override
			public void free() {
				//freePtr(ptr);
				ptr = 0;
			}
			
			public void nativeCallDelegate(int param) {
				assertExists();
			}
		}
	}
	
	@Test
	@SuppressWarnings("resource")
	public void exampleWeakRef() {
		TestResource r = new TestResource(1234);
		r.nativeCallDirect(true);
	}
	
	@Test
	public void exampleTryWithResource() {
		try (TestResource resource = new TestResource(1256)) {
			resource.nativeCallDirect(true);
		}
	}
}
