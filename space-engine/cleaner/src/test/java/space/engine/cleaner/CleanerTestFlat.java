package space.engine.cleaner;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import space.engine.cleaner.CleanerTestFlat.TestResource.TestCleaner;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This tests basic cleaner functionality for individual objects without dependencies between each other
 */
public class CleanerTestFlat {
	
	public static class TestResource implements Resource {
		
		private final @NotNull TestCleaner cleaner;
		
		public TestResource(String data) {
			cleaner = new TestCleaner(this, data);
		}
		
		@Override
		public @NotNull TestCleaner cleaner() {
			return cleaner;
		}
		
		//calls
		public String getData() {
			return cleaner.getData();
		}
		
		public static class TestCleaner extends Cleaner {
			
			private String data;
			
			public TestCleaner(Object referent, String data) {
				super(referent);
				this.data = data;
			}
			
			@Override
			@SuppressWarnings("ConstantConditions")
			public void free() {
				data = null;
			}
			
			//calls
			public String getData() {
				assertExists();
				return data;
			}
		}
	}
	
	@Test
	@Timeout(1)
	@SuppressWarnings("resource")
	public void testWeakRef() {
		TestResource testResource = new TestResource("weakRef");
		TestCleaner cleaner = testResource.cleaner();
		assertFalse(cleaner.freed());
		assertEquals("weakRef", testResource.getData());
		
		//noinspection UnusedAssignment
		testResource = null;
		
		int i = 0;
		for (; !cleaner.freed(); i++) {
			System.gc();
			System.runFinalization();
		}
		assertTrue(cleaner.freed());
		assertThrows(FreedException.class, cleaner::getData);
		
		System.out.println("Cleaner cleaned after " + i + " GC iterations");
	}
	
	@Test
	public void testTryWithResource() {
		@NotNull TestCleaner testCleaner;
		try (TestResource testResource = new TestResource("twr")) {
			testCleaner = testResource.cleaner();
			assertFalse(testResource.cleaner().freed());
			assertEquals("twr", testResource.getData());
		}
		assertTrue(testCleaner.freed());
		assertThrows(FreedException.class, testCleaner::getData);
	}
}
