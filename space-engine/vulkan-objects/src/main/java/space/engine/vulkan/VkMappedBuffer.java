package space.engine.vulkan;

import org.jetbrains.annotations.NotNull;
import space.engine.barrier.Barrier;
import space.engine.struct.Struct;

public interface VkMappedBuffer extends VkBuffer {
	
	//mapping
	Struct mapMemory(Object[] parents);
	
	/**
	 * always completes when this Method returns -> always returns {@link Barrier#DONE_BARRIER}
	 */
	@Override
	default @NotNull Barrier uploadData(Struct src) {
		return uploadData(src, 0, 0, src.sizeOf());
	}
	
	/**
	 * always completes when this Method returns -> always returns {@link Barrier#DONE_BARRIER}
	 */
	@Override
	@NotNull Barrier uploadData(Struct src, long srcOffset, long dstOffset, long length);
}
