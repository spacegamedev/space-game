package space.engine.plugin;

import net.bytebuddy.dynamic.ClassFileLocator;
import org.gradle.api.GradleException;
import org.gradle.api.file.FileCollection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

public interface ByteBuddyUtils {
	
	static ClassFileLocator classFileLocatorFromFile(File file) {
		if (file.isFile()) {
			try {
				return ClassFileLocator.ForJarFile.of(file);
			} catch (IOException e) {
				throw new GradleException("Unreadable jar file " + file + ": " + e.getMessage(), e);
			}
		} else {
			return new ClassFileLocator.ForFolder(file);
		}
	}
	
	static ClassFileLocator classFileLocatorFromClassPath(FileCollection files) {
		return new ClassFileLocator.Compound(Stream.concat(
				files.getFiles().stream().map(ByteBuddyUtils::classFileLocatorFromFile),
				Stream.of(ClassFileLocator.ForClassLoader.ofPlatformLoader())
		).toArray(ClassFileLocator[]::new));
	}
	
	static @Nullable String getClassNameFromFile(String relativePath) {
		if (!relativePath.endsWith(".class"))
			return null;
		String name = relativePath.replace('/', '.');
		return name.substring(0, name.length() - ".class".length());
	}
	
	static @NotNull String getFileFromClassName(String className) {
		return className.replace('.', '/') + ".class";
	}
	
	static @NotNull <T> BinaryOperator<T> reduceMergeUnsupported() {
		return (b1, b2) -> {throw new UnsupportedOperationException();};
	}
}
