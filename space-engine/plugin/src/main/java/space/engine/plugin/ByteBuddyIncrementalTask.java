package space.engine.plugin;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.type.TypeDefinition;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.ClassFileLocator;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.dynamic.DynamicType.Unloaded;
import net.bytebuddy.pool.TypePool;
import net.bytebuddy.pool.TypePool.Default;
import net.bytebuddy.pool.TypePool.Resolution;
import org.gradle.work.InputChanges;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sun.misc.Unsafe;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

import static space.engine.plugin.ByteBuddyUtils.*;

public abstract class ByteBuddyIncrementalTask extends IncrementalTask {
	
	public static final String[] STRING_ARRAY_EMPTY = new String[0];
	
	@FunctionalInterface
	public interface ByteBuddyWorkFunction {
		
		/**
		 * Computes ByteBuddy changes for a supplied class.
		 *
		 * @param pool             use this {@link TypePool} for querying any types
		 * @param classFileLocator use this {@link ClassFileLocator} for {@link ByteBuddy#rebase(TypeDescription, ClassFileLocator)} rebasing}, {@link ByteBuddy#subclass(TypeDefinition)} or {@link DynamicType.Builder#make(TypePool)} making} any types
		 * @param clazz            the {@link TypeDescription} that should be processed
		 * @return all {@link Unloaded} to be written
		 */
		@Nullable Collection<Unloaded<?>> work(TypePool pool, ClassFileLocator classFileLocator, TypeDescription clazz) throws RuntimeException;
	}
	
	@NotNull
	protected ClassFileLocator createClassFileLocator() {
		return ByteBuddyUtils.classFileLocatorFromClassPath(getProject().files(getSource(), getClasspath()));
	}
	
	protected void incrementalByteBuddy(InputChanges inputChanges, ByteBuddyWorkFunction work) {
		incrementalByteBuddy(createClassFileLocator(), inputChanges, work);
	}
	
	protected void incrementalByteBuddy(ClassFileLocator locator, InputChanges inputChanges, ByteBuddyWorkFunction work) {
		incrementalByteBuddy(locator, Default.of(locator), inputChanges, work);
	}
	
	protected void incrementalByteBuddy(ClassFileLocator locator, TypePool pool, InputChanges inputChanges, ByteBuddyWorkFunction work) {
		super.incremental(inputChanges, normalizedPath -> {
			String className = getClassNameFromFile(normalizedPath);
			if (className == null)
				return null;
			
			AccessRecordingTypePool accessRecord = new AccessRecordingTypePool(locator, pool);
			TypeDescription clazz;
			{
				Resolution describe = accessRecord.describe(className);
				if (!describe.isResolved())
					throw error("Could not resolve class " + className + "!");
				clazz = describe.resolve();
			}
			
			Collection<Unloaded<?>> toWrite = work.work(accessRecord, accessRecord, clazz);
			if (toWrite != null) {
				for (Unloaded<?> unloaded : toWrite) {
					try {
						mkdirsOutput(getOutputDirectory().get().file(getFileFromClassName(unloaded.getTypeDescription().getName())).getAsFile().getParentFile());
						unloaded.saveIn(getOutputDirectory().get().getAsFile());
					} catch (IOException e) {
						throw new RuntimeException("IO error writing '" + unloaded.getTypeDescription().getName() + "': " + e.getMessage());
					}
				}
			}
			
			return new Dependency(
					normalizedPath,
					accessRecord.getAccessedClasses().stream().map(ByteBuddyUtils::getFileFromClassName).toArray(String[]::new),
					toWrite != null ? toWrite.stream().map(u -> u.getTypeDescription().getName()).map(ByteBuddyUtils::getFileFromClassName).toArray(String[]::new) : STRING_ARRAY_EMPTY
			);
		});
	}
	
	public static class AccessRecordingTypePool extends TypePool.Default implements ClassFileLocator {
		
		private static final Unsafe unsafe;
		private static final Field[] allFields;
		private static final Field typePoolField;
		
		static {
			try {
				Field unsafeField = Unsafe.class.getDeclaredField("theUnsafe");
				unsafeField.setAccessible(true);
				unsafe = (Unsafe) unsafeField.get(null);
				
				AtomicReference<Field> typePoolF = new AtomicReference<>();
				allFields = Arrays.stream(LazyTypeDescription.class.getDeclaredFields())
								  .peek(f -> f.setAccessible(true))
								  .filter(f -> {
									  if (Modifier.isStatic(f.getModifiers()))
										  return false;
									  if (TypePool.class.isAssignableFrom(f.getType())) {
										  if (!typePoolF.weakCompareAndSetPlain(null, f))
											  throw new RuntimeException("Two TypePools found in LazyTypeDescriptor! Please update implementation!");
										  return false;
									  }
									  return true;
								  })
								  .toArray(Field[]::new);
				typePoolField = typePoolF.getPlain();
				if (typePoolField == null)
					throw new RuntimeException("TypePool Field not found in LazyTypeDescriptor! Please update implementation!");
			} catch (Exception e) {
				throw new ExceptionInInitializerError(e);
			}
		}
		
		final ClassFileLocator parentClassFileLocator;
		final TypePool parentTypePool;
		
		ConcurrentHashMap<String, Boolean> accessedClassesMap = new ConcurrentHashMap<>();
		
		public AccessRecordingTypePool(ClassFileLocator parentClassFileLocator, TypePool parentTypePool) {
			super(CacheProvider.NoOp.INSTANCE, null, ReaderMode.FAST);
			this.parentClassFileLocator = parentClassFileLocator;
			this.parentTypePool = parentTypePool;
		}
		
		public Set<String> getAccessedClasses() {
			return accessedClassesMap.keySet();
		}
		
		//TypePool
		@Override
		protected TypePool.Resolution doDescribe(String name) {
			TypePool.Resolution resolution = parentTypePool.describe(name);
			if (!resolution.isResolved())
				return resolution;
			
			accessedClassesMap.putIfAbsent(name, Boolean.TRUE);
			LazyTypeDescription original = (LazyTypeDescription) resolution.resolve();
			
			try {
				LazyTypeDescription replace = (LazyTypeDescription) unsafe.allocateInstance(LazyTypeDescription.class);
				for (Field f : allFields)
					f.set(replace, f.get(original));
				typePoolField.set(replace, this);
				return new TypePool.Resolution.Simple(replace);
			} catch (InstantiationException | IllegalAccessException e) {
				throw new Error("Fatal Error during clone! Please update implementation!", e);
			}
		}
		
		//ClassFileLocator
		@Override
		public ClassFileLocator.Resolution locate(String name) throws IOException {
			ClassFileLocator.Resolution resolution = parentClassFileLocator.locate(name);
			if (!resolution.isResolved())
				return resolution;
			
			accessedClassesMap.putIfAbsent(name, Boolean.TRUE);
			return resolution;
		}
		
		@Override
		public void close() {
		
		}
	}
}
