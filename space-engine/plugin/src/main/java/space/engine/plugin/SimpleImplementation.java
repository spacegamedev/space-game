package space.engine.plugin;

import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.dynamic.scaffold.InstrumentedType;
import net.bytebuddy.implementation.Implementation;
import net.bytebuddy.implementation.bytecode.ByteCodeAppender;
import net.bytebuddy.implementation.bytecode.StackManipulation;
import net.bytebuddy.jar.asm.MethodVisitor;

public class SimpleImplementation implements Implementation, ByteCodeAppender {
	
	@FunctionalInterface
	public interface Code {
		
		StackManipulation.Compound apply(MethodDescription method);
	}
	
	private final Code code;
	private final int localVariableExpansion;
	
	public SimpleImplementation(Code code) {
		this(code, 0);
	}
	
	public SimpleImplementation(Code code, int localVariableExpansion) {
		this.code = code;
		this.localVariableExpansion = localVariableExpansion;
		
		if (localVariableExpansion < 0)
			throw new RuntimeException("localVariableExpansion is negative!");
	}
	
	@Override
	public Size apply(MethodVisitor methodVisitor, Context context, MethodDescription methodDescription) {
		return new Size(
				code.apply(methodDescription).apply(methodVisitor, context).getMaximalSize(),
				methodDescription.getStackSize() + localVariableExpansion
		);
	}
	
	@Override
	public InstrumentedType prepare(InstrumentedType instrumentedType) {
		return instrumentedType;
	}
	
	@Override
	public ByteCodeAppender appender(Target implementationTarget) {
		return this;
	}
}
