package space.engine.cleaner.plugin;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.field.FieldDescription;
import net.bytebuddy.description.field.FieldDescription.InDefinedShape;
import net.bytebuddy.description.field.FieldDescription.Latent;
import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.method.MethodList;
import net.bytebuddy.description.method.ParameterDescription;
import net.bytebuddy.description.method.ParameterDescription.Token;
import net.bytebuddy.description.method.ParameterList;
import net.bytebuddy.description.modifier.ModifierContributor.ForField;
import net.bytebuddy.description.modifier.ModifierContributor.ForMethod;
import net.bytebuddy.description.modifier.ModifierContributor.Resolver;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.description.type.TypeDescription.ForLoadedType;
import net.bytebuddy.description.type.TypeDescription.Generic;
import net.bytebuddy.dynamic.DynamicType.Builder;
import net.bytebuddy.dynamic.DynamicType.Builder.MethodDefinition.ReceiverTypeDefinition;
import net.bytebuddy.dynamic.DynamicType.Unloaded;
import net.bytebuddy.implementation.Implementation;
import net.bytebuddy.implementation.Implementation.Simple;
import net.bytebuddy.implementation.MethodCall;
import net.bytebuddy.implementation.SuperMethodCall;
import net.bytebuddy.implementation.bytecode.Duplication;
import net.bytebuddy.implementation.bytecode.Removal;
import net.bytebuddy.implementation.bytecode.StackManipulation;
import net.bytebuddy.implementation.bytecode.StackManipulation.Compound;
import net.bytebuddy.implementation.bytecode.TypeCreation;
import net.bytebuddy.implementation.bytecode.assign.TypeCasting;
import net.bytebuddy.implementation.bytecode.constant.NullConstant;
import net.bytebuddy.implementation.bytecode.member.FieldAccess;
import net.bytebuddy.implementation.bytecode.member.MethodInvocation;
import net.bytebuddy.implementation.bytecode.member.MethodReturn;
import net.bytebuddy.implementation.bytecode.member.MethodVariableAccess;
import org.gradle.api.tasks.TaskAction;
import org.gradle.work.InputChanges;
import space.engine.cleaner.Cleaner;
import space.engine.cleaner.Resource;
import space.engine.cleaner.generator.DependentResource;
import space.engine.cleaner.generator.GeneratedCleaner;
import space.engine.cleaner.generator.GeneratedResource;
import space.engine.plugin.ByteBuddyIncrementalTask;
import space.engine.plugin.SimpleImplementation;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.bytebuddy.description.method.MethodDescription.CONSTRUCTOR_INTERNAL_NAME;
import static net.bytebuddy.description.modifier.FieldManifestation.FINAL;
import static net.bytebuddy.description.modifier.Visibility.*;
import static net.bytebuddy.matcher.ElementMatchers.*;
import static space.engine.plugin.ByteBuddyUtils.reduceMergeUnsupported;

/**
 * {@link DecorateCleanerTask} is not as clean as it should be, expecially when it comes to incremental builds.
 * Potential breakages:
 * - Cleaner class changes (needs fixing)
 * - Cleaner which the Resource uses changes (very rare, no fix?)
 * - no check for duplicate use of cleaners
 */
public abstract class DecorateCleanerTask extends ByteBuddyIncrementalTask {
	
	static class Unit {
		
		final FieldDescription.InDefinedShape resourceField;
		final FieldDescription.InDefinedShape cleanerField;
		final boolean cleanerFieldNeedsDefinition;
		
		public Unit(InDefinedShape resourceField, InDefinedShape cleanerField, boolean cleanerFieldNeedsDefinition) {
			this.resourceField = resourceField;
			this.cleanerField = cleanerField;
			this.cleanerFieldNeedsDefinition = cleanerFieldNeedsDefinition;
		}
	}
	
	@TaskAction
	public void work(InputChanges inputFiles) {
		ByteBuddy byteBuddy = new ByteBuddy();
		
		MethodDescription.InDefinedShape global_resource_cleaner = ForLoadedType.of(Resource.class).getDeclaredMethods().filter(named("cleaner").and(takesArguments(0)).and(returns(Cleaner.class))).getOnly();
//		MethodDescription.InDefinedShape global_generatedResource_newCleaner = ForLoadedType.of(GeneratedResource.class).getDeclaredMethods().filter(named("newCleaner").and(takesArguments(0)).and(returns(GeneratedCleaner.class))).getOnly();
		MethodDescription.InDefinedShape global_cleaner_refIncrement = ForLoadedType.of(Cleaner.class).getDeclaredMethods().filter(named("refIncrement").and(takesArguments(0)).and(returns(Cleaner.class))).getOnly();
		MethodDescription.InDefinedShape global_cleaner_refDecrement = ForLoadedType.of(Cleaner.class).getDeclaredMethods().filter(named("refDecrement").and(takesArguments(0)).and(returns(TypeDescription.VOID))).getOnly();
		MethodDescription.InDefinedShape global_generatedCleaner_constructor = ForLoadedType.of(GeneratedCleaner.class).getDeclaredMethods().filter(isConstructor().and(takesArguments(TypeDescription.OBJECT))).getOnly();
		
		incrementalByteBuddy(inputFiles, (pool, classFileLocator, resource) -> {
			TypeDescription cleaner;
			boolean cleanerHasConstructor;
			{
				Optional<Generic> interfaceGeneric = resource.getInterfaces().stream().filter(g -> ForLoadedType.of(GeneratedResource.class).equals(g.asErasure())).findFirst();
				if (interfaceGeneric.isEmpty())
					return null;
				cleaner = interfaceGeneric.get().getTypeArguments().get(0).asErasure();
				
				boolean cleanerHasFields = !cleaner.getDeclaredFields().isEmpty();
				MethodList<MethodDescription.InDefinedShape> cleanerConstructors = cleaner.getDeclaredMethods().filter((cleanerHasFields ? isConstructor() : isConstructor().and(not(isDefaultConstructor()))));
				cleanerHasConstructor = cleanerConstructors.size() > 0;
				if (cleanerHasConstructor) {
					if (cleanerConstructors.size() != 1)
						throw error("Cleaner '" + cleaner + "' has multiple Constructors! Only one constructor of shape " + cleaner + "(" + resource + " resource); is allowed!");
					MethodDescription.InDefinedShape cleanerConstructor = cleanerConstructors.get(0);
					ParameterList<ParameterDescription.InDefinedShape> parameters = cleanerConstructor.getParameters();
					if (!(parameters.size() == 1 && parameters.get(0).getType().asErasure().equals(resource)))
						throw error("Cleaner '" + cleaner + "' has invalid signature! Only one constructor of shape " + cleaner + "(" + resource + " resource); is allowed!");
				}
			}
			
			List<Unit> units = resource
					.getDeclaredFields().stream()
					.filter(f -> f.getDeclaredAnnotations().asTypeList().contains(ForLoadedType.of(DependentResource.class)))
					.map(f -> {
						Optional<Generic> interfaceGeneric = f.getType().asErasure().getInterfaces().stream().filter(g -> ForLoadedType.of(GeneratedResource.class).equals(g.asErasure())).findFirst();
						if (interfaceGeneric.isEmpty())
							return null;
						Generic dependentCleaner = interfaceGeneric.get().getTypeArguments().get(0);
						List<InDefinedShape> dependentExistingFields = cleaner.getDeclaredFields().stream().filter(df -> dependentCleaner.equals(df.getType())).collect(Collectors.toList());
						if (dependentExistingFields.size() == 0) {
							return new Unit(f, new Latent(cleaner, getCleanerFieldName(f), Resolver.of(PRIVATE, FINAL).resolve(), dependentCleaner, List.of()), true);
						} else if (dependentExistingFields.size() == 1) {
							return new Unit(f, dependentExistingFields.get(0), false);
						} else {
							throw error("Cleaner '" + cleaner + "' has multiple fields of type '" + dependentCleaner + "', don't know which one to use!");
						}
					})
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
			
			//cleaner
			MethodDescription cleaner_constructor = new MethodDescription.Latent(
					cleaner,
					CONSTRUCTOR_INTERNAL_NAME, Resolver.of((ForMethod) PACKAGE_PRIVATE).resolve(), List.of(), Generic.VOID, List.of(new Token(resource.asGenericType())),
					List.of(), List.of(), null, null
			);
			MethodDescription cleaner_free = new MethodDescription.Latent(
					cleaner,
					"free", Resolver.of((ForMethod) PUBLIC).resolve(), List.of(), Generic.VOID, List.of(),
					List.of(), List.of(), null, null
			);
			
			Unloaded<?> unloadedCleaner;
			{
				Builder<Object> builder = byteBuddy.rebase(cleaner, classFileLocator);
				builder = units.stream()
							   .filter(unit -> unit.cleanerFieldNeedsDefinition)
							   .reduce(builder, (b, f) -> b.define(f.cleanerField), reduceMergeUnsupported());
				
				//constructor
				Compound constructorImplementation = new Compound(
						new Compound(units.stream()
										  .map(u -> new Compound(
												  MethodVariableAccess.loadThis(),
												  MethodVariableAccess.REFERENCE.loadFrom(1),
												  FieldAccess.forField(u.resourceField).read(),
												  MethodInvocation.invoke(global_resource_cleaner),
												  TypeCasting.to(u.cleanerField.getType()),
												  Duplication.SINGLE,
												  MethodInvocation.invoke(global_cleaner_refIncrement),
												  Removal.SINGLE,
												  FieldAccess.forField(u.cleanerField).write()
										  ))
										  .toArray(StackManipulation[]::new)
						),
						MethodReturn.VOID
				);
				if (cleanerHasConstructor) {
					builder = builder.invokable(is(cleaner_constructor)).intercept(
							SuperMethodCall.INSTANCE.andThen(new Implementation.Simple(constructorImplementation))
					);
				} else {
					//this one generates invalid bytecode for a subclass, but the inlining process will make it valid
					builder = builder.define(cleaner_constructor).intercept(
							new Implementation.Simple(
									MethodVariableAccess.loadThis(),
									MethodVariableAccess.REFERENCE.loadFrom(1),
									MethodInvocation.invoke(global_generatedCleaner_constructor),
									constructorImplementation
							)
					);
				}
				
				//free
				builder = builder.invokable(is(cleaner_free)).intercept(
						MethodCall.invokeSuper().withAllArguments().andThen(new Simple(
								new Compound(units.stream()
												  .map(u -> new Compound(
														  MethodVariableAccess.loadThis(),
														  FieldAccess.forField(u.cleanerField).read(),
														  MethodInvocation.invoke(global_cleaner_refDecrement),
														  MethodVariableAccess.loadThis(),
														  NullConstant.INSTANCE,
														  FieldAccess.forField(u.cleanerField).write()
												  ))
												  .toArray(StackManipulation[]::new)
								),
								MethodReturn.VOID
						))
				);
				
				try {
					unloadedCleaner = builder.make();
				} catch (RuntimeException e) {
					throw error("Error decorating class " + resource.getName() + ": " + e.getMessage());
				}
			}
			
			//resource
			FieldDescription resource_cleanerField = new Latent(resource, "$cleaner", Resolver.of((ForField) PRIVATE).resolve(), cleaner.asGenericType(), List.of());
			MethodDescription resource_newCleaner = new MethodDescription.Latent(
					resource,
					"newCleaner", Resolver.of((ForMethod) PUBLIC).resolve(), List.of(), cleaner.asGenericType(), List.of(),
					List.of(), List.of(), null, null
			);
			MethodDescription resource_cleaner = new MethodDescription.Latent(
					resource,
					"cleaner", Resolver.of((ForMethod) PUBLIC).resolve(), List.of(), cleaner.asGenericType(), List.of(),
					List.of(), List.of(), null, null
			);
			
			Unloaded<Object> unloadedResource;
			{
				ReceiverTypeDefinition<Object> builder = byteBuddy
						.rebase(resource, classFileLocator)
						.define(resource_cleanerField)
						//cleaner()
						.define(resource_cleaner).intercept(new Simple(
								MethodVariableAccess.loadThis(),
								FieldAccess.forField(resource_cleanerField).read(),
								MethodReturn.REFERENCE
						))
						//newCleaner()
						.define(resource_newCleaner).intercept(new SimpleImplementation(method -> new Compound(
								TypeCreation.of(cleaner),
								Duplication.of(cleaner),
								MethodVariableAccess.loadThis(),
								MethodInvocation.invoke(cleaner_constructor),
								MethodVariableAccess.REFERENCE.storeAt(1),
								
								MethodVariableAccess.loadThis(),
								MethodVariableAccess.REFERENCE.loadFrom(1),
								FieldAccess.forField(resource_cleanerField).write(),
								
								MethodVariableAccess.REFERENCE.loadFrom(1),
								MethodReturn.REFERENCE
						), 1));
				
				try {
					unloadedResource = builder.make();
				} catch (RuntimeException e) {
					throw error("Error decorating class " + resource.getName() + ": " + e.getMessage());
				}
			}
			
			return List.of(unloadedCleaner, unloadedResource);
		});
	}
	
	public static String getCleanerFieldName(FieldDescription.InDefinedShape from) {
		return from.getName() + "$Cleaner";
	}
}
