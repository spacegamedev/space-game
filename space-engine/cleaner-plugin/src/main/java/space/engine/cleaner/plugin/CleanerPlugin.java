package space.engine.cleaner.plugin;

import org.gradle.api.NonNullApi;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.TaskProvider;
import space.engine.platform.DecorationExtension;
import space.engine.plugin.IncrementalTask;

@NonNullApi
public class CleanerPlugin implements Plugin<Project> {
	
	@Override
	public void apply(Project project) {
		project.getPlugins().apply("space-platform-base");
		
		DirectoryProperty buildDirBase = project.getObjects().directoryProperty().value(project.getLayout().getBuildDirectory().dir("cleaner"));
		JavaPluginExtension javaExtension = project.getExtensions().getByType(JavaPluginExtension.class);
		javaExtension.getSourceSets().all(sourceSet -> {
			DecorationExtension decoration = sourceSet.getExtensions().getByType(DecorationExtension.class);
			DirectoryProperty buildDir = project.getObjects().directoryProperty().convention(buildDirBase.dir(sourceSet.getName()));
			
			//decorateCleaner
			TaskProvider<DecorateCleanerTask> decorateCleanerTask = project.getTasks().register(sourceSet.getTaskName("decorate", "Cleaner"), DecorateCleanerTask.class, t -> {
				t.getCacheDir().set(buildDir.dir("cache"));
				t.getOutputDirectory().set(buildDir.dir("cleaner"));
			});
			decoration.addDecorate(decorateCleanerTask, IncrementalTask::configureDecorate);
		});
	}
}
