package space.engine.service;

import space.engine.interfaces.Freeable;

import java.lang.reflect.InvocationTargetException;

public abstract class AbstractTarget implements Freeable {
	
	//static
	public static final String TARGET_APPENDIX = "$DecoratedTarget";
	
	public static String getDecoratedName(String clazzName) {
		return clazzName + TARGET_APPENDIX;
	}
	
	public static String getDecoratedName(Class<? extends AbstractTarget> clazz) {
		return getDecoratedName(clazz.getName());
	}
	
	public static Class<?> getDecorated(Class<? extends AbstractTarget> clazz) {
		try {
			return clazz.getClassLoader().loadClass(getDecoratedName(clazz));
		} catch (ClassNotFoundException e) {
			throw new TargetError("Error finding decorated class, was it packaged correctly?", e);
		}
	}
	
	public static void startTarget(Class<? extends AbstractTarget> clazz) {
		try {
			getDecorated(clazz).getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | NoSuchMethodException e) {
			throw new TargetError("Error invoking decorated class, was it packaged correctly?", e);
		} catch (InvocationTargetException e) {
			throw new TargetError("Exception thrown while starting Target: " + e.getMessage(), e);
		}
	}
	
	//object
	@Override
	public void close() {
		throw new UnsupportedOperationException("implemented by packaging");
	}
}
