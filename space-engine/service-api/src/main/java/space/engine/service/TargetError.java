package space.engine.service;

public class TargetError extends Error {
	
	public TargetError() {
	}
	
	public TargetError(String message) {
		super(message);
	}
	
	public TargetError(String message, Throwable cause) {
		super(message, cause);
	}
	
	public TargetError(Throwable cause) {
		super(cause);
	}
}
