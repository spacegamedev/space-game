package testmain;

import org.jetbrains.annotations.NotNull;
import space.engine.cleaner.generator.GeneratedCleaner;
import space.engine.cleaner.generator.GeneratedResource;
import space.engine.service.Service;
import testmain.WindowSystemService.Cleaner;

import java.util.Random;

@Service
public class WindowSystemService implements GeneratedResource<Cleaner> {
	
	public final long pointer = new Random().nextLong();
	
	public WindowSystemService() {
		newCleaner();
	}
	
	public static class Cleaner extends GeneratedCleaner {
		
		public final long pointer;
		
		public Cleaner(@NotNull WindowSystemService referent) {
			super(referent);
			this.pointer = referent.pointer;
		}
		
		@Override
		protected void free() {
			System.out.println("freeWindowSystem(" + pointer + ")");
		}
	}
	
}
