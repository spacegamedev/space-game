package testmain;

import space.engine.Shutdown;
import space.engine.buffer.CByte;
import space.engine.cleaner.generator.DependentResource;
import space.engine.cleaner.generator.GeneratedCleaner;
import space.engine.cleaner.generator.GeneratedResource;
import space.engine.service.AbstractTarget;
import space.engine.service.Target;
import space.engine.struct.Allocator;
import testmain.ClientTarget.Cleaner;
import teststruct.Vector3f;

@Target
public class ClientTarget extends AbstractTarget implements GeneratedResource<Cleaner>, Runnable {
	
	@DependentResource
	final RenderService renderService;
	@DependentResource
	final GameService gameService;
	@DependentResource
	final InputService inputService;
	
	public ClientTarget(RenderService renderService, GameService gameService, InputService inputService) {
		this.renderService = renderService;
		this.gameService = gameService;
		this.inputService = inputService;
		newCleaner();
		
		run();
		CByte.malloc(Allocator.heap());
	}
	
	@Override
	public void run() {
		gameService.addEntity();
	}
	
	public static void main(String[] args) throws InterruptedException {
		try (Vector3f vec = Vector3f.alloc(Allocator.heap(), 1, 2, 3)) {
			System.out.println(vec.x() + ", " + vec.y() + ", " + vec.z().get());
		}
		
		try {
			startTarget(ClientTarget.class);
		} finally {
			Shutdown.exit();
		}
	}
	
	public static class Cleaner extends GeneratedCleaner {
		
		@Override
		protected void free() {
		
		}
	}
}
