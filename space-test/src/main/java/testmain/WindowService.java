package testmain;

import org.jetbrains.annotations.NotNull;
import space.engine.cleaner.generator.DependentResource;
import space.engine.cleaner.generator.GeneratedCleaner;
import space.engine.cleaner.generator.GeneratedResource;
import space.engine.service.Service;
import testmain.WindowService.Cleaner;

import java.util.Random;

@Service
public class WindowService implements GeneratedResource<Cleaner> {
	
	public final long pointer = new Random().nextLong();
	
	@DependentResource
	final WindowSystemService windowSystemService;
	
	@Service
	public WindowService(WindowSystemService windowSystemService) {
		this.windowSystemService = windowSystemService;
		newCleaner();
	}
	
	public static class Cleaner extends GeneratedCleaner {
		
		public final long pointer;
		WindowSystemService.Cleaner system;
		
		protected Cleaner(@NotNull WindowService referent) {
			super(referent);
			this.pointer = referent.pointer;
		}
		
		@Override
		protected void free() {
			System.out.println(system.pointer + ".freeWindow(" + pointer + ")");
		}
	}
}
