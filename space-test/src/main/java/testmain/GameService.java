package testmain;

import space.engine.cleaner.generator.GeneratedCleaner;
import space.engine.cleaner.generator.GeneratedResource;
import space.engine.service.Service;
import space.engine.struct.Allocator.Heap;
import testmain.GameService.Cleaner;
import teststruct.GameObject;

import java.util.ArrayList;

@Service
public class GameService implements GeneratedResource<Cleaner> {
	
	public GameService() {
		newCleaner();
	}
	
	ArrayList<GameObject> objects = new ArrayList<>();
	
	public void addEntity() {
		objects.add(GameObject.malloc(Heap.INSTANCE));
	}
	
	public static class Cleaner extends GeneratedCleaner {
		
		@Override
		protected void free() {
		
		}
	}
}
