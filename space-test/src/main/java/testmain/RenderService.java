package testmain;

import space.engine.cleaner.generator.DependentResource;
import space.engine.cleaner.generator.GeneratedCleaner;
import space.engine.cleaner.generator.GeneratedResource;
import space.engine.service.Service;
import testmain.RenderService.Cleaner;

@Service
public class RenderService implements GeneratedResource<Cleaner> {
	
	@DependentResource
	final WindowService windowService;
	
	public RenderService(WindowService windowService) {
		this.windowService = windowService;
		newCleaner();
	}
	
	public static class Cleaner extends GeneratedCleaner {
		
		@Override
		protected void free() {
		
		}
	}
}
