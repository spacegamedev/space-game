package testmain;

import space.engine.cleaner.generator.DependentResource;
import space.engine.cleaner.generator.GeneratedCleaner;
import space.engine.cleaner.generator.GeneratedResource;
import space.engine.service.Service;
import testmain.InputService.Cleaner;

@Service
public class InputService implements GeneratedResource<Cleaner> {
	
	@DependentResource
	final WindowService windowService;
	
	public InputService(WindowService windowService) {
		this.windowService = windowService;
		newCleaner();
	}
	
	public static class Cleaner extends GeneratedCleaner {
		
		@Override
		protected void free() {
		
		}
	}
}
