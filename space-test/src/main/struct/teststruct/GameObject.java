package teststruct;

import space.engine.struct.VoidPointer;

public class GameObject {
	
	public Vector3f position;
	public float radius;
	public byte someByte;
	public int blaCounter;
	public Vector3f hand_itemPos;
	public VoidPointer hand_itemRef;
	public short someShort;
	public byte someByte2;
	public byte someByte3;
	public int hand_itemId;
	public int hand_itemCnt;
	public short someShort2;
}